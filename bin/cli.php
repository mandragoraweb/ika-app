<?php
/**
 * Application CLI
 *
 * PHP version 5.3
 *
 * This source file is subject to the license that is bundled with this package in the
 * file LICENSE.
 *
 * @package    Bootstrap
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 */
require 'vendor/autoload.php';

use \Symfony\Component\Console\Application;
use \Symfony\Component\Console\Helper\HelperSet;
use \Symfony\Component\Console\Helper\DialogHelper;
use \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use \Doctrine\DBAL\Tools\Console\Command\RunSqlCommand;
use \Doctrine\DBAL\Tools\Console\Command\ImportCommand;
use \Doctrine\DBAL\DriverManager;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\GenerateCommand;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\StatusCommand;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\VersionCommand;
use \Mandragora\Doctrine\Command\CreateDatabaseCommand;
use \Mandragora\Doctrine\Command\DropDatabaseCommand;

$cli = new Application('IKÁ CLI', '1.0.0');
$cli->setCatchExceptions(true);

$connection = DriverManager::getConnection(require 'config/db.config.php');

$helperSet = new HelperSet();
$helperSet->set(new DialogHelper(), 'dialog');
$helperSet->set(new ConnectionHelper($connection), 'db');

$cli->setHelperSet($helperSet);

$cli->addCommands(array(
    // DBAL Commands
    new RunSqlCommand(),
    new ImportCommand(),
    // Mandragora DBAL Commands
    new CreateDatabaseCommand(),
    new DropDatabaseCommand(),
    // Migrations Commands
    new ExecuteCommand(),
    new GenerateCommand(),
    new MigrateCommand(),
    new StatusCommand(),
    new VersionCommand(),
));

$cli->run();