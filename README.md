# Doctrine Migrations

Change this declaration in line 28 in in vendor/doctrine/migrations/package.php

    $dirs = array(
        './lib'                 =>  '/Doctrine\/DBAL\/Migrations/',
        '../dbal/lib'           =>  '/Doctrine/',
        '../common/lib'         =>  '/Doctrine/',
        '../../symfony/console' =>  '/Symfony/'
    );
    
command execute migration phar in raiz project

php doctrine-migrations.phar migrations:execute --configuration migrations/migrations.xml --db-configuration config/db.config.php version-migration-execute