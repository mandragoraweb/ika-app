<?php
/**
 * Application's entry point
 *
 * PHP version 5.3
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 * @license    MIT
 */
chdir(__DIR__ . '/../');

require 'application.php';