(function($) {
    $.widget('ika.accountBalance', {
        options: {
            clientsUrlTemplate: '/condominos/fraccionamiento/{zoneId}',
            propertiesUrlTemplate: '/propiedades/condomino/{clientId}'
        },
        _create: function() {
            var $widget = this;
            this.$zoneId = $('#zone-id');
            this.$clientId = $('#client-id');
            if (this.$zoneId.length <= 0) {
                return;
            }
            this.$zoneId.on('change', function() {
               var zoneId = $(this).val();
               if ('' === zoneId) {
                   return;
               }
               var clientsUrl = $widget.options.clientsUrlTemplate.replace('{zoneId}', zoneId);
               var $clients = $('#client-id');
               $clients.ajaxLoader().ajaxError();
               $.ajax({
                   url: clientsUrl,
                   dataType: 'html',
                   type: 'POST',
                   success: function(clients) {
                       $clients.empty();
                       $clients.html(clients);
                       $clients.val('');
                   },
                   beforeSend: function(jqXHR, settings) {
                       $clients.ajaxLoader('show');
                       $clients.attr('disabled', 'disabled');
                   },
                   complete: function(jqXHR, settings) {
                       $clients.ajaxLoader('hide');
                       $clients.removeAttr('disabled');
                   }
               });
            });
            this.$clientId.on('change', function() {
                var clientId = $(this).val();
                if ('' === clientId) {
                    return;
                }
                var propertiesUrl = $widget.options.propertiesUrlTemplate.replace('{clientId}', clientId);
                var $properties = $('#property-id');
                $properties.ajaxLoader().ajaxError();
                $.ajax({
                    url: propertiesUrl,
                    dataType: 'html',
                    type: 'POST',
                    success: function(properties) {
                        $properties.empty();
                        $properties.html(properties);
                        $properties.val('');
                    },
                    beforeSend: function(jqXHR, settings) {
                        $properties.ajaxLoader('show');
                        $properties.attr('disabled', 'disabled');
                    },
                    complete: function(jqXHR, settings) {
                        $properties.ajaxLoader('hide');
                        $properties.removeAttr('disabled');
                    }
                });
            });
        }
    });
})(jQuery);
