/**
 * Payment
 *
 * @package    mandragora
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 */
(function($) {
    $.widget("ika.zoneexpenses", {
        options: {
            urlExpenses: '/gastos/fraccionamiento/gastos/{zoneId}'
        },
        _init: function() {
            this._setListOnChange(this.options.urlExpenses);
        },
        _setListOnChange: function(urlProperties) {
            $('#zone').change(function() {
                var $zoneId = $(this).val();
                var url = urlProperties.replace('{zoneId}', $zoneId);
                var $content = $('#listExpenses');
                $content.ajaxLoader().ajaxError();
                $.ajax({
                    url: url,
                    dataType: 'html',
                    data: ({format: 'html'}),
                    success: function(response) {
                        var $list = $(response);
                        $content.empty();
                        $content.html($list);
                    },
                    beforeSend: function(jqXHR, settings) {
                        $content.ajaxLoader('show');
                        $content.attr('disabled', 'disabled');
                    },
                    complete: function(jqXHR, settings) {
                        $content.ajaxLoader('hide');
                        $content.removeAttr('disabled');
                    }
                });
            });
        }
    });
})(jQuery);