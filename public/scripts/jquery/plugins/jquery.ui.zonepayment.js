/**
 * Payment
 *
 * @package    mandragora
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 */
(function($) {
    $.widget("ika.zonepayment", {
        options: {
            urlTemplate: '/pagos/fraccionamiento/propiedades/{zoneId}',
            urlPayments: '/pagos/fraccionamiento/pagos/{zoneId}'
        },
        _init: function() {
            this._setZoneOnChange(this.options.urlTemplate);
            this._setListOnChange(this.options.urlPayments);
        },
        _setZoneOnChange: function(urlTemplate) {
            $('#zone').change(function() {
                var zoneId = $(this).val();
                var url = urlTemplate.replace('{zoneId}', zoneId);
                var $fee = $('#fee');
                $fee.ajaxLoader().ajaxError();
                $.ajax({
                    url: url,
                    dataType: 'html',
                    data: ({format: 'html'}),
                    success: function(response) {
                        var $fees = $(response).first('select').children('option');
                        $fee.empty();
                        $fee.html($fees);
                        $fee.val('');
                    },
                    beforeSend: function(jqXHR, settings) {
						$fee.ajaxLoader('show');
						$fee.attr('disabled', 'disabled');
						$fee.attr('src', 'disabled');
                    },
                    complete: function(jqXHR, settings) {
						$fee.ajaxLoader('hide');
						$fee.removeAttr('disabled');
                    }
                });
            });
        },
        _setListOnChange: function(urlProperties) {
            $('#zone').change(function() {
                var $zoneId = $(this).val();
                var url = urlProperties.replace('{zoneId}', $zoneId);
                var $content = $('#listPayment');
                $content.ajaxLoader().ajaxError();
                $.ajax({
                    url: url,
                    dataType: 'html',
                    data: ({format: 'html'}),
                    success: function(response) {
                        var $list = $(response);
                        $content.empty();
                        $content.html($list);
                    },
                    beforeSend: function(jqXHR, settings) {
						$content.ajaxLoader('show');
						$content.attr('disabled', 'disabled');
                    },
                    complete: function(jqXHR, settings) {
                    	$content.ajaxLoader('hide');
                    	$content.removeAttr('disabled');
                    }
                });
            });
        }
    });
})(jQuery);