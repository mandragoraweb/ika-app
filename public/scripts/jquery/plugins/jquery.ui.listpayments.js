/**
 * Payment
 *
 * @package    ika
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 */
(function($) {
    $.widget("ika.payment", {
        options: {
            urlTemplate: '/pagos/propiedades/{zoneId}',
            urlPayments: '/pagos/propiedad/{propertyId}'
        },
        _init: function() {
            this._setZoneOnChange(this.options.urlTemplate);
            this._setPropertyOnChange(this.options.urlPayments);
        },
        _setZoneOnChange: function(urlTemplate) {
            $('#zone').change(function() {
                var zoneId = $(this).val();
                var url = urlTemplate.replace('{zoneId}', zoneId);
                var $property = $('#property');
                $property.ajaxLoader().ajaxError();
                $.ajax({
                    url: url,
                    dataType: 'html',
                    data: ({format: 'html'}),
                    success: function(response) {
                        var $properties = $(response).first('select').children('option');
                        $property.empty();
                        $property.html($properties);
                        $property.val('');
                    },
                    beforeSend: function(jqXHR, settings) {
                        $property.ajaxLoader('show');
                        $property.attr('disabled', 'disabled');
                    },
                    complete: function(jqXHR, settings) {
                        $property.ajaxLoader('hide');
                        $property.removeAttr('disabled');
                    }
                });
            });
        },
        _setPropertyOnChange: function(urlProperties) {
            $('#property').change(function() {
                var $propertyId = $(this).val();
                var url = urlProperties.replace('{propertyId}', $propertyId);
                var $content = $('#listPayment');
                $content.ajaxLoader().ajaxError();
                $.ajax({
                    url: url,
                    dataType: 'html',
                    data: ({format: 'html'}),
                    success: function(response) {
                        var $list = $(response);
                        $content.empty();
                        $content.html($list);
                    },
                    beforeSend: function(jqXHR, settings) {
                        $content.ajaxLoader('show');
                        $content.attr('disabled', 'disabled');
                    },
                    complete: function(jqXHR, settings) {
                        $content.ajaxLoader('hide');
                        $content.removeAttr('disabled');
                    }
                });
            });
        }
    });
})(jQuery);
