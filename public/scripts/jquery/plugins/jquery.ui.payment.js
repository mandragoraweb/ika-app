/**
 * Payment
 *
 * @package    mandragora
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 */
(function($) {
    $.widget("ika.payment", {
        options: {
            urlTemplate: '/pagos/propiedades/{zoneId}',
            urlPayments: '/pagos/propiedad/{propertyId}',
            urlOpeningBalance: '/pagos/propiedad/{propertyId}/saldo-inicial',
            $emptyPayment: $('#empty_payment'),
            $folio: $('input[name="folio_receipt"]'),
            $amountPaid: $('input[name="amount_paid"]')
        },
        _init: function() {
            this._setZoneOnChange(this.options.urlTemplate);
            this._setPropertyOnChange(this.options.urlPayments);
            this._setOpeningBalance(this.options.urlOpeningBalance);
            this._setAmountPaidWhenCheckedOpeningBalance();
            this._changeOpeningBalance(this.options.urlOpeningBalance);
            this._setFolioAndAmountPaidWhenCheckedEmptyPayment(this.options.$emptyPayment, this.options.$folio, this.options.$amountPaid);

            if (this.options.$emptyPayment.is(':checked')) {
                this.options.$folio.val(null);
                this.options.$folio.attr('disabled','disabled');
                this.options.$amountPaid.val('0.00');
                this.options.$amountPaid.attr('disabled','disabled');
            }
        },
        _setZoneOnChange: function(urlTemplate) {
            $('#zone').change(function() {
                var zoneId = $(this).val();
                var url = urlTemplate.replace('{zoneId}', zoneId);
                var $property = $('#property-apply');
                var $spinner = $('#autocomplete');
                var $fee = $('#fee');
                $spinner.ajaxLoader().ajaxError();
                $fee.ajaxLoader();
                $.ajax({
                    url: url,
                    dataType: 'html',
                    data: ({format: 'html'}),
                    success: function(response) {
                        var $properties = $(response).first('select').children('option');
                        $property.empty();
                        $property.html($properties);
                        $property.val('');

                        var $fees = $(response).first('select').next();
                        if (!$fees.is('select')) {
                        	$fee.parent().hide().prev().hide();
                        } else {
                        	$fee.empty().parent().show().prev().show();
                        }
                        $fee.html($fees.children('option'));
                        $fee.val('');
                    },
                    beforeSend: function(jqXHR, settings) {
                        $spinner.ajaxLoader('show');
                        $spinner.attr('disabled', 'disabled');
                    	$fee.ajaxLoader('show');
                    	$fee.attr('disabled', 'disabled');
                    },
                    complete: function(jqXHR, settings) {
                        $spinner.ajaxLoader('hide');
                        $spinner.removeAttr('disabled');
                    	$fee.ajaxLoader('hide');
                    	$fee.removeAttr('disabled');
                    }
                });
            });
        },
        _setPropertyOnChange: function(urlProperties) {
            $('#property').change(function() {
                var $propertyId = $(this).val();
                var url = urlProperties.replace('{propertyId}', $propertyId);
                var $content = $('#listPayment');
                $content.ajaxLoader().ajaxError();
                $.ajax({
                    url: url,
                    dataType: 'html',
                    data: ({format: 'html'}),
                    success: function(response) {
                        var $list = $(response);
                        $content.empty();
                        $content.html($list);
                    },
                    beforeSend: function(jqXHR, settings) {
                    	$content.ajaxLoader('show');
                    	$content.attr('disabled', 'disabled');
                    },
                    complete: function(jqXHR, settings) {
                    	$content.ajaxLoader('hide');
                    	$content.removeAttr('disabled');
                    }
                });
            });
        },
        _setOpeningBalance: function(urlOpeningBalance) {
            var $widget = this;
            $('#property-apply').change(function() {
                var $propertyId = $(this).val();
                var url = urlOpeningBalance.replace('{propertyId}', $propertyId);
                $widget._retrieveOpeningBalance(url);
            });
        },
        _setAmountPaidWhenCheckedOpeningBalance: function() {
            var $amountPaiBefore = $('input[name=amount_paid]').val();
            var $isOpeningBalance = $('#is_opening_balance');
            var $openingBalance = $('#opening-balance');
            var $fee = $('#fee');
            $isOpeningBalance.change(function() {
                var $amountPaid = /\d+/.exec($("#fee option:selected").html());
                if ($isOpeningBalance.is(':checked')) {
                    if ( $openingBalance.val() < 0) {
                        var $newOpeningBalance = $openingBalance.val() * -1;
                    } else {
                        var $newOpeningBalance = $openingBalance.val() * 1;
                    }
                    if ($openingBalance.val() != 0 && $amountPaid[0] < $newOpeningBalance) {
                        $('input[name=amount_paid]').val($amountPaid[0]);
                    } else {
                        $('input[name=amount_paid]').val($newOpeningBalance);
                    }
                } else {
                    $('input[name=amount_paid]').val($amountPaiBefore);
                }
            });
            $fee.change(function() {
                var $amountPaid = /\d+/.exec($("#fee option:selected").html());
                if ($isOpeningBalance.is(':checked') && $openingBalance.val() > 0) {
                    $('input[name=amount_paid]').val($amountPaid);
                }
            });
        },
        _changeOpeningBalance: function(urlOpeningBalance) {
            if ( $('#opening-balance').val() != 0 ) {
                $('#opening-balance').removeClass('ui-helper-hidden-accessible');
                $('#opening-balance').parent().siblings().removeClass('ui-helper-hidden-accessible');
                $('#is_opening_balance').removeClass('ui-helper-hidden-accessible');
                $('#is_opening_balance').parent().siblings().removeClass('ui-helper-hidden-accessible');
            }
            if ($('#property-apply').val() != '') {
                var $propertyId = $('#property-apply').val();
                var url = urlOpeningBalance.replace('{propertyId}', $propertyId);
                this._retrieveOpeningBalance(url);
            }
        },
        _retrieveOpeningBalance: function(url) {
            var $openingBalance = $('#opening-balance');
            var $isOpeningBalance = $('#is_opening_balance');
            $openingBalance.ajaxLoader().ajaxError();
            $.ajax({
                url: url,
                dataType: 'json',
                success: function(balance) {
                    if ( balance.opening_balance != 0 ) {
                        $openingBalance.val(balance.opening_balance);
                        $openingBalance.removeClass('ui-helper-hidden-accessible');
                        $openingBalance.parent().siblings().removeClass('ui-helper-hidden-accessible');
                    }
                    if ( balance.opening_balance > 0 ) {
                        $isOpeningBalance.parent().siblings().text('Tomar del Saldo Inicial');
                        $isOpeningBalance.removeClass('ui-helper-hidden-accessible');
                        $isOpeningBalance.parent().siblings().removeClass('ui-helper-hidden-accessible');
                    }
                    if ( balance.opening_balance < 0 ) {
                        $isOpeningBalance.parent().siblings().text('Abonar al Saldo Inicial');
                        $isOpeningBalance.removeClass('ui-helper-hidden-accessible');
                        $isOpeningBalance.parent().siblings().removeClass('ui-helper-hidden-accessible');
                    }
                    if ( balance.opening_balance == 0 ) {
                        $openingBalance.val('');
                        $openingBalance.addClass('ui-helper-hidden-accessible');
                        $openingBalance.parent().siblings().addClass('ui-helper-hidden-accessible');
                        $isOpeningBalance.attr('checked', false);
                        $isOpeningBalance.addClass('ui-helper-hidden-accessible');
                        $isOpeningBalance.parent().siblings().addClass('ui-helper-hidden-accessible');
                    }
                },
                beforeSend: function(jqXHR, settings) {
                    $openingBalance.ajaxLoader('show');
                    $openingBalance.attr('disabled', 'disabled');
                },
                complete: function(jqXHR, settings) {
                    $openingBalance.ajaxLoader('hide');
                    $openingBalance.removeAttr('disabled');
                }
            });
        },

        _setFolioAndAmountPaidWhenCheckedEmptyPayment: function($emptyPayment, $folio, $amountPaid) {
            $emptyPayment.change(function() {
                if ($emptyPayment.is(':checked')) {
                    $folio.val(null);
                    $folio.attr('disabled','disabled');
                    $amountPaid.val('0.00');
                    $amountPaid.attr('disabled','disabled');
                } else {
                    $folio.val('');
                    $folio.removeAttr('disabled','disabled');
                    $amountPaid.val('');
                    $amountPaid.removeAttr('disabled','disabled');
                }
            });
        }
    });
})(jQuery);