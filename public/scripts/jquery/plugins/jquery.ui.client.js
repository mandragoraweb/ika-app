/**
 * Payment
 *
 * @package    mandragora
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 */
(function($) {
    $.widget("ika.client", {
        options: {
            urlTemplate: '/pagos/propiedades/{zoneId}'
        },
        _init: function() {
            this._setZoneOnChange(this.options.urlTemplate);
            this._enableSelectionOfExistingProperties();
        },
        _setZoneOnChange: function(urlTemplate) {
            $('#zone').change(function() {
                var zoneId = $(this).val();
                var url = urlTemplate.replace('{zoneId}', zoneId);
                var $property = $('#property');
                $property.ajaxLoader().ajaxError();
                $.ajax({
                    url: url,
                    dataType: 'html',
                    success: function(response) {
                        var $properties = $(response).first('select').children('option');
                        $property.empty();
                        $property.html($properties);
                        $property.val('');
                    },
                    beforeSend: function(jqXHR, settings) {
                        $property.ajaxLoader('show');
                        $property.attr('disabled', 'disabled');
                    },
                    complete: function(jqXHR, settings) {
                        $property.ajaxLoader('hide');
                        $property.removeAttr('disabled');

                    }
                });
            });
        },

        _enableSelectionOfExistingProperties: function() {
            var $existingProperty = $('#existing_property');
            var $zone = $('#zone');
            var $property = $('#property');
            var $completeProperty = $('#autocomplete');
            $existingProperty.change(function() {
                if ($existingProperty.is(':checked')) {
                    $zone.removeClass('ui-helper-hidden-accessible');
                    $zone.parent().siblings().removeClass('ui-helper-hidden-accessible');
                    $property.removeClass('ui-helper-hidden-accessible');
                    $property.parent().siblings().removeClass('ui-helper-hidden-accessible');
                    $completeProperty.removeClass('ui-helper-hidden-accessible');
                } else {
                    $zone.addClass('ui-helper-hidden-accessible');
                    $zone.parent().siblings().addClass('ui-helper-hidden-accessible');
                    $property.addClass('ui-helper-hidden-accessible');
                    $property.parent().siblings().addClass('ui-helper-hidden-accessible');
                    $completeProperty.addClass('ui-helper-hidden-accessible');
                }
            });
        }
    });
})(jQuery);