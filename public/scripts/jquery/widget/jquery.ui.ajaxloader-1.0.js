(function($) {
    $.widget('mandragora.ajaxLoader', {
        options: {
            imgSrc: '/imgns/ajax-loader.gif',
            message: '',
            showOnDialog: false,
            dialog: {
                height: 70,
                width: 120
            }
        },
        _create: function() {
            var $loader = $('<span class="loading"></span>');
            var $image = $('<img alt="" />').attr('src', this.options.imgSrc);
            if (this.options.showOnDialog) {
                this._createDialog();
                $loader.append($image);
                $loader.append(this.options.message);
                this.dialog.html($loader.html());
            } else {
                $loader.append($image).insertAfter(this.element).hide();
            }
        },
        _createDialog: function() {
          this.dialog = $('<div class="loading-container"></div>');
          this.dialog.dialog({
              closeOnEscape: false,
              open: function(event, ui) {
                  $('.ui-dialog-titlebar', $(this).parent()).hide(); 
              },
              modal: true,
              resizable: false,
              draggable: false,
              autoOpen: false,
              height: this.options.dialog.height,
              width: this.options.dialog.width
           });
        },
        destroy: function() {
            if (this.options.showOnDialog) {
                this.dialog.remove();
            }else{
                this.element.next().remove();
            }
        },
        hide: function() {
            if (this.options.showOnDialog) {
                this.dialog.dialog('close');
            } else {
                this.element.next().hide('slow');
            }
        },
        show: function() {
            if (this.options.showOnDialog) {
                this.dialog.dialog('open');
                this.dialog.dialog('moveToTop');
            } else {
                this.element.next().show('slow');
            }
        }
    });
})(jQuery);