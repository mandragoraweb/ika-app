$.widget('ika.login', {
    _create: function() {
        var $widget = this;
        
        this.element.submit(function($e) {
            $e.preventDefault();
            
            $.ajax({
                'url': '/cuentas/verificar',
                'dataType': 'json',
                'data': $widget._getFormData(),
                'type': 'POST',
                'success': function(response) {

                    if (response.messages) {
                        var $messages = $widget._buildErrorMessages(response.messages);
                        
                        $('input[type=submit]').after($messages);
                    }
                }
            });
        });
    },
    _buildErrorMessages: function(messages) {
        var messagesList = '<ul class="form-errors">';
        
        for (elements in messages) {
            for (message in messages[elements]) {
                messagesList += '<li>' + messages[elements][message] + '</li>';
            }
        }
        
        messagesList += '</ul>';
        
        return $(messagesList);
    },
    _getFormData: function() {
        var values = this.element.serializeArray();
        var keyValues = {};

        $.map(values, function(item) {
            keyValues[item['name']] = item['value'];
        });

        return keyValues;
    }
});