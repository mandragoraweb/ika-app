INSERT INTO `profile` (`profile_id`, `paternal_surname`, `maternal_surname`, `first_name`, `birth`, `email`) VALUES
(1, 'Web', 'Mandrágora', 'Administrador del Sistema', NULL, 'webmaster@mandragora-web-systems.com'),
(2, 'Web', 'Mandrágora', 'Administrador Condominio', NULL, 'admin@mandragora-web-systems.com'),
(3, 'Web', 'Mandrágora', 'Cliente', NULL, 'contacto@mandragora-web-systems.com'),
(4, 'Web', 'Mandrágora', 'Comité de Vigilancia', NULL, 'comitte@mandragora-web-systems.com');

INSERT INTO `user` (`username`, `profile_id`, `password`, `role`, `comment`) VALUES
('admin', 2, '$2y$14$JBhmfdeGwsqOy54jCycEx..2p0EaUg92OXI5YpMie5DoxJqTp5fEa', 'admin', NULL),
('client', 3, '$2y$14$vqhAUtT4XUcsA3m1dQ1dOuAhxhPh4dyPG8h37IryyxeEWQ8u6.jly', 'client', NULL),
('committee', 4, '$2y$14$2OHYi4MBaP2140syMeABe.T4Nq1qpJUi2.Lheds.WLkLEnplmDr4S', 'committee', NULL),
('webmaster', 1, '$2y$14$9a6PT.SV3pkPja7y7r4xR.VkK1bGscwdqdCkQJ3WeM8Re/0.4xyue', 'webmaster', NULL);

INSERT INTO `type` (`type_id`, `description`) VALUES (1, 'Casa');

INSERT INTO `zone` (`zone_id`, `name`) VALUES (1, 'Bellas Artes');

INSERT INTO `user_zone` (`zone_id`, `username`) VALUES
(1, 'admin'),   -- Administrator
(1, 'committee'); -- Committee

INSERT INTO `client` (`client_id`, `profile_id`, `social_reason`, `phone`, `car`, `mobile`, `rfc`) VALUES
(1, 3, 'Mandrágora Web Systems',  '222222222', '', '', '');

INSERT INTO `property` (`property_id`, `street`, `number`, `internal_number`, `block`, `lot`, `type_id`, `zone_id`, `client_id`)
VALUES (1, '13 sur', '10900', null, null, null, 1, 1, 1);

INSERT INTO `concept` (`code`, `concept`, `type`) VALUES
('101', 'Seguridad', 'O'),
('102', 'Conserjería', 'O'),
('103', 'Mantenimiento', 'O'),
('104', 'Limpieza', 'O'),
('105', 'Jardinería', 'O'),
('106', 'Estacionamiento', 'O'),
('107', 'Personal Administrativo', 'O'),
('108', 'Uniformes del Personal', 'O'),
('201', 'IMSS', 'O'),
('202', 'Impuestos del Personal', 'O'),
('203', 'Impuesto Predial', 'O'),
('204', 'Otros Impuestos', 'O'),
('205', 'Agua', 'O'),
('206', 'Gas', 'O'),
('207', 'Energía Eléctrica', 'O'),
('208', 'Servicio Telefónico', 'O'),
('209', 'Seguros Contra Riesgos', 'O'),
('301', 'Papelería', 'O'),
('302', 'Gastos Varios', 'O'),
('303', 'Servicio de Cómputo, Administrativo', 'O'),
('304', 'Honorarios Administrativos', 'O'),
('305', 'Honorarios  a Terceros', 'O'),
('306', 'Gastos y Honorarios Legales', 'O'),
('307', 'Asociación de Colonos', 'O'),
('401', 'Limpieza de Vidrios', 'O'),
('402', 'Limpieza de Cisternas', 'O'),
('403', 'Pulido de Pisos', 'O'),
('404', 'Recolección de Basura', 'O'),
('405', 'Fumigación', 'O'),
('406', 'Limpieza de Albercas', 'O'),
('501', 'Compra de Material Eléctrico', 'O'),
('502', 'Material de Limpieza', 'O'),
('503', 'Herramienta y Equipo', 'O'),
('504', 'Material para Jardinería', 'O'),
('505', 'Servicio de Electricidad y Plomería', 'O'),
('506', 'Herrería, Carpintería y Cerrajería', 'O'),
('507', 'Albañilería y Pintura', 'O'),
('508', 'Impermeabilización', 'O'),
('601', 'Elevadores y Escaleras eléctricas', 'O'),
('602', 'Subestación', 'O'),
('603', 'Planta de Emergencia', 'O'),
('604', 'Equipo de Seguridad', 'O'),
('605', 'Equipo de Bombeo', 'O'),
('606', 'Equipo de Aire Acondicionado y Extracción', 'O'),
('607', 'Antenas Parabólicas y de Cable', 'O'),
('608', 'Equipo de Albercas', 'O'),
('609', 'Equipo Computarizado de Control', 'O'),
('610', 'Equipos Varios', 'O'),
('611', 'Puertas Automáticas', 'O'),
('612', 'Planta de Tratamiento', 'O'),
('701', 'Egresos No Presupuestados', 'O'),
('702', 'Incremento a Caja Chica', 'O'),
('703', 'Publicidad y Promoción', 'O'),
('704', 'Eventos Especiales', 'O'),
('705', 'Traspaso al Fondo de Reserva', 'O'),
('706', 'Cargos y Comisiones Bancos', 'O'),
('707', 'Cargo por Ingreso Improcedente', 'O'),
('708', 'Operación de Estacionamientos', 'O'),
('709', 'Compra de Activos', 'O'),
('710', 'Mejoras', 'O'),
('801', 'Cuotas Condominales', 'I'),
('802', 'Cuotas Extraordinarias', 'I'),
('803', 'Renta de Áreas Comunes', 'I'),
('804', 'Uso de Áreas Comunes', 'I'),
('805', 'Intereses Moratorios', 'I'),
('806', 'Intereses Fondo Común', 'I'),
('807', 'Renta de Estacionamiento', 'I'),
('808', 'Traspaso del Fondo de Reserva.', 'I'),
('809', 'Venta de Activos', 'I'),
('810', 'Indemnización por Daños', 'I'),
('811', 'Depósitos en Garantía', 'I'),
('812', 'Depósito Improcedente', 'I'),
('901', 'Reembolso Teléfonos', 'I'),
('902', 'Reembolso Consumo de Agua', 'I'),
('903', 'Reembolso Consumo de Gas', 'I'),
('904', 'Reembolsos por Consumo de Energía Eléctrica', 'I'),
('905', 'Reembolsos por Servicios Diversos', 'I'),
('906', 'Reembolsos por Gastos a Comprobar', 'I'),
('907', 'Reembolsos de Nómina', 'I');

INSERT INTO fee (fee_id, type_id, concept_id, zone_id, amount) VALUES
(1, 1, 61, 1, 100);
