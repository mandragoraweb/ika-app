<?php
class HomePage
{
    public static $URL = '/';
    public static $userField = '#login-form input[name="username"]';
    public static $passwordField = '#login-form input[name="password"]';
    public static $loginButton = '#login-form input[name="send"]';
}
