<?php
class ClientPage
{
    public static $URL = '/condominos/crear';
    public static $userNameField = '#client-form input[name=user]';
    public static $nameField = '#client-form input[name=first_name]';
    public static $paternalSurnameField = '#client-form input[name=paternal_surname]';
    public static $maternalSurnameField = '#client-form input[name=maternal_surname]';
    public static $openingBalanceField = '#client-form input[name=opening_balance]';
    public static $balanceDateField = '#client-form input[name=balance_date]';
    public static $saveButton = '#send';
    public static $propertyForm = '#property-form';
}
