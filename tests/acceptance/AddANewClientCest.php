<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2014 (http://www.mandragora-web-systems.com)
 */
use AcceptanceTester\SecuritySteps;
use Faker\Factory;

class AddANewClientCest
{
    /** @type SecuritySteps */
    protected $steps;

    /** @type Faker\Generator */
    protected $faker;

    public function _before()
    {
        $this->steps = new SecuritySteps();
        $this->faker = Factory::create();
    }

    public function tryToAddANewHomeownerWithAnExistingOpeningBalance(AcceptanceTester $I)
    {
        $I->am('administrator');
        $I->wantTo('add a new homeowner');
        $I->lookForwardTo('to administer its payments');
        $this->steps->login($I);
        $I->amOnPage(ClientPage::$URL);
        $I->see('Agregar condómino', 'h1');
        $I->fillField(ClientPage::$userNameField, $this->faker->userName);
        $I->fillField(ClientPage::$nameField, $this->faker->name);
        $I->fillField(ClientPage::$paternalSurnameField, $this->faker->lastName);
        $I->fillField(ClientPage::$maternalSurnameField, $this->faker->lastName);
        $I->fillField(ClientPage::$openingBalanceField, $this->faker->randomNumber());
        $I->fillField(ClientPage::$balanceDateField, '2014-01-30');
        $I->see('Guardar');
        $I->click('Guardar');
        $I->see('Fraccionamiento');
        $I->see('Tipo de propiedad');
        $I->see('Calle');
        $I->see('Número');
        $I->see('Número Interior');
        $I->see('Manzana');
        $I->see('Lote');
    }
}
