<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2014 (http://www.mandragora-web-systems.com)
 */
namespace AcceptanceTester;

use AcceptanceTester;
use HomePage;

class SecuritySteps
{
    /**
     * Login as a user with the webmaster role
     *
     * @param AcceptanceTester $administrator
     */
    public function login(AcceptanceTester $administrator)
    {
        $administrator->amOnPage(HomePage::$URL);
        $administrator->fillField(HomePage::$userField, 'webmaster');
        $administrator->fillField(HomePage::$passwordField, 'webmaster');
        $administrator->click(HomePage::$loginButton);
    }
}
