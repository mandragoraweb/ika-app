<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140715114536 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $client = $schema->getTable('property');
        $client->addColumn('opening_balance', 'decimal', array('precision' => 8, 'scale' => 2, 'unsigned' => false));
        $client->addColumn('balance_date', 'date');

    }

    public function down(Schema $schema)
    {
        $client = $schema->getTable('property');
        $client->dropColumn('opening_balance');
        $client->dropColumn('balance_date');

    }
}
