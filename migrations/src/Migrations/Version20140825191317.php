<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140825191317 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $zoneExpenses = $schema->getTable('zone_expenses');
        $zoneExpenses->addColumn('comment', 'text', array('notnull' => false));

    }

    public function down(Schema $schema)
    {
        $zoneExpenses = $schema->getTable('zone_expenses');
        $zoneExpenses->dropColumn('comment');

    }
}
