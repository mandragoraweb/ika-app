<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140917164358 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $payment = $schema->getTable('payment');
        $payment->addColumn('client_id', 'integer');
    }

    public function down(Schema $schema)
    {
        $payment = $schema->getTable('payment');
        $payment->dropColumn('client_id');
    }
}
