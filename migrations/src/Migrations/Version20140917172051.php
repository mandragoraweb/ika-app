<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140917172051 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = "SELECT p.payment_id, pr.client_id FROM payment p LEFT JOIN property pr ON p.property_id = pr.property_id";
        $stm = $this->connection->prepare($sql);
        $stm->execute();
        $rows = $stm->fetchAll();
        foreach ($rows as $row) {
            $update = 'UPDATE payment SET client_id = :clientId WHERE payment_id = :paymentId';
            $params = ['clientId' => $row['client_id'], 'paymentId' => $row['payment_id']];
            $edit = $this->connection->prepare($update);
            $edit->execute($params);
        }
    }

    public function down(Schema $schema)
    {
    }
}
