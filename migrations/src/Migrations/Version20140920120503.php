<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140920120503 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $payment = $schema->getTable('payment');
        $payment->changeColumn('folio_receipt', ['length' => 100, 'notnull' => false]);
    }

    public function down(Schema $schema)
    {
        $payment = $schema->getTable('payment');
        $payment->changeColumn('folio_receipt', ['length' => 100]);
    }
}
