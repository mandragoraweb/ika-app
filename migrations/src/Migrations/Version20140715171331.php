<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140715171331 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $payment = $schema->getTable('payment');
        $payment->addColumn('is_opening_balance', 'boolean', array('notnull' => false));
    }

    public function down(Schema $schema)
    {
        $payment = $schema->getTable('payment');
        $payment->dropColumn('is_opening_balance');
    }
}
