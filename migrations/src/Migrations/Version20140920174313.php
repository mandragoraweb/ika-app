<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140920174313 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $payment = $schema->getTable('payment');
        $payment->addForeignKeyConstraint('client', ['client_id'], ['client_id']);
    }

    public function down(Schema $schema)
    {
        $payment = $schema->getTable('payment');
        $payment->removeForeignKey('client_id');
    }
}
