<?php
/**
 * Generate initial schema
 *
 * PHP version 5.3
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 * @license    MIT
 */
namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Generate initial schema
 */
class Version20130718002439 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $profile = $schema->createTable('profile');
        $profile->addColumn('profile_id', 'integer', array('autoincrement' => true));
        $profile->addColumn('paternal_surname', 'string', array('length' => 90));
        $profile->addColumn('maternal_surname', 'string', array('length' => 90));
        $profile->addColumn('first_name', 'string', array('length' => 90));
        $profile->addColumn('birth', 'date', array('notnull' => false));
        $profile->addColumn('email', 'string', array('length' => 90));
        $profile->setPrimaryKey(array('profile_id'));

        $user = $schema->createTable('user');
        $user->addColumn('username', 'string', array('length' => 20));
        $user->addColumn('password', 'string', array('length' => 100, 'notnull' => false));
        $user->addColumn('role', 'string', array('length' => 45));
        $user->addColumn('comment', 'text', array('notnull' => false));
        $user->addColumn('profile_id', 'integer');
        $user->setPrimaryKey(array('username'));
        $user->addForeignKeyConstraint('profile', array('profile_id'), array('profile_id'));

        $zone = $schema->createTable('zone');
        $zone->addColumn('zone_id', 'integer', array('autoincrement' => true));
        $zone->addColumn('name', 'string', array('length' => 150));
        $zone->setPrimaryKey(array('zone_id'));

        $userZone = $schema->createTable('user_zone');
        $userZone->addColumn('username', 'string', array('length' => 20));
        $userZone->addColumn('zone_id', 'integer');
        $userZone->setPrimaryKey(array('username', 'zone_id'));
        $userZone->addForeignKeyConstraint('user', array('username'), array('username'));
        $userZone->addForeignKeyConstraint('zone', array('zone_id'), array('zone_id'));

        $type = $schema->createTable('type');
        $type->addColumn('type_id', 'integer', array('autoincrement' => true));
        $type->addColumn('description', 'string', array('length' => 90));
        $type->setPrimaryKey(array('type_id'));

        $client = $schema->createTable('client');
        $client->addColumn('client_id', 'integer', array('autoincrement' => true));
        $client->addColumn('social_reason', 'string', array('length' => 150, 'notnull' => false));
        $client->addColumn('phone', 'string', array('length' => 15, 'notnull' => false));
        $client->addColumn('car', 'string', array('length' => 90, 'notnull' => false));
        $client->addColumn('mobile', 'string', array('length' => 20, 'notnull' => false));
        $client->addColumn('rfc', 'string', array('length' => 15, 'notnull' => false));
        $client->addColumn('profile_id', 'integer');
        $client->setPrimaryKey(array('client_id'));
        $client->addForeignKeyConstraint('profile', array('profile_id'), array('profile_id'));

        $property = $schema->createTable('property');
        $property->addColumn('property_id', 'integer', array('autoincrement' => true));
        $property->addColumn('street', 'string', array('length' => 90));
        $property->addColumn('number', 'string', array('length' => 30));
        $property->addColumn('internal_number', 'string', array('length' => 30, 'notnull' => false));
        $property->addColumn('block', 'string', array('length' => 50, 'notnull' => false));
        $property->addColumn('lot', 'string', array('length' => 50, 'notnull' => false));
        $property->addColumn('type_id', 'integer');
        $property->addColumn('zone_id', 'integer');
        $property->addColumn('client_id', 'integer');
        $property->setPrimaryKey(array('property_id'));
        $property->addForeignKeyConstraint('type', array('type_id'), array('type_id'));
        $property->addForeignKeyConstraint('zone', array('zone_id'), array('zone_id'));
        $property->addForeignKeyConstraint('client', array('client_id'), array('client_id'));

        $concept = $schema->createTable('concept');
        $concept->addColumn('concept_id', 'integer', array('autoincrement' => true));
        $concept->addColumn('code', 'string', array('length' => 45));
        $concept->addColumn('concept', 'string', array('length' => 100));
        $concept->addColumn('type', 'string', array('length' => 1));
        $concept->setPrimaryKey(array('concept_id'));

        $share = $schema->createTable('fee');
        $share->addColumn('fee_id', 'integer', array('autoincrement' => true));
        $share->addColumn('amount', 'decimal', array('precision' => 8, 'scale' => 2, 'unsigned' => true));
        $share->addColumn('type_id', 'integer');
        $share->addColumn('concept_id', 'integer');
        $share->addColumn('zone_id', 'integer');
        $share->setPrimaryKey(array('fee_id'));
        $share->addForeignKeyConstraint('type', array('type_id'), array('type_id'));
        $share->addForeignKeyConstraint('concept', array('concept_id'), array('concept_id'));
        $share->addForeignKeyConstraint('zone', array('zone_id'), array('zone_id'));

        $payment = $schema->createTable('payment');
        $payment->addColumn('payment_id', 'integer', array('autoincrement' => true));
        $payment->addColumn('amount_paid', 'decimal', array('precision' => 8, 'scale' => 2));
        $payment->addColumn('date', 'date');
        $payment->addColumn('comment', 'text', array('notnull' => false));
        $payment->addColumn('folio_receipt', 'string', array('length' => 100));
        $payment->addColumn('username', 'string', array('length' => 20));
        $payment->addColumn('date_record', 'date');
        $payment->addColumn('fee_id', 'integer');
        $payment->addColumn('property_id', 'integer');
        $payment->setPrimaryKey(array('payment_id'));
        $payment->addForeignKeyConstraint('fee', array('fee_id'), array('fee_id'));
        $payment->addForeignKeyConstraint('property', array('property_id'), array('property_id'));

        $surcharge = $schema->createTable('surcharge');
        $surcharge->addColumn('surcharge_id', 'integer', array('autoincrement' => true));
        $surcharge->addColumn('fixed', 'integer');
        $surcharge->addColumn('surcharge_type', 'string', array('length' => 1));
        $surcharge->addColumn('payment_id', 'integer');
        $surcharge->setPrimaryKey(array('surcharge_id'));
        $surcharge->addForeignKeyConstraint('payment', array('payment_id'), array('payment_id'));

        $zonePayment = $schema->createTable('zone_payment');
        $zonePayment->addColumn('zone_payment_id', 'integer', array('autoincrement' => true));
        $zonePayment->addColumn('amount_paid', 'decimal', array('precision' => 8, 'scale' => 2));
        $zonePayment->addColumn('date', 'date');
        $zonePayment->addColumn('folio_receipt', 'string', array('length' => 100));
        $zonePayment->addColumn('date_record', 'date');
        $zonePayment->addColumn('zone_id', 'integer');
        $zonePayment->addColumn('fee_id', 'integer');
        $zonePayment->setPrimaryKey(array('zone_payment_id'));
        $zonePayment->addForeignKeyConstraint('fee', array('fee_id'), array('fee_id'));
        $zonePayment->addForeignKeyConstraint('zone', array('zone_id'), array('zone_id'));

        $zoneExpenses = $schema->createTable('zone_expenses');
        $zoneExpenses->addColumn('zone_expenses_id', 'integer', array('autoincrement' => true));
        $zoneExpenses->addColumn('amount_paid', 'decimal', array('precision' => 8, 'scale' => 2));
        $zoneExpenses->addColumn('iva', 'decimal', array('precision' => 8, 'scale' => 2, 'notnull' => false));
        $zoneExpenses->addColumn('date', 'date');
        $zoneExpenses->addColumn('date_record', 'date');
        $zoneExpenses->addColumn('zone_id', 'integer');
        $zoneExpenses->addColumn('concept_id', 'integer');
        $zoneExpenses->setPrimaryKey(array('zone_expenses_id'));
        $zoneExpenses->addForeignKeyConstraint('concept', array('concept_id'), array('concept_id'));
        $zoneExpenses->addForeignKeyConstraint('zone', array('zone_id'), array('zone_id'));
    }

    public function down(Schema $schema)
    {
        $schema->dropTable('zone_expenses');
        $schema->dropTable('zone_payment');
        $schema->dropTable('surcharge');
        $schema->dropTable('payment');
        $schema->dropTable('fee');
        $schema->dropTable('concept');
        $schema->dropTable('property');
        $schema->dropTable('client');
        $schema->dropTable('type');
        $schema->dropTable('user_zone');
        $schema->dropTable('zone');
        $schema->dropTable('user');
        $schema->dropTable('profile');
    }
}
