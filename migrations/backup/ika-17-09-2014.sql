INSERT INTO `profile` (`profile_id`, `paternal_surname`, `maternal_surname`, `first_name`, `birth`, `email`) VALUES
(1, 'Web', 'Mandrágora', 'Administrador del Sistema', NULL, 'webmaster@mandragora-web-systems.com'),
(2, 'Web', 'Mandrágora', 'Administrador Condominio', NULL, 'admin@mandragora-web-systems.com'),
(3, 'Web', 'Mandrágora', 'Cliente', NULL, 'contacto@mandragora-web-systems.com'),
(4, 'Web', 'Mandrágora', 'Comité de Vigilancia', NULL, 'comitte@mandragora-web-systems.com'),
(5, 'Romero', 'Romero', 'Mayahuel', NULL, 'mayahuel.romero@gmail.com'),
(6, 'Tello', 'Tello', 'José Manuel', NULL, 'jose.manuel.tello@hotmail.com'),
(7, 'Monter', 'Monter', 'Juan Francisco', NULL, 'juan_fco_monter@hotmail.com'),
(8, 'Bojalil', 'Gonzalez', 'Emma Teresita', '1954-08-15', 'jannellm9@hotmail.com'),
(9, 'Moctezuma', 'Sanchez', 'Martha', '1965-02-01', 'mmoctezume@cruzazul.com.mx'),
(10, 'Miranda', 'x', 'Juan Pablo', NULL, 'mirasoljp@hotmail.com'),
(11, 'Pinto', 'X', 'Susana', NULL, ''),
(12, 'Couder', 'X', 'Eduardo', NULL, 'ing_couder@hotmail.com'),
(13, 'Flores', 'X', 'Juan Antonio', NULL, ''),
(14, 'Gomez', 'Silva', 'Omar', NULL, ''),
(15, 'Robles', 'Yañez', 'Enrique', NULL, ''),
(16, 'Hernandez', 'Paz', 'Jaime', NULL, ''),
(17, 'Escartin', 'X', 'Miguel', NULL, ''),
(18, 'Rodriguez', 'de la Concha', 'Francisco Javier', NULL, ''),
(19, 'Rodriguez', 'X', 'Marcela', NULL, ''),
(20, 'Del Valle', 'X', 'Jose Luis', NULL, ''),
(21, 'Proal', 'X', 'Jose Ramon', NULL, ''),
(22, 'Urreta', 'x', 'Iñigo', NULL, ''),
(23, 'Pacheco', 'X', 'Jose Luis', NULL, ''),
(24, 'Gonzalez', 'X', 'Alfredo', NULL, ''),
(25, 'Rangel', 'Lopez', 'Ricardo', NULL, ''),
(26, 'Malanco', 'X', 'Jorge', NULL, ''),
(27, 'Tenorio', 'Ambris', 'Blanca Lizeth', NULL, ''),
(28, 'Valtierra', 'X', 'Ernesto', NULL, ''),
(29, 'Carmen', 'X', 'Samuel', NULL, ''),
(30, 'Ibañez', 'X', 'Rodrigo', NULL, ''),
(31, 'Naude', 'Gonzalez', 'Maria Sofia', NULL, ''),
(32, 'Avila', 'X', 'Adriana', NULL, ''),
(33, 'Platas', 'X', 'Jesus', NULL, ''),
(34, 'Marin', 'Lozano', 'Juan Martin', NULL, ''),
(35, 'Rodriguez', 'Carpio', 'Bruno', NULL, ''),
(36, 'Huerta', 'Maxil', 'Clara', NULL, ''),
(37, 'Lopez', 'Romero', 'Angel', NULL, ''),
(38, 'Suarez', 'X', 'Marisol', NULL, ''),
(39, 'Carrera', 'Carrera', 'Jose Antonio', NULL, ''),
(40, 'Del Valle', 'X', 'Adrian', NULL, ''),
(41, 'Gali', 'Boadella', 'Monserrat', NULL, ''),
(42, 'Alvarez', 'Gardea', 'Blanca Maria', NULL, ''),
(43, 'Ramirez', 'X', 'Irma', NULL, ''),
(44, 'Avaloz', 'Garcia', 'Analiz', NULL, ''),
(45, 'Alatorre', 'X', 'Jorge', NULL, ''),
(46, 'Mendez', 'Meneses', 'Mark', NULL, ''),
(47, 'Montiel', 'Brito', 'Eduardo', NULL, ''),
(48, 'Robinson', 'Olvera', 'Norma Angelica', NULL, ''),
(49, 'Porrero', 'X', 'Alberto', NULL, ''),
(50, 'Vazquez', 'Gonzalez', 'Jose Luis', NULL, ''),
(51, 'Aquino', 'X', 'Caludia', NULL, ''),
(52, 'Castillo', 'X', 'Jorge', NULL, ''),
(53, 'Arellano', 'X', 'Maria de Jesus', NULL, ''),
(54, 'Ruiz', 'Lara', 'Ladislao Salvador', NULL, ''),
(55, 'Flores', 'X', 'Lilia', NULL, ''),
(56, 'Sanchez', 'Vera', 'Manuel', NULL, ''),
(57, 'Lara', 'Tostado', 'Francisco', NULL, ''),
(58, 'Osorio', 'Juarez', 'Maria Teresa', NULL, ''),
(59, 'Muñoz', 'Cano', 'Luis Francisco', NULL, ''),
(60, 'Saldivar', 'Santini', 'Alfredo', NULL, ''),
(61, 'Merelli', 'X', 'Alexandro', NULL, ''),
(62, 'Mojica', 'Contreras', 'Luz Elena', NULL, ''),
(63, 'Leal', 'Pérez', 'Jesus', NULL, ''),
(64, 'Posadas', 'X', 'Danae', NULL, ''),
(65, 'Merchand', 'X', 'Israel', NULL, ''),
(66, 'Torres', 'Landa', 'Alfredo', NULL, ''),
(67, 'Torres', 'Landa', 'Javier Aldair', NULL, ''),
(68, 'Flores', 'X', 'Fernando', NULL, ''),
(69, 'Duran', 'Sosa', 'Gerardo Ivan', NULL, ''),
(70, 'Islas', 'Ruiz', 'Nestor', NULL, ''),
(71, 'Barcelo', 'X', 'Carlos Edwin', NULL, ''),
(72, 'Pavón', 'Macuil', 'Ricardo', NULL, ''),
(73, 'Alvarez', 'Centeno', 'Jose Hugo', NULL, ''),
(74, 'Solares', 'X', 'Fernando', NULL, ''),
(75, 'Lozano', 'Garcia', 'David', NULL, ''),
(76, 'Palacios', 'Chavez', 'Silvia', NULL, ''),
(77, 'Ortiz', 'Paredez', 'Carlos', NULL, ''),
(78, 'Muñoz', 'Tostado', 'Ramiro', NULL, ''),
(79, 'Momox', 'X', 'Sandra', NULL, ''),
(80, 'Herrera', 'X', 'Marcela', NULL, ''),
(81, 'Garcia', 'Villegas', 'Favian Enrique', NULL, ''),
(82, 'GARCIA', 'MARTINEZ', 'ALBERTO', NULL, 'calbertogm@prodigy.net.mx'),
(83, 'RIVAS', 'POZOS', 'ALBERTO', '2014-05-06', 'rivasalberto@yahoo.com.mx'),
(84, 'SANDERS', 'PERALTA', 'ALBERTO', NULL, 'f_rosders@yahoo.com.mx'),
(85, 'GARCIA', 'ORTIZ', 'ALEJANDRA', NULL, ''),
(86, 'JIMENEZ', 'HERNANDEZ', 'ALEJANDRA', NULL, 'consultoriaasociados@yahoo.com.mx'),
(87, 'LAVALLE', 'MORENO', 'ALEJANDRA', NULL, 'alejalavalle@gmail.com'),
(88, 'ARIZA', 'DE LA FUENTE', 'ALEJANDRO', NULL, 'adinaacon@hotmail.com'),
(89, 'DE LA FUENTE', 'CHACON', 'ALEJANDRO', NULL, 'alefuent@hotmail.com'),
(90, 'DIEGO', 'CASTILLO', 'ALEJANDRO', NULL, 'diego_alex2001@yahoo.com.mx'),
(91, 'MORENO', 'ROA', 'ALEJANDRO', NULL, 'mora6727@yahoo.com'),
(92, 'REYES', 'DE CORCUERA', 'ALEJANDRO', NULL, 'sistemasparaelagua@hotmail.com'),
(93, 'ROMERO', 'JIMENEZ', 'ALEJANDRO', NULL, 'alejandro.romero@itesm.mx'),
(94, 'MEDELLIN', 'CAMPOS', 'ALEX', NULL, ''),
(95, 'MIRAFUENTES', 'GONZALEZ', 'ALICIA', NULL, ''),
(96, 'IBARRA', 'Y TEJERO', 'ALMA', NULL, ''),
(97, 'LORANCA', 'RAMOS', 'ANABELLE', NULL, ''),
(98, 'OSORIO', 'MENDEZ', 'ANGELICA', NULL, ''),
(99, 'RODRIGUEZ', 'LEZAMA', 'ANTONIO', NULL, ''),
(100, 'TABOADA', 'RIVERA', 'ARISTEO', NULL, 'taboada5_23@hotmail.com'),
(101, 'SALGADO', 'ROMAN', 'ARMINDA', NULL, 'maniere.vernis@gmail.com'),
(102, 'ESCOTO', 'CERVANTES', 'ARNOL RAUL', NULL, 'arnol@microsipgolfo.com'),
(103, 'LOYA', 'HERRERA', 'ARTURO', NULL, 'zuilmaainee@hotmail.com'),
(104, 'ESCOTO', 'CERVANTES', 'ARNOL RAUL', NULL, 'arnol@microsipgolfo.com'),
(105, 'SANCHEZ', 'ARMENDARIZ', 'ARTURO', NULL, ''),
(106, 'SALGADO', 'MIRANDA', 'SALVADOR', NULL, ''),
(107, 'REY', 'CERRO', 'AVELINO', NULL, 'missmanola@yahoo.com.mx'),
(108, 'BARRAGAN', 'HUERGO', 'BEATRIZ ELENA', NULL, 'bettyhuergo@hotmail.com'),
(109, 'MACIP', 'CASTELAN', 'BRENDA', NULL, 'aldanamacip@hotmail.com'),
(110, 'LEON', 'FLORES', 'CARLOS', NULL, 'carlosleonflores@hotmail.com'),
(111, 'ROBLES', 'GARCIA', 'CARLOS', NULL, 'cysol24@hotmail.com'),
(112, 'HERNANDEZ', 'ALCANTARA', 'CLAUDIA', NULL, 'tammy_68_@hotmail.com'),
(113, 'VELEZ', 'CAMPOS', 'CLAUDIA', NULL, 'rmarquez@audipuebla.com'),
(114, 'MAZZA', 'OLMOS', 'DAVID ERNESTO', NULL, 'dr_mazza@hotmail.com'),
(115, 'FERNANDEZ', 'FERNANDEZ', 'EDGAR', NULL, 'mzp5@yahoo.com.mx'),
(116, 'HERNANDEZ', 'RODRIGUEZ', 'EDGAR', NULL, ''),
(117, 'KLEIN', 'HACES', 'EDGAR', NULL, 'edgarklein@hotmail.com'),
(118, 'CRUZ', 'HERNANDEZ', 'EDGARDO ALBERTO', NULL, 'capied2003@yahoo.com.mx'),
(119, 'ALONSO', 'CONTRERAS', 'EDUARDO', NULL, 'ealonso@lanamax.com'),
(120, 'MONROY', 'HERNANDEZ', 'ELIA IRMA', NULL, 'mimi_monroy@hotmail.com'),
(121, 'ZETUNE', 'LOPEZ', 'ELIAS', NULL, 'impor_simone@hotmail.com'),
(122, 'MARTINEZ', 'REHLAENDER', 'ELOY', NULL, 'dr.martinez@servmet.com'),
(123, 'LAFER', 'SARMIENTO', 'EMILIO', NULL, 'rayavinosylicores@yahoo.com'),
(124, 'TAPIA', 'BOLFETA', 'ENRIQUE', NULL, 'etapiab@hotmail.com'),
(125, 'SANCHEZ', 'MONTIEL', 'ERICK', NULL, 'ericksan@hotmail.com'),
(126, 'BORRI', 'ISLAS', 'ERICKA', NULL, ''),
(127, 'FARIAS', 'SOSA', 'ERNESTO', NULL, 'ernestofarias@live.com.mx'),
(128, 'FARIAS', 'SOSA', 'ERNESTO', NULL, 'ernestofarias@live.com.mx'),
(129, 'CAMACHO', 'ROGEL', 'FELIPE', NULL, 'jfcr2026@gmail.com'),
(130, 'PARDO', 'CASTILLO', 'FERNANDO', NULL, 'f.pardocas@gmail.com'),
(131, 'PARDO', 'CASTILLO', 'FERNANDO', NULL, ''),
(132, 'PARDO', 'CASTILLO', 'FERNANDO', NULL, 'f.pardocas@gmail.com'),
(133, 'GONZALEZ', 'CHAVEZ', 'FRANCISCO ESTEBAN', NULL, ''),
(134, 'HERNANDEZ', 'MORALES', 'FRANCISCO JAVIER', NULL, 'dulceg68@yahoo.com.mx'),
(135, 'RAMIREZ', 'GARCIA', 'FRANCISCO MANUEL', NULL, 'francisco.ramirez@kimmak.com'),
(136, 'AGUILAR', 'ELIZONDO', 'GABRIEL', NULL, 'matumx1@yahoo.com.mx'),
(137, 'BLANCO', 'PEREZ', 'GABRIEL', NULL, 'gab71122@prodigy.net.mx'),
(138, 'MENESES', 'OLARTE', 'GABRIELA', NULL, 'gmolarte@telmex.com'),
(139, 'LUIS', 'REYES', 'GEORGINA GUADALUPE', NULL, 'armguerrero@hotmail.com'),
(140, 'LING', 'ALMEIDA', 'GONZALO', NULL, 'malena_perez74@hotmail.com'),
(141, 'BIGURRA', 'HERRERA', 'GRISELDA', NULL, ''),
(142, 'GONZALEZ', 'SOLORZANO', 'GUILLERMINA', NULL, 'wdlgco@prodigy.net.com'),
(143, 'MONTIEL', 'VARGAS', 'GUILLERMO', NULL, 'memotov@hotmail.com'),
(144, 'GOMEZ', 'ROJANO', 'GUSTAVO', NULL, 'arq.ggomezr@gmail.com'),
(145, 'RODRIGUEZ', 'ISLAS', 'GUSTAVO', NULL, 'gustavo.rodriguez@multiaceros.com.mx'),
(146, 'MARTINEZ', 'ROMANO', 'HECTOR', NULL, 'hector.mtzromano@eleasadam.com'),
(147, 'HERRERA', 'SILVA', 'HEDILBERTO TOMAS', NULL, 'hhscool@hotmail.com'),
(148, 'KONRAD', 'KONRAD', 'HENNING', NULL, 'anchoveta2003@yahoo.de'),
(149, 'AGUIRRE', 'BACEROT', 'HORACIO', NULL, 'habacerot@hotmail.com'),
(150, 'NAVARRO', 'PEREZ', 'IRMA YOLANDA', NULL, 'yolanda_navpe@hotmail.com'),
(151, 'DEL CUETO', 'RIVERA', 'ISOLDA FERNANDA', NULL, 'fernanda7309@hotmail.com'),
(152, 'ALVAREZ', 'ALVAREZ', 'ISRAEL', NULL, 'israelalal@gmail.com'),
(153, 'PARRA', 'ALDAVE', 'JAIME ANTONIO', NULL, 'tadiac@hotmail.com'),
(154, 'GUERRERO', 'CRUZ', 'JAIME', NULL, 'erivillalobos@hotmail.com'),
(155, 'BOJALIL', 'PATIÑO', 'JANETTE', NULL, 'janette_873@yahoo.com'),
(156, 'RODRIGUEZ', 'ARELLANO', 'JAVIER', NULL, 'xavirdgz@hotmail.com'),
(157, 'LOTTA', 'ANDERSSON', 'JENNY ANNA', NULL, ''),
(158, 'BASAÑEZ', 'FERNANDEZ', 'JESUS', NULL, 'rociobastard@hotmail.com'),
(159, 'CHAVEZ', 'BIANCHINI', 'JORGE MARTIN', NULL, 'jochavi@hotmail.com'),
(160, 'MAZA', 'MORAN', 'JORGE', NULL, ''),
(161, 'STECK', 'MARTINEZ', 'JORGE', NULL, 'inamartinzp@hotmail.com'),
(162, 'GONZALEZ', 'VILLALBA', 'JOSE ANTONIO', NULL, 'motaglez@ultranet2go.net'),
(163, 'SOLIS', 'CAMACHO', 'JOSE ANTONIO', NULL, ''),
(164, 'CAMACHO', 'GONZALEZ', 'JOSE CANDIDO', NULL, 'ana_luisal@hotmail.com'),
(165, 'MACHORRO', 'CASTILLO', 'JOSE MARIO', NULL, 'rosyroco@yahoo.com.mx'),
(166, 'OLVERA', 'MUÑOZ', 'JOSE', NULL, 'jose.olvera@mxnestle.com'),
(167, 'ITURBE', 'AGUILAR', 'JOSE RODOLFO', NULL, ''),
(168, 'BENITEZ', 'MONDRAGON', 'JUAN CARLOS', NULL, 'inox_man@yahoo.com.mx'),
(169, 'NUÑEZ', 'HERNANDEZ', 'JUAN CARLOS', NULL, ''),
(170, 'GALEAZZI', 'JULIAN', 'JUAN JOSE', NULL, ''),
(171, 'MARTINEZ', 'ROMANO', 'JUAN PABLO', NULL, 'jpmromano@yahoo.com'),
(172, 'ALVAREZ', 'DOMINGUEZ', 'JULIETA', NULL, 'millerjulieta@yahoo.com'),
(173, 'MIJANGOS', 'PATIÑO', 'JULIO', NULL, 'julio_mijangos@ymail.com'),
(174, 'AGUILAR', 'GUTIERREZ', 'JUSTO ALONSO', NULL, 'justin44@live'),
(175, 'SERRANO', 'MIRANDA', 'LEONARDO', NULL, ''),
(176, 'MUÑOZ', 'HERNANDEZ', 'LEOVIGILDO', NULL, 'gral47@hotmail.com'),
(177, 'GARCIA', 'ZEPEDA', 'LETICIA', NULL, ''),
(178, 'VARILLAS', 'SANCHEZ', 'LETICIA', NULL, ''),
(179, 'GARCIA', 'LUNA', 'LILIANA', NULL, ''),
(180, 'GUERRERO', 'GERARD', 'LORENA', NULL, 'mgd_50@hotmail.com'),
(181, 'FERNANDEZ', 'DIEZ DE URDANIVIA', 'LUIS A', NULL, 'luisfdez10@mac.com'),
(182, 'ZENTENO', 'RIVERA', 'LUIS ARMANDO', NULL, 'cruzenteno@yahoo.com.mx'),
(183, 'AZOTLA', 'VELAZQUEZ', 'LUIS', NULL, 'lazotla@hotmail.com'),
(184, 'BELLO', 'CHAVEZ', 'LUIS EDGAR', NULL, 'le_bello@hotmail.com'),
(185, 'LEYVA', 'BATHORY', 'LUIS HUMBERTO', NULL, 'leyluba@hotmail.com'),
(186, 'OSORIO', 'MENDEZ', 'LUIS', NULL, ''),
(187, 'REYES', 'CARLIN', 'LUIS', NULL, 'ringc007@gmail.com'),
(188, 'ROCCHI', 'MINUTTI', 'LUIS', NULL, 'lromi60@hotmail.com'),
(189, 'GOMEZ', 'VALADEZ', 'LUIS SALVADOR', NULL, 'l.gomezvaladez@yahoo.com.mx'),
(190, 'MURO', 'DE FARRERA', 'MARCELA', NULL, 'chisfabricademallas@hotmail.es'),
(191, 'AGUILAR', 'GUTIERREZ', 'MARCO ANTONIO', NULL, 'markram44@aol.com'),
(192, 'RIOS', 'REYES', 'MARCO ANTONIO', NULL, 'teresareyescalixto@hotmail.com'),
(193, 'MARIN', 'DE GARCIA', 'MARGARITA', NULL, 'mms@teisa.com'),
(194, 'TORRES', 'AGUIRRE', 'MARIA ANTONIA', NULL, ''),
(195, 'GARCIA', 'SERRANO', 'MARIA CITLALI', NULL, 'citlalig.alcalde@yahoo.com.mx'),
(196, 'ASCENCIO', 'DE LARA', 'MARIA CRISTINA GOMEZ', NULL, 'elfernan@prodigy.net.mx'),
(197, 'CABAÑAS', 'DE VELASCO', 'MARIA DE LOS DOLORES', NULL, 'fsalvadorcabanas@yahoo.com.mx'),
(198, 'RODRIGUEZ', 'DE LA FUENTE', 'MARIA DE LOURDES', NULL, 'edgar_hr7_1@hotmail.com'),
(199, 'PILONI', 'BERRA', 'MARIA DEL CARMEN', NULL, ''),
(200, 'SANCHEZ', 'RECHE', 'MARIA DEL ROSARIO', NULL, 'rosarin_san@yahoo.com'),
(201, 'SORIANO', 'ZERMEÑO', 'MARIA DOLORES PILAR', NULL, 'smith_4231@hotmail.com'),
(202, 'DE CASTRO', 'TORRES', 'MARIA ELENA', NULL, '1vi_bep@hotmail.com'),
(203, 'GOMEZ', 'AGUIRRE', 'MARIA ELENA', NULL, 'ram_villagran@ymail.com'),
(204, 'GARCIA', 'AURELIO', 'MARIANA D.', NULL, ''),
(205, 'BERMUDEZ', 'ROSAS', 'MARICARMEN', NULL, ''),
(206, 'COSSIO', 'HERNANDEZ', 'MARICELA', NULL, 'licmaricossio@gmail.com'),
(207, 'GUTIERREZ', 'BRITO', 'MARICRUZ', NULL, 'marycrunch@hotmail.com'),
(208, 'ORTIZ', 'AZUARA', 'MARTHA EUGENIA', NULL, 'marthitaortiza@hotmail.com'),
(209, 'MALDONADO', 'RAVELO', 'MAURICIO', NULL, 'verosa@hotmail.com'),
(210, 'AREVALO', 'SOSA', 'MIGUEL ANGEL', NULL, 'lili71283@hotmail.com'),
(211, 'SALAMANCA', 'PASCUAL', 'MIGUEL ANGEL', NULL, 'miguelsalamanca@yahoo.com'),
(212, 'SANCHEZ', 'VILLALONSO', 'MIGUEL ANGEL', NULL, 'zafiro007@live.com'),
(213, 'DOMINGUEZ', 'SEDANO', 'MIGUEL', NULL, ''),
(214, 'ESTRADA', 'MORA', 'MILDRET', NULL, 'milkymora@hotmail.com'),
(215, 'NUÑEZ', 'HUERTA', 'MONICA', NULL, ''),
(216, 'VIVANCO', 'GONZALEZ', 'NORMA BRISEIDA', NULL, 'gallo_yes@hotmail.com'),
(217, 'MORALES', 'MELO', 'NORMA MIRIAN', NULL, 'armitael_dark@hotmail.com'),
(218, 'VALERIO', 'MUÑOZ', 'NORMA', NULL, ''),
(219, 'VILLEGAS', 'DIAZ', 'NORMA', NULL, ''),
(220, 'RAMIREZ', 'GONZALEZ', 'ODETE', NULL, ''),
(221, 'RAMIREZ', 'FLORES', 'OFELIA', NULL, 'ofelia.ramirez@vw.com.mx'),
(222, 'JURGEN', 'SAHLMANN', 'OLIVER', NULL, 'oliver.sahlmann@gmx'),
(223, 'VERGARA', 'VALDERRABANO', 'OMAR', NULL, 'covergara@yahoo.com.mx'),
(224, 'CERVANTES', 'JUAREZ', 'ORLANDO', NULL, 'o-cervantes@hotmail.com'),
(225, 'DELGADO', 'BALDERAS', 'ORLANDO', NULL, 'orlando.delgado@cfe.gob.mx'),
(226, 'DIAZ', 'VARGAS', 'PABLO', NULL, ''),
(227, 'VARGAS', 'GONZALEZ', 'PAULA', NULL, 'paula@live.com.mx'),
(228, 'CABRERA', 'PEREZ', 'PAULINA', NULL, 'sorela14@hotmail.com'),
(229, 'QUIROZ', 'GAONA', 'PILAR', NULL, 'cpuuiroz@msn.com'),
(230, 'CORRO', 'LOPEZ', 'RAFAEL', NULL, 'sagrariocafz@hotmail.com'),
(231, 'COVARRUBIAS', 'SALVATORI', 'RAFAEL', NULL, 'rcsplaza@hotmail.com'),
(232, 'CANTALAPIEDRA', 'LANDIRIBAR', 'RAMON', NULL, 'jrcantalapiedra@prodigy.com'),
(233, 'RAMIREZ', 'DROUAILLET', 'RAUL', NULL, 'telomanvpt@yahoo.com.mx'),
(234, 'POMASKY', 'KELLY', 'RAYMUNDO', NULL, 'maric66diaz@hotmail.com'),
(235, 'JUAREZ', 'ESPINOZA', 'RENE', NULL, 'rene-u2@hotmail.com'),
(236, 'MIRANDA', 'PEÑA', 'RICARDO', NULL, 'rimirandap@hotmail.com'),
(237, 'CASTILLO', 'MUÑOZ', 'ROBERTO', NULL, 'eyri16@hotmail.com'),
(238, 'CORVERA', 'GUZMAN', 'ROBERTO', NULL, 'robertocg2005@hotmail.com'),
(239, 'RODRIGUEZ', 'MIAJA', 'ROCIO DEL MAR', NULL, 'rociodelmar_82@hotmail.com'),
(240, 'MARTIN', 'HERRERA', 'RODRIGO', NULL, 'roy.mh777@hotmail.com'),
(241, 'ORTIZ', 'GOMEZ', 'ROMEO DAVID', NULL, 'd_ogomez@hotmail.com'),
(242, 'BELMONT', 'RIVAS', 'ROSA MARIA', NULL, ''),
(243, 'JUAREZ', 'LOPEZ', 'SALVADOR', NULL, 'enjl@live.com.mx'),
(244, 'NIETO', 'CONSUEGRA', 'SAMUEL', NULL, 'nietosam@gmail.com'),
(245, 'GARCIA', 'MENDEZ', 'SANDRA NOELIA', NULL, 'rubenchoabc@hotmail.com'),
(246, 'FRAGOSO', 'SORIANO', 'SERGIO', NULL, 'sfs_gotcha@yahoo.com.mx'),
(247, 'GUTIERREZ', 'MARQUEZ', 'SERGIO', NULL, ''),
(248, 'VELEZ', 'DE LA ROSA', 'SOCORRO', NULL, 'socovelez@hotmail.com'),
(249, 'SANCHEZ', 'LEAL', 'TERESA', NULL, 'teresa@3punto14.com'),
(250, 'MUNDO', 'AGUILAR', 'TOMAS', NULL, 'pola19-19@hotmail.com'),
(251, 'ALARCON', 'AQUINO', 'VICENTE', NULL, 'vicente.alarcon@udlap.mx'),
(252, 'HERNANDEZ', 'MENDEZ', 'VICTOR ADAN', NULL, 'vicermen1967@hotmail.com'),
(253, 'BORRI', 'ISLAS', 'VICTOR', NULL, ''),
(254, 'JUAREZ', 'ISLAS', 'VICTOR', NULL, 'talento15@hotmail.com'),
(255, 'VILLA', 'MONDRAGON', 'VICTOR MANUEL', NULL, ''),
(256, 'SUAREZ', 'JUAREZ', 'VICTOR', NULL, ''),
(257, 'AGUIRRE', 'PERALTA', 'VIVIANA PAOLA', NULL, 'viviana-aguirre@hotmail.com'),
(258, 'NAVA', 'LOPEZ', 'YOZGARTH', NULL, 'nava_lopezyo@lilly.com'),
(259, 'GUINEA', 'LARRAZABAL', 'JUAN CARLOS', NULL, 'marlou_mo@hotmail.com'),
(260, 'ORTIZ', 'GOMEZ', 'ANA GUADALUPE', NULL, ''),
(261, 'Borroto', 'Rodriguez', 'Ana', NULL, ''),
(262, 'Meneses', 'Mijangos', 'Erick', NULL, ''),
(263, 'de Maria', 'Mendez', 'Arlem', NULL, ''),
(264, 'Gallardo', 'Lima', 'David', NULL, ''),
(265, 'Jimenez', 'X', 'Mireya', NULL, ''),
(266, 'Sanchez', 'Perez', 'Monica', NULL, ''),
(267, 'Amaro', 'X', 'Marcos', NULL, ''),
(268, 'Brito', 'Flores', 'Slavador', NULL, ''),
(269, 'Gonzalez', 'Gutierrez', 'Marlene', NULL, ''),
(270, 'Juarez', 'X', 'Rafael', NULL, ''),
(271, 'Blanco', 'X', 'Angelica', NULL, ''),
(272, 'Tellez', 'Mora', 'Blanca', NULL, ''),
(273, 'Diaz', 'X', 'Gabriela', NULL, ''),
(274, 'Rojas', 'X', 'Moises', NULL, ''),
(275, 'Rosas', 'Acevedo', 'Miguel Angel', NULL, ''),
(276, 'X', 'X', 'Inmobiliaria', NULL, ''),
(277, 'Fuentes', 'Limon', 'Monserrat', NULL, ''),
(278, 'Toral', 'Alfaro', 'Maria de Lourdes', NULL, ''),
(279, 'Diaz', 'Macotela', 'David', NULL, ''),
(280, 'Vargas', 'X', 'Francisco Javier', NULL, ''),
(281, 'Morales', 'Garcia', 'Maria del Pilar', NULL, ''),
(282, 'Gafford', 'X', 'William', NULL, ''),
(283, 'Hernandez', 'Pacheco', 'Pedro', NULL, ''),
(284, 'Mendoza', 'De la Rosa', 'Carlos', NULL, ''),
(285, 'Mendez', 'Tellez', 'Maria Ester', NULL, ''),
(286, 'Cisneros', 'X', 'Francisco', NULL, ''),
(287, 'Quiroz', 'Mendieta', 'Adriana', NULL, ''),
(288, 'Teran', 'Cerqueda', 'Vanesa', NULL, ''),
(289, 'Vazquez', 'Gonzalez', 'Marisol', NULL, ''),
(290, 'Angeles', 'Ramirez', 'Eduardo', NULL, ''),
(291, 'Tecuapetla', 'X', 'Jannette', NULL, ''),
(292, 'Caballero', 'Dominguez', 'Alma Nydia', NULL, ''),
(293, 'X', 'X', 'Sin datos', NULL, ''),
(294, 'Andrade', 'Cervantes', 'Carlos Erik', NULL, ''),
(295, 'Brigete', 'Lindig', 'Maria', NULL, ''),
(296, 'Dominguez', 'Sanchez', 'Porfirio', NULL, ''),
(297, 'Espejel', 'Juarez', 'Norman Giovanni', NULL, ''),
(298, 'Vazquez', 'Palacios', 'Maria Lilia', NULL, ''),
(299, 'Cancino', 'Cruz', 'Ernesto', NULL, ''),
(300, 'Romero', 'X', 'Joel', NULL, ''),
(301, 'Mozzoco', 'X', 'Maria Ines', NULL, ''),
(302, 'Grajeda', 'X', 'Sandra', NULL, ''),
(303, 'Garduño', 'Navarrete', 'Rene', NULL, ''),
(304, 'Gomez', 'Manzano', 'Omar', NULL, ''),
(305, 'Perez', 'X', 'Jacob', NULL, ''),
(306, 'Guzman', 'Gomez', 'Luis Edwin', NULL, ''),
(307, 'Altamirano', 'X', 'Maria Esther', NULL, ''),
(308, 'Martinez', 'X', 'Sergio', NULL, ''),
(309, 'Ocampo', 'Gomez', 'Joaquin', NULL, ''),
(310, 'Valles', 'X', 'Guadalupe', NULL, ''),
(311, 'Gomez', 'Estebez', 'Andres Miguel', NULL, ''),
(312, 'Velzaquez', 'X', 'Luis Gabriel', NULL, ''),
(313, 'Altamirano', 'X', 'Fausto Ramon', NULL, ''),
(314, 'Heredia', 'Marchi', 'Omar', NULL, ''),
(315, 'Luna', 'Ruiz', 'Araceli Monica', NULL, ''),
(316, 'Pacheco', 'Vazquez', 'Maria Angelica', NULL, ''),
(317, 'Alba', 'Montes', 'Maria Patricia', NULL, ''),
(318, 'Breton', 'X', 'Guadalupe', NULL, ''),
(319, 'Valadez', 'Gomez', 'Jennifer', NULL, ''),
(320, 'Blancarte', 'Palacios', 'Josue', NULL, ''),
(321, 'Paz', 'X', 'Lizbeth', NULL, ''),
(322, 'Nekuzi', 'Darjam', 'Teresa', NULL, ''),
(323, 'Lopez', 'Cabrera', 'Edgar Sadoth', NULL, ''),
(324, 'Cid', 'X', 'Mayelli', NULL, ''),
(325, 'Mejia', 'Osante', 'Estefania', NULL, ''),
(326, 'Botello', 'X', 'Emigdio', NULL, ''),
(327, 'Herrera', 'X', 'Arturo', NULL, ''),
(328, 'Buzo', 'X', 'Jorge', NULL, ''),
(329, 'Muñoz', 'Mejia', 'Jorge', NULL, ''),
(330, 'Baez', 'X', 'Raul', NULL, ''),
(331, 'Rojas', 'Mendez', 'Beatriz', NULL, ''),
(332, 'Millan', 'X', 'Luis', NULL, ''),
(333, 'Juarez', 'X', 'Paola', NULL, ''),
(334, 'Diez', 'Santos', 'Leticia', NULL, ''),
(335, 'Madrid', 'X', 'Guadalupe', NULL, ''),
(336, 'Suarez', 'Fernandez', 'Lorena', NULL, ''),
(337, 'Castillo', 'Rodriguez', 'Manuel A.', NULL, ''),
(338, 'Olivas', 'Zazueta', 'Galicia', NULL, ''),
(339, 'Constructora', 'Alhemos', 'Inmobiliaria', NULL, ''),
(340, 'Rodriguez', 'Villanueva', 'Francisco', NULL, ''),
(341, 'Ortiz', 'X', 'Federico', NULL, ''),
(342, 'Salgado', 'X', 'Carmen', NULL, ''),
(343, 'Ochoa', 'X', 'Antonio', NULL, ''),
(344, 'Carrillo', 'X', 'Lizet', NULL, ''),
(345, 'Patiño', 'Rivera', 'Fernando', NULL, ''),
(346, 'Galindo', 'X', 'Jenny', NULL, ''),
(347, 'Contreras', 'Montejo', 'Elisbeydi', NULL, ''),
(348, 'Ramirez', 'Rivas', 'Martha', NULL, ''),
(349, 'Priego', 'X', 'Jose Daniel', NULL, ''),
(350, 'Issasi', 'Alvarez', 'Iracema', NULL, ''),
(351, 'Clavijo', 'X', 'Elizabeth', NULL, ''),
(352, 'Maldonado', 'X', 'Juan Carlos', NULL, ''),
(353, 'Gomez', 'Aguirre', 'Claudia', NULL, ''),
(354, 'Garrido', 'Guatelama', 'Carlos', NULL, ''),
(355, 'Gonzalez', 'Alcaraz', 'Dafne', NULL, ''),
(356, 'Ramirez', 'Gasca', 'Magdalena', NULL, ''),
(357, 'Garcia', 'Garcia', 'Maribel', NULL, ''),
(358, 'Guzman', 'X', 'Patricia', NULL, ''),
(359, 'Rojas', 'X', 'Ana Maria', NULL, ''),
(360, 'Alva', 'Camacho', 'Beatriz', NULL, ''),
(361, 'Ramirez', 'Sanchez', 'Jairzinho', NULL, ''),
(362, 'Marquez', 'Sanchez', 'Miguel Angel', NULL, ''),
(363, 'Pineda', 'Garrido', 'Jose Luis', NULL, ''),
(364, 'Sumuano', 'Dominguez', 'Adrian', NULL, ''),
(365, 'Cid', 'Rolon', 'Gerardo', NULL, ''),
(366, 'Villagran', 'X', 'Diana', NULL, ''),
(367, 'Ruiz', 'Del sol Leon', 'Marco Antonio', NULL, ''),
(368, 'Vazquez', 'Mellado', 'Alberto', NULL, ''),
(369, 'Alonso', 'Sanchez', 'Raul', NULL, ''),
(370, 'Perez', 'Flores', 'Gilberto', NULL, ''),
(371, 'Diaz', 'Mora', 'Marian', NULL, ''),
(372, 'Mendoza', 'Martinez', 'Sergio', NULL, ''),
(373, 'Olmos', 'X', 'Jorge', NULL, ''),
(374, 'Huerta', 'X', 'Jose Antonio', NULL, ''),
(375, 'Galindo', 'Juarez', 'Alejandro', NULL, ''),
(376, 'Carpinteiro', 'X', 'Cesar', NULL, ''),
(377, 'Sanchez', 'Lopez', 'Maria Ludmila', NULL, ''),
(378, 'Santoyo', 'Flores', 'Carlos Alberto', NULL, ''),
(379, 'Aramburo', 'X', 'Elsa', NULL, ''),
(380, 'Vazquezmellado', 'y Dominquez', 'Ireri', NULL, ''),
(381, 'Gonzalez', 'X', 'Francisco', NULL, ''),
(382, 'Daly', 'X', 'Arquitecta', NULL, ''),
(383, 'Orozco', 'X', 'Armando', NULL, ''),
(384, 'Huerta', 'Ramos', 'Alejandro', NULL, ''),
(385, 'Martinez', 'Luna', 'Norberto', NULL, ''),
(386, 'Baez', 'Chino', 'Ezequiel', NULL, ''),
(387, 'Perez', 'Castro', 'Jose Edgar', NULL, ''),
(388, 'Hurtado', 'Ruiz', 'Alejandro', NULL, '');

INSERT INTO `user` (`username`, `profile_id`, `password`, `role`, `comment`) VALUES
('admin', 2, '$2y$14$JBhmfdeGwsqOy54jCycEx..2p0EaUg92OXI5YpMie5DoxJqTp5fEa', 'admin', NULL),
('Adrian S', 364, '$2y$14$c8afbaWg16fjp2C7Wg1y2udpb6bsBdOQy/Tx1MYuMZDXNQfXh2Mle', 'client', NULL),
('Adrian V', 40, '$2y$14$0ioVPtwqjnQhyGxKERWG9.QiIQxyFQr9.N5sGhjWf61N1rkVLzCf2', 'client', NULL),
('Adriana A', 32, NULL, 'client', NULL),
('Adriana Q', 287, '$2y$14$DwG2Rgu5ayqD3ZrW8lP4VedG2S5Tt4Jt6MWSeZS1QkABn10mESkfW', 'client', NULL),
('ALBERTO GARCIA  MTNZ', 82, '$2y$14$d2TZtZ6ZySjmK/DtjkHrROizLHwdQHYXngMs475AmFRfIT/FhTCyu', 'client', NULL),
('Alberto P', 49, '$2y$14$8bIRlOv5x8SDqZfXkrqYfu5ADYohM3iBA44ZqFWziwIkE46NNVoQO', 'client', NULL),
('ALBERTO RIVAS', 83, '$2y$14$vGJXwrTJAYRh0VQ4ZDq50OPH2WXuY.wSGL4ZCG9pcjWkCoshXZcu.', 'client', NULL),
('ALBERTO SANDERS', 84, '$2y$14$4/FMZdZMGoBpCSRpMpDc.u2nKUGVoP7lsH.LU8XgTb8P0bdUS9nVW', 'client', NULL),
('Alberto V', 368, '$2y$14$7j34HNsmjNnpaihNI4qdnuqv79hhDBKXRqXgkb8qphj.pCPxmgqe2', 'client', NULL),
('ALEJANDRA GARCIA', 85, '$2y$14$rj56JHI4lo2oNAGlzA4rJOWy3kJ90txgcI4Y8yJYrfV/idEpcJe/e', 'client', NULL),
('ALEJANDRA JIMENEZ', 86, '$2y$14$yyvJI2KqetPUWADvhn4mKev/lrizvIpNjJHVXYQmVd4LSWZiJwPLa', 'client', NULL),
('ALEJANDRA LAVALLE', 87, '$2y$14$UtaxqdiGhad.e7vJdz3AWu9ivcmvVZ0tREAk/YRXPSiN2Lq3yf9HO', 'client', NULL),
('ALEJANDRO ARIZA', 88, '$2y$14$cBGuznL2IlUkEkv/EZVXJejAenAPGU80eMbYK33NOqlMc.NzIiIv.', 'client', NULL),
('ALEJANDRO DE LA FUEN', 89, '$2y$14$yQgGJcBQjRRw0crvhd7FmeOnE28qhI/9HFu.9xtrbyKJF3QjOnOLi', 'client', NULL),
('ALEJANDRO DIEGO', 90, '$2y$14$1ydu8MHrhpB2YHfT03lS4e6KbDbdYIqKYXjBcgZYCHFmw8tGK30KO', 'client', NULL),
('Alejandro G', 375, '$2y$14$8mmUTkZxNdi42WzRTrPTH.tkxaijxRq0OXOXGubI0VUttBa5BGWsO', 'client', NULL),
('Alejandro H', 384, '$2y$14$rsfYK2nejhP3zaiy1bnA3O9.c.KVuDWESecSySOMdHYT6PMI5mHhK', 'client', NULL),
('Alejandro HR', 388, '$2y$14$0kUpf9kX8xoxLM2OOEQH7eS7aykmGoPNNCO/7SBhDJltIBr2M4Oya', 'client', NULL),
('ALEJANDRO MORENO', 91, '$2y$14$..FVu2RRRA4SJBlwstZzdu0XFSn/oWgPJrwQk7MEDScMomamRfgxO', 'client', NULL),
('ALEJANDRO REYES', 92, '$2y$14$w0gKetXD.feXSE5wTDnBVerROkSiYWoUAA6J2l8iuBKimOB3/Yguq', 'client', NULL),
('ALEJANDRO ROMERO', 93, '$2y$14$p4x.OTHqGGozZArtaE9tWOqp6pQ/8KPJxielB0p5UpXv539tBXa66', 'client', NULL),
('ALEX MEDELLIN CAMPOS', 94, '$2y$14$ZBFhFtddexlqFfVRKNyH4u9FXEc2JYr943/YqPERk74TeyGILmrpW', 'client', NULL),
('Alexandro M', 61, '$2y$14$SrkHNFXXcNKTlJWUir4OtufJWshoCBMAK9QqwwUbGjV7zGmy1Yf6i', 'client', NULL),
('Alfredo G', 24, '$2y$14$XEZejzWUhj5w82771y9OsOR2WMYGtRAWU1rQJjMIH8mn3pzD5W7HK', 'client', NULL),
('Alfredo S', 60, '$2y$14$3csUFpZhMAPWEdMF3ZQOo.4hIUEdQbOqbdwGFp2pDejIAU76l.VDG', 'client', NULL),
('Alfredo T', 66, '$2y$14$mRQA2a2tW5cmdM/aSiXDderrnQ6ebYA6ai6O7FoyM9gBq0LEPH0Ri', 'client', NULL),
('ALICIA MIRAFUENTES', 95, '$2y$14$o4cIAmatXEmqJSR5V47Rg.inqL3bNzIjSmKWIrUOJSozo1QtXGIb6', 'client', NULL),
('Alma C', 292, '$2y$14$hNXzuxeUz8iMqv3cOQVGhuf6QDIgfMuXQfKTRcAcy2ZKKKj3jLhdq', 'client', NULL),
('ALMA IBARRA', 96, '$2y$14$Za./lrw0CS6GgUBN/Tihy.HI8k7M2QzP9O4SyPjFh/abNhDiNwrwq', 'client', NULL),
('Ana B', 261, '$2y$14$fUECrKmn1BFhlQo0GVZ6P.pEr8lfWW5oZEUFSu/gOvJWALMlawQTy', 'client', NULL),
('ANA GUADALUPE ORTIZ', 260, '$2y$14$Ou2cdkSa8QSX8xhMpSp4f.hfJMfwlLOAfcUO1DXIGnCv2FEYcTpHy', 'client', NULL),
('Ana R', 359, '$2y$14$4aGuVupBvz5eohiL/oCfbuNUR1JOdqASjQQNKTROKt1i5E9UMSj56', 'client', NULL),
('ANABELLE LORANCA', 97, '$2y$14$dPFzXEEsymsey.O/Micn7uK/Lzd9oo.Ny.4tQDZwW56K7SR13S3mS', 'client', NULL),
('Analiz A', 44, '$2y$14$hRDrud4M/lVeNc4zSw2k6Onb6mdKjzrvhgVRbaeELAw5RGmlxjGyy', 'client', NULL),
('Andres G', 311, '$2y$14$Bu7whT1wiVRrVxGlEQMNv.sjjAbwnrUNWrwGX5FhSfkITNYmQNH7u', 'client', NULL),
('Angel L', 37, '$2y$14$aEO2fdOB8MHXfrL8l.QmnOuNeznlYeQ1rixcvUysS9U6tqs6wHC6O', 'client', NULL),
('Angelica B', 271, '$2y$14$DKamUmt2hyACwlSFODMneuN2cjzGatt8D5.6U0z1gegOCbZzsBQo6', 'client', NULL),
('ANGELICA OSORIO', 98, '$2y$14$UoiBI2pDf42GrI/zHVMSGuMG.gIfHBlGBz6DmHAJLwESgDY/FzDdK', 'client', NULL),
('Angelica P', 316, '$2y$14$V/LkDoMjdeTRQO6.c.dkKOBybCSm6iFUDwHGbiSZHCgIzzCJV0j0K', 'client', NULL),
('Angelica R', 48, '$2y$14$AxqTGg87Pzgj8swCrZqfruM4/RZ7x50ayjl.mzu5xu7XOpBOQq1ba', 'client', NULL),
('Antonio C', 39, '$2y$14$j3ZKfDg5hd/AtX9VztVmPO.THggyYYR2DqP8PNVMD24P/wAy9.W5K', 'client', NULL),
('Antonio O', 343, '$2y$14$Ra2eLK5P03AFMsMVl7.JxulwKkUzFy/Bx1ey46R9cZmWhczlVEH02', 'client', NULL),
('ANTONIO RODRIGUEZ', 99, '$2y$14$1j28tHznGlcVmBLcIjQUvu9qW4OLmr5vCaLm5tBVKBsdiycVMOSae', 'client', NULL),
('Araceli L', 315, '$2y$14$pi4CdW.pDQcd3u1yBqSJV.w3OUSoEw4/fE.z5o5jJEOgCgoG0KapC', 'client', NULL),
('ARISTEO TABOADA', 100, '$2y$14$4ItQrPZG0JteSJg07iKSsuVRHpxT1eTaPmZKx1rJMQWLh6P50tXvG', 'client', NULL),
('Arlem M', 263, '$2y$14$Tc/UsdZb/Yh9g.PpwGMDIOw9A3sOy1IIh6xt5TFe36.o8ckiLoS7y', 'client', NULL),
('Armando O', 383, '$2y$14$UEcVQ1YstexJ8s1avQd1peP59HMC3AK63d/6ZdmgJmNBbmbKnaMD.', 'client', NULL),
('ARMINDA SALGADO', 101, '$2y$14$fivMcHCvHLBWKqmGO6zwzOtbn5T9oCPbpXNknA1qQdEj5DrTbQhxa', 'client', NULL),
('ARNOL RAUL ESCOTO', 102, NULL, 'client', NULL),
('ARNOL RAUL ESCOTO CE', 104, '$2y$14$o8NqIBKcvDj12LeQ73OfyuCqds.k5BSy8EvfLoWgxqOwlXqx/.2WG', 'client', NULL),
('Arquitecta', 382, '$2y$14$1pbWXtxU41s7BA6VsUeuJOJYpXc6c0ShyNdHlRYVFSM0Ir/yBz2ie', 'client', NULL),
('Arturo H', 327, '$2y$14$fQWNkcQzqYYt7vtBy7nXfOSEL2IzaSHRmTREPxkjGLMdmjDbNJ9c6', 'client', NULL),
('ARTURO LOYA', 103, '$2y$14$GTFOn6Mvt3NWAJDzWt6eWu2jsXKEmCrUXR8ZSqwI4E0vjDt82zV22', 'client', NULL),
('ARTURO SANCHEZ', 105, '$2y$14$pu1j3zoix1LMdQ6cjZDj8eGNyqOcxGfljaLG0SppqTYEdAdVTUpUy', 'client', NULL),
('ASESORES DEL SURESTE', 106, '$2y$14$awk42euhjzlfZnVeRmS8decelZSVT/WiDNuxuxogTCLHlNqncXiV2', 'client', NULL),
('AVELINO REY', 107, '$2y$14$3A5CoTrC61xyFnIMS0shf.wCGKo5OkwB5lANoO08PAbw257eDrSvm', 'client', NULL),
('Beatriz A', 360, '$2y$14$D.xnX9mxmV5x9F0Z8KxZ8O9gTygQTbr2R.S.XyED7x26821MQVUVy', 'client', NULL),
('BEATRIZ E. BARRAGAN', 108, '$2y$14$DYXhK520aJ1o0xJEt8RWlOjFSJ6Bw5/Fs3h2g.dpsVTVeEvIMmxhq', 'client', NULL),
('Beatriz R', 331, '$2y$14$zTXpKS1p3Vi37J1I.XroaeiHX5tw82rdvrst8haIz5ZyR0ku9BktW', 'client', NULL),
('Blanca A', 42, NULL, 'client', NULL),
('Blanca T', 27, '$2y$14$rFaMIygZnrfWNsmuWMYnXev5y32WbBrdCN5/sWbwvle1xQUqdts5W', 'client', NULL),
('Blanca TM', 272, '$2y$14$G1TdFtysiwaVtjHGbsC/OO8CO7gi2h.qG8tfOeXqf/Ayqp8AYQ6pO', 'client', NULL),
('BRENDA MACIP', 109, '$2y$14$tT6A7gUsBkjFbfPdF.vU2OyplPqhaxDVBxqdc1RJO8.TLplUMaBqK', 'client', NULL),
('Bruno R', 35, '$2y$14$VrpBiyH1gSG55Aly1FR/burpwRmpzLWOe3I4Zf5xEcE/jZWgyPDM2', 'client', NULL),
('Carlo O', 77, '$2y$14$RyC/u5cyatLgrxmRHfMG/OXwKeEFwEQL2DzKeraM7zoYmLXlkK99.', 'client', NULL),
('Carlos A', 294, '$2y$14$j3gNGFitSexSWGrWHz0I7eiMgumtH85kv8d.LvvazN9kQPoqXOMDK', 'client', NULL),
('Carlos B', 71, '$2y$14$garfKpgFJoJEi7Qr6g15rOlXSKTP2vFh.c79JJ9Ei1Z2FWdTVF1i2', 'client', NULL),
('Carlos G', 354, '$2y$14$I3cm9HDEBstQLgGNocnu8eXhr8.GRpjVP.vrcn.WWjRUuDLmxUBuC', 'client', NULL),
('CARLOS LEON', 110, '$2y$14$h8G6aMCqSX7xy1l65afRseDPLjQ19Ik32b7yiqkbaNeaeVlNP5LFi', 'client', NULL),
('Carlos M', 284, '$2y$14$5djTnD01XY2OuVvS24ihIuhzyXR365fmwdUL4LhN19kNzL/irLtI6', 'client', NULL),
('CARLOS ROBLES', 111, '$2y$14$qOxtJLHq6K8zw6LiTSZSDeqEM8RcC4VEd8Mob1KmVFkZFz0jy9A/W', 'client', NULL),
('Carlos S', 378, '$2y$14$sRAsvC9nShD37Sq0C/qu2u64SzRpwSexPwd5k8ZjDBblTQsvpTumy', 'client', NULL),
('Carmen S', 342, '$2y$14$vsYM/tGbWzedWTDczep8Y.9IdDENcONZtkNYxIWgh4c6ZFhEsuIge', 'client', NULL),
('Casa 33', 293, '$2y$14$KKymr3.lzdF7JmAc0/JKhuIwIGxyNKzlyOSziVFmKoxMRdJFzXZk2', 'client', NULL),
('Cesar C', 376, '$2y$14$NzWbW/BlLkfFbpe/dxNL8e0rIiHF9g2kt/Dnykk2QzcdrSJgvyR.S', 'client', NULL),
('Clara H', 36, '$2y$14$NhOixVNUnZFSk2FuklMug.E5zBkGcO14lfRelZoieTPZYyBgtAIRu', 'client', NULL),
('Claudia A', 51, '$2y$14$A7WFMWKBZcKtgfgP7QQyWOE7gmgt4T5AekBYD9OKZOu5YORfCr3qu', 'client', NULL),
('Claudia G', 353, '$2y$14$3U2fzZoQcGRGi4rh1EaBLu7TLz2yazPhm.pR3rsKVdKExKHadYXXu', 'client', NULL),
('CLAUDIA HERNANDEZ', 112, '$2y$14$3h6wdSLA9KY7P5/GBWqx3eK.b9yicoIbbVO.F7pua6JrFBGVhhgZ6', 'client', NULL),
('CLAUDIA VELEZ', 113, '$2y$14$Zv//GCf2w0Eyi5FouxhF1OHQdt9WG.HZuKhDZ9KPvCZRQiU60nf8S', 'client', NULL),
('client', 3, '$2y$14$vqhAUtT4XUcsA3m1dQ1dOuAhxhPh4dyPG8h37IryyxeEWQ8u6.jly', 'client', NULL),
('committee', 4, '$2y$14$2OHYi4MBaP2140syMeABe.T4Nq1qpJUi2.Lheds.WLkLEnplmDr4S', 'committee', NULL),
('Dafne G', 355, '$2y$14$cmhBzbXtXEFSccaiQKSfyuaTwPoklKObHK1Rz6UsxAk4FH1HOLg1e', 'client', NULL),
('Danae P', 64, '$2y$14$SNLwHOzTT0rbRTrA1652deD1hcBxNErSHVwnEX2KRnHx3XACCRu.e', 'client', NULL),
('David D', 279, '$2y$14$zw133CdBo9aUxGW4dMQGy.u7wuOZso/D0nw9C/kzuxw/giLuhH97y', 'client', NULL),
('DAVID ERNESTO MAZZA', 114, '$2y$14$VaIPw1NLxfL8egQtOQ8pU.IuuHetGwtJWVO8h8fFpBsbgrFO5rNoS', 'client', NULL),
('David G', 264, '$2y$14$cHjm39B.eQjUwCe8ZkhmNuz/.yyhkJDMn6g9t0avKAwRqc4S/h1IO', 'client', NULL),
('David L', 75, '$2y$14$V.ejhHEN8dCgqUMcfqdh8u6RIhYi4F2zIK6IsgPJuer3bPdZf9RqO', 'client', NULL),
('Diana V', 366, '$2y$14$hz56Luaf0eq6NBirKE4oV.Kj7Bg/.G8DFah0I2pt..AxtqEZrdvEq', 'client', NULL),
('EDGAR FERNANDEZ', 115, '$2y$14$w9ZCxt.wJjBygh8wwT9Ht.dzBQ7wkr72NjFs3SGy4m4iWEi7RrUGC', 'client', NULL),
('EDGAR HERNANDEZ', 116, '$2y$14$E1YGZIJMyH3xCBc1F3cY.OR1RkCaYOx6qpK/Qp5T6YvL4wjg8FI8W', 'client', NULL),
('EDGAR KLEIN', 117, '$2y$14$K4BIdVPtR68.QeAeo70KreBklmfE9147vJ026Xzmmhu6EcuwWJMf.', 'client', NULL),
('Edgar L', 323, '$2y$14$EiUtQYCJLtfeddWahE3UVuIYIO6AWy2t5pt1DVhiBmFAEN2eJ2ysm', 'client', NULL),
('Edgar P', 387, '$2y$14$uj11drd0zHjMzOwu.VIEAexaTLadKMRNYG6fcs325XRCFyJsDhBSy', 'client', NULL),
('EDGARDO ALBERTO CRUZ', 118, '$2y$14$cRccH1fQHbWIr9N3noBHTufSbkgD6c8ZyZNLqqD2F.6Mbz2y3EBzy', 'client', NULL),
('Eduardo A', 290, '$2y$14$rCiUDayJGiXxHCHWCnyPlO2CPuegVWdv8mLTKzWotCydFilZ.zy/q', 'client', NULL),
('EDUARDO ALONSO', 119, '$2y$14$8yvnsCP23.D5UOEE6X1QZemRyn0N8Zaa5c/Ot6Gpt7tMvM4qoQNlG', 'client', NULL),
('Eduardo C', 12, '$2y$14$2OzndC/6b0XEtHnTKC9FMeKAGo98opxfpmTkgrXvILq0MmQUvzuce', 'client', NULL),
('Eduardo M', 47, '$2y$14$7w5IfO.dEHj.o0Epf.vZh.dIKUA2iygryPPxJAFuitwmZYVWD08YK', 'client', NULL),
('Elena M', 62, '$2y$14$EK0JEI.jgYp5xb4.0d02aeYHPqN/U2wb4jUKgN9Sly1wg5EqlZFni', 'client', NULL),
('ELIA IRMA MONROY', 120, '$2y$14$GC.O2Q7EFUhCmvdwo9I5Uuy3ok2JG6K1jJmnWWTcD.XTKJKZ1xjri', 'client', NULL),
('ELIAS ZETUNE', 121, '$2y$14$TBpkfogJ2d74UntuUd5gveBhafm0McWjB8gNaJ6qgkKooXAULA586', 'client', NULL),
('Elisbeydi C', 347, '$2y$14$6aNjz7KmBr2H7Mj6NSovYORsfi5iwA0tEyXdgKtcfdsyuid7Jc8KW', 'client', NULL),
('Elizabeth C', 351, '$2y$14$ykoSwRPoJp3yUwWaFqjKmu3a2X6pEBBBRen0LLlkQk.krU11l.W1u', 'client', NULL),
('ELOY MARTINEZ', 122, '$2y$14$e6V18MhsF5uaGcXjft/L1.ZZgFDm/ikvsqmEYlo.ZoUjf44SPCqJW', 'client', NULL),
('Elsa A', 379, '$2y$14$mokMrgv6Pq/vdbSm4g6zDOVzcfcvEcJw/aHQcRp1L/GiukcYb2hga', 'client', NULL),
('Emigdio B', 326, '$2y$14$ZC3Z59b6GEdHXmw8FCzOjewifehX6bUy1sI1jBOH3yo0lE8rvNXeO', 'client', NULL),
('EMILIO LAFER', 123, '$2y$14$JnXC/Us1RpPH9zpnVqJe0usj8IC02vzOZoZF0sbpLVD4fh0W4AO1i', 'client', NULL),
('Emma B', 8, '$2y$14$EGS1IemhQt2ozctq.exIC.6FkahQeuNaXEzVUXkvvVQUcCyJXQUhi', 'client', NULL),
('Enrique R', 15, '$2y$14$aJ9aMzw6OkjhAFoR9LFU8u1VnUjcFUMqlGvJd4Sxka5XQ2dskHvPa', 'client', NULL),
('ENRIQUE TAPIA', 124, '$2y$14$juTupQOoL7yQlO3fNTirhuSAV3QOrO0un5RXSUqfx1Yx7UgZf5qJW', 'client', NULL),
('Erick M', 262, '$2y$14$pK4cHajhYo/aKWsbWUef.e1jXGDnldbTZu8k91if6YLQImfARKTDi', 'client', NULL),
('ERICK SANCHEZ', 125, '$2y$14$ElA5l09zgQpW1EhyPh0B8edW/5dtFSh1xdEqswXwe7b2aFLUbur1S', 'client', NULL),
('ERICKA BORRI', 126, '$2y$14$6iXMX3LcUP9EWPr5Pfh4UuE8iAjE6ZKjGV939y/3kamBBqBwT8Wku', 'client', NULL),
('Ernesto C', 299, NULL, 'client', NULL),
('ERNESTO FARIAS', 127, '$2y$14$LlJQ7NTbjWFc9clfkT.L7.VYHLaXFW.Tjc5KS5VZn2hcvNhjhbkT6', 'client', NULL),
('ERNESTO FARIAS SOSA', 128, '$2y$14$zFidARUmhaBBB4kXr23kmuLLydkPSWGAZ3jlOH19xsKmf/jesLqrC', 'client', NULL),
('Ernesto V', 28, '$2y$14$HH9EkaO/REbJD.WZGgG4Muz25x.WHIVJ4iWY6I3sDV0f/ARkhHBhS', 'client', NULL),
('Estefania M', 325, '$2y$14$0zRm3dPidKHoqeO0eZm6AOgx1UuMSZxmv7ls1lwkVUMB4gFcBtrUe', 'client', NULL),
('Esther A', 307, '$2y$14$9LqWLygviVjr0yMRC/Qe/uNSsxrVOVri8HXIH1VHrxOtzMsIFaTd2', 'client', NULL),
('Esther M', 285, '$2y$14$NTXZNWv9Hj6VLHH2XQQ7WOqLjw7YlaqosTUMGsVEzLMIaVOIh21i.', 'client', NULL),
('Ezequiel B', 386, '$2y$14$TZcjKoJ9HjBuLoA1gryTyOv0XaCJLSc.Ops6SmI39vgg32181otKG', 'client', NULL),
('F. JAVIER HERNANDEZ.', 134, '$2y$14$1izRiiFa5o8S0iRnctlMyOc4Zkka5jWkc1bX7uGz8Vz.iuWmO5Uwq', 'client', NULL),
('Fausto A', 313, '$2y$14$px4733BU5s7q6lHkTh0lq.QAQIfytftMVXQ5VSDwMy5dykWNF63Gi', 'client', NULL),
('Favian G', 81, '$2y$14$dThMUuJo5aCiQuurDZS1puXful5C3fQm2/FWvMJGusfRI6p2m2iyW', 'client', NULL),
('FCO. MANUEL RAMIREZ', 135, '$2y$14$Q6NEyoudjVBsZauOFqbz5.8NLos8Tk4B7XslJDl5Ztf1uusmZZrIW', 'client', NULL),
('Federico O', 341, '$2y$14$GkDaWpD6aYqGv5BtazUGFOGpaVQhKiP5VsqC83J8a3A7.TS.F22VC', 'client', NULL),
('FELIPE CAMACHO', 129, '$2y$14$O3YfhEsfVaFDiNHYeZtNQ.pd1GLq2k8Qykpl105wL1VcnpwNo/O06', 'client', NULL),
('Fernando F', 68, '$2y$14$TPigSiplZbvIlSv.OB7Ku.6vbYhORG9VWx8PjT/kBbPcWrfGHj8gq', 'client', NULL),
('Fernando P', 345, '$2y$14$0qvsDwqjHVToNoaGUCjLx.pkWv4DOCi7MYzkAb7.iUlNPxr/wFPa6', 'client', NULL),
('FERNANDO PARDO', 130, NULL, 'client', NULL),
('FERNANDO PARDO C', 132, '$2y$14$wqr.rEPhbARL.mkGGHe9TeWUnnHVYJeOC2Q2nPi8DjFsvzny8Blna', 'client', NULL),
('FERNANDO PARDO C.', 131, NULL, 'client', NULL),
('Fernando S', 74, '$2y$14$/oJIJ2xsM9R668Fb4WL6puEdzhU5JqDRfuX0tYBdq32AQC00RyG0i', 'client', NULL),
('Francisco C', 286, '$2y$14$AnELDgr/1fPYCk0SAkYt6eVKOlEASwE6BW4WiLJKBt2heJrbdyi82', 'client', NULL),
('FRANCISCO E GONZALEZ', 133, '$2y$14$Ht7lEHzjyjvVN5qzAvcAtOlzSjlN3CTS8fT9914az7OPgpO7spGjC', 'client', NULL),
('Francisco G', 381, '$2y$14$7YY/BT/2EwRJ3ZIsjcHQBOzRE339xplXLLsaBpsbvMriG1dkJBBAy', 'client', NULL),
('Francisco L', 57, '$2y$14$57zKZN13WbatLhQOBiZQ7OpoZKDkKd5XCb0j8j4RdLyYKd5Fe5rMy', 'client', NULL),
('Francisco R', 18, '$2y$14$B9sn769GZrMt1R7XvSMGdeYpYRTUc.XQq9cWllbFhVHP04vOzTJZu', 'client', NULL),
('Francisco RV', 340, '$2y$14$aQt1sSgTLCKpyI1lW7.47OeMWxIVDqPoKtiCDnbS0bpx2JnCZpn4e', 'client', NULL),
('Francisco V', 280, '$2y$14$KpLZqROBMG8jSsc65KTctesa9w.ym5P6nZEs9yT.XqUrTobE4dGZ6', 'client', NULL),
('GABRIEL AGUILAR', 136, '$2y$14$b0fA5hVtbwsaM5flOuPG5O1zInaJme1DFKdQvhztI5VnlwDvIBPsC', 'client', NULL),
('GABRIEL BLANCO', 137, '$2y$14$Ae6KiHt7WU576bAczg7pqu22Chr0DJhXE4rtVJiZXDlbbHwFlMwIC', 'client', NULL),
('Gabriela D', 273, '$2y$14$zvcPdpCFcbi7Y3FVZ1/MoebQkyU132TD8BwHUvECR3GCAYI21z8f.', 'client', NULL),
('GABRIELA MENESES', 138, '$2y$14$pOYF7NtqFnrUyA64R3OI0O8X8VamKJiCk6YQTFVdZ1e/iJBPkbDYK', 'client', NULL),
('Galicia O', 338, '$2y$14$28G.WCJC2KV9ADvlPxjBlOKIQBU7cYHLOGzSSEEk4IkJgxICDV9LG', 'client', NULL),
('GEORGINA GUADALUPE L', 139, '$2y$14$Ho6WqNN5idpNarC5PfAkTejqzK1uqMBVcGfsnp9jRLbZGWJqPGyee', 'client', NULL),
('Gerardo C', 365, '$2y$14$Yb0D4cq.6TREd5sQM8jP/OVBqmbWJ1KmQjOgZ1Jn41pGkU7IcVpna', 'client', NULL),
('Gerardo D', 69, '$2y$14$WSow5QXkZFfGKFtR9zAb2ufbnrCFG.JjRFEtQjYFK0LOLacV8zywS', 'client', NULL),
('Gilberto P', 370, '$2y$14$4vsHBBN/OrbxS8NAXJ0ht.rlMpfTEh453Ow0wFE8CzQhMracDNZhO', 'client', NULL),
('GONZALO LING', 140, '$2y$14$MJrQ2q6PW3eV403RSNnIuOESu5URcXu6QlUDzFg9lEhJ1Y3jVimgW', 'client', NULL),
('GRISELDA BIGURRA', 141, '$2y$14$RD3yoYcW8JXBmQbRYXD3TOCa3pHUl1yVEFvQwM58cZL21VEg2SncC', 'client', NULL),
('Guadalupe B', 318, '$2y$14$NlCHdKtrPZSVJ7VL1Apiau/JzmSEr2c0lSI.mipVOZw5vbo0Pmev2', 'client', NULL),
('Guadalupe M', 335, '$2y$14$gkf3EQscNW6s9haB3vHCGOwDfjg9bq2WZzoNSiWLLtC2V5sRW84BC', 'client', NULL),
('Guadalupe V', 310, '$2y$14$fMosUcd5x2VprbX2/UUTm.rAnxTZvbnzmf5zrz6AWWUe/TNgA.y7.', 'client', NULL),
('GUILLERMINA GONZALEZ', 142, '$2y$14$5r1iewiHt1C8XYLN7KcwXOsEQqqKSqawDPRA/WEaPqDteUXNxjtyK', 'client', NULL),
('GUILLERMO MONTIEL', 143, '$2y$14$k5nu6o.c.uZaIUcoezDLdurWGHFCy4.tf.7M5q2oR8lxOHjgXvRjm', 'client', NULL),
('GUSTAVO GOMEZ', 144, '$2y$14$dCbHbTCAOlzIKwAR2BGRIevFmmUJYLeR2IVc7CuXTuZkEgYG6KKR2', 'client', NULL),
('GUSTAVO RODRIGUEZ', 145, '$2y$14$RZX9iZl1lhi3akq5K/ZZZ.mWP00YaCToqY2L7zUT/jk5rA3kBHdOW', 'client', NULL),
('HECTOR MARTINEZ', 146, '$2y$14$8ZFd.FLkfZKK6D8DjtYQoesJxjZq6Znlf0z4rCcZg.4JOGeC0LP.e', 'client', NULL),
('HEDILBERTO TOMAS', 147, '$2y$14$OM/QqnUeAHJlVT60aSY0UeeEwaeyiGJo9gtect.qDry4f2KqczzBW', 'client', NULL),
('HENNING KONRAD', 148, '$2y$14$Jp7Rr/5t2LyqygsUkG7gB.hTo4wOySTXydtjWRk5rFty6oGYIN8WG', 'client', NULL),
('HORACIO AGUIRRE', 149, '$2y$14$CEy79xud1yfYgs0i3BhPfOnEtMB7vjt8EJEElM6CfiKGYPij2KMqS', 'client', NULL),
('Hugo A', 73, '$2y$14$iYTsBzYMEkWLA04KGGw/6ufnak0b.brC.thFrrVIeImw0E993g8gm', 'client', NULL),
('Ines M', 301, '$2y$14$ooOhBRR5eM3IxPZXWeU4l.I1nFSVEAEKClr7dUWibyUFz1af4Eywq', 'client', NULL),
('Iñigo U', 22, '$2y$14$6hkmRwkX9iDqW4l1XtfS1.lRhS9i5IbMQUbRsBbHneSACIrnb1/ra', 'client', NULL),
('Inmobiliaria', 276, '$2y$14$o4UHMgbx2iIDl.HPXUtqseG9EUXC5gPbfWlShcFwzbtuQj8cPohI6', 'client', NULL),
('Inmobiliaria C', 339, '$2y$14$HBXiWR20J1g.8Ia0Cf2kgu4YJ4dfJQRWRfU7twCHz.JvdUgBRUZZS', 'client', NULL),
('Iracema I', 350, '$2y$14$6AO3W/p9WTMvzmL5X2eI8OBnNHDBhk4BoX0iwtEpArqrjngNl.sta', 'client', NULL),
('Ireri V', 380, '$2y$14$ZHTFminJVjuVArfprzMtuO2FG2BR6J.iVP23509gXxsb6KwivU/ZS', 'client', NULL),
('Irma R', 43, '$2y$14$12NcYJsJtrD9xiWpVZcQwOwrr75ITbvwveLxVs03k/vtKCRd8CUDq', 'client', NULL),
('IRMA YOLANDA NAVARRO', 150, '$2y$14$jlEA0oKQIkxdflA3oYIguOdfFpk4tPOWk/qlzw4ZbY5NFTGft7sPy', 'client', NULL),
('ISOLDA F. DEL CUETO', 151, '$2y$14$c4U6wDhxvFB3K1YlAa.FKuGIaFaXOedb2BZauoFQQ0KE2r.nINQQq', 'client', NULL),
('ISRAEL ALVAREZ', 152, '$2y$14$I0DNBHlasUj2SdASnIUcY.RCyMHERB5Rj.ZQcLgnQjg3HxENKA2nO', 'client', NULL),
('Israel M', 65, '$2y$14$6maqW5ab0xdA9eow8XdI5eYkVtnW0OKc0er.44aqI6ZmOedTqcyLK', 'client', NULL),
('J. ANTONIO GONZALEZ', 162, '$2y$14$CrCLoBnwSnBXQurfxyYDx.OP630QJGH9.yK7ykM35Lwm309MsUSti', 'client', NULL),
('Jacob P', 305, '$2y$14$eIBmb9v0JEVx20rmBAcuOejo/mFHcc75RH/bYh.JhVw6BUkN5ayMu', 'client', NULL),
('JAIME ANTONIO PARRA', 153, '$2y$14$XpuE8aJehBa5xUbH7H.gsuM.ZNADuBSr.AujHOOyxBL.UaBiacOxW', 'client', NULL),
('JAIME GUERRERO', 154, '$2y$14$WL.Za.lar/qwuMpOffv1tuvx5BldNHuQ0ECD4BC/7KodBhKXTnHWq', 'client', NULL),
('Jaime H', 16, '$2y$14$KswwsEeRazXCewjdKUTbXum7oVCBZZoJPacyF4KiczyjIliKQdcsi', 'client', NULL),
('Jairzinho R', 361, '$2y$14$jZJDqXYGj7WAo.0XZ6zzNezCtkSZ27/ezIcoiBxfo/dX8SJ1T2UmK', 'client', NULL),
('JANETTE BOJALIL', 155, '$2y$14$iN99u3SsFbtYlPvP2dyQzOaQbYLhalO4.SVOh4KgzyxLEYiGS3CDC', 'client', NULL),
('Jannette T', 291, '$2y$14$DlAVAewJ.Ip6VwSTsIBm1u4CeMuGNEM89t1KHLGycOM3O5dg5W5kq', 'client', NULL),
('JAVIER RODRIGUEZ', 156, '$2y$14$vG88MJKXO6WdXWpYgwmW9O5FXO2gknrwilqaFfeVpdBiZEL4CqF8W', 'client', NULL),
('Javier T', 67, '$2y$14$eDDKpoBvxYxBHpiyULSH/.J1Fm2rysJUj7OtWHZjLNZ17QzaTWhw2', 'client', NULL),
('Jennifer V', 319, '$2y$14$ulclYXxFc.ra7/MfQmXto.aL8tgChffk3w3AaY.oB9mlMFXceWeSi', 'client', NULL),
('JENNY ANNA LOTTA', 157, '$2y$14$0Hpd0Ka33RCQRymlgkT7h.83JSt4RxTWNSjmK3p91NT2Sj.LKn/t6', 'client', NULL),
('Jenny G', 346, '$2y$14$Al9FwJoNcTxwzOzxzckbie8yEjoZ1PYNsfQHnRo1MBJFl6m8fOguu', 'client', NULL),
('Jesus A', 53, '$2y$14$JQlr85/7bf63OgkKF81AW.SHkbRKSVMK5n.OdfYV.7rs7mql/fp5G', 'client', NULL),
('JESUS BASAÑEZ', 158, '$2y$14$RI36HFxr.SqNNzfPHjAQjeJZ/b5KKJ9GvnRKZRsrvfFDoWETWsqp2', 'client', NULL),
('Jesus L', 63, '$2y$14$iA/wsPHWvtIxLB3DNBoJmu1ABJO1tvk6yX3PqZ8ZMRg.7NY/BDByi', 'client', NULL),
('Jesus P', 33, '$2y$14$9k7UswjPaA5OngTw.njzG.5xXFTx4I0KTMOhDPvNQX3tTrxF8UTFC', 'client', NULL),
('Joaquin O', 309, '$2y$14$/tvEyYdmlv.BCiiEBL.OAupee9MW8zsffUwWfbrsSn06Ga9Yzv77O', 'client', NULL),
('Joel R', 300, '$2y$14$nhMKBsdxAUFN8vpo0gKCjudogX8nxU84WRV2eoQ6HcYszveaIuzja', 'client', NULL),
('Jorge A', 45, '$2y$14$2llbX/OFl4zMgeG/32ZfWOECqpRrXaFxUt5c490itxJpyRhQMtp9S', 'client', NULL),
('Jorge B', 328, '$2y$14$vUBbE5XKH6KLGJBF8cKbC.qP5zFpDQOvKtOJMiqfQV0Hf.qujFNQK', 'client', NULL),
('Jorge C', 52, '$2y$14$xgN3dL1baByqZA4dWm33..I67Z8QkPxwKUcNHWa9qjolQlcy.DoQ.', 'client', NULL),
('Jorge M', 26, '$2y$14$x3ofWrOfydcK8DS7PoyP2eMMUhrGL.YO0uM11fMM8hwmovUSjbsLS', 'client', NULL),
('JORGE MARTIN CHAVEZ', 159, '$2y$14$38eNASEeuO2wVNfH4tPFCeqLBkKu/gfUPNyASn4S27bR7no4E3c1i', 'client', NULL),
('JORGE MAZZA', 160, '$2y$14$Uu22UYw7fHVx4fOI5UThE.sDhskWDCLR.lGL.HSIydW9HBAs3jE1i', 'client', NULL),
('Jorge MM', 329, '$2y$14$zADU72AbYZ2G95kTsUdZN.rWE0CqWtXGhsyriUgsKAUvcqDvfkFiS', 'client', NULL),
('Jorge O', 373, '$2y$14$3gajwlEaIyqQtwaa0in5sejttYw3NwJdZ0YszWmrHmgbQesXs0Ni2', 'client', NULL),
('JORGE STECK', 161, '$2y$14$vjH6dvgcGvr/V9TLk5daDO.y72Hl6yZ14fGvzKhgvuRua0zVBdO2O', 'client', NULL),
('JOSE ANTONIO SOLIS', 163, '$2y$14$5rmxL/yR0XNf0LRcMuGGCO0CA6Ko8ldvnZMaZGqVQTVENdOb5YmY6', 'client', NULL),
('JOSE CANDIDO CAMACHO', 164, '$2y$14$sB1/G//FMGZIfS/BupI1n.u3YkbD2ZFQqRTFjr7BOtYF99tV95bVe', 'client', NULL),
('Jose DP', 349, '$2y$14$dWA9NWlMmfAnhy.Wt8tYM.jngMUXB41pcBlvO7kfSHI1ikkMSGYum', 'client', NULL),
('Jose H', 374, '$2y$14$fYQJ0HvRSS8hEeyVWI1bteyRo86INzJHC6MQoQ1qmF4ND1zYhCGdC', 'client', NULL),
('Jose LP', 23, '$2y$14$zN5ZW5.bsuUCp6T/cGGz7.ea3JlDwV70/Z3qqonXqruqUuPmsN8F.', 'client', NULL),
('JOSE MARIO MACHORRO', 165, '$2y$14$nDTOsuO8MYjUddVx0.BNa.d.TqdkN3Gj1xuh6ElH90XcZ98yVrdCi', 'client', NULL),
('JOSE OLVERA', 166, '$2y$14$qL/9JuN40FQZrOgy4nDx.eyB6LABVIs4U953WFE4ommvuJ99kNgDe', 'client', NULL),
('Jose P', 21, '$2y$14$uM5RyELOPZ45Lhw2i3mGzOppMnugND8QjW9j9GYnELNaT.SVVchzS', 'client', NULL),
('Jose PG', 363, '$2y$14$0JEWIkua4L6KYfIOLevlFu6KHJcQzAFf5JDBwAXk/ItN1L0fJga/W', 'client', NULL),
('JOSE RODOLFO ITURBE', 167, '$2y$14$f7WIAePhd7e.Swb6oDVJgu8ZhJLuBSvBU8x.p9M86hEyAZExgMpo6', 'client', NULL),
('Jose V', 20, '$2y$14$4Pvine3AC4HeB8wWA/lqK.quOCAyN1eOzASHFkm1XETtMO2Bq8XC6', 'client', NULL),
('jose.manuel.tello', 6, '$2y$14$qMi6OVzgJAH8RGqBsGmK/.vJIUHtysjhLYVQLUaJ3S797PtUK5/Lu', 'admin', NULL),
('Josue B', 320, '$2y$14$puuuFVks4Y4uWKP4SoydB.Jn23xHVdvbHVTVugBUDKnsYP26/5uMO', 'client', NULL),
('JUAN CARLOS BENITEZ', 168, '$2y$14$KWjS4aTPiNR3222AG1CbjOtfpL8nAErf0EYbC0HhnPBQbPG7Xbdme', 'client', NULL),
('JUAN CARLOS GUINEA', 259, '$2y$14$eRC0/yLHwgAl9s3aqDDsget9y9SL6SVxImN1UStyZEiVskABFv/fe', 'client', NULL),
('JUAN CARLOS NUÑEZ', 169, '$2y$14$BT2XnwgIP1BNtBkdbkYQeOaC3YneOIS1Zxmxc7NlvkRrPBjH/SIaW', 'client', NULL),
('Juan CM', 352, '$2y$14$9NWeRLNStBwLuB01./ZJ.Oe/F.c7cHFnwaySKh6XKjDEPVqPFXoom', 'client', NULL),
('Juan F', 13, '$2y$14$vp7gi46xUxB7yPnaodh1F.YsyCys/wsmcRSybR09FXM0LVP8LXA86', 'client', NULL),
('JUAN JOSE GALEAZZI', 170, '$2y$14$5Q76UsPpnZDP9Gc41k.0wOmPu1g1JqG7dRVAHM8xy48r4n1LwrYCi', 'client', NULL),
('Juan M', 10, '$2y$14$gRLfPA/D1fSWEq9YiWR0iOeZuvfkC39eah2QubqPl1rgUXCctF2yO', 'client', NULL),
('Juan MM', 34, '$2y$14$FFT23w4uSd8YM7g8SBfTX.7JE2WkxuUjpfAX10mFAnkuKe5Hq0dQG', 'client', NULL),
('JUAN PABLO MARTINEZ', 171, '$2y$14$YQZi5ukfIZ9HRQWR/.mHBOyyDOJwSkDsnF7C75lvyqG9w1A70LrWq', 'client', NULL),
('juan_fco_monter', 7, '$2y$14$5Ei3JaoVi9vf4DJCcy7CN.zzgZ7DTILK3jZADZGHXkv9S/ZJ9yBN.', 'admin', NULL),
('JULIETA ALVAREZ', 172, '$2y$14$9snPJrC.aEjh/cQqWPSfouFV0omX/V0X.fuYcBNNlpiDYOFJOSBVa', 'client', NULL),
('JULIO MIJANGOS', 173, '$2y$14$Py4cTCTke64pUkmExolDdeKmH.p67ObUkDkbSlQ2JbEVyFIm8mZPu', 'client', NULL),
('JUSTO ALONSO AGUILAR', 174, '$2y$14$296BReLU9rwX58MGGniIFemTEQcY08EBgz.vtGbVEYrnJ91wtMEzO', 'client', NULL),
('LEONARDO SERRANO', 175, '$2y$14$j4QB2yL6H8x3F/y6iNWYU.PL7f1mLIxusVaxhlc5VjMveqN0Rt78m', 'client', NULL),
('LEOVIGILDO MUÑOZ', 176, '$2y$14$TNOYd/jmOEUZJcH9ROCt.Ogt4rIn.bY2fnmgomzGBV7MR1rglHRna', 'client', NULL),
('Leticia D', 334, '$2y$14$CQ4.BYp6Sy7O8NLfN19psO72hMNnrVeZLQvmFj/6OdKJyUHevuJXC', 'client', NULL),
('LETICIA GARCIA', 177, '$2y$14$w8Gnbjubl/jIKD699pZWGeQXNWugdXwvZ32REpYr46y2SpK1ijQ5y', 'client', NULL),
('LETICIA VARILLAS', 178, '$2y$14$tC3chKCrAKMyTnrTMbnHiOA6TnMoeZLa0QblagfNvqyNlZR2f5uZ2', 'client', NULL),
('Lilia F', 55, '$2y$14$7BpNw4He73kZ3/eHv6LPKuysqimYrPtJwMgLy8BYaGgbE4HPlZgKy', 'client', NULL),
('Lilia V', 298, '$2y$14$z2lreJsdBS4Ftd.pyXApNOtt6sm8Vw82GKM.4N1qY0RgLwwOqjL6W', 'client', NULL),
('LILIANA GARCIA', 179, '$2y$14$/b4XICJwqXNrgEBTdOzrdeNMgpfjytjrBPcVNAxs1bniK/DZNDpdC', 'client', NULL),
('Lizbeth P', 321, '$2y$14$o.Zj5EtodDt49eQkA7gM3.BwLElCAdWoQQwwICAl5Lnv90W.IOWU6', 'client', NULL),
('Lizet C', 344, '$2y$14$hkI1jXik9T8riNHDacXKVOvYGmeMPJpBisAhwjAb5WUpeDgnv.vWq', 'client', NULL),
('LORENA GUERRERO', 180, '$2y$14$/vGd9oDbZfxTmdpRN7unIO9PEEukIMi2cmV4goIPn4EQ9xKCHE/9y', 'client', NULL),
('Lorena S', 336, '$2y$14$Nnb7Frc/CU3H0OuUrGJSoODat6TDgJUWJD1bJq37.kkNdnCyuIaTC', 'client', NULL),
('Lourdes T', 278, '$2y$14$lasDhcJKAIYHx4wIgXnpYOLrhvRAJ5oxy.2.QGqeOTR8npL49qkrC', 'client', NULL),
('Ludmila S', 377, '$2y$14$/CXVVqUML8M.UkRa5X9o9e7fWtJ..MPoQjXspX39FJnOix9dWYRhm', 'client', NULL),
('LUIS A FERNANDEZ', 181, '$2y$14$gYpwobOi4MLKxxTM9QiTDOeDbtknilii0GIZjyGddCTfYSTZcvMUK', 'client', NULL),
('LUIS ARMANDO ZENTENO', 182, '$2y$14$0zpQAY15L5K0xG13Z/yc3uNtgizi/Y1POFlvREkjaDIpqAmBFylqy', 'client', NULL),
('LUIS AZOTLA', 183, '$2y$14$CPl9sPX2DG7cpwF.73UjV..6Th8jl1rF9FiM8VxWEpxiJnhRUg3jq', 'client', NULL),
('LUIS EDGAR BELLO', 184, '$2y$14$NoE7g3kActkyFlewJshnA.q0UuvmM.93ar4ByWa2m55yE6Fs3wrKC', 'client', NULL),
('Luis G', 306, '$2y$14$5Nkv4boHZBg0ygg5o3TZue1XZQiWg0ekI271HAMWs55b8g2ptzBS6', 'client', NULL),
('Luis GV', 312, '$2y$14$z51vx3gGgdnaIH8DQ5KL8eEX5kswglTz.Wftr8qI9Ff3TWDK6oyCu', 'client', NULL),
('LUIS HUMBERTO LEYVA', 185, '$2y$14$pSgCC1XUeOMr67TJ1bwbJu1eKqERraSOD6AtYs8vmTWmjNirMeyRK', 'client', NULL),
('Luis M', 59, '$2y$14$voPFfUFmbCWFRPb/IkGvNelhxEqjp5EhkDtv/HharzVutkvXzY4i2', 'client', NULL),
('LUIS OSORIO', 186, '$2y$14$fNI2hPd.cV4.pirCql.RqexJPUNQPmQZpRzitlEzG7UuQ27NWfH62', 'client', NULL),
('LUIS REYES', 187, '$2y$14$dbI6HUr/aU7WAyMUm5D3FO5wmsJz7WC3INafJCpWVqkafTZCgQg7i', 'client', NULL),
('LUIS ROCCHI', 188, '$2y$14$0DZlUOn24uFeVZNDPFSEcuaz/Jq5eul..H/XpucfMKZMkx.sTQMcq', 'client', NULL),
('LUIS SALVADOR GOMEZ', 189, '$2y$14$G4PjzgN2ZtDWX1EtkM1RBOcOrjg3/WmKQQsTYb6oOXgF40YOyeghu', 'client', NULL),
('Luis V', 50, '$2y$14$C5FRl0A3mKfVAKacxg/liefoKAjitdU.dHQP8Ki3wkugXpapHHNJa', 'client', NULL),
('LuisM', 332, '$2y$14$Eh8YMduUuoUiqP4jKEgVUu8zMK.9r06cDL9zL.FFJdKy81KUA3ere', 'client', NULL),
('Magdalena R', 356, '$2y$14$dh1XDLIuPHaMOV2MfC4QsO/L8.O6XYE9Y/6IjolRATySqKHmC1Vuu', 'client', NULL),
('Manuel S', 56, '$2y$14$z466S59Z2ippYohSz22wPeS7z7Yc0Blotou1wVavtjua4DJh2w1NK', 'client', NULL),
('Marcela H', 80, '$2y$14$qQ6xkF9.C.8EHxd3kkjRsuqee8t8NbOQICoWmj29p5rLRCnoE4B5C', 'client', NULL),
('MARCELA MURO', 190, '$2y$14$0ci.ot4I2fnRHJ3EzSfhBuiO3mOYlXbwa0uTmA9LPBRwB8jDK8BRG', 'client', NULL),
('Marcela R', 19, '$2y$14$kStqgRWVCyVWplYDqt/FbO./Y8L/P5QFDyPjYh70L7xFyIqJPe1c6', 'client', NULL),
('Marco A', 367, '$2y$14$.X8zJrhMaaAnft7cM2nnIea55iPKIlM/r2kIaEt3sCIHWxBwiy5IW', 'client', NULL),
('MARCO A. AGUILAR', 191, '$2y$14$HUfjtU9H0Kfim.SAEr2a3Ouza1/Khh.NZjo33NtlFrw4p/NR95tUO', 'client', NULL),
('MARCO ANTONIO RIOS', 192, '$2y$14$199T09/hztWn/8Lsr8dzsusaes6AW6k5w47N8iEe5ZL30LHnP7b5y', 'client', NULL),
('Marcos A', 267, '$2y$14$ZO.bdaNxp53uQ2C0nMRcoObUdAHedy/38iPDwzsXvS6D.iPvYFjna', 'client', NULL),
('MARGARITA MARIN', 193, '$2y$14$xvqETiJIWO8TOeCgxqh6B.9ro7uOVtcdwtc0v6XZEKbKcnS6xrsiy', 'client', NULL),
('MARIA ANTONIA TORRES', 194, '$2y$14$9kWk0TT/4oA/.g/M8.HMCu9O.dDCHYBDJkqQL1EB426euzfC46Gmq', 'client', NULL),
('Maria B', 295, '$2y$14$B3xcbVFKVQUl4UQ.7HiFBOsUsF2kbBGwwnTGVDfRtRPUwy.lOcqZy', 'client', NULL),
('MARIA CITLALI GARCIA', 195, '$2y$14$V/6NldKoU7E8QdE8CXY2hu.QjR/UL3scsBTzSloDn5BFnGmnfBHYG', 'client', NULL),
('MARIA CRISTINA GOMEZ', 196, '$2y$14$8U/CBgtUQVXnhoNI66Ofh.oS3g5dQmcq701gE4wuYnObBp0d3djFy', 'client', NULL),
('MARIA DE LOS DOLORES', 197, '$2y$14$aifj6i33FyyLmdaFITk51.Mq1kZtGJROxLi34NWXmaptfsmosu4mu', 'client', NULL),
('MARIA DE LOURDES', 198, '$2y$14$/9Aek.0ijXF/i.Wde/OmJ.G5L/gCCYAbfTjT7NEFjTu0RdnMIFspm', 'client', NULL),
('MARIA DEL CARMEN', 199, '$2y$14$hDuyi4CxnMslwcqEEr1NPeFxcWAdJkqlXzrwKskkSUU6LjiJrT9Ee', 'client', NULL),
('MARIA DEL ROSARIO', 200, '$2y$14$WCLXbkpWGJ.afzy8cPDmw.nalk4//mjsBQ7feP6RCrChhXqiUN0sS', 'client', NULL),
('MARIA DOLORES PILAR', 201, '$2y$14$AABlyHTqrJ3Vbxkd9lmTg.T1tZtO5qFJnCBQj9vMk9FP9T9OH/asW', 'client', NULL),
('MARIA ELENA', 202, '$2y$14$3KKwiXIDbBYKIDAj93hVPerxD0qxWtWvTsymUIe5hmaz4hMHAV3ry', 'client', NULL),
('MARIA ELENA GOMEZ', 203, '$2y$14$SS1jPokN1aVEtB4M3qkFmOkly8u2Z12JJmHN7YNbydtT/qu3tD332', 'client', NULL),
('Marian D', 371, '$2y$14$AVI0WLHXpoIGklotB0xLjeIbVMKrJngTwQbHcKDsUIOksln9ns9lu', 'client', NULL),
('MARIANA D. GARCIA', 204, '$2y$14$Da3uymUTe.B/KriQV5cp0.Dr4f1xCA0ZwOUkXz9pOeWCBM2T53wKG', 'client', NULL),
('Maribel G', 357, '$2y$14$iu7/QtEjK9vpVXP1wd06ge4/wpRbtaI9IkmZuHfi1CauB8Kk9DRdS', 'client', NULL),
('MARICARMEN BERMUDEZ', 205, '$2y$14$YJGAwxbIIsGmZNV2OjcaUuNGBUMH/dSSx5C0pn2GLjTMUFVPTgbty', 'client', NULL),
('MARICELA COSSIO', 206, '$2y$14$xxOEZrSyuEzrlhNkjZ/XHOwpj3V.OuNr/HT99RCZFjQdkhVbV6Aqq', 'client', NULL),
('MARICRUZ GUTIERREZ', 207, '$2y$14$cIoCyCqlEIrBuN1IlfNxk.vXYU9tEgSXHpMnYR2FyC1EYwQMzdUVm', 'client', NULL),
('Marisol S', 38, '$2y$14$5Xywlmc3YwzTaO2iUSwsjuwtIerMFFxb5EA.5DRcJbVRaEIyxbcSO', 'client', NULL),
('Marisol V', 289, '$2y$14$GizJFRBNCl3xVUPn.Q/nGu7x.ONlAGy65WveqWQioQHLqbM1fHJTa', 'client', NULL),
('Mark M', 46, '$2y$14$a85KNlFFs5CbC1zV15kCoO5DxZZVrqjw2Hoq7/16keyx9HTMhHyZW', 'client', NULL),
('Marlene G', 269, '$2y$14$xb9WpPrzPJWbP/r6Zqtmm.4msVTEJFUwdLPmACtkR14BcRZVqghqC', 'client', NULL),
('MARTHA EUGENIA ORTIZ', 208, '$2y$14$jvg5daUCa4eOikojGJ8MI.Y7AuAHtGONxv6iBWmD0iFk4fDjiL6gK', 'client', NULL),
('Martha M', 9, '$2y$14$xxWWeFLo7dsuDZ7erIQeHe9NBbTZDYFunbmVtwXtUfbLxEpJn1xCK', 'client', NULL),
('Martha R', 348, '$2y$14$H8.9ibDm0qHjXWGe5Drc7OILElKT8LEEEBb42pvPJAEmbcistkg6q', 'client', NULL),
('MAURICIO MALDONADO', 209, '$2y$14$F74EQZNTzQTpm/E1LF3XZ.i3mx99tQ0QqFgbOaaZSo31w9iHxsDwu', 'client', NULL),
('mayahuel.romero', 5, '$2y$14$c4JdC0lzX8y8PW/HLG.yZeFModGtLqwX7RTukwTWRHENn9idZwvAe', 'admin', NULL),
('Mayelli C', 324, '$2y$14$HuM/xBIGYyQHf2V.4p8rGuLnyzRMZxWDYidY2RrXa85QtDwg/GzKy', 'client', NULL),
('MIGUEL A. SALAMANCA', 211, '$2y$14$KCGJ6vCpDncFjw5WV9lzn.iGfvLV7Uuzr9CtNVYZMyGQN3N7Ti8hK', 'client', NULL),
('MIGUEL ANGEL AREVALO', 210, '$2y$14$47jyMTCi5fAos.cxxbPBne1vckupdPnoYjx9qENnq2UFHRY0GSVG6', 'client', NULL),
('MIGUEL ANGEL SANCHEZ', 212, '$2y$14$dMGuNzN12.61gbopI0CnhOiG.jC8L57dlOoXaB2I7AnQ8kd6n07xy', 'client', NULL),
('Miguel C', 337, '$2y$14$5JYT.RG3sOGeY8htnb9cIer4UiZ110n77GeC4/UiD4T8fc./vtbF6', 'client', NULL),
('MIGUEL DOMINGUEZ', 213, '$2y$14$x3ramvS.pZqYjqKQ5W6d2uS4O.w5jHTtzTKpPHsfWeVvocfCVr00O', 'client', NULL),
('Miguel E', 17, '$2y$14$0c5coJ6gYHeAV4uir.uWLu/RdcgJbrZRmWsuw4p9P5QKqz2.bV9/m', 'client', NULL),
('Miguel M', 362, '$2y$14$jU80kZS2z56AiyZTutRToOZ6afFoJ/HuIEYI1ElZYy7TWSg7sYjHC', 'client', NULL),
('Miguel R', 275, '$2y$14$AuhiaqGNKZ0WA4bgshHvKeQ91dR9yhFfs7HYsktaYUbb0DMfyHMJe', 'client', NULL),
('MILDRET ESTRADA', 214, '$2y$14$4PZoKIDQUIfYdmKQwCRPhe0eFoQt.o/TA1XGnbXH/3a8EC6cbBb66', 'client', NULL),
('Mireya J', 265, '$2y$14$Ljbq4Fur74Epbwj7KZ/2EOkpxSswdSLfNPyIZ.pgXxxleKTtH.54i', 'client', NULL),
('Moises R', 274, '$2y$14$Y0v9K15m5RXKTs5kZFcdkuVt0N4dV58pZf7nwQI6biVYHhIMdaBce', 'client', NULL),
('MONICA NUÑEZ', 215, '$2y$14$f8No6HUK77Z2PVgh1LwlHOfYnAOwPyWjsj2CKtvvwI3BHQOYDOM1y', 'client', NULL),
('Monica S', 266, '$2y$14$rCzuWXGSPYR4u3Rc0vypM.dJFNereqWnV.Cl4mJQBnGwawga9i7Ty', 'client', NULL),
('Monserrat F', 277, '$2y$14$ES0pi3Xi9wzZ65D5ttx8nu5ZgmDgSF9OG1NwNHnS6w1XzvN3A8ecu', 'client', NULL),
('Monserrat G', 41, '$2y$14$q2XqLgUURlyWC1YsGTXz1uB36ZkmkvZiq.WDRxl77rw.EwNb27uAW', 'client', NULL),
('Nestor I', 70, '$2y$14$uWvCRILmxi5m1uLpUCK.Q.vUXy6Zx/Qw0bA1JkKp/i.MeLpIlvptO', 'client', NULL),
('Norberto M', 385, '$2y$14$Cbjgb9bv.T9F0VDoFStwau9WTeQdv0WwhRsywqOKnwsuhWYcb5GOO', 'client', NULL),
('NORMA BRISEIDA', 216, '$2y$14$OgqXjmHoDmugEFSt2s6EoeAbjpG227zcjZTqZLI6F2EpCuk7gE1.C', 'client', NULL),
('NORMA MIRIAN MORALES', 217, '$2y$14$99rT6x.fUYtMe//4toqNh.y94ApieNzolIqY.duXKAJhgMPvQg0eG', 'client', NULL),
('NORMA VALERIO', 218, '$2y$14$O4wMjJC35j6NWsEpKAoSC.rzI94byNX7TQD1SUjUX5it6v6Q2WU7.', 'client', NULL),
('NORMA VILLEGAS', 219, '$2y$14$uqgwTxf4CceQil0LVhHLGeG3WZuLYvZCbOc2eBU5KsTjEC.jbXfHG', 'client', NULL),
('Norman E', 297, '$2y$14$7V/S.7C5Zvx4yxOaY8JI8eID0yLhxWp9tKoW2beryoLmKPHM7EB/6', 'client', NULL),
('ODETE RAMIREZ', 220, '$2y$14$zWWRFNthBheEAUC35CSZB.oEJ0H1OdRKSAEaE.UlgvjBE8jN184ny', 'client', NULL),
('OFELIA RAMIREZ', 221, '$2y$14$2is29MeOkfQyMfSEpC0qrO49qMWHFsA9hvdluDCZUw6e7s7ewujua', 'client', NULL),
('OLIVER SAHLMANN', 222, '$2y$14$gZRzpf9UQtzt2Kbm4CnRIuXooYkMqA7.mpSku.1VvtuPJcfGLGiGO', 'client', NULL),
('Omar G', 14, '$2y$14$L3ulqFhXpRnz1YwtWYF7leQgGTtBqv1pQDrsaGOySF8g0q1agXUDq', 'client', NULL),
('Omar GM', 304, '$2y$14$5uDDw5IdBLqDSINLTuVmbeTBxf8ITXmg2jlUcfJwUhpULXNWjgeeq', 'client', NULL),
('Omar H', 314, '$2y$14$l.wmhlE7aWee9Cwq1I7hvuB6uULYurw3jHTjWRW2Vv29gcU7YzGhe', 'client', NULL),
('OMAR VERGARA', 223, '$2y$14$fa/PymJrUtN0lCK9SLK2AuwPvq5ROoLZDXt4YWdb7sNAShFlQ4TDG', 'client', NULL),
('ORLANDO CERVANTES', 224, '$2y$14$d0xdMJauEMV3rxkaLx13x.aEevIjmnkwsTqdfaoo2SekoxazKRJdC', 'client', NULL),
('ORLANDO DELGADO', 225, '$2y$14$obEOVFR4zNlYhy9l15PzTe/PRXdikLu/jNV.HeLtdZrKbBd40sWCG', 'client', NULL),
('PABLO DIAZ', 226, '$2y$14$c/yHaAxkDkz/xpyYPOfMVO/cUd1j7o/csu8N1eKj4sNW7Z95WER5C', 'client', NULL),
('Paola J', 333, '$2y$14$KwPj1Cfi1Pbr5ipJYoWtGeEtyb4KBVxUjY7DQVw3tu9zzsOlGcYVC', 'client', NULL),
('Patricia A', 317, '$2y$14$XoCfsULuRoXp2otR1t25euLC7EFjus./GCoY1E.ehys./Ev/EklrK', 'client', NULL),
('Patricia G', 358, '$2y$14$mc8mg4JU7H3/OtqV/DhIWeP2ZarTWPdjN7PcnhcpNZGUWBvdeD8JW', 'client', NULL),
('PAULA VARGAS', 227, '$2y$14$CRhMVgjC5miM3LOEyTob9O1l4YrMRW8ltA4/LEMQ9hX70dwxneWMe', 'client', NULL),
('PAULINA CABRERA', 228, '$2y$14$JujDfyvZEI.37NY0jW7pneA3J8VPvXkphi1ueved.sm5YGizZW7hC', 'client', NULL),
('Pedro H', 283, '$2y$14$uk4meq12G.DSpSsZqMLZo.8DDkosPgwbtrXiNt7UAzYsZGFkE5p4y', 'client', NULL),
('Pilar M', 281, NULL, 'client', NULL),
('PILAR QUIROZ', 229, '$2y$14$rLUBrK8kWUAc.ZoxqkBK9OH./EMgMs0nIMaYyrpRICYnPRTVFGRWS', 'client', NULL),
('Porfirio D', 296, '$2y$14$Op8XfF/.NwaVTbrZsB21/Ow7nmlW5rhTQmDVeOwFaHuzrQ2IIi3MW', 'client', NULL),
('RAFAEL CORRO', 230, '$2y$14$0veZBCvww7nNKmvVw3mS1uMMAYsUFkCXfvZzOYxrBh96vzI2x5pn6', 'client', NULL),
('RAFAEL COVARRUBIAS', 231, '$2y$14$kr3CcD7xSYXS4dX21gLOxuqy30yW98aH.dHwzZlfIYiyYNTb70TGu', 'client', NULL),
('Rafael J', 270, NULL, 'client', NULL),
('Ramiro M', 78, '$2y$14$WtrRTLJn12lz/uFdVb023egayemhH85azmOUiZSFwfdrQSoTH4gW6', 'client', NULL),
('RAMON CANTALAPIEDRA', 232, '$2y$14$./bb8N9v9XdIlwE4irVa/OETfIvdHvuK8Famu4pYSct4Jw4KaCVp.', 'client', NULL),
('Raul A', 369, '$2y$14$cZ2wgbMjsGLFHCB7B6F1x.cYAPIVdNzuWJ9JIFkRkU.UioC4Ic8ra', 'client', NULL),
('Raul B', 330, '$2y$14$.DeO3fa4lbGyXsUwymRCwOJPB/JdBKthjsZoyFt42An4v6QE0gnpe', 'client', NULL),
('RAUL RAMIREZ', 233, '$2y$14$zMHYh0TumdR5D3Jywh.H0eEQjylwnL749ZrRI0PbXTWiu6hFi2QDK', 'client', NULL),
('RAYMUNDO POMASKY', 234, '$2y$14$kfRPy.L/wUB1ylpbHYbys.W7.A.q7nshhaOV7vbzvssKJc95YnrhW', 'client', NULL),
('Rene G', 303, NULL, 'client', NULL),
('RENE JUAREZ', 235, '$2y$14$xDHMMQfZUkUuOOsJ84rzgOCFcg/o1elAoCTRDELOZ9pQBD9CxQm0K', 'client', NULL),
('RICARDO MIRANDA', 236, '$2y$14$G86GdNGsoirD2axfVrYHO.exr735/a6lzbILLpQ3C8gIzec3YMUzq', 'client', NULL),
('Ricardo P', 72, '$2y$14$gSa8HFxhCmdMp9TzFZ3Ymu04k7SW84kJR3W/rbccio8mSv7nLCt/O', 'client', NULL),
('Ricardo R', 25, '$2y$14$TRN50ph/S/ErQ4T/MximVe7Bsdyh1rs5e0reZW4Jp6rppxL1V4vJW', 'client', NULL),
('ROBERTO CASTILLO', 237, '$2y$14$wWA44X3Ga2cNjU42wtei6O9/Z6i6gm.JgdQYyx2p4knpHYjFuKVGa', 'client', NULL),
('ROBERTO CORVERA', 238, '$2y$14$8piIN3/C0r7KVN.KK6WxVuEaDV3FzloCC/rtc6f760w1wLoK6vREW', 'client', NULL),
('ROCIO DEL MAR', 239, '$2y$14$4cW7C0ag0BfIFEj.ITfTHuHkWIZWbBG4Jc.DSojOcnRH9OCOy29t2', 'client', NULL),
('Rodrigo I', 30, '$2y$14$eJaSQ6JGh.DgR5YRbafhyeoiQB/DPfLfTMP8SKXoI0FLjG/.H2AWG', 'client', NULL),
('RODRIGO MARTIN', 240, '$2y$14$ZucS4ZZo6/hEvtu/.mdBpuDyh./0a7dPasl6epSR4UlFP8p3wpY2.', 'client', NULL),
('ROMEO DAVID ORTIZ', 241, '$2y$14$DLwnw2V6BnpELKq0K7w4mu82NHYm2pXdvReiApgymsRuVwWAuggqy', 'client', NULL),
('ROSA MARIA BELMONT', 242, '$2y$14$eZtTsENuzfv4TIfOzerPNeKOKxMI.p/wE2kzHGBiX5iX.lhFoFg7K', 'client', NULL),
('Salvador B', 268, '$2y$14$YVojj0fWEgzawke2jTE65OTv5N.cNJgi2PnHVKx4e7EyVB2LPXbGa', 'client', NULL),
('SALVADOR JUAREZ', 243, '$2y$14$iZz/jjJfHADw2XcMjMGsUuFSSw7SOnsZ6BCFMHYHKuHpQWryaEVQe', 'client', NULL),
('Salvador R', 54, '$2y$14$ah5oji75VoPv5.QvrP2V9O1WnvlWc5m9XuhD6j6WFwAsmGslnvymO', 'client', NULL),
('Samuel C', 29, '$2y$14$EeuViK16UBYGcIh2L51thO.zR0eTYWZCl5Hpg3ailqQLhhvE./f8u', 'client', NULL),
('SAMUEL NIETO', 244, '$2y$14$3zl5qlwUjoR7fg.dzb9rU.zFC63rhbz3.LnxRRyVqcYaTy6kNVdyy', 'client', NULL),
('Sandra G', 302, '$2y$14$4k7vxZdXOj7nGAxvFGPhSuErNQQ0.Y2WCvmHYopP7WDRqT8Ujbw4u', 'client', NULL),
('Sandra M', 79, '$2y$14$/kAJ.nQs/ybngyfWrA/Nj.pTr4T0eJmoAt9fUiCyLzRwhKMMJDlFC', 'client', NULL),
('SANDRA NOELIA GARCIA', 245, '$2y$14$6ZMZ.fbKjLQ8.FK.XUXaQ.IA6r/dcBkKxhh1nEZ6gulqcgJJYoOEi', 'client', NULL),
('SERGIO FRAGOSO', 246, '$2y$14$tHjJzWRWho9sn3T8o4.d4eCvE1bO80OsH/EN1WI/Vh.nif9MXfRRi', 'client', NULL),
('SERGIO GUTIERREZ', 247, '$2y$14$Xv47MXmC55OlJXSjsC./T.TSUC1uDgmtBz1Lf4.gM7/hz.jzWGWNm', 'client', NULL),
('Sergio M', 308, '$2y$14$r1702H5iCuIla7isE3m8sOsYGpJw1Q5A57V8zP2wvx10kuDM6UvMG', 'client', NULL),
('Sergio MM', 372, '$2y$14$uboSOLDRGWvF02SAeE5b5uphzTwNO8fdUhXm3svXEnuYcPz4Tl4W6', 'client', NULL),
('Silvia P', 76, '$2y$14$yYRqIgp4k.gCNMN1aS9IB./36dJExf7m8j/nztDOK93MFu57xe18O', 'client', NULL),
('SOCORRO VELEZ', 248, '$2y$14$pIfoiNH/7rMkDZFuO7aafuVVLHH67cdvbvN.5fRDCw0Hn2NgWmL3S', 'client', NULL),
('Sofia N', 31, '$2y$14$ytqICMnw2ru6H2VvuQ5vNOMjWlywoVrVIXWA1fJIZ4qaLbZxNm55G', 'client', NULL),
('Susana P', 11, '$2y$14$EYBwMRdeYU5IpSjti9HrTuvUnjFhNPDpVlTGhgaXObSapKvOHhRvS', 'client', NULL),
('Teresa N', 322, '$2y$14$Yy4l3.hEeRnGoBV3UyEk6eYo3pb8qM2PFn8NuybjK4fBUiGkHtqz6', 'client', NULL),
('Teresa O', 58, '$2y$14$x4kyWNg0Vg6yXA55k6ZuCOrYABgH2xLQJ2.Xyva9QUEcN7HeoxIOu', 'client', NULL),
('TERESA SANCHEZ', 249, '$2y$14$7gjM8i2l342RowxZW6g8b.SHDt1iwpgXss6Pe06j2iZNBagCqtKWq', 'client', NULL),
('TOMAS MUNDO', 250, '$2y$14$/c1icH2Gao9XOBh57KtpLOAezLikVua4931JmYWIckWGY131.4XHm', 'client', NULL),
('Vanesa T', 288, '$2y$14$IdPZKiy71.VtPbiq9IdPcO8.e1YO/uwMlDwnQ1ok.irF5GbQF2LF6', 'client', NULL),
('VICENTE ALARCON', 251, '$2y$14$HKC63287L1O4iYYnjKKwSuWDIQCsyK39rkooQWojp9ZfFEDPZOnCi', 'client', NULL),
('VICTOR ADAN HDZ.', 252, '$2y$14$Tzc0SPAvko3RoOZRwPzTsemvZb8vryVRrdP/JxEk9lZPuBGw7tesK', 'client', NULL),
('VICTOR BORRI', 253, '$2y$14$YLEXetIuaLhwjkqb5oJ2Iu6QcYb59K9kwr.uOGALIqntS8qawSvkm', 'client', NULL),
('VICTOR JUAREZ', 254, '$2y$14$jxNZ7mYWXlgxQxNf4vu2POnmvd.ZZU3tPx6LGnhV3ftoqJ5MF6VI.', 'client', NULL),
('VICTOR MANUEL VILLA', 255, '$2y$14$gh6IxsY8MhlKGg1PwZHC8uO.gXn3ccATmzZU61ntdEGat35ejkpfa', 'client', NULL),
('VICTOR SUAREZ', 256, '$2y$14$VqaRt/meBsqgd/GSk9oc4umPqJdLKrwsVUc4kTrmRSr8Gia1MRIk2', 'client', NULL),
('VIVIANA PAOLA', 257, '$2y$14$AzNLINkxV2RPiytasWHmRO4RiQXh0ZUKCANIntwNY07jkcRGAZmpW', 'client', NULL),
('webmaster', 1, '$2y$14$9a6PT.SV3pkPja7y7r4xR.VkK1bGscwdqdCkQJ3WeM8Re/0.4xyue', 'webmaster', NULL),
('William G', 282, '$2y$14$aGjNBzIkdsNFoziBagrXreFT9S4Gnj5.tfJ.WEj1lvwuild1jS8Xq', 'client', NULL),
('YOZGARTH NAVA', 258, '$2y$14$YfnIWizvpaLHO3Abcya/z.mX5.wue5ucPTJxX6bGWfZlBB/MpANqe', 'client', NULL);

INSERT INTO `concept` (`concept_id`, `code`, `concept`, `type`) VALUES
(1, '101', 'Seguridad', 'O'),
(2, '102', 'Conserjería', 'O'),
(3, '103', 'Mantenimiento', 'O'),
(4, '104', 'Limpieza', 'O'),
(5, '105', 'Jardinería', 'O'),
(6, '106', 'Estacionamiento', 'O'),
(7, '107', 'Personal Administrativo', 'O'),
(8, '108', 'Uniformes del Personal', 'O'),
(9, '201', 'IMSS', 'O'),
(10, '202', 'Impuestos del Personal', 'O'),
(11, '203', 'Impuesto Predial', 'O'),
(12, '204', 'Otros Impuestos', 'O'),
(13, '205', 'Agua', 'O'),
(14, '206', 'Gas', 'O'),
(15, '207', 'Energía Eléctrica', 'O'),
(16, '208', 'Servicio Telefónico', 'O'),
(17, '209', 'Seguros Contra Riesgos', 'O'),
(18, '301', 'Papelería', 'O'),
(19, '302', 'Gastos Varios', 'O'),
(20, '303', 'Servicio de Cómputo, Administrativo', 'O'),
(21, '304', 'Honorarios Administrativos', 'O'),
(22, '305', 'Honorarios  a Terceros', 'O'),
(23, '306', 'Gastos y Honorarios Legales', 'O'),
(24, '307', 'Asociación de Colonos', 'O'),
(25, '401', 'Limpieza de Vidrios', 'O'),
(26, '402', 'Limpieza de Cisternas', 'O'),
(27, '403', 'Pulido de Pisos', 'O'),
(28, '404', 'Recolección de Basura', 'O'),
(29, '405', 'Fumigación', 'O'),
(30, '406', 'Limpieza de Albercas', 'O'),
(31, '501', 'Compra de Material Eléctrico', 'O'),
(32, '502', 'Material de Limpieza', 'O'),
(33, '503', 'Herramienta y Equipo', 'O'),
(34, '504', 'Material para Jardinería', 'O'),
(35, '505', 'Servicio de Electricidad y Plomería', 'O'),
(36, '506', 'Herrería, Carpintería y Cerrajería', 'O'),
(37, '507', 'Albañilería y Pintura', 'O'),
(38, '508', 'Impermeabilización', 'O'),
(39, '601', 'Elevadores y Escaleras eléctricas', 'O'),
(40, '602', 'Subestación', 'O'),
(41, '603', 'Planta de Emergencia', 'O'),
(42, '604', 'Equipo de Seguridad', 'O'),
(43, '605', 'Equipo de Bombeo', 'O'),
(44, '606', 'Equipo de Aire Acondicionado y Extracción', 'O'),
(45, '607', 'Antenas Parabólicas y de Cable', 'O'),
(46, '608', 'Equipo de Albercas', 'O'),
(47, '609', 'Equipo Computarizado de Control', 'O'),
(48, '610', 'Equipos Varios', 'O'),
(49, '611', 'Puertas Automáticas', 'O'),
(50, '612', 'Planta de Tratamiento', 'O'),
(51, '701', 'Egresos No Presupuestados', 'O'),
(52, '702', 'Incremento a Caja Chica', 'O'),
(53, '703', 'Publicidad y Promoción', 'O'),
(54, '704', 'Eventos Especiales', 'O'),
(55, '705', 'Traspaso al Fondo de Reserva', 'O'),
(56, '706', 'Cargos y Comisiones Bancos', 'O'),
(57, '707', 'Cargo por Ingreso Improcedente', 'O'),
(58, '708', 'Operación de Estacionamientos', 'O'),
(59, '709', 'Compra de Activos', 'O'),
(60, '710', 'Mejoras', 'O'),
(61, '801', 'Cuotas Condominales', 'I'),
(62, '802', 'Cuotas Extraordinarias', 'I'),
(63, '803', 'Renta de Áreas Comunes', 'I'),
(64, '804', 'Uso de Áreas Comunes', 'I'),
(65, '805', 'Intereses Moratorios', 'I'),
(66, '806', 'Intereses Fondo Común', 'I'),
(67, '807', 'Renta de Estacionamiento', 'I'),
(68, '808', 'Traspaso del Fondo de Reserva.', 'I'),
(69, '809', 'Venta de Activos', 'I'),
(70, '810', 'Indemnización por Daños', 'I'),
(71, '811', 'Depósitos en Garantía', 'I'),
(72, '812', 'Depósito Improcedente', 'I'),
(73, '901', 'Reembolso Teléfonos', 'I'),
(74, '902', 'Reembolso Consumo de Agua', 'I'),
(75, '903', 'Reembolso Consumo de Gas', 'I'),
(76, '904', 'Reembolsos por Consumo de Energía Eléctrica', 'I'),
(77, '905', 'Reembolsos por Servicios Diversos', 'I'),
(78, '906', 'Reembolsos por Gastos a Comprobar', 'I'),
(79, '907', 'Reembolsos de Nómina', 'I'),
(80, '813', 'Cuota condominal complementaria', 'I'),
(81, '814', 'Pago administracion anterior', 'I'),
(82, '908', 'Condonacion', 'O'),
(83, '613', 'Cerco electrico', 'O');

INSERT INTO `type` (`type_id`, `description`) VALUES
(1, 'Casa'),
(2, 'Lote'),
(3, 'Obra en construccion'),
(4, 'Casa vacia');

INSERT INTO `zone` (`zone_id`, `name`) VALUES
(1, 'Bellas Artes'),
(2, 'Residencial Fronda'),
(3, 'Lomas Cruz del Sur'),
(4, 'La Trinidad');

INSERT INTO `fee` (`fee_id`, `type_id`, `concept_id`, `zone_id`, `amount`) VALUES
(1, 1, 61, 1, 600.00),
(2, 1, 61, 2, 650.00),
(3, 2, 61, 2, 450.00),
(4, 3, 61, 2, 700.00),
(5, 1, 61, 3, 270.00),
(6, 1, 80, 3, 0.00),
(7, 1, 61, 4, 500.00),
(8, 2, 61, 4, 100.00),
(9, 3, 71, 4, 5000.00),
(10, 1, 80, 1, 0.00),
(11, 1, 81, 1, 0.00),
(12, 4, 61, 1, 300.00),
(13, 1, 61, 3, 200.00);

INSERT INTO `user_zone` (`username`, `zone_id`) VALUES
('committee', 1),
('juan_fco_monter', 1),
('juan_fco_monter', 2),
('juan_fco_monter', 3),
('jose.manuel.tello', 4);

INSERT INTO `client` (`client_id`, `profile_id`, `social_reason`, `phone`, `car`, `mobile`, `rfc`) VALUES
(1, 3, 'Mandrágora Web Systems', '222222222', '', '', ''),
(2, 8, '', '4030111', '', '92*12*45630', 'boge5408151j7'),
(3, 9, '', '', '', '', 'mosm6502015y0'),
(4, 10, '', '', '', '', ''),
(5, 11, '', '', '', '', ''),
(6, 12, '', '', '', '', ''),
(7, 13, '', '', '', '', ''),
(8, 14, '', '', '', '', ''),
(9, 15, '', '', '', '', ''),
(10, 16, '', '', '', '', ''),
(11, 17, '', '', '', '', ''),
(12, 18, '', '', '', '', ''),
(13, 19, '', '', '', '', ''),
(14, 20, '', '', '', '', ''),
(15, 21, '', '', '', '', ''),
(16, 22, '', '', '', '', ''),
(17, 23, '', '', '', '', ''),
(18, 24, '', '', '', '', ''),
(19, 25, '', '', '', '', ''),
(20, 26, '', '', '', '', ''),
(21, 27, '', '', '', '', ''),
(22, 28, '', '', '', '', ''),
(23, 29, '', '', '', '', ''),
(24, 30, '', '', '', '', ''),
(25, 31, '', '', '', '', ''),
(26, 32, '', '', '', '', ''),
(27, 33, '', '', '', '', ''),
(28, 34, '', '', '', '', ''),
(29, 35, '', '', '', '', ''),
(30, 36, '', '', '', '', ''),
(31, 37, '', '', '', '', ''),
(32, 38, '', '', '', '', ''),
(33, 39, '', '', '', '', ''),
(34, 40, '', '', '', '', ''),
(35, 41, '', '', '', '', ''),
(36, 42, '', '', '', '', ''),
(37, 43, '', '', '', '', ''),
(38, 44, '', '', '', '', ''),
(39, 45, '', '', '', '', ''),
(40, 46, '', '', '', '', ''),
(41, 47, '', '', '', '', ''),
(42, 48, '', '', '', '', ''),
(43, 49, '', '', '', '', ''),
(44, 50, '', '', '', '', ''),
(45, 51, '', '', '', '', ''),
(46, 52, '', '', '', '', ''),
(47, 53, '', '', '', '', ''),
(48, 54, '', '', '', '', ''),
(49, 55, '', '', '', '', ''),
(50, 56, '', '', '', '', ''),
(51, 57, '', '', '', '', ''),
(52, 58, '', '', '', '', ''),
(53, 59, '', '', '', '', ''),
(54, 60, '', '', '', '', ''),
(55, 61, '', '', '', '', ''),
(56, 62, '', '', '', '', ''),
(57, 63, '', '', '', '', ''),
(58, 64, '', '', '', '', ''),
(59, 65, '', '', '', '', ''),
(60, 66, '', '', '', '', ''),
(61, 67, '', '', '', '', ''),
(62, 68, '', '', '', '', ''),
(63, 69, '', '', '', '', ''),
(64, 70, '', '', '', '', ''),
(65, 71, '', '', '', '', ''),
(66, 72, '', '', '', '', ''),
(67, 73, '', '', '', '', ''),
(68, 74, '', '', '', '', ''),
(69, 75, '', '', '', '', ''),
(70, 76, '', '', '', '', ''),
(71, 77, '', '', '', '', ''),
(72, 78, '', '', '', '', ''),
(73, 79, '', '', '', '', ''),
(74, 80, '', '', '', '', ''),
(75, 81, '', '', '', '', ''),
(76, 82, '', '', '', '22-22-85-99-00', ''),
(77, 83, '', '', '', '', ''),
(78, 84, '', '', '', '22-23-52-70-18', ''),
(79, 85, '', '395-71-25', '', '', ''),
(80, 86, '', '', '', '22-27-62-92-77', ''),
(81, 87, '', '2-85-89-97', '', '', ''),
(82, 88, '', '', '', '22-27-62-94-59', ''),
(83, 89, '', '', '', '22-27-62-91-62', ''),
(84, 90, '', '4-66-03-33', '', '', ''),
(85, 91, '', '4-09-56-85', '', '', ''),
(86, 92, '', '', '', '22-22-48-03-10', ''),
(87, 93, '', '', '', '22-27-62-91-14', ''),
(88, 94, '', '5-90-21-69', '', '', ''),
(89, 95, '', '284-14-03', '', '', ''),
(90, 96, '', '', '', '22-22-85-98-24', ''),
(91, 97, '', '249-21-94', '', '22-23-56-94-53', ''),
(92, 98, '', '284-04-97', '', '', ''),
(93, 99, '', '2-85-96-12', '', '22-22-85-96-12', ''),
(94, 100, '', '2-84-13-34', '', '52-24-61-45-31', ''),
(95, 101, '', '6-16-92-39', '', '', ''),
(96, 102, '', '', '', '22-27-62-91-89', ''),
(97, 103, '', '', '', '22-27-62-91-89', ''),
(98, 104, '', '', '', '22-27-62-91-89', ''),
(99, 105, '', '7-62-94-62', '', '', ''),
(100, 106, '', '6-16-88-06', '', '', ''),
(101, 107, '', '', '', '22-22-85-99-35', ''),
(102, 108, '', '', '', '55-14-77-98-42', ''),
(103, 109, '', '', '', '22-21-61-49-24', ''),
(104, 110, '', '', '', '22-27-62-93-89', ''),
(105, 111, '', '1-69-52-71', '', '', ''),
(106, 112, '', '', '', '22-27-62-92-47', ''),
(107, 113, '', '', '', '22-22-84-05-72', ''),
(108, 114, '', '', '', '22-27-62-91-68', ''),
(109, 115, '', '', '', '22-27-62-94-31', ''),
(110, 116, '', '', '', '', ''),
(111, 117, '', '', '', '22-22-39-51-21', ''),
(112, 118, '', '', '', '22-22-85-98-55', ''),
(113, 119, '', '', '', '22-22-.84-23-26', ''),
(114, 120, '', '1-30-88-75', '', '', ''),
(115, 121, '', '616-13-49', '', '', ''),
(116, 122, '', '2-30-15-13', '', '', ''),
(117, 123, '', '466-83-10', '', '', ''),
(118, 124, '', '', '', '22-22-85-93-93', ''),
(119, 125, '', '', '', '22-22-84-04-66', ''),
(120, 126, '', '7-62-91-65', '', '', ''),
(121, 127, '', '2-85-98-50', '', '', ''),
(122, 128, '', '2-85-98-50', '', '', ''),
(123, 129, '', '', '', '22-27-62-93-70', ''),
(124, 130, '', '', '', '22-22-84-00-48', 'PACF581115HVA'),
(125, 131, '', '', '', '22-22-84-00-48', ''),
(126, 132, '', '', '', '22-22-84-00-48', 'PACF581115HVA'),
(127, 133, '', '', '', '22-22-85-91-17', ''),
(128, 134, '', '', '', '22-22-84-01-90', ''),
(129, 135, '', '2-11-42-57', '', '', ''),
(130, 136, '', '', '', '22-27-62-90-74', ''),
(131, 137, '', '', '', '22-27-62-90-51', ''),
(132, 138, '', '', '', '22-22-71-66-00', ''),
(133, 139, '', '', '', '22-22-84-17-77', ''),
(134, 140, '', '', '', '22-27-62-92-16', ''),
(135, 141, '', '', '', '', ''),
(136, 142, '', '', '', '22-27-62-91-24', ''),
(137, 143, '', '', '', '22-27-62-93-40', ''),
(138, 144, '', '2-84-16-71', '', '', ''),
(139, 145, '', '', '', '22-22-84-01-00', ''),
(140, 146, '', '', '', '22-22-84-01-40', ''),
(141, 147, '', '2-85-79-86', '', '', ''),
(142, 148, '', '', '', '22-22-84-03-73', ''),
(143, 149, '', '', '', '22-22-85-91-39', ''),
(144, 150, '', '', '', '22-22-85-98-49', ''),
(145, 151, '', '', '', '55-66-96-35-88', ''),
(146, 152, '', '2-84-03-53', '', '', ''),
(147, 153, '', '', '', '22-22-84-05-25', ''),
(148, 154, '', '4-03-67-14', '', '', ''),
(149, 155, '', '2-85-94-46', '', '', ''),
(150, 156, '', '', '', '22-21-02-75-57', ''),
(151, 157, '', '', '', '22-23-43-84-19', ''),
(152, 158, '', '', '', '22-27-62-91-28', ''),
(153, 159, '', '', '', '22-22-12-09-24', ''),
(154, 160, '', '1-22-73-05', '', '', ''),
(155, 161, '', '', '', '22-22-49-91-38', ''),
(156, 162, '', '', '', '22-22-85-97-55', ''),
(157, 163, '', '', '', '22-24-34-58-70', ''),
(158, 164, '', '', '', '22-22-26-63-38', ''),
(159, 165, '', '', '', '22-25-88-70-40', ''),
(160, 166, '', '', '', '22-27-62-92-93', ''),
(161, 167, '', '', '', '22-24-69-82-47', ''),
(162, 168, '', '', '', '22-27-62-91-43', ''),
(163, 169, '', '2-84-17-31', '', '', ''),
(164, 170, '', '2-42-35-81', '', '', ''),
(165, 171, '', '', '', '22-22-70-86-67', ''),
(166, 172, '', '', '', '22-27-62-93-13', ''),
(167, 173, '', '', '', '22-22-84-05-86', ''),
(168, 174, '', '', '', '22-27-62-91-65', ''),
(169, 175, '', '', '', '', ''),
(170, 176, '', '', '', '22-22-34-20-02', ''),
(171, 177, '', '7-62-91-74', '', '', ''),
(172, 178, '', '', '', '22-22-49-15-24', ''),
(173, 179, '', '', '', '', ''),
(174, 180, '', '1-69-50-83', '', '', ''),
(175, 181, '', '2-84-02-78', '', '', ''),
(176, 182, '', '', '', '22-27-62-91-61', ''),
(177, 183, '', '', '', '045-97-17-02-99-57', ''),
(178, 184, '', '', '', '22-27-62-93-12', ''),
(179, 185, '', '', '', '22-22-85-98-68', ''),
(180, 186, '', '', '', '22-27-62-90-22', ''),
(181, 187, '', '', '', '22-27-62-93-88', ''),
(182, 188, '', '', '', '22-22-84-25-64', ''),
(183, 189, '', '', '', '22-22-85-99-42', ''),
(184, 190, '', '2-84-02-09', '', '', ''),
(185, 191, '', '', '', '22-27-62-91-66', ''),
(186, 192, '', '2-84-03-25', '', '', ''),
(187, 193, '', '', '', '22-22-85-93-68', ''),
(188, 194, '', '', '', '22-27-62-90-96', ''),
(189, 195, '', '', '', '22-27-62-91-31', ''),
(190, 196, '', '', '', '22-22-85-95-32', ''),
(191, 197, '', '', '', '22-27-62-93-22', ''),
(192, 198, '', '', '', '22-27-62-91-92', ''),
(193, 199, '', '2-40-00-77', '', '', ''),
(194, 200, '', '', '', '22-22-84-18-82', ''),
(195, 201, '', '', '', '22-22-84-16-80', ''),
(196, 202, '', '', '', '22-27-62-92-20', ''),
(197, 203, '', '', '', '22-27-62-91-31', ''),
(198, 204, '', '', '', '22-21-69-77-38', ''),
(199, 205, '', '', '', '22-22-84-10-18', ''),
(200, 206, '', '', '', '22-22-84-00-77', ''),
(201, 207, '', '', '', '22-22-85-98-85', ''),
(202, 208, '', '', '', '22-27-62-90-79', ''),
(203, 209, '', '1-69-0037', '', '', ''),
(204, 210, '', '', '', '22-22-84-13-95', ''),
(205, 211, '', '', '', '22-27-62-92-64', ''),
(206, 212, '', '6-50-85-79', '', '', ''),
(207, 213, '', '2 84-26-18', '', '', ''),
(208, 214, '', '2-85-93-24', '', '', ''),
(209, 215, '', '', '', '', ''),
(210, 216, '', '', '', '22-27-62-90-01', ''),
(211, 217, '', '', '', '22-22-85-99-51', ''),
(212, 218, '', '', '', '', ''),
(213, 219, '', '4-13-87-59', '', '', ''),
(214, 220, '', '', '', '', ''),
(215, 221, '', '2-30-96-38', '', '', ''),
(216, 222, '', '', '', '22-22-85-95-75', ''),
(217, 223, '', '', '', '22-24-66-28-10', ''),
(218, 224, '', '', '', '22-27-62-90-08', ''),
(219, 225, '', '', '', '22-22-84-07-21', ''),
(220, 226, '', '2-84-29-57', '', '', ''),
(221, 227, '', '', '', '22-22-17-88-54', ''),
(222, 228, '', '', '', '22-21-77-97-93', ''),
(223, 229, '', '', '', '22-22-84-08-35', ''),
(224, 230, '', '', '', '22-22-84-28-92', ''),
(225, 231, '', '', '', '22-22-84-29-52', ''),
(226, 232, '', '2-25-05-59', '', '', ''),
(227, 233, '', '', '', '22-22-85-99-72', ''),
(228, 234, '', '5-82-85-67', '', '', ''),
(229, 235, '', '', '', '22-27-62-92-83', ''),
(230, 236, '', '', '', '22-22-85-99-12', ''),
(231, 237, '', '', '', '22-21-89-01-51', ''),
(232, 238, '', '', '', '22-22-38-30-71', ''),
(233, 239, '', '', '', '22-22-84-23-28', ''),
(234, 240, '', '', '', '22-21-92-10-62', ''),
(235, 241, '', '', '', '22-22-85-94-18', ''),
(236, 242, '', '2-16-07-82', '', '', ''),
(237, 243, '', '', '', '22-22-84-10-05', ''),
(238, 244, '', '7-50-71-95', '', '', ''),
(239, 245, '', '4-30-61-64 y 65', '', '', ''),
(240, 246, '', '', '', '22-22-84-01-89', ''),
(241, 247, '', '', '', '2-84-17-61', ''),
(242, 248, '', '', '', '22-22-43-35-08', ''),
(243, 249, '', '2-84-05-90', '', '', ''),
(244, 250, '', '6-15-86-69', '', '', ''),
(245, 251, '', '', '', '22-23-23-31-36', ''),
(246, 252, '', '2-85-97-35', '', '', ''),
(247, 253, '', '2-61-47-09', '', '', ''),
(248, 254, '', '', '', '22-27-62-90-11', ''),
(249, 255, '', '', '', '22-22-85-95-21', ''),
(250, 256, '', '6-50-09-54', '', '', ''),
(251, 257, '', '', '', '22-27-62-90-47', ''),
(252, 258, '', '2-49-68-88', '', '', ''),
(253, 259, '', '4-67-18-20', '', '', ''),
(254, 260, '', '7-62-94-62', '', '', ''),
(255, 261, '', '', '', '', ''),
(256, 262, '', '', '', '', ''),
(257, 263, '', '', '', '', ''),
(258, 264, '', '', '', '', ''),
(259, 265, '', '', '', '', ''),
(260, 266, '', '', '', '', ''),
(261, 267, '', '', '', '', ''),
(262, 268, '', '', '', '', ''),
(263, 269, '', '', '', '', ''),
(264, 270, '', '', '', '', ''),
(265, 271, '', '', '', '', ''),
(266, 272, '', '', '', '', ''),
(267, 273, '', '', '', '', ''),
(268, 274, '', '', '', '', ''),
(269, 275, '', '', '', '', ''),
(270, 276, '', '', '', '', ''),
(271, 277, '', '', '', '', ''),
(272, 278, '', '', '', '', ''),
(273, 279, '', '', '', '', ''),
(274, 280, '', '', '', '', ''),
(275, 281, '', '', '', '', ''),
(276, 282, '', '', '', '', ''),
(277, 283, '', '', '', '', ''),
(278, 284, '', '', '', '', ''),
(279, 285, '', '', '', '', ''),
(280, 286, '', '', '', '', ''),
(281, 287, '', '', '', '', ''),
(282, 288, '', '', '', '', ''),
(283, 289, '', '', '', '', ''),
(284, 290, '', '', '', '', ''),
(285, 291, '', '', '', '', ''),
(286, 292, '', '', '', '', ''),
(287, 293, '', '', '', '', ''),
(288, 294, '', '', '', '', ''),
(289, 295, '', '', '', '', ''),
(290, 296, '', '', '', '', ''),
(291, 297, '', '', '', '', ''),
(292, 298, '', '', '', '', ''),
(293, 299, '', '', '', '', ''),
(294, 300, '', '', '', '', ''),
(295, 301, '', '', '', '', ''),
(296, 302, '', '', '', '', ''),
(297, 303, '', '', '', '', ''),
(298, 304, '', '', '', '', ''),
(299, 305, '', '', '', '', ''),
(300, 306, '', '', '', '', ''),
(301, 307, '', '', '', '', ''),
(302, 308, '', '', '', '', ''),
(303, 309, '', '', '', '', ''),
(304, 310, '', '', '', '', ''),
(305, 311, '', '', '', '', ''),
(306, 312, '', '', '', '', ''),
(307, 313, '', '', '', '', ''),
(308, 314, '', '', '', '', ''),
(309, 315, '', '', '', '', ''),
(310, 316, '', '', '', '', ''),
(311, 317, '', '', '', '', ''),
(312, 318, '', '', '', '', ''),
(313, 319, '', '', '', '', ''),
(314, 320, '', '', '', '', ''),
(315, 321, '', '', '', '', ''),
(316, 322, '', '', '', '', ''),
(317, 323, '', '', '', '', ''),
(318, 324, '', '', '', '', ''),
(319, 325, '', '', '', '', ''),
(320, 326, '', '', '', '', ''),
(321, 327, '', '', '', '', ''),
(322, 328, '', '', '', '', ''),
(323, 329, '', '', '', '', ''),
(324, 330, '', '', '', '', ''),
(325, 331, '', '', '', '', ''),
(326, 332, '', '', '', '', ''),
(327, 333, '', '', '', '', ''),
(328, 334, '', '', '', '', ''),
(329, 335, '', '', '', '', ''),
(330, 336, '', '', '', '', ''),
(331, 337, '', '', '', '', ''),
(332, 338, '', '', '', '', ''),
(333, 339, '', '', '', '', ''),
(334, 340, '', '', '', '', ''),
(335, 341, '', '', '', '', ''),
(336, 342, '', '', '', '', ''),
(337, 343, '', '', '', '', ''),
(338, 344, '', '', '', '', ''),
(339, 345, '', '', '', '', ''),
(340, 346, '', '', '', '', ''),
(341, 347, '', '', '', '', ''),
(342, 348, '', '', '', '', ''),
(343, 349, '', '', '', '', ''),
(344, 350, '', '', '', '', ''),
(345, 351, '', '', '', '', ''),
(346, 352, '', '', '', '', ''),
(347, 353, '', '', '', '', ''),
(348, 354, '', '', '', '', ''),
(349, 355, '', '', '', '', ''),
(350, 356, '', '', '', '', ''),
(351, 357, '', '', '', '', ''),
(352, 358, '', '', '', '', ''),
(353, 359, '', '', '', '', ''),
(354, 360, '', '', '', '', ''),
(355, 361, '', '', '', '', ''),
(356, 362, '', '', '', '', ''),
(357, 363, '', '', '', '', ''),
(358, 364, '', '', '', '', ''),
(359, 365, '', '', '', '', ''),
(360, 366, '', '', '', '', ''),
(361, 367, '', '', '', '', ''),
(362, 368, '', '', '', '', ''),
(363, 369, '', '', '', '', ''),
(364, 370, '', '', '', '', ''),
(365, 371, '', '', '', '', ''),
(366, 372, '', '', '', '', ''),
(367, 373, '', '', '', '', ''),
(368, 374, '', '', '', '', ''),
(369, 375, '', '', '', '', ''),
(370, 376, '', '', '', '', ''),
(371, 377, '', '', '', '', ''),
(372, 378, '', '', '', '', ''),
(373, 379, '', '', '', '', ''),
(374, 380, '', '', '', '', ''),
(375, 381, '', '', '', '', ''),
(376, 382, '', '', '', '', ''),
(377, 383, '', '', '', '', ''),
(378, 384, '', '', '', '', ''),
(379, 385, '', '', '', '', ''),
(380, 386, '', '', '', '', ''),
(381, 387, '', '', '', '', ''),
(382, 388, '', '', '', '', '');

INSERT INTO `zone_expenses` (`zone_expenses_id`, `concept_id`, `zone_id`, `amount_paid`, `iva`, `date`, `date_record`) VALUES
(1, 5, 2, 2000.00, 0.00, '2012-11-02', '2014-05-30'),
(2, 18, 2, 700.00, 0.00, '2012-11-02', '2014-05-30'),
(3, 1, 2, 12400.00, 0.00, '2012-11-30', '2014-05-30'),
(4, 15, 2, 261.00, 0.00, '2012-12-13', '2014-05-31'),
(5, 15, 2, 2993.00, 0.00, '2012-12-18', '2014-05-31'),
(6, 21, 2, 3000.00, 0.00, '2012-12-18', '2014-05-31'),
(7, 1, 2, 12400.00, 0.00, '2012-12-21', '2014-05-31'),
(8, 1, 2, 6400.00, 0.00, '2013-01-15', '2014-05-31'),
(9, 1, 2, 6400.00, 0.00, '2013-01-31', '2014-08-02'),
(10, 5, 2, 2000.00, 0.00, '2013-01-31', '2014-08-02'),
(11, 15, 2, 7946.00, 0.00, '2013-01-31', '2014-08-02'),
(12, 15, 2, 473.00, 0.00, '2013-01-31', '2014-08-02'),
(13, 21, 2, 3000.00, 0.00, '2013-01-31', '2014-08-02'),
(14, 18, 2, 16.00, 0.00, '2013-02-05', '2014-08-02'),
(15, 21, 2, 4000.00, 0.00, '2013-02-15', '2014-08-02'),
(16, 18, 2, 27.20, 0.00, '2013-02-22', '2014-08-02'),
(17, 1, 2, 12400.00, 0.00, '2013-02-28', '2014-08-02'),
(18, 1, 2, 12400.00, 0.00, '2013-03-30', '2014-08-02'),
(19, 21, 2, 5000.00, 0.00, '2013-03-31', '2014-08-02'),
(20, 15, 2, 328.00, 0.00, '2013-03-31', '2014-08-02'),
(21, 15, 2, 7721.00, 0.00, '2013-03-31', '2014-08-02'),
(22, 5, 2, 2000.00, 0.00, '2013-04-06', '2014-08-02'),
(23, 83, 2, 2600.00, 0.00, '2013-04-09', '2014-08-02'),
(24, 1, 2, 6000.00, 0.00, '2013-04-15', '2014-08-02'),
(25, 1, 2, 6000.00, 0.00, '2013-04-30', '2014-08-02'),
(26, 21, 2, 5000.00, 0.00, '2013-04-30', '2014-08-02'),
(27, 1, 2, 6400.00, 0.00, '2013-05-15', '2014-08-02'),
(28, 1, 2, 6000.00, 0.00, '2013-05-31', '2014-08-02'),
(29, 21, 2, 5000.00, 0.00, '2013-05-31', '2014-08-02'),
(30, 15, 2, 275.00, 0.00, '2013-06-06', '2014-08-02'),
(31, 15, 2, 7496.00, 0.00, '2013-06-08', '2014-08-02'),
(32, 1, 2, 12000.00, 0.00, '2013-06-29', '2014-08-02'),
(33, 21, 2, 5000.00, 0.00, '2013-06-30', '2014-08-02'),
(34, 51, 2, 1885.00, 0.00, '2013-07-20', '2014-08-02'),
(35, 1, 2, 12000.00, 0.00, '2013-07-31', '2014-08-02'),
(36, 21, 2, 5000.00, 0.00, '2013-07-31', '2014-08-02'),
(37, 5, 2, 300.00, 0.00, '2013-08-01', '2014-08-02'),
(38, 18, 2, 100.00, 0.00, '2013-08-05', '2014-08-02'),
(39, 15, 2, 278.00, 0.00, '2008-08-05', '2014-08-02'),
(40, 15, 2, 7414.00, 0.00, '2013-08-07', '2014-08-02'),
(41, 83, 2, 4000.00, 0.00, '2013-08-12', '2014-08-02'),
(42, 1, 2, 12000.00, 0.00, '2013-08-16', '2014-08-02'),
(43, 5, 2, 1200.00, 0.00, '2013-08-20', '2014-08-02'),
(44, 21, 2, 5000.00, 0.00, '2013-08-31', '2014-08-02'),
(45, 18, 2, 120.00, 0.00, '2013-09-10', '2014-08-02'),
(46, 1, 2, 6000.00, 0.00, '2013-09-15', '2014-08-02'),
(47, 18, 2, 4.80, 0.00, '2013-09-24', '2014-08-02'),
(48, 18, 2, 31.90, 0.00, '2013-09-27', '2014-08-02'),
(49, 1, 2, 6400.00, 0.00, '2013-09-30', '2014-08-02'),
(50, 21, 2, 5000.00, 0.00, '2013-09-30', '2014-08-02'),
(51, 18, 2, 53.00, 0.00, '2013-10-03', '2014-08-02'),
(52, 15, 2, 6400.00, 0.00, '2013-10-05', '2014-08-02'),
(53, 15, 2, 322.00, 0.00, '2013-10-05', '2014-08-02'),
(54, 18, 2, 4.00, 0.00, '2013-10-07', '2014-08-02'),
(55, 5, 2, 2000.00, 0.00, '2013-10-09', '2014-08-02'),
(56, 29, 2, 3700.00, 0.00, '2013-10-14', '2014-08-02'),
(57, 1, 2, 6000.00, 0.00, '2013-10-15', '2014-08-02'),
(58, 1, 2, 6000.00, 0.00, '2013-10-31', '2014-08-02'),
(59, 21, 2, 5000.00, 0.00, '2013-10-31', '2014-08-02'),
(60, 19, 2, 450.00, 0.00, '2013-11-15', '2014-08-02'),
(61, 1, 2, 6000.00, 0.00, '2013-11-15', '2014-08-02'),
(62, 5, 2, 2000.00, 0.00, '2013-11-16', '2014-08-02'),
(63, 1, 2, 6400.00, 0.00, '2013-11-30', '2014-08-02'),
(64, 21, 2, 5000.00, 0.00, '2013-11-30', '2014-08-02'),
(65, 15, 2, 373.00, 0.00, '2013-12-02', '2014-08-02'),
(66, 15, 2, 7261.00, 0.00, '2013-12-02', '2014-08-02'),
(67, 19, 2, 90.00, 0.00, '2013-12-10', '2014-08-02'),
(68, 37, 2, 6624.00, 0.00, '2013-12-10', '2014-08-02'),
(69, 1, 2, 6000.00, 0.00, '2013-12-15', '2014-08-02'),
(70, 18, 2, 5.20, 0.00, '2013-12-16', '2014-08-02'),
(71, 5, 2, 2000.00, 0.00, '2013-12-17', '2014-08-02'),
(72, 32, 2, 248.86, 0.00, '2013-12-17', '2014-08-02'),
(73, 83, 2, 750.00, 0.00, '2013-12-23', '2014-08-02'),
(74, 31, 2, 549.49, 0.00, '2013-12-24', '2014-08-02'),
(75, 35, 2, 400.00, 0.00, '2013-12-24', '2014-08-02'),
(76, 1, 2, 6400.00, 0.00, '2013-12-31', '2014-08-02'),
(77, 21, 2, 5000.00, 0.00, '2013-12-31', '2014-08-02'),
(78, 32, 2, 152.88, 0.00, '2014-01-13', '2014-08-02'),
(79, 19, 2, 350.00, 0.00, '2014-01-14', '2014-08-02'),
(80, 19, 2, 12.00, 0.00, '2014-01-14', '2014-08-02'),
(81, 1, 2, 6400.00, 0.00, '2014-01-15', '2014-08-02'),
(82, 19, 2, 11.00, 0.00, '2014-01-15', '2014-08-02'),
(83, 18, 2, 49.40, 0.00, '2014-01-22', '2014-08-02'),
(84, 5, 2, 1400.00, 0.00, '2014-01-24', '2014-08-02'),
(85, 1, 2, 6000.00, 0.00, '2014-01-31', '2014-08-02'),
(86, 21, 2, 5000.00, 0.00, '2014-01-31', '2014-08-02'),
(87, 19, 2, 11.00, 0.00, '2014-02-06', '2014-08-02'),
(88, 15, 2, 350.00, 0.00, '2014-02-06', '2014-08-02'),
(89, 15, 2, 7227.00, 0.00, '2014-02-06', '2014-08-02'),
(90, 32, 2, 179.87, 0.00, '2014-02-10', '2014-08-02'),
(91, 18, 2, 12.80, 0.00, '2014-02-12', '2014-08-02'),
(92, 1, 2, 6400.00, 0.00, '2014-02-14', '2014-08-02'),
(93, 19, 2, 11.00, 0.00, '2014-02-20', '2014-08-02'),
(94, 32, 2, 34.00, 0.00, '2014-02-24', '2014-08-02'),
(95, 2, 2, 250.00, 0.00, '2014-02-24', '2014-08-02'),
(96, 5, 2, 1400.00, 0.00, '2014-02-26', '2014-08-02'),
(97, 1, 2, 6000.00, 0.00, '2014-02-28', '2014-08-02'),
(98, 21, 2, 5000.00, 0.00, '2014-02-28', '2014-08-02'),
(99, 18, 2, 12.00, 0.00, '2014-03-04', '2014-08-02'),
(100, 32, 2, 137.93, 0.00, '2014-03-11', '2014-08-02'),
(101, 2, 2, 250.00, 0.00, '2014-03-11', '2014-08-02'),
(102, 19, 2, 11.00, 0.00, '2014-03-12', '2014-08-02'),
(103, 1, 2, 6000.00, 0.00, '2014-03-15', '2014-08-02'),
(104, 19, 2, 11.00, 0.00, '2014-03-20', '2014-08-02'),
(105, 2, 2, 250.00, 0.00, '2014-03-26', '2014-08-02'),
(106, 35, 2, 350.00, 0.00, '2014-03-28', '2014-08-02'),
(107, 5, 2, 2000.00, 0.00, '2014-03-29', '2014-08-02'),
(108, 1, 2, 6400.00, 0.00, '2014-03-31', '2014-08-02'),
(109, 21, 2, 5000.00, 0.00, '2014-03-31', '2014-08-02'),
(110, 47, 2, 390.00, 0.00, '2014-04-03', '2014-08-02'),
(111, 31, 2, 174.51, 0.00, '2014-04-04', '2014-08-02'),
(112, 19, 2, 11.00, 0.00, '2014-04-04', '2014-08-02'),
(113, 48, 2, 3000.00, 0.00, '2014-04-05', '2014-08-02'),
(114, 31, 2, 75.01, 0.00, '2014-04-05', '2014-08-02'),
(115, 15, 2, 335.00, 0.00, '2014-04-06', '2014-08-02'),
(116, 15, 2, 6340.00, 0.00, '2014-04-06', '2014-08-02'),
(117, 32, 2, 254.73, 0.00, '2014-04-08', '2014-08-02'),
(118, 31, 2, 151.01, 0.00, '2014-04-08', '2014-08-02'),
(119, 47, 2, 1850.00, 0.00, '2014-04-10', '2014-08-02'),
(120, 2, 2, 250.00, 0.00, '2014-04-10', '2014-08-02'),
(121, 31, 2, 279.00, 0.00, '2014-04-10', '2014-08-02'),
(122, 18, 2, 13.60, 0.00, '2014-04-11', '2014-08-02'),
(123, 35, 2, 200.00, 0.00, '2014-04-12', '2014-08-02'),
(124, 1, 2, 6000.00, 0.00, '2014-04-15', '2014-08-02'),
(125, 5, 2, 250.00, 0.00, '2014-04-23', '2014-08-02'),
(126, 19, 2, 11.00, 0.00, '2014-04-25', '2014-08-02'),
(127, 1, 2, 6000.00, 0.00, '2014-04-30', '2014-08-02'),
(128, 21, 2, 5000.00, 0.00, '2014-04-30', '2014-08-02'),
(129, 5, 2, 1400.00, 0.00, '2014-05-02', '2014-08-02'),
(130, 2, 2, 250.00, 0.00, '2014-05-05', '2014-08-02'),
(131, 19, 2, 11.00, 0.00, '2014-05-09', '2014-08-02'),
(132, 32, 2, 10.00, 0.00, '2014-05-12', '2014-08-02'),
(133, 32, 2, 130.97, 0.00, '2014-05-12', '2014-08-02'),
(134, 18, 2, 48.90, 0.00, '2014-05-15', '2014-08-02'),
(135, 31, 2, 44.00, 0.00, '2014-05-15', '2014-08-02'),
(136, 32, 2, 33.00, 0.00, '2014-05-15', '2014-08-02'),
(137, 1, 2, 6500.00, 0.00, '2014-05-15', '2014-08-02'),
(138, 19, 2, 11.00, 0.00, '2014-05-21', '2014-08-02'),
(139, 2, 2, 250.00, 0.00, '2014-05-21', '2014-08-02'),
(140, 19, 2, 11.00, 0.00, '2014-05-28', '2014-08-02'),
(141, 1, 2, 6000.00, 0.00, '2014-05-31', '2014-08-02'),
(142, 21, 2, 5000.00, 0.00, '2014-05-31', '2014-08-02'),
(143, 29, 2, 1500.00, 0.00, '2014-06-01', '2014-08-02'),
(144, 32, 2, 58.80, 0.00, '2014-06-04', '2014-08-02'),
(145, 2, 2, 250.00, 0.00, '2014-06-04', '2014-08-02'),
(146, 15, 2, 404.00, 0.00, '2014-06-06', '2014-08-02'),
(147, 15, 2, 5369.00, 0.00, '2014-06-06', '2014-08-02'),
(148, 31, 2, 275.81, 0.00, '2014-06-09', '2014-08-02'),
(149, 35, 2, 200.00, 0.00, '2014-06-09', '2014-08-02'),
(150, 19, 2, 11.00, 0.00, '2014-06-12', '2014-08-02'),
(151, 31, 2, 271.38, 0.00, '2014-06-13', '2014-08-02'),
(152, 35, 2, 200.00, 0.00, '2014-06-13', '2014-08-02'),
(153, 1, 2, 6000.00, 0.00, '2014-06-15', '2014-08-02'),
(154, 5, 2, 1000.00, 0.00, '2014-06-15', '2014-08-02'),
(155, 32, 2, 177.90, 0.00, '2014-06-17', '2014-08-02'),
(156, 2, 2, 250.00, 0.00, '2014-06-18', '2014-08-02'),
(157, 19, 2, 11.00, 0.00, '2014-06-26', '2014-08-02'),
(158, 1, 2, 6000.00, 0.00, '2014-06-30', '2014-08-02'),
(159, 21, 2, 5000.00, 0.00, '2014-06-30', '2014-08-02'),
(160, 19, 2, 11.00, 0.00, '2014-07-03', '2014-08-02'),
(161, 2, 2, 250.00, 0.00, '2014-07-02', '2014-08-02'),
(162, 18, 2, 32.50, 0.00, '2014-07-10', '2014-08-02'),
(163, 31, 2, 250.62, 0.00, '2014-07-10', '2014-08-02'),
(164, 35, 2, 200.00, 0.00, '2014-07-11', '2014-08-02'),
(165, 5, 2, 2400.00, 0.00, '2014-07-14', '2014-08-02'),
(166, 1, 2, 6000.00, 0.00, '2014-07-15', '2014-08-02'),
(167, 19, 2, 11.00, 0.00, '2014-07-16', '2014-08-02'),
(168, 2, 2, 250.00, 0.00, '2014-07-17', '2014-08-02'),
(169, 32, 2, 194.89, 0.00, '2014-07-22', '2014-08-02'),
(170, 19, 2, 11.00, 0.00, '2014-07-29', '2014-08-02'),
(171, 2, 2, 250.00, 0.00, '2014-07-30', '2014-08-02'),
(172, 1, 2, 6000.00, 0.00, '2014-07-31', '2014-08-02'),
(173, 21, 2, 5000.00, 0.00, '2014-07-31', '2014-08-02'),
(174, 18, 3, 125.00, 0.00, '2013-02-04', '2014-08-02'),
(175, 1, 3, 6000.00, 0.00, '2013-02-15', '2014-08-02'),
(176, 18, 3, 375.00, 0.00, '2013-02-21', '2014-08-02'),
(177, 18, 3, 106.20, 0.00, '2013-02-22', '2014-08-02'),
(178, 18, 3, 27.20, 0.00, '2013-02-22', '2014-08-02'),
(179, 5, 3, 1275.00, 0.00, '2013-02-19', '2014-08-02'),
(180, 18, 3, 40.00, 0.00, '2013-02-25', '2014-08-02'),
(181, 1, 3, 6000.00, 0.00, '2013-02-28', '2014-08-02'),
(182, 35, 3, 759.23, 0.00, '2013-02-28', '2014-08-02'),
(183, 35, 3, 200.00, 0.00, '2013-02-28', '2014-08-02'),
(184, 18, 3, 25.00, 0.00, '2013-03-01', '2014-08-02'),
(185, 5, 3, 600.00, 0.00, '2013-03-04', '2014-08-02'),
(186, 19, 3, 102.00, 0.00, '2013-03-05', '2014-08-02'),
(187, 5, 3, 25.00, 0.00, '2013-03-05', '2014-08-02'),
(188, 5, 3, 109.01, 0.00, '2013-03-05', '2014-08-02'),
(189, 31, 3, 29.60, 0.00, '2013-03-05', '2014-08-02'),
(190, 5, 3, 97.00, 0.00, '2013-03-05', '2014-08-02'),
(191, 32, 3, 318.87, 0.00, '2013-03-05', '2014-08-02'),
(192, 5, 3, 55.77, 0.00, '2013-03-05', '2014-08-02'),
(193, 15, 3, 5801.00, 0.00, '2013-03-07', '2014-08-02'),
(194, 42, 3, 315.00, 0.00, '2013-03-07', '2014-08-02'),
(195, 35, 3, 123.53, 0.00, '2013-03-07', '2014-08-02'),
(196, 35, 3, 200.00, 0.00, '2013-03-07', '2014-08-02'),
(197, 5, 3, 625.00, 0.00, '2013-03-11', '2014-08-02'),
(198, 1, 3, 6000.00, 0.00, '2013-03-15', '2014-08-02'),
(199, 5, 3, 625.00, 0.00, '2013-03-16', '2014-08-02'),
(200, 5, 3, 625.00, 0.00, '2013-03-23', '2014-08-02'),
(201, 18, 3, 415.00, 0.00, '2013-03-26', '2014-08-02'),
(202, 22, 3, 2700.00, 0.00, '2013-03-27', '2014-08-02'),
(203, 19, 3, 11.00, 0.00, '2013-03-28', '2014-08-02'),
(204, 5, 3, 625.00, 0.00, '2013-03-30', '2014-08-02'),
(205, 1, 3, 6000.00, 0.00, '2013-03-30', '2014-08-02'),
(206, 21, 3, 8500.00, 0.00, '2013-03-31', '2014-08-02'),
(207, 37, 3, 1200.00, 0.00, '2013-04-04', '2014-08-02'),
(208, 15, 3, 4799.00, 0.00, '2013-04-05', '2014-08-02'),
(209, 5, 3, 1250.00, 0.00, '2013-04-06', '2014-08-02'),
(210, 5, 3, 625.00, 0.00, '2013-04-06', '2014-08-02'),
(211, 19, 3, 11.00, 0.00, '2013-04-08', '2014-08-02'),
(212, 22, 3, 2696.00, 0.00, '2013-04-09', '2014-08-02'),
(213, 5, 3, 24.00, 0.00, '2013-04-10', '2014-08-02'),
(214, 31, 3, 95.00, 0.00, '2013-04-10', '2014-08-02'),
(215, 5, 3, 700.00, 0.00, '2013-04-13', '2014-08-02'),
(216, 1, 3, 6000.00, 0.00, '2013-04-15', '2014-08-02'),
(217, 18, 3, 122.90, 0.00, '2013-04-16', '2014-08-02'),
(218, 19, 3, 100.00, 0.00, '2013-04-16', '2014-08-02'),
(219, 36, 3, 150.00, 0.00, '2013-04-16', '2014-08-02'),
(220, 32, 3, 100.00, 0.00, '2013-04-17', '2014-08-02'),
(221, 32, 3, 14.89, 0.00, '2013-04-17', '2014-08-02'),
(222, 32, 3, 24.50, 0.00, '2013-04-17', '2014-08-02'),
(223, 19, 3, 11.00, 0.00, '2013-04-19', '2014-08-02'),
(224, 5, 3, 700.00, 0.00, '2013-04-20', '2014-08-02'),
(225, 5, 3, 70.00, 0.00, '2013-04-27', '2014-08-02'),
(226, 5, 3, 49.00, 0.00, '2013-04-27', '2014-08-02'),
(227, 19, 3, 24.00, 0.00, '2013-04-29', '2014-08-02'),
(228, 18, 3, 250.00, 0.00, '2013-04-29', '2014-08-02'),
(229, 1, 3, 6250.00, 0.00, '2013-04-30', '2014-08-02'),
(230, 21, 3, 8500.00, 0.00, '2013-04-30', '2014-08-02'),
(231, 5, 3, 700.00, 0.00, '2013-05-04', '2014-08-02'),
(232, 5, 3, 700.00, 0.00, '2013-05-11', '2014-08-02'),
(233, 15, 3, 4861.00, 0.00, '2013-05-10', '2014-08-02'),
(234, 15, 3, 677.00, 0.00, '2013-05-10', '2014-08-02'),
(235, 5, 3, 31.00, 0.00, '2013-05-10', '2014-08-02'),
(236, 5, 3, 64.25, 0.00, '2013-05-10', '2014-08-02'),
(237, 19, 3, 12.00, 0.00, '2013-05-13', '2014-08-02'),
(238, 1, 3, 6250.00, 0.00, '2013-05-15', '2014-08-02'),
(239, 5, 3, 700.00, 0.00, '2013-05-18', '2014-08-02'),
(240, 19, 3, 12.00, 0.00, '2013-05-22', '2014-08-02'),
(241, 36, 3, 1750.00, 0.00, '2013-05-24', '2014-08-02'),
(242, 1, 3, 6250.00, 0.00, '2013-05-28', '2014-08-02'),
(243, 18, 3, 375.00, 0.00, '2013-05-28', '2014-08-02'),
(244, 5, 3, 2000.00, 0.00, '2013-05-29', '2014-08-02'),
(245, 13, 3, 239.50, 0.00, '2013-05-31', '2014-08-02'),
(246, 13, 3, 239.50, 0.00, '2013-05-31', '2014-08-02'),
(247, 21, 3, 8500.00, 0.00, '2013-05-31', '2014-08-02'),
(248, 36, 3, 1750.00, 0.00, '2013-06-01', '2014-08-02'),
(249, 5, 3, 2000.00, 0.00, '2013-06-01', '2014-08-02'),
(250, 32, 3, 140.00, 0.00, '2013-06-05', '2014-08-02'),
(251, 32, 3, 100.95, 0.00, '2013-06-05', '2014-08-02'),
(252, 15, 3, 5075.00, 0.00, '2013-06-06', '2014-08-02'),
(253, 19, 3, 12.00, 0.00, '2013-06-11', '2014-08-02'),
(254, 1, 3, 6250.00, 0.00, '2013-06-13', '2014-08-02'),
(255, 31, 3, 587.35, 0.00, '2013-06-15', '2014-08-02'),
(256, 35, 3, 400.00, 0.00, '2013-06-18', '2014-08-02'),
(257, 19, 3, 12.00, 0.00, '2013-06-20', '2014-08-02'),
(258, 19, 3, 58.00, 0.00, '2013-06-21', '2014-08-02'),
(259, 2, 3, 250.00, 0.00, '2013-06-21', '2014-08-02'),
(260, 31, 3, 49.00, 0.00, '2013-06-24', '2014-08-02'),
(261, 18, 3, 435.00, 0.00, '2013-06-25', '2014-08-02'),
(262, 1, 3, 6250.00, 0.00, '2013-06-27', '2014-08-02'),
(263, 2, 3, 250.00, 0.00, '2013-06-28', '2014-08-02'),
(264, 19, 3, 19.70, 0.00, '2013-06-28', '2014-08-02'),
(265, 21, 3, 8500.00, 0.00, '2013-06-30', '2014-08-02'),
(266, 2, 3, 250.00, 0.00, '2013-07-05', '2014-08-02'),
(267, 15, 3, 451.00, 0.00, '2013-07-09', '2014-08-02'),
(268, 15, 3, 4286.00, 0.00, '2013-07-09', '2014-08-02'),
(269, 5, 3, 4000.00, 0.00, '2013-07-10', '2014-08-02'),
(270, 2, 3, 250.00, 0.00, '2013-07-12', '2014-08-02'),
(271, 19, 3, 12.00, 0.00, '2013-07-12', '2014-08-02'),
(272, 31, 3, 50.34, 0.00, '2013-07-13', '2014-08-02'),
(273, 31, 3, 912.55, 0.00, '2013-07-13', '2014-08-02'),
(274, 35, 3, 500.00, 0.00, '2013-07-13', '2014-08-02'),
(275, 1, 3, 6250.00, 0.00, '2013-07-15', '2014-08-02'),
(276, 2, 3, 250.00, 0.00, '2013-07-19', '2014-08-02'),
(277, 31, 3, 238.89, 0.00, '2013-07-20', '2014-08-02'),
(278, 35, 3, 150.00, 0.00, '2013-07-20', '2014-08-02'),
(279, 19, 3, 12.00, 0.00, '2013-07-24', '2014-08-02'),
(280, 2, 3, 250.00, 0.00, '2013-07-26', '2014-08-02'),
(281, 18, 3, 375.00, 0.00, '2013-07-26', '2014-08-02'),
(282, 1, 3, 6250.00, 0.00, '2013-07-30', '2014-08-02'),
(283, 21, 3, 8500.00, 0.00, '2013-07-31', '2014-08-02'),
(284, 2, 3, 250.00, 0.00, '2013-08-02', '2014-08-02'),
(285, 32, 3, 150.00, 0.00, '2013-08-06', '2014-08-02'),
(286, 15, 3, 3295.00, 0.00, '2013-08-05', '2014-08-02'),
(287, 2, 3, 250.00, 0.00, '2013-08-09', '2014-08-02'),
(288, 19, 3, 24.00, 0.00, '2013-08-09', '2014-08-02'),
(289, 5, 3, 4000.00, 0.00, '2013-08-15', '2014-08-02'),
(290, 1, 3, 6250.00, 0.00, '2013-08-14', '2014-08-02'),
(291, 2, 3, 250.00, 0.00, '2013-08-16', '2014-08-02'),
(292, 19, 3, 12.00, 0.00, '2013-08-16', '2014-08-02'),
(293, 18, 3, 28.50, 0.00, '2013-08-19', '2014-08-02'),
(294, 31, 3, 466.00, 0.00, '2013-08-22', '2014-08-02'),
(295, 35, 3, 300.00, 0.00, '2013-08-22', '2014-08-02'),
(296, 18, 3, 375.00, 0.00, '2013-08-27', '2014-08-02'),
(297, 19, 3, 12.00, 0.00, '2013-08-28', '2014-08-02'),
(298, 1, 3, 6250.00, 0.00, '2013-08-31', '2014-08-02'),
(299, 21, 3, 8500.00, 0.00, '2013-08-31', '2014-08-02'),
(300, 51, 3, 1400.00, 0.00, '2013-08-28', '2014-08-02'),
(301, 32, 3, 136.92, 0.00, '2013-09-04', '2014-08-02'),
(302, 32, 3, 27.30, 0.00, '2013-09-04', '2014-08-02'),
(303, 18, 3, 39.90, 0.00, '2013-09-04', '2014-08-02'),
(304, 18, 3, 105.00, 0.00, '2013-09-06', '2014-08-02'),
(305, 2, 3, 250.00, 0.00, '2013-09-06', '2014-08-02'),
(306, 15, 3, 3252.00, 0.00, '2013-09-08', '2014-08-02'),
(307, 19, 3, 33.00, 0.00, '2013-09-10', '2014-08-02'),
(308, 5, 3, 4000.00, 0.00, '2013-09-13', '2014-08-02'),
(309, 1, 3, 6250.00, 0.00, '2013-09-14', '2014-08-02'),
(310, 15, 3, 462.00, 0.00, '2013-09-14', '2014-08-02'),
(311, 36, 3, 250.00, 0.00, '2013-09-14', '2014-08-02'),
(312, 35, 3, 195.00, 0.00, '2013-09-14', '2014-08-02'),
(313, 2, 3, 250.00, 0.00, '2013-09-20', '2014-08-02'),
(314, 19, 3, 24.00, 0.00, '2013-09-26', '2014-08-02'),
(315, 18, 3, 375.00, 0.00, '2013-09-27', '2014-08-02'),
(316, 18, 3, 31.90, 0.00, '2013-09-27', '2014-08-02'),
(317, 1, 3, 5805.00, 0.00, '2013-09-30', '2014-08-02'),
(318, 21, 3, 8500.00, 0.00, '2013-09-30', '2014-08-02'),
(319, 32, 3, 152.92, 0.00, '2013-10-03', '2014-08-02'),
(320, 2, 3, 250.00, 0.00, '2013-10-05', '2014-08-02'),
(321, 18, 3, 5.20, 0.00, '2013-10-07', '2014-08-02'),
(322, 19, 3, 20.00, 0.00, '2013-10-08', '2014-08-02'),
(323, 15, 3, 4329.00, 0.00, '2013-10-10', '2014-08-02'),
(324, 5, 3, 4000.00, 0.00, '2013-10-11', '2014-08-02'),
(325, 83, 3, 20000.00, 0.00, '2013-10-11', '2014-08-02'),
(326, 1, 3, 6250.00, 0.00, '2013-10-12', '2014-08-02'),
(327, 31, 3, 95.00, 0.00, '2013-10-15', '2014-08-02'),
(328, 32, 3, 54.50, 0.00, '2013-10-15', '2014-08-02'),
(329, 19, 3, 10.00, 0.00, '2013-10-17', '2014-08-02'),
(330, 2, 3, 250.00, 0.00, '2013-10-18', '2014-08-02'),
(331, 18, 3, 375.00, 0.00, '2013-10-28', '2014-08-02'),
(332, 1, 3, 6250.00, 0.00, '2013-10-29', '2014-08-02'),
(333, 19, 3, 20.00, 0.00, '2013-10-31', '2014-08-02'),
(334, 21, 3, 8500.00, 0.00, '2013-10-31', '2014-08-02'),
(335, 2, 3, 250.00, 0.00, '2013-11-02', '2014-08-02'),
(336, 32, 3, 181.88, 0.00, '2013-11-06', '2014-08-02'),
(337, 83, 3, 14500.00, 0.00, '2013-11-08', '2014-08-02'),
(338, 15, 3, 3202.00, 0.00, '2013-11-09', '2014-08-02'),
(339, 19, 3, 20.00, 0.00, '2013-11-12', '2014-08-02'),
(340, 5, 3, 4000.00, 0.00, '2013-11-13', '2014-08-02'),
(341, 1, 3, 6250.00, 0.00, '2013-11-13', '2014-08-02'),
(342, 35, 3, 200.00, 0.00, '2013-11-14', '2014-08-02'),
(343, 18, 3, 74.40, 0.00, '2013-11-15', '2014-08-02'),
(344, 2, 3, 250.00, 0.00, '2013-11-16', '2014-08-02'),
(345, 15, 3, 479.00, 0.00, '2013-11-16', '2014-08-02'),
(346, 19, 3, 400.00, 0.00, '2013-11-21', '2014-08-02'),
(347, 18, 3, 375.00, 0.00, '2013-11-26', '2014-08-02'),
(348, 83, 3, 600.00, 0.00, '2013-11-26', '2014-08-02'),
(349, 32, 3, 76.00, 0.00, '2013-11-27', '2014-08-02'),
(350, 18, 3, 15.00, 0.00, '2013-11-28', '2014-08-02'),
(351, 1, 3, 6250.00, 0.00, '2013-11-28', '2014-08-02'),
(352, 18, 3, 25.20, 0.00, '2013-11-29', '2014-08-02'),
(353, 2, 3, 250.00, 0.00, '2013-11-30', '2014-08-02'),
(354, 21, 3, 8500.00, 0.00, '2013-11-30', '2014-08-02'),
(355, 15, 3, 3734.00, 0.00, '2013-12-03', '2014-08-02'),
(356, 19, 3, 20.00, 0.00, '2013-12-10', '2014-08-02'),
(357, 5, 3, 4000.00, 0.00, '2013-12-11', '2014-08-02'),
(358, 2, 3, 250.00, 0.00, '2013-12-14', '2014-08-02'),
(359, 1, 3, 6000.00, 0.00, '2013-12-15', '2014-08-02'),
(360, 19, 3, 10.00, 0.00, '2013-12-17', '2014-08-02'),
(361, 32, 3, 236.83, 0.00, '2013-12-17', '2014-08-02'),
(362, 18, 3, 164.30, 0.00, '2013-12-19', '2014-08-02'),
(363, 31, 3, 239.59, 0.00, '2013-12-20', '2014-08-02'),
(364, 31, 3, 23.50, 0.00, '2013-12-23', '2014-08-02'),
(365, 31, 3, 50.00, 0.00, '2013-12-23', '2014-08-02'),
(366, 35, 3, 400.00, 0.00, '2013-12-23', '2014-08-02'),
(367, 31, 3, 549.49, 0.00, '2013-12-24', '2014-08-02'),
(368, 35, 3, 600.00, 0.00, '2013-12-24', '2014-08-02'),
(369, 18, 3, 375.00, 0.00, '2013-12-27', '2014-08-02'),
(370, 2, 3, 250.00, 0.00, '2013-12-28', '2014-08-02'),
(371, 1, 3, 6000.00, 0.00, '2013-12-31', '2014-08-02'),
(372, 21, 3, 8500.00, 0.00, '2013-12-31', '2014-08-02'),
(373, 19, 3, 22.00, 0.00, '2014-01-06', '2014-08-02'),
(374, 15, 3, 3481.00, 0.00, '2014-01-10', '2014-08-02'),
(375, 15, 3, 488.00, 0.00, '2014-01-10', '2014-08-02'),
(376, 5, 3, 4000.00, 0.00, '2014-01-11', '2014-08-02'),
(377, 2, 3, 250.00, 0.00, '2014-01-11', '2014-08-02'),
(378, 32, 3, 171.89, 0.00, '2013-01-13', '2014-08-02'),
(379, 1, 3, 6000.00, 0.00, '2014-01-13', '2014-08-02'),
(380, 19, 3, 11.00, 0.00, '2014-01-15', '2014-08-02'),
(381, 18, 3, 56.80, 0.00, '2014-01-22', '2014-08-02'),
(382, 18, 3, 375.00, 0.00, '2014-01-23', '2014-08-02'),
(383, 19, 3, 124.00, 0.00, '2014-01-23', '2014-08-02'),
(384, 19, 3, 15.00, 0.00, '2014-01-23', '2014-08-02'),
(385, 83, 3, 650.00, 0.00, '2014-01-24', '2014-08-02'),
(386, 2, 3, 250.00, 0.00, '2014-01-25', '2014-08-02'),
(387, 1, 3, 6000.00, 0.00, '2014-01-31', '2014-08-02'),
(388, 21, 3, 8500.00, 0.00, '2014-01-31', '2014-08-02'),
(389, 19, 3, 11.00, 0.00, '2014-02-06', '2014-08-02'),
(390, 15, 3, 3518.00, 0.00, '2014-02-06', '2014-08-02'),
(391, 32, 3, 46.01, 0.00, '2014-02-07', '2014-08-02'),
(392, 2, 3, 250.00, 0.00, '2014-02-08', '2014-08-02'),
(393, 32, 3, 227.21, 0.00, '2014-02-10', '2014-08-02'),
(394, 5, 3, 4000.00, 0.00, '2014-02-11', '2014-08-02'),
(395, 19, 3, 11.00, 0.00, '2014-02-13', '2014-08-02'),
(396, 1, 3, 6000.00, 0.00, '2014-02-14', '2014-08-02'),
(397, 19, 3, 55.00, 0.00, '2014-02-15', '2014-08-02'),
(398, 31, 3, 283.26, 0.00, '2014-02-19', '2014-08-02'),
(399, 35, 3, 200.00, 0.00, '2014-02-20', '2014-08-02'),
(400, 2, 3, 250.00, 0.00, '2014-02-22', '2014-08-02'),
(401, 36, 3, 200.00, 0.00, '2014-02-25', '2014-08-02'),
(402, 18, 3, 375.00, 0.00, '2014-02-26', '2014-08-02'),
(403, 31, 3, 283.26, 0.00, '2014-02-28', '2014-08-02'),
(404, 35, 3, 200.00, 0.00, '2014-02-28', '2014-08-02'),
(405, 1, 3, 6000.00, 0.00, '2014-02-28', '2014-08-02'),
(406, 21, 3, 8500.00, 0.00, '2014-02-28', '2014-08-02'),
(407, 18, 3, 3.60, 0.00, '2014-03-04', '2014-08-02'),
(408, 15, 3, 6465.52, 0.00, '2014-03-06', '2014-08-02'),
(409, 2, 3, 250.00, 0.00, '2014-03-08', '2014-08-02'),
(410, 15, 3, 962.00, 0.00, '2014-03-10', '2014-08-02'),
(411, 32, 3, 236.21, 0.00, '2014-03-11', '2014-08-02'),
(412, 5, 3, 4000.00, 0.00, '2014-03-12', '2014-08-02'),
(413, 19, 3, 11.00, 0.00, '2014-03-12', '2014-08-02'),
(414, 31, 3, 418.21, 0.00, '2014-03-13', '2014-08-02'),
(415, 35, 3, 300.00, 0.00, '2014-03-14', '2014-08-02'),
(416, 1, 3, 6000.00, 0.00, '2014-03-15', '2014-08-02'),
(417, 2, 3, 250.00, 0.00, '2014-03-22', '2014-08-02'),
(418, 18, 3, 375.00, 0.00, '2014-03-24', '2014-08-02'),
(419, 19, 3, 11.00, 0.00, '2014-03-25', '2014-08-02'),
(420, 31, 3, 279.00, 0.00, '2014-03-27', '2014-08-02'),
(421, 35, 3, 200.00, 0.00, '2014-03-27', '2014-08-02'),
(422, 1, 3, 6000.00, 0.00, '2014-03-31', '2014-08-02'),
(423, 21, 3, 8500.00, 0.00, '2014-03-31', '2014-08-02'),
(424, 19, 3, 11.00, 0.00, '2014-04-01', '2014-08-02'),
(425, 47, 3, 3900.00, 0.00, '2014-04-04', '2014-08-02'),
(426, 31, 3, 155.95, 0.00, '2014-04-04', '2014-08-02'),
(427, 48, 3, 3000.00, 0.00, '2014-04-05', '2014-08-02'),
(428, 31, 3, 75.01, 0.00, '2014-04-05', '2014-08-02'),
(429, 2, 3, 250.00, 0.00, '2014-04-05', '2014-08-02'),
(430, 31, 3, 92.00, 0.00, '2014-04-08', '2014-08-02'),
(431, 32, 3, 27.00, 0.00, '2014-04-08', '2014-08-02'),
(432, 32, 3, 237.74, 0.00, '2014-04-08', '2014-08-02'),
(433, 13, 3, 933.85, 0.00, '2014-04-08', '2014-08-02'),
(434, 13, 3, 933.85, 0.00, '2014-04-08', '2014-08-02'),
(435, 47, 3, 1850.00, 0.00, '2014-04-10', '2014-08-02'),
(436, 31, 3, 279.00, 0.00, '2014-04-10', '2014-08-02'),
(437, 35, 3, 200.00, 0.00, '2014-04-11', '2014-08-02'),
(438, 18, 3, 2.40, 0.00, '2014-04-11', '2014-08-02'),
(439, 5, 3, 4000.00, 0.00, '2014-04-12', '2014-08-02'),
(440, 1, 3, 6000.00, 0.00, '2014-04-14', '2014-08-02'),
(441, 83, 3, 600.00, 0.00, '2014-04-18', '2014-08-02'),
(442, 31, 3, 4.00, 0.00, '2014-04-18', '2014-08-02'),
(443, 31, 3, 1510.46, 0.00, '2014-04-18', '2014-08-02'),
(444, 83, 3, 1250.00, 0.00, '2014-04-21', '2014-08-02'),
(445, 83, 3, 549.00, 0.00, '2014-04-22', '2014-08-02'),
(446, 83, 3, 400.00, 0.00, '2014-04-22', '2014-08-02'),
(447, 2, 3, 250.00, 0.00, '2014-04-25', '2014-08-02'),
(448, 19, 3, 11.00, 0.00, '2014-04-25', '2014-08-02'),
(449, 1, 3, 6000.00, 0.00, '2014-04-28', '2014-08-02'),
(450, 18, 3, 375.00, 0.00, '2014-04-29', '2014-08-02'),
(451, 16, 3, 100.00, 0.00, '2014-04-30', '2014-08-02'),
(452, 21, 3, 8500.00, 0.00, '2014-04-30', '2014-08-02'),
(453, 15, 3, 9960.00, 0.00, '2014-05-06', '2014-08-02'),
(454, 19, 3, 11.00, 0.00, '2014-05-09', '2014-08-02'),
(455, 2, 3, 250.00, 0.00, '2014-05-10', '2014-08-02'),
(456, 5, 3, 43.00, 0.00, '2014-05-10', '2014-08-02'),
(457, 32, 3, 120.50, 0.00, '2014-05-10', '2014-08-02'),
(458, 32, 3, 132.91, 0.00, '2014-05-12', '2014-08-02'),
(459, 1, 3, 2700.00, 0.00, '2014-05-14', '2014-08-02'),
(460, 1, 3, 6000.00, 0.00, '2014-05-14', '2014-08-02'),
(461, 18, 3, 90.70, 0.00, '2014-05-15', '2014-08-02'),
(462, 5, 3, 4000.00, 0.00, '2014-05-17', '2014-08-02'),
(463, 15, 2, 575.00, 0.00, '2014-05-18', '2014-08-02'),
(464, 19, 3, 11.00, 0.00, '2014-05-21', '2014-08-02'),
(465, 2, 3, 250.00, 0.00, '2014-05-24', '2014-08-02'),
(466, 18, 3, 375.00, 0.00, '2014-05-27', '2014-08-02'),
(467, 19, 3, 11.00, 0.00, '2014-05-28', '2014-08-02'),
(468, 1, 3, 6000.00, 0.00, '2014-05-31', '2014-08-02'),
(469, 21, 3, 8500.00, 0.00, '2014-05-31', '2014-08-02'),
(470, 13, 3, 165.30, 0.00, '2014-06-05', '2014-08-02'),
(471, 13, 3, 165.30, 0.00, '2014-06-05', '2014-08-02'),
(472, 2, 3, 250.00, 0.00, '2014-06-07', '2014-08-02'),
(473, 15, 3, 2616.00, 0.00, '2014-06-09', '2014-08-02'),
(474, 19, 3, 11.00, 0.00, '2014-06-12', '2014-08-02'),
(475, 5, 3, 4000.00, 0.00, '2014-06-12', '2014-08-02'),
(476, 1, 3, 6000.00, 0.00, '2014-06-16', '2014-08-02'),
(477, 32, 3, 223.86, 0.00, '2014-06-17', '2014-08-02'),
(478, 16, 3, 100.00, 0.00, '2014-06-19', '2014-08-02'),
(479, 2, 3, 250.00, 0.00, '2014-06-21', '2014-08-02'),
(480, 19, 3, 11.00, 0.00, '2014-06-26', '2014-08-02'),
(481, 31, 3, 250.62, 0.00, '2014-06-26', '2014-08-02'),
(482, 31, 3, 250.62, 0.00, '2014-06-26', '2014-08-02'),
(483, 31, 3, 250.62, 0.00, '2014-06-26', '2014-08-02'),
(484, 35, 3, 600.00, 0.00, '2014-06-27', '2014-08-02'),
(485, 18, 3, 375.00, 0.00, '2014-06-27', '2014-08-02'),
(486, 1, 3, 6000.00, 0.00, '2014-06-30', '2014-08-02'),
(487, 21, 3, 8500.00, 0.00, '2014-06-30', '2014-08-02'),
(488, 19, 3, 11.00, 0.00, '2014-07-03', '2014-08-02'),
(489, 2, 3, 250.00, 0.00, '2014-07-03', '2014-08-02'),
(490, 15, 3, 2560.00, 0.00, '2014-07-07', '2014-08-02'),
(491, 18, 3, 32.50, 0.00, '2014-07-10', '2014-08-02'),
(492, 5, 3, 4000.00, 0.00, '2014-07-11', '2014-08-02'),
(493, 15, 3, 588.00, 0.00, '2014-07-14', '2014-08-02'),
(494, 1, 3, 6000.00, 0.00, '2014-07-15', '2014-08-02'),
(495, 1, 3, 6000.00, 0.00, '2014-07-15', '2014-08-02'),
(496, 19, 3, 11.00, 0.00, '2014-07-16', '2014-08-02'),
(497, 2, 3, 250.00, 0.00, '2014-07-17', '2014-08-02'),
(498, 83, 3, 350.00, 0.00, '2014-07-17', '2014-08-02'),
(499, 32, 3, 217.89, 0.00, '2014-07-22', '2014-08-02'),
(500, 19, 3, 11.00, 0.00, '2014-07-29', '2014-08-02'),
(501, 18, 3, 375.00, 0.00, '2014-07-29', '2014-08-02'),
(502, 2, 3, 250.00, 0.00, '2014-07-31', '2014-08-02'),
(503, 1, 3, 6000.00, 0.00, '2014-07-31', '2014-08-02'),
(504, 21, 3, 8500.00, 0.00, '2014-07-31', '2014-08-02');

INSERT INTO `property` (`property_id`, `type_id`, `zone_id`, `client_id`, `street`, `number`, `internal_number`, `block`, `lot`, `opening_balance`, `balance_date`) VALUES
(2, 1, 1, 2, 'Casa', '01', '', '', '', 0.00, '0000-00-00'),
(3, 1, 1, 3, 'Casa', '02', '', '', '', 0.00, '0000-00-00'),
(4, 1, 1, 4, 'Casa', '03', '', '', '', 0.00, '0000-00-00'),
(5, 1, 1, 5, 'Casa', '04', '', '', '', 0.00, '0000-00-00'),
(6, 1, 1, 6, 'Casa', '05', '', '', '', 0.00, '0000-00-00'),
(7, 1, 1, 7, 'Casa', '06', '', '', '', 0.00, '0000-00-00'),
(8, 1, 1, 8, 'Casa', '07', '', '', '', 0.00, '0000-00-00'),
(9, 1, 1, 9, 'Casa', '08', '', '', '', 0.00, '0000-00-00'),
(10, 3, 1, 10, 'Casa', '09', '', '', '', 0.00, '0000-00-00'),
(11, 1, 1, 10, 'Casa', '10', '', '', '', 0.00, '0000-00-00'),
(12, 2, 1, 11, 'Lote', '11', '', '', '', 0.00, '0000-00-00'),
(13, 1, 1, 12, 'Casa', '12', '', '', '', 0.00, '0000-00-00'),
(14, 1, 1, 13, 'Casa', '13', '', '', '', 0.00, '0000-00-00'),
(15, 1, 1, 14, 'Casa', '14', '', '', '', 0.00, '0000-00-00'),
(16, 1, 1, 15, 'Casa', '15', '', '', '', 0.00, '0000-00-00'),
(17, 1, 1, 16, 'Casa', '16 - 17', '', '', '', 0.00, '0000-00-00'),
(18, 2, 1, 17, 'Lote', '18', '', '', '', 0.00, '0000-00-00'),
(19, 2, 1, 17, 'Lote', '19', '', '', '', 0.00, '0000-00-00'),
(20, 2, 1, 18, 'Lote', '20', '', '', '', 0.00, '0000-00-00'),
(21, 1, 1, 19, 'Casa', '21', '', '', '', 0.00, '0000-00-00'),
(22, 1, 1, 20, 'Casa', '22 -23 -24', '', '', '', 0.00, '0000-00-00'),
(23, 1, 1, 21, 'Casa', '25', '', '', '', 0.00, '0000-00-00'),
(24, 1, 1, 22, 'Casa', '26', '', '', '', 0.00, '0000-00-00'),
(25, 1, 1, 23, 'Casa', '27', '', '', '', 0.00, '0000-00-00'),
(26, 1, 1, 24, 'Casa', '29', '', '', '', 0.00, '0000-00-00'),
(27, 1, 1, 25, 'Casa', '30', '', '', '', 0.00, '0000-00-00'),
(29, 1, 1, 26, 'Casa', '31', '', '', '', 0.00, '0000-00-00'),
(30, 1, 1, 27, 'Casa', '32', '', '', '', 0.00, '0000-00-00'),
(31, 1, 1, 28, 'Casa', '33 - 34', '', '', '', 0.00, '0000-00-00'),
(32, 1, 1, 29, 'Casa', '35', '', '', '', 0.00, '0000-00-00'),
(33, 1, 1, 30, 'Casa', '36', '', '', '', 0.00, '0000-00-00'),
(34, 1, 1, 31, 'Casa', '37', '', '', '', 0.00, '0000-00-00'),
(35, 1, 1, 32, 'Casa', '38', '', '', '', 0.00, '0000-00-00'),
(36, 1, 1, 33, 'Casa', '39', '', '', '', 0.00, '0000-00-00'),
(37, 1, 1, 34, 'Casa', '40', '', '', '', 0.00, '0000-00-00'),
(38, 1, 1, 35, 'Casa', '41', '', '', '', 0.00, '0000-00-00'),
(39, 1, 1, 36, 'Casa', '42', '', '', '', 0.00, '0000-00-00'),
(40, 1, 1, 37, 'Casa', '43', '', '', '', 0.00, '0000-00-00'),
(41, 1, 1, 38, 'Casa', '44', '', '', '', 0.00, '0000-00-00'),
(42, 1, 1, 39, 'Casa', '45', '', '', '', 0.00, '0000-00-00'),
(43, 2, 1, 18, 'Lote', '46', '', '', '', 0.00, '0000-00-00'),
(44, 1, 2, 40, 'Fresno', '01', '', '', '', 0.00, '0000-00-00'),
(45, 1, 2, 41, 'Fresno', '02', '', '', '', 0.00, '0000-00-00'),
(46, 1, 2, 42, 'Fresno', '03', '', '', '', 0.00, '0000-00-00'),
(47, 1, 2, 43, 'Fresno', '04', '', '', '', 0.00, '0000-00-00'),
(48, 1, 2, 44, 'Fresno', '05', '', '', '', 0.00, '0000-00-00'),
(49, 1, 2, 45, 'Roble', '06', '', '', '', 0.00, '0000-00-00'),
(50, 1, 2, 46, 'Roble', '07', '', '', '', 0.00, '0000-00-00'),
(51, 2, 2, 47, 'Roble', '08', '', '', '', 0.00, '0000-00-00'),
(52, 2, 2, 47, 'Roble', '09', '', '', '', 0.00, '0000-00-00'),
(53, 2, 2, 48, 'Roble', '10', '', '', '', 0.00, '0000-00-00'),
(54, 1, 2, 48, 'Roble', '11', '', '', '', 0.00, '0000-00-00'),
(55, 1, 2, 49, 'Roble', '12', '', '', '', 0.00, '0000-00-00'),
(56, 1, 2, 50, 'Roble', '13', '', '', '', 0.00, '0000-00-00'),
(58, 1, 2, 51, 'Roble', '14', '', '', '', 0.00, '0000-00-00'),
(59, 2, 2, 52, 'Roble', '15', '', '', '', 0.00, '0000-00-00'),
(60, 1, 2, 53, 'Roble', '16', '', '', '', 0.00, '0000-00-00'),
(61, 1, 2, 54, 'Roble', '17', '', '', '', 0.00, '0000-00-00'),
(62, 1, 2, 55, 'Abedul', '18', '', '', '', 0.00, '0000-00-00'),
(63, 1, 2, 56, 'Abedul', '19', '', '', '', 0.00, '0000-00-00'),
(64, 1, 2, 57, 'Abedul', '20', '', '', '', 0.00, '0000-00-00'),
(65, 2, 2, 58, 'Abedul', '21', '', '', '', 0.00, '0000-00-00'),
(66, 1, 2, 59, 'Abedul', '22', '', '', '', 0.00, '0000-00-00'),
(67, 1, 2, 60, 'Abedul', '23', '', '', '', 0.00, '0000-00-00'),
(68, 1, 2, 61, 'Abedul', '24', '', '', '', 0.00, '0000-00-00'),
(69, 2, 2, 62, 'Abedul', '25', '', '', '', 0.00, '0000-00-00'),
(70, 2, 2, 62, 'Abedul', '26', '', '', '', 0.00, '0000-00-00'),
(71, 2, 2, 62, 'Abedul', '27', '', '', '', 0.00, '0000-00-00'),
(72, 1, 2, 63, 'Abedul', '28', '', '', '', 0.00, '0000-00-00'),
(73, 1, 2, 64, 'Abedul', '29', '', '', '', 0.00, '0000-00-00'),
(74, 1, 2, 65, 'Abedul', '30', '', '', '', 0.00, '0000-00-00'),
(75, 1, 2, 66, 'Abedul', '31', '', '', '', 0.00, '0000-00-00'),
(76, 3, 2, 67, 'Ceiba', '32', '', '', '', 0.00, '0000-00-00'),
(77, 1, 2, 68, 'Ceiba', '33', '', '', '', 0.00, '0000-00-00'),
(78, 1, 2, 69, 'Ceiba', '34', '', '', '', 0.00, '0000-00-00'),
(79, 2, 2, 70, 'Ceiba', '35', '', '', '', 0.00, '0000-00-00'),
(80, 1, 2, 71, 'Ceiba', '36', '', '', '', 0.00, '0000-00-00'),
(81, 1, 2, 72, 'Ceiba', '37', '', '', '', 0.00, '0000-00-00'),
(82, 1, 2, 73, 'Ceiba', '38', '', '', '', 0.00, '0000-00-00'),
(83, 1, 2, 74, 'Ceiba', '39', '', '', '', 0.00, '0000-00-00'),
(84, 1, 2, 75, 'Ceiba', '40', '', '', '', 0.00, '0000-00-00'),
(85, 2, 2, 75, 'Ceiba', '41', '', '', '', 0.00, '0000-00-00'),
(86, 2, 2, 75, 'Ceiba', '42', '', '', '', 0.00, '0000-00-00'),
(87, 1, 4, 76, 'SAN ANTONIO', '15', '', '', '', 0.00, '0000-00-00'),
(88, 1, 4, 77, 'SAN ANTONIO', '53-A', '', '', '', 0.00, '0000-00-00'),
(89, 1, 4, 78, 'SAN LUIS', '60', '', '', '', 0.00, '0000-00-00'),
(90, 1, 4, 79, 'SAN ANDRES', '4', '', '', '', 0.00, '0000-00-00'),
(91, 1, 4, 80, 'SAN ANDRES', '18', '', '', '', 0.00, '0000-00-00'),
(92, 1, 4, 81, 'SAN MATEO', '11', '', '', '', 0.00, '0000-00-00'),
(93, 1, 4, 82, 'SAN LUCAS', '6', '', '', '', 0.00, '0000-00-00'),
(94, 1, 4, 83, 'SAN JUAN', '13', '', '', '', 0.00, '0000-00-00'),
(95, 1, 4, 84, 'BOULEVARD LA TRINIDAD', '95', '', '', '', 0.00, '0000-00-00'),
(96, 1, 4, 85, 'SAN ANTONIO', '61', '', '', '', 0.00, '0000-00-00'),
(97, 1, 4, 86, 'SAN ANTONIO', '57', '', '', '', 0.00, '0000-00-00'),
(98, 1, 4, 87, 'SAN JOSÉ', '86-1', '', '', '', 0.00, '0000-00-00'),
(99, 1, 4, 88, 'BOULEVARD LA TRINIDAD', '32', '', '', '', 0.00, '0000-00-00'),
(100, 1, 4, 89, 'SAN LUIS', '46', '', '', '', 0.00, '0000-00-00'),
(101, 1, 4, 90, 'SAN ANTONIO', '51', '', '', '', 0.00, '0000-00-00'),
(102, 1, 4, 91, 'SAN ANTONIO', '59', '', '', '', 0.00, '0000-00-00'),
(103, 1, 4, 92, 'CERRADA LA TRINIDAD', '3', '', '', '', 0.00, '0000-00-00'),
(104, 1, 4, 93, 'BOULEVARD LA TRINIDAD', '13', '', '', '', 0.00, '0000-00-00'),
(105, 1, 4, 94, 'BOULEVARD LA TRINIDAD', '37-A', '', '', '', 0.00, '0000-00-00'),
(106, 1, 4, 95, 'SAN ANTONIO', '13', '', '', '', 0.00, '0000-00-00'),
(107, 1, 4, 97, 'SAN JUAN', '14', '', '', '', 0.00, '0000-00-00'),
(108, 1, 4, 98, 'BOULEVARD LA TRINIDAD', '13-A', '', '', '', 0.00, '0000-00-00'),
(109, 1, 4, 99, 'CERRADA LA TRINIDAD', '13', '', '', '', 0.00, '0000-00-00'),
(110, 1, 4, 100, 'SAN FELIPE', '9', '', '', '', 0.00, '0000-00-00'),
(111, 1, 4, 101, 'BOULEVARD LA TRINIDAD', '22', '', '', '', 0.00, '0000-00-00'),
(112, 1, 4, 102, 'SAN PEDRO', '8', '', '', '', 0.00, '0000-00-00'),
(113, 1, 4, 103, 'CERRADA LA TRINIDAD', '8', '', '', '', 0.00, '0000-00-00'),
(114, 1, 4, 104, 'CERRADA LA TRINIDAD', '2', '', '', '', 0.00, '0000-00-00'),
(115, 1, 4, 105, 'BOULEVARD LA TRINIDAD', '16', '', '', '', 0.00, '0000-00-00'),
(116, 1, 4, 106, 'SAN FELIPE', '11', '', '', '', 0.00, '0000-00-00'),
(117, 1, 4, 107, 'SAN ANTONIO', '18', '', '', '', 0.00, '0000-00-00'),
(118, 1, 4, 108, 'SAN ANDRES', '9', '', '', '', 0.00, '0000-00-00'),
(119, 1, 4, 109, 'SAN JUAN', '7', '', '', '', 0.00, '0000-00-00'),
(120, 1, 4, 110, 'CERRADA LA TRINIDAD', '11', '', '', '', 0.00, '0000-00-00'),
(121, 1, 4, 111, 'SAN ANTONIO', '2', '', '', '', 0.00, '0000-00-00'),
(122, 1, 4, 112, 'SAN LUIS', '58', '', '', '', 0.00, '0000-00-00'),
(123, 1, 4, 113, 'BOULEVARD LA TRINIDAD', '89', '', '', '', 0.00, '0000-00-00'),
(124, 1, 4, 114, 'SAN ANDRES', '14', '', '', '', 0.00, '0000-00-00'),
(125, 1, 4, 115, 'CERRADA LA TRINIDAD', '6', '', '', '', 0.00, '0000-00-00'),
(126, 1, 4, 116, 'BOULEVARD LA TRINIDAD', '3', '', '', '', 0.00, '0000-00-00'),
(127, 1, 4, 117, 'CERRADA LA TRINIDAD', '14', '', '', '', 0.00, '0000-00-00'),
(128, 1, 4, 118, 'SAN ANDRES', '1', '', '', '', 0.00, '0000-00-00'),
(129, 1, 4, 119, 'SAN LUIS', '54', '', '', '', 0.00, '0000-00-00'),
(130, 1, 4, 120, 'SAN JUAN', '10-A', '', '', '', 0.00, '0000-00-00'),
(131, 1, 4, 121, 'SAN COSME', '6', '', '', '', 0.00, '0000-00-00'),
(132, 3, 4, 122, 'SAN JUAN', '6', '', '', '', 0.00, '0000-00-00'),
(133, 1, 4, 123, 'SAN PEDRO', '4', '', '', '', 0.00, '0000-00-00'),
(136, 1, 4, 126, 'SAN ANDRES', '7', '', '', '', 0.00, '0000-00-00'),
(137, 1, 4, 127, 'SAN ANDRES', '34', '', '', '', 0.00, '0000-00-00'),
(138, 1, 4, 128, 'SAN ANDRES', '1-A', '', '', '', 0.00, '0000-00-00'),
(139, 1, 4, 129, 'SAN LUIS', '56', '', '', '', 0.00, '0000-00-00'),
(140, 1, 4, 130, 'BOULEVARD LA TRINIDAD', '58', '', '', '', 0.00, '0000-00-00'),
(141, 1, 4, 131, 'SAN JUAN', '11', '', '', '', 0.00, '0000-00-00'),
(142, 1, 4, 132, 'SAN COSME', '15', '', '', '', 0.00, '0000-00-00'),
(143, 1, 4, 133, 'SAN JOSÉ', '82', '', '', '', 0.00, '0000-00-00'),
(144, 1, 4, 134, 'SAN ANDRES', '10', '', '', '', 0.00, '0000-00-00'),
(145, 1, 4, 135, 'SAN MATEO', '5', '', '', '', 0.00, '0000-00-00'),
(146, 1, 4, 136, 'SAN LUIS', '44', '', '', '', 0.00, '0000-00-00'),
(147, 1, 4, 137, 'BOULEVARD LA TRINIDAD', '75', '', '', '', 0.00, '0000-00-00'),
(148, 1, 4, 138, 'SAN ANDRES', '38', '', '', '', 0.00, '0000-00-00'),
(149, 1, 4, 139, 'SAN PEDRO', '13', '', '', '', 0.00, '0000-00-00'),
(150, 1, 4, 140, 'BOULEVARD LA TRINIDAD', '70', '', '', '', 0.00, '0000-00-00'),
(151, 1, 4, 141, 'SAN MATEO', '13', '', '', '', 0.00, '0000-00-00'),
(152, 1, 4, 142, 'BOULEVARD LA TRINIDAD', '108', '', '', '', 0.00, '0000-00-00'),
(153, 1, 4, 143, 'SAN MARCOS', '114', '', '', '', 0.00, '0000-00-00'),
(154, 1, 4, 144, 'SAN ANDRES', '16', '', '', '', 0.00, '0000-00-00'),
(155, 1, 4, 145, 'SAN ANDRES', '36', '', '', '', 0.00, '0000-00-00'),
(156, 1, 4, 146, 'SAN ANTONIO', '43', '', '', '', 0.00, '0000-00-00'),
(157, 1, 4, 147, 'SAN ANTONIO', '19', '', '', '', 0.00, '0000-00-00'),
(158, 1, 4, 148, 'SAN COSME', '10', '', '', '', 0.00, '0000-00-00'),
(159, 1, 4, 149, 'SAN ANTONIO', '33', '', '', '', 0.00, '0000-00-00'),
(160, 1, 4, 150, 'SAN LUIS', '52', '', '', '', 0.00, '0000-00-00'),
(161, 1, 4, 151, 'CERRADA LA TRINIDAD', '5', '', '', '', 0.00, '0000-00-00'),
(162, 1, 4, 152, 'SAN ANTONIO', '65', '', '', '', 0.00, '0000-00-00'),
(163, 1, 4, 153, 'SAN ANTONIO', '35', '', '', '', 0.00, '0000-00-00'),
(164, 1, 4, 154, 'SAN ANDRES', '30', '', '', '', 0.00, '0000-00-00'),
(165, 1, 4, 155, 'SAN MARCOS', '120', '', '', '', 0.00, '0000-00-00'),
(166, 1, 4, 156, 'SAN ANTONIO', '32', '', '', '', 0.00, '0000-00-00'),
(167, 1, 4, 157, 'CERRADA LA TRINIDAD', '10', '', '', '', 0.00, '0000-00-00'),
(168, 1, 4, 158, 'SAN MARCOS', '118', '', '', '', 0.00, '0000-00-00'),
(169, 1, 4, 159, 'BOULEVARD LA TRINIDAD', '73', '', '', '', 0.00, '0000-00-00'),
(170, 1, 4, 160, 'SAN JUAN', '16', '', '', '', 0.00, '0000-00-00'),
(171, 1, 4, 161, 'SAN ANDRES', '22', '', '', '', 0.00, '0000-00-00'),
(172, 1, 4, 162, 'BOULEVARD LA TRINIDAD', '79', '', '', '', 0.00, '0000-00-00'),
(173, 1, 4, 163, 'SAN JOSÉ', '90', '', '', '', 0.00, '0000-00-00'),
(174, 1, 4, 164, 'SAN PEDRO', '12', '', '', '', 0.00, '0000-00-00'),
(175, 1, 4, 165, 'BOULEVARD LA TRINIDAD', '68', '', '', '', 0.00, '0000-00-00'),
(176, 1, 4, 166, 'BOULEVARD LA TRINIDAD', '66', '', '', '', 0.00, '0000-00-00'),
(177, 1, 4, 167, 'SAN MATEO', '10', '', '', '', 0.00, '0000-00-00'),
(178, 1, 4, 168, 'SAN JUAN', '10', '', '', '', 0.00, '0000-00-00'),
(179, 1, 4, 169, 'SAN PEDRO', '18', '', '', '', 0.00, '0000-00-00'),
(180, 1, 4, 170, 'SAN ANDRES', '24', '', '', '', 0.00, '0000-00-00'),
(181, 1, 4, 171, 'SAN JOSÉ', '86-2', '', '', '', 0.00, '0000-00-00'),
(182, 1, 4, 172, 'SAN COSME', '4', '', '', '', 0.00, '0000-00-00'),
(183, 1, 4, 173, 'SAN MATEO', '14', '', '', '', 0.00, '0000-00-00'),
(184, 1, 4, 174, 'SAN COSME', '12', '', '', '', 0.00, '0000-00-00'),
(185, 1, 4, 175, 'BOULEVARD LA TRINIDAD', '83', '', '', '', 0.00, '0000-00-00'),
(186, 1, 4, 176, 'CERRADA LA TRINIDAD', '7', '', '', '', 0.00, '0000-00-00'),
(187, 1, 4, 177, 'SAN ANDRES', '8', '', '', '', 0.00, '0000-00-00'),
(188, 1, 4, 178, 'SAN LUIS', '48', '', '', '', 0.00, '0000-00-00'),
(189, 1, 4, 179, 'SAN ANDRES', '20', '', '', '', 0.00, '0000-00-00'),
(190, 1, 4, 180, 'SAN ANTONIO', '30', '', '', '', 0.00, '0000-00-00'),
(191, 1, 4, 181, 'SAN FELIPE', '13', '', '', '', 0.00, '0000-00-00'),
(192, 1, 4, 182, 'BOULEVARD LA TRINIDAD', '104', '', '', '', 0.00, '0000-00-00'),
(193, 1, 4, 183, 'BOULEVARD LA TRINIDAD', '14', '', '', '', 0.00, '0000-00-00'),
(194, 1, 4, 184, 'BOULEVARD LA TRINIDAD', '77', '', '', '', 0.00, '0000-00-00'),
(195, 1, 4, 185, 'SAN JUAN', '12', '', '', '', 0.00, '0000-00-00'),
(196, 1, 4, 186, 'SAN JOSÉ', '100', '', '', '', 0.00, '0000-00-00'),
(197, 1, 4, 187, 'SAN PEDRO', '10', '', '', '', 0.00, '0000-00-00'),
(198, 1, 4, 188, 'SAN ANDRES', '2-B', '', '', '', 0.00, '0000-00-00'),
(199, 1, 4, 189, 'SAN COSME', '8', '', '', '', 0.00, '0000-00-00'),
(200, 1, 4, 190, 'SAN ANDRES', '27', '', '', '', 0.00, '0000-00-00'),
(201, 1, 4, 191, 'BOULEVARD LA TRINIDAD', '64', '', '', '', 0.00, '0000-00-00'),
(202, 1, 4, 192, 'BOULEVARD LA TRINIDAD', '28', '', '', '', 0.00, '0000-00-00'),
(203, 1, 4, 193, 'SAN ANTONIO', '9', '', '', '', 0.00, '0000-00-00'),
(204, 1, 4, 194, 'SAN JOSÉ', '102', '', '', '', 0.00, '0000-00-00'),
(205, 1, 4, 195, 'CERRADA LA TRINIDAD', '1', '', '', '', 0.00, '0000-00-00'),
(206, 1, 4, 196, 'SAN MATEO', '3', '', '', '', 0.00, '0000-00-00'),
(207, 1, 4, 197, 'SAN JOSÉ', '80', '', '', '', 0.00, '0000-00-00'),
(208, 1, 4, 198, 'SAN LUIS', '42', '', '', '', 0.00, '0000-00-00'),
(209, 1, 4, 199, 'SAN ANTONIO', '11', '', '', '', 0.00, '0000-00-00'),
(210, 1, 4, 200, 'SAN PEDRO', '20', '', '', '', 0.00, '0000-00-00'),
(211, 1, 4, 201, 'SAN LUIS', '62', '', '', '', 0.00, '0000-00-00'),
(212, 1, 4, 202, 'SAN PEDRO', '15', '', '', '', 0.00, '0000-00-00'),
(213, 1, 4, 203, 'SAN ANDRES', '6', '', '', '', 0.00, '0000-00-00'),
(214, 1, 4, 204, 'SAN FELIPE', '8', '', '', '', 0.00, '0000-00-00'),
(215, 1, 4, 205, 'BOULEVARD LA TRINIDAD', '34', '', '', '', 0.00, '0000-00-00'),
(216, 1, 4, 206, 'SAN MATEO', '12', '', '', '', 0.00, '0000-00-00'),
(217, 1, 4, 207, 'SAN ANTONIO', '25', '', '', '', 0.00, '0000-00-00'),
(218, 1, 4, 208, 'SAN ANTONIO', '53-B', '', '', '', 0.00, '0000-00-00'),
(219, 3, 4, 209, 'SAN ANTONIO', '31-A', '', '', '', 0.00, '0000-00-00'),
(220, 1, 4, 210, 'SAN LUIS', '36', '', '', '', 0.00, '0000-00-00'),
(221, 1, 4, 211, 'SAN MATEO', '4', '', '', '', 0.00, '0000-00-00'),
(222, 3, 4, 212, 'BOULEVARD LA TRINIDAD', '78', '', '', '', 0.00, '0000-00-00'),
(223, 1, 4, 213, 'SAN LUIS', '40', '', '', '', 0.00, '0000-00-00'),
(224, 3, 4, 214, 'SAN COSME', '9', '', '', '', 0.00, '0000-00-00'),
(225, 1, 4, 215, 'BOULEVARD LA TRINIDAD', '26', '', '', '', 0.00, '0000-00-00'),
(226, 1, 4, 216, 'BOULEVARD LA TRINIDAD', '81', '', '', '', 0.00, '0000-00-00'),
(227, 1, 4, 217, 'BOULEVARD LA TRINIDAD', '50', '', '', '', 0.00, '0000-00-00'),
(228, 1, 4, 218, 'SAN JUAN', '15', '', '', '', 0.00, '0000-00-00'),
(229, 1, 4, 219, 'SAN ANTONIO', '49', '', '', '', 0.00, '0000-00-00'),
(230, 1, 4, 220, 'SAN ANDRES', '32', '', '', '', 0.00, '0000-00-00'),
(231, 1, 4, 221, 'BOULEVARD LA TRINIDAD', '62', '', '', '', 0.00, '0000-00-00'),
(232, 1, 4, 222, 'SAN COSME', '14', '', '', '', 0.00, '0000-00-00'),
(233, 1, 4, 223, 'SAN LUIS', '50', '', '', '', 0.00, '0000-00-00'),
(234, 1, 4, 224, 'BOULEVARD LA TRINIDAD', '74', '', '', '', 0.00, '0000-00-00'),
(235, 1, 4, 225, 'BOULEVARD LA TRINIDAD', '37', '', '', '', 0.00, '0000-00-00'),
(236, 1, 4, 226, 'SAN JOSÉ', '98', '', '', '', 0.00, '0000-00-00'),
(237, 1, 4, 227, 'SAN MARCOS', '112', '', '', '', 0.00, '0000-00-00'),
(238, 1, 4, 228, 'BOULEVARD LA TRINIDAD', '72', '', '', '', 0.00, '0000-00-00'),
(239, 1, 4, 229, 'BOULEVARD LA TRINIDAD', '30', '', '', '', 0.00, '0000-00-00'),
(240, 1, 4, 230, 'BOULEVARD LA TRINIDAD', '20', '', '', '', 0.00, '0000-00-00'),
(241, 1, 4, 231, 'CERRADA LA TRINIDAD', '15', '', '', '', 0.00, '0000-00-00'),
(242, 1, 4, 232, 'BOULEVARD LA TRINIDAD', '8', '', '', '', 0.00, '0000-00-00'),
(243, 1, 4, 233, 'SAN ANDRES', '2', '', '', '', 0.00, '0000-00-00'),
(244, 1, 4, 234, 'SAN MATEO', '9', '', '', '', 0.00, '0000-00-00'),
(245, 1, 4, 235, 'BOULEVARD LA TRINIDAD', '1', '', '', '', 0.00, '0000-00-00'),
(246, 1, 4, 236, 'BOULEVARD LA TRINIDAD', '6', '', '', '', 0.00, '0000-00-00'),
(247, 1, 4, 237, 'SAN LUCAS', '4', '', '', '', 0.00, '0000-00-00'),
(248, 1, 4, 238, 'SAN JUAN', '4', '', '', '', 0.00, '0000-00-00'),
(249, 1, 4, 239, 'SAN ANDRES', '28', '', '', '', 0.00, '0000-00-00'),
(250, 1, 4, 240, 'SAN ANTONIO', '47', '', '', '', 0.00, '0000-00-00'),
(251, 1, 4, 241, 'SAN LUCAS', '8', '', '', '', 0.00, '0000-00-00'),
(252, 1, 4, 242, 'BOULEVARD LA TRINIDAD', '97', '', '', '', 0.00, '0000-00-00'),
(253, 1, 4, 243, 'SAN ANDRES', '15', '', '', '', 0.00, '0000-00-00'),
(254, 1, 4, 244, 'CERRADA LA TRINIDAD', '4', '', '', '', 0.00, '0000-00-00'),
(255, 1, 4, 245, 'SAN ANDRES', '13', '', '', '', 0.00, '0000-00-00'),
(256, 1, 4, 246, 'SAN JUAN', '17', '', '', '', 0.00, '0000-00-00'),
(257, 1, 4, 247, 'SAN JUAN', '8', '', '', '', 0.00, '0000-00-00'),
(258, 1, 4, 248, 'CERRADA LA TRINIDAD', '9', '', '', '', 0.00, '0000-00-00'),
(259, 1, 4, 249, 'BOULEVARD LA TRINIDAD', '15', '', '', '', 0.00, '0000-00-00'),
(260, 1, 4, 250, 'SAN COSME', 'MZ-1  L-2', '', '1', '2', 0.00, '0000-00-00'),
(261, 1, 4, 251, 'SAN ANTONIO', '37', '', '', '', 0.00, '0000-00-00'),
(262, 1, 4, 252, 'CERRADA LA TRINIDAD', '12', '', '', '', 0.00, '0000-00-00'),
(263, 1, 4, 253, 'BOULEVARD LA TRINIDAD', '79', '', '', '', 0.00, '0000-00-00'),
(264, 1, 4, 254, 'CERRADA LA TRINIDAD', '16', '', '', '', 0.00, '0000-00-00'),
(265, 1, 3, 255, 'Casa', '001', '', '', '', 0.00, '0000-00-00'),
(266, 1, 3, 256, 'Casa', '002', '', '', '', 0.00, '0000-00-00'),
(267, 4, 3, 257, 'Casa', '003', '', '', '', 0.00, '0000-00-00'),
(268, 1, 3, 258, 'Casa', '004', '', '', '', 0.00, '0000-00-00'),
(269, 1, 3, 259, 'Casa', '005', '', '', '', 0.00, '0000-00-00'),
(270, 1, 3, 260, 'Casa', '006', '', '', '', 0.00, '0000-00-00'),
(271, 1, 3, 261, 'Casa', '007', '', '', '', 0.00, '0000-00-00'),
(272, 1, 3, 262, 'Casa', '008', '', '', '', 0.00, '0000-00-00'),
(276, 1, 3, 263, 'Casa', '009', '', '', '', 0.00, '0000-00-00'),
(277, 1, 3, 264, 'Casa', '010', '', '', '', 0.00, '0000-00-00'),
(278, 1, 3, 265, 'Casa', '011', '', '', '', 0.00, '0000-00-00'),
(279, 1, 3, 266, 'Casa', '012', '', '', '', 0.00, '0000-00-00'),
(280, 4, 3, 267, 'Casa', '013', '', '', '', 0.00, '0000-00-00'),
(281, 1, 3, 268, 'Casa', '014', '', '', '', 0.00, '0000-00-00'),
(282, 1, 3, 269, 'Casa', '015', '', '', '', 0.00, '0000-00-00'),
(283, 4, 3, 270, 'Casa', '016', '', '', '', 0.00, '0000-00-00'),
(284, 1, 3, 271, 'Casa', '017', '', '', '', 0.00, '0000-00-00'),
(285, 1, 3, 272, 'Casa', '018', '', '', '', 0.00, '0000-00-00'),
(286, 1, 3, 273, 'Casa', '019', '', '', '', 0.00, '0000-00-00'),
(287, 1, 3, 274, 'Casa', '020', '', '', '', 0.00, '0000-00-00'),
(291, 1, 3, 275, 'Casa', '021', '', '', '', 0.00, '0000-00-00'),
(292, 1, 3, 276, 'Casa', '022', '', '', '', 0.00, '0000-00-00'),
(293, 1, 3, 277, 'Casa', '023', '', '', '', 0.00, '0000-00-00'),
(294, 1, 3, 278, 'Casa', '024', '', '', '', 0.00, '0000-00-00'),
(295, 1, 3, 279, 'Casa', '025', '', '', '', 0.00, '0000-00-00'),
(296, 1, 3, 280, 'Casa', '026', '', '', '', 0.00, '0000-00-00'),
(297, 1, 3, 281, 'Casa', '027', '', '', '', 0.00, '0000-00-00'),
(298, 1, 3, 282, 'Casa', '028', '', '', '', 0.00, '0000-00-00'),
(299, 1, 3, 283, 'Casa', '029', '', '', '', 0.00, '0000-00-00'),
(300, 1, 3, 284, 'Casa', '030', '', '', '', 0.00, '0000-00-00'),
(301, 1, 3, 285, 'Casa', '031', '', '', '', 0.00, '0000-00-00'),
(302, 1, 3, 286, 'Casa', '032', '', '', '', 0.00, '0000-00-00'),
(303, 4, 3, 287, 'Casa', '033', '', '', '', 0.00, '0000-00-00'),
(304, 4, 3, 288, 'Casa', '034', '', '', '', 0.00, '0000-00-00'),
(305, 1, 3, 289, 'Casa', '035', '', '', '', 0.00, '0000-00-00'),
(306, 1, 3, 290, 'Casa', '036', '', '', '', 0.00, '0000-00-00'),
(307, 1, 3, 291, 'Casa', '037', '', '', '', 0.00, '0000-00-00'),
(308, 1, 3, 292, 'Casa', '038', '', '', '', 0.00, '0000-00-00'),
(310, 1, 3, 293, 'Casa', '039', '', '', '', 0.00, '0000-00-00'),
(311, 1, 3, 294, 'Casa', '040', '', '', '', 0.00, '0000-00-00'),
(312, 1, 3, 295, 'Casa', '041', '', '', '', 0.00, '0000-00-00'),
(313, 1, 3, 296, 'Casa', '042', '', '', '', 0.00, '0000-00-00'),
(315, 1, 3, 297, 'Casa', '043', '', '', '', 0.00, '0000-00-00'),
(316, 1, 3, 298, 'Casa', '044', '', '', '', 0.00, '0000-00-00'),
(317, 1, 3, 299, 'Casa', '045', '', '', '', 0.00, '0000-00-00'),
(318, 1, 3, 300, 'Casa', '046', '', '', '', 0.00, '0000-00-00'),
(319, 1, 3, 301, 'Casa', '047', '', '', '', 0.00, '0000-00-00'),
(320, 1, 3, 302, 'Casa', '048', '', '', '', 0.00, '0000-00-00'),
(321, 1, 3, 303, 'Casa', '049', '', '', '', 0.00, '0000-00-00'),
(322, 1, 3, 304, 'Casa', '050', '', '', '', 0.00, '0000-00-00'),
(323, 1, 3, 305, 'Casa', '051', '', '', '', 0.00, '0000-00-00'),
(324, 1, 3, 306, 'Casa', '052', '', '', '', 0.00, '0000-00-00'),
(325, 1, 3, 307, 'Casa', '053', '', '', '', 0.00, '0000-00-00'),
(326, 1, 3, 308, 'Casa', '054', '', '', '', 0.00, '0000-00-00'),
(327, 1, 3, 309, 'Casa', '055', '', '', '', 0.00, '0000-00-00'),
(328, 1, 3, 310, 'Casa', '056', '', '', '', 0.00, '0000-00-00'),
(329, 1, 3, 311, 'Casa', '057', '', '', '', 0.00, '0000-00-00'),
(330, 1, 3, 312, 'Casa', '058', '', '', '', 0.00, '0000-00-00'),
(331, 1, 3, 313, 'Casa', '059', '', '', '', 0.00, '0000-00-00'),
(332, 1, 3, 314, 'Casa', '060', '', '', '', 0.00, '0000-00-00'),
(333, 1, 3, 315, 'Casa', '061', '', '', '', 0.00, '0000-00-00'),
(334, 1, 3, 316, 'Casa', '062', '', '', '', 0.00, '0000-00-00'),
(335, 1, 3, 317, 'Casa', '063', '', '', '', 0.00, '0000-00-00'),
(336, 1, 3, 318, 'Casa', '064', '', '', '', 0.00, '0000-00-00'),
(337, 1, 3, 319, 'Casa', '065', '', '', '', 0.00, '0000-00-00'),
(338, 1, 3, 320, 'Casa', '066', '', '', '', -150.00, '2013-02-28'),
(339, 1, 3, 321, 'Casa', '067', '', '', '', 0.00, '0000-00-00'),
(340, 1, 3, 322, 'Casa', '068', '', '', '', 0.00, '0000-00-00'),
(341, 1, 3, 323, 'Casa', '069', '', '', '', 0.00, '0000-00-00'),
(342, 1, 3, 324, 'Casa', '070', '', '', '', 0.00, '0000-00-00'),
(343, 1, 3, 325, 'Casa', '071', '', '', '', 0.00, '0000-00-00'),
(344, 1, 3, 326, 'Casa', '072', '', '', '', 0.00, '0000-00-00'),
(345, 1, 3, 327, 'Casa', '073', '', '', '', 0.00, '0000-00-00'),
(346, 1, 3, 328, 'Casa', '074', '', '', '', 0.00, '0000-00-00'),
(347, 1, 3, 329, 'Casa', '075', '', '', '', 0.00, '0000-00-00'),
(348, 1, 3, 330, 'Casa', '076', '', '', '', 0.00, '0000-00-00'),
(349, 1, 3, 331, 'Casa', '077', '', '', '', -140.00, '2013-02-28'),
(350, 1, 3, 332, 'Casa', '078', '', '', '', 0.00, '0000-00-00'),
(351, 1, 3, 333, 'Casa', '079', '', '', '', 0.00, '0000-00-00'),
(352, 1, 3, 334, 'Casa', '080', '', '', '', 0.00, '0000-00-00'),
(353, 1, 3, 335, 'Casa', '081', '', '', '', 0.00, '0000-00-00'),
(354, 1, 3, 336, 'Casa', '082', '', '', '', 0.00, '0000-00-00'),
(355, 1, 3, 337, 'Casa', '083', '', '', '', 0.00, '0000-00-00'),
(356, 1, 3, 338, 'Casa', '084', '', '', '', 0.00, '0000-00-00'),
(357, 1, 3, 339, 'Casa', '085', '', '', '', 0.00, '0000-00-00'),
(358, 1, 3, 340, 'Casa', '086', '', '', '', 0.00, '0000-00-00'),
(359, 1, 3, 341, 'Casa', '087', '', '', '', 0.00, '0000-00-00'),
(360, 1, 3, 342, 'Casa', '088', '', '', '', 0.00, '0000-00-00'),
(361, 1, 3, 343, 'Casa', '089', '', '', '', 0.00, '0000-00-00'),
(362, 1, 3, 344, 'Casa', '090', '', '', '', 0.00, '0000-00-00'),
(363, 1, 3, 345, 'Casa', '091', '', '', '', 0.00, '0000-00-00'),
(364, 1, 3, 346, 'Casa', '092', '', '', '', 0.00, '0000-00-00'),
(365, 1, 3, 347, 'Casa', '093', '', '', '', 0.00, '0000-00-00'),
(366, 1, 3, 348, 'Casa', '094', '', '', '', 0.00, '0000-00-00'),
(367, 1, 3, 349, 'Casa', '095', '', '', '', 0.00, '0000-00-00'),
(368, 1, 3, 350, 'Casa', '096', '', '', '', 0.00, '0000-00-00'),
(369, 1, 3, 351, 'Casa', '097', '', '', '', 0.00, '0000-00-00'),
(370, 1, 3, 352, 'Casa', '098', '', '', '', 0.00, '0000-00-00'),
(371, 1, 3, 353, 'Casa', '099', '', '', '', 0.00, '0000-00-00'),
(372, 1, 3, 354, 'Casa', '100', '', '', '', 0.00, '0000-00-00'),
(373, 1, 3, 355, 'Casa', '101', '', '', '', 0.00, '0000-00-00'),
(374, 1, 3, 356, 'Casa', '102', '', '', '', 0.00, '0000-00-00'),
(375, 1, 3, 357, 'Casa', '103', '', '', '', 0.00, '0000-00-00'),
(376, 1, 3, 358, 'Casa', '104', '', '', '', 0.00, '0000-00-00'),
(377, 1, 3, 359, 'Casa', '105', '', '', '', 0.00, '0000-00-00'),
(378, 1, 3, 360, 'Casa', '106', '', '', '', 0.00, '0000-00-00'),
(379, 1, 3, 361, 'Casa', '107', '', '', '', 0.00, '0000-00-00'),
(380, 1, 3, 362, 'Casa', '108', '', '', '', 0.00, '0000-00-00'),
(381, 1, 3, 363, 'Casa', '109', '', '', '', 0.00, '0000-00-00'),
(382, 1, 3, 364, 'Casa', '110', '', '', '', 0.00, '0000-00-00'),
(383, 1, 3, 365, 'Casa', '111', '', '', '', 0.00, '0000-00-00'),
(384, 1, 3, 366, 'Casa', '112', '', '', '', 0.00, '0000-00-00'),
(385, 1, 3, 367, 'Casa', '113', '', '', '', 0.00, '0000-00-00'),
(386, 1, 3, 368, 'Casa', '114', '', '', '', 0.00, '0000-00-00'),
(387, 1, 3, 369, 'Casa', '115', '', '', '', 0.00, '0000-00-00'),
(388, 1, 3, 370, 'Casa', '116', '', '', '', 0.00, '0000-00-00'),
(389, 1, 3, 371, 'Casa', '117', '', '', '', 0.00, '0000-00-00'),
(390, 1, 3, 372, 'Casa', '118', '', '', '', 0.00, '0000-00-00'),
(391, 1, 3, 373, 'Casa', '119', '', '', '', 0.00, '0000-00-00'),
(392, 1, 3, 374, 'Casa', '120', '', '', '', 0.00, '0000-00-00'),
(393, 1, 3, 375, 'Casa', '121', '', '', '', 0.00, '0000-00-00'),
(394, 1, 3, 376, 'Casa', '122', '', '', '', 0.00, '0000-00-00'),
(395, 1, 3, 377, 'Casa', '123', '', '', '', 0.00, '0000-00-00'),
(396, 1, 3, 378, 'Casa', '124', '', '', '', 0.00, '0000-00-00'),
(397, 1, 3, 379, 'Casa', '125', '', '', '', 0.00, '0000-00-00'),
(398, 1, 3, 380, 'Casa', '126', '', '', '', 0.00, '0000-00-00'),
(399, 1, 3, 381, 'Casa', '127', '', '', '', 0.00, '0000-00-00'),
(400, 1, 3, 382, 'Casa', '128', '', '', '', 0.00, '0000-00-00'),
(401, 1, 1, 7, 'Casa', '28', '', '', '', 0.00, '0000-00-00');

INSERT INTO `payment` VALUES (1,1,2,600.00,'2013-01-05','','0004','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (2,1,2,600.00,'2013-02-07','','0037','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (3,1,2,600.00,'2013-03-07','','0064','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (4,1,2,600.00,'2013-04-04','','0084','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (5,1,2,600.00,'2013-05-07','','0113','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (6,1,2,600.00,'2013-06-06','','0146','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (7,1,2,600.00,'2013-07-09','','0173','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (8,1,2,600.00,'2013-08-06','','0195','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (9,1,2,600.00,'2013-09-05','','0227','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (10,1,2,600.00,'2013-10-07','','0253','juan_fco_monter','2014-05-06',NULL,2);
INSERT INTO `payment` VALUES (11,1,2,600.00,'2013-11-05','','0279','juan_fco_monter','2014-05-07',NULL,2);
INSERT INTO `payment` VALUES (12,1,2,600.00,'2013-12-11','','0317','juan_fco_monter','2014-05-07',NULL,2);
INSERT INTO `payment` VALUES (13,1,2,600.00,'2014-01-06','','0340','juan_fco_monter','2014-05-07',NULL,2);
INSERT INTO `payment` VALUES (14,1,2,600.00,'2014-02-05','','0370','juan_fco_monter','2014-05-07',NULL,2);
INSERT INTO `payment` VALUES (15,1,2,600.00,'2014-03-05','','0405','juan_fco_monter','2014-05-07',NULL,2);
INSERT INTO `payment` VALUES (16,2,44,0.00,'2012-11-30','','0000','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (17,2,44,1300.00,'2012-12-14','','0038','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (18,2,44,1250.00,'2013-01-26','','0000','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (19,2,44,0.00,'2013-02-28','','0000','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (20,2,44,0.00,'2013-03-31','','0000','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (21,2,44,0.00,'2013-04-30','','0000','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (22,2,44,700.00,'2013-05-11','Remanente marzo 2013','0159','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (23,2,44,650.00,'2013-06-15','Remanente abril 2013','0000','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (24,2,44,0.00,'2013-07-31','','0000','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (25,2,44,2500.00,'2013-08-10','remanente mayo, junio, julio y agosto','0230','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (26,2,44,650.00,'2013-09-13','','0275','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (27,2,44,650.00,'2013-09-13','Adelantado octubre 2013','0276','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (28,2,44,0.00,'2013-10-31','','0000','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (29,2,44,650.00,'2013-11-15','','0340','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (30,2,44,650.00,'2013-12-14','','0369','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (31,2,44,650.00,'2014-01-13','','0395','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (32,2,44,650.00,'2014-02-15','','0438','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (33,2,44,650.00,'2014-03-15','','0464','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (34,2,44,400.00,'2014-04-14','pago parcial','0500','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (35,2,44,200.00,'2014-04-17','','0505','juan_fco_monter','2014-05-10',NULL,40);
INSERT INTO `payment` VALUES (36,3,45,0.00,'2012-11-30','','0000','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (37,3,45,450.00,'2012-12-05','Remanente nov 2012','0027','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (38,3,45,1350.00,'2012-12-28','actual, adelantado enero y febrero 2013','0053','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (39,3,45,0.00,'2013-01-31','Pagado','0000','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (40,3,45,0.00,'2013-02-28','Pagado','0000','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (41,3,45,0.00,'2012-03-31','Debe','0000','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (42,3,45,1350.00,'2013-04-08','Remanente mar, actual y adelantado abr','0136','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (43,3,45,0.00,'2013-05-31','Pagado','0000','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (44,4,45,0.00,'2013-06-30','Debe','0000','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (45,4,45,0.00,'2013-07-31','Debe','0000','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (46,4,45,700.00,'2013-08-02','Remanente junio 2013','0221','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (47,4,45,1400.00,'2013-08-07','Remanente julio 2013 y actual','0227','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (48,2,45,650.00,'2013-09-20','','0264','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (49,2,45,650.00,'2013-10-11','','0292','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (50,2,45,650.00,'2013-11-08','','0322','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (51,2,45,650.00,'2013-11-26','Adelantado dic 2013','0347','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (52,2,45,0.00,'2013-12-31','Pagado','0000','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (53,2,45,650.00,'2014-01-10','','0393','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (54,2,45,650.00,'2014-02-14','','0430','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (55,2,45,650.00,'2014-03-15','','0465','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (56,2,45,650.00,'2014-04-23','','0508','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (57,2,45,650.00,'2014-05-02','','0514','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (58,2,45,600.00,'2012-11-26','','0021','juan_fco_monter','2014-05-10',NULL,41);
INSERT INTO `payment` VALUES (59,2,46,600.00,'2012-11-26','','0021','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (60,2,46,700.00,'2012-12-11','','0037','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (61,2,46,650.00,'2013-01-09','','0064','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (62,2,46,650.00,'2013-02-11','','0087','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (63,2,46,650.00,'2013-03-08','','0126','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (64,2,46,0.00,'2013-04-30','','0000','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (65,2,46,650.00,'2013-05-15','','0168','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (66,2,46,650.00,'2013-06-04','','0193','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (67,2,46,650.00,'2013-07-02','','0211','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (68,2,46,650.00,'2013-08-09','','0240','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (69,2,46,650.00,'2013-09-09','','0272','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (70,2,46,650.00,'2013-10-01','','0281','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (71,2,46,650.00,'2013-11-06','','0348','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (72,2,46,650.00,'2013-12-03','','0354','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (73,2,46,650.00,'2014-01-03','','0408','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (74,2,46,650.00,'2014-02-06','','0443','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (75,2,46,650.00,'2014-03-04','','0476','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (76,2,46,650.00,'2014-04-07','','0510','juan_fco_monter','2014-05-10',NULL,42);
INSERT INTO `payment` VALUES (77,2,47,1300.00,'2012-11-22','Actual y adelantado dic 2012','0019','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (78,2,47,0.00,'2012-12-31','Pagado','0000','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (79,2,47,650.00,'2013-01-15','','0068','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (80,2,47,650.00,'2013-02-12','','0089','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (81,2,47,0.00,'2013-03-31','','0000','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (82,2,47,1300.00,'2013-04-08','Remante marzo y actual','0133','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (83,2,47,650.00,'2013-05-11','','0160','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (84,2,47,650.00,'2013-06-13','','0179','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (85,2,47,0.00,'2013-07-31','Debe','0000','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (86,2,47,1300.00,'2013-08-13','Remantente julio y actual','0238','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (87,2,47,0.00,'2013-09-30','Debe','0000','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (88,2,47,650.00,'2013-10-04','Remanente septiembre','0286','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (89,2,47,650.00,'2013-10-04','','0287','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (90,2,47,650.00,'2013-11-12','','0330','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (91,2,47,650.00,'2013-12-17','','0378','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (92,2,47,650.00,'2014-01-20','','0413','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (93,2,47,0.00,'2014-02-28','Debe','0000','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (94,2,47,650.00,'2014-03-05','Remanente febrero','0447','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (95,2,47,650.00,'2014-03-31','','0482','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (96,2,47,650.00,'2014-04-10','','0492','juan_fco_monter','2014-05-10',NULL,43);
INSERT INTO `payment` VALUES (97,2,48,600.00,'2012-11-13','','0010','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (98,2,48,700.00,'2012-12-07','','0031','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (99,2,48,650.00,'2013-01-08','','0059','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (100,2,48,650.00,'2013-02-09','','0084','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (101,2,48,650.00,'2013-03-09','','0111','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (102,2,48,650.00,'2013-05-10','','0157','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (103,2,48,1300.00,'2013-06-14','Actual y adelantado','0180','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (104,2,48,650.00,'2013-07-29','adelantado agosto','0216','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (105,2,48,0.00,'2013-08-31','pagado agosto','0000','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (106,2,48,650.00,'2013-09-28','','0277','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (107,2,48,650.00,'2013-10-03','','0312','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (108,2,48,650.00,'2013-11-07','','0321','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (109,2,48,650.00,'2013-12-10','','0362','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (110,2,48,650.00,'2014-01-09','','0391','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (111,2,48,650.00,'2014-02-07','','0420','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (112,2,48,650.00,'2014-03-10','','0455','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (113,2,48,650.00,'2014-04-03','','0487','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (114,2,49,650.00,'2012-11-06','','0003','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (115,2,49,650.00,'2012-12-04','','0024','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (116,2,49,650.00,'2013-01-03','','0054','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (117,2,49,650.00,'2013-02-12','','0090','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (118,2,48,650.00,'2013-03-11','','0112','juan_fco_monter','2014-05-12',NULL,44);
INSERT INTO `payment` VALUES (119,2,49,650.00,'2013-04-08','','0137','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (120,2,49,650.00,'2013-05-15','','0164','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (121,2,49,650.00,'2013-06-14','','0182','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (122,2,49,650.00,'2013-07-05','','0200','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (123,2,49,650.00,'2013-08-10','','0235','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (124,2,49,650.00,'2013-09-13','','0259','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (125,2,49,650.00,'2013-10-07','','0289','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (126,2,49,650.00,'2013-11-07','','0319','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (127,2,49,650.00,'2013-12-10','','0361','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (128,2,49,650.00,'2014-01-09','','0392','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (129,2,49,650.00,'2014-02-06','','0422','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (130,2,49,650.00,'2014-03-14','','0462','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (131,2,49,650.00,'2014-04-10','','0495','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (132,2,49,650.00,'2014-05-09','','0521','juan_fco_monter','2014-05-12',NULL,45);
INSERT INTO `payment` VALUES (133,3,50,450.00,'2012-11-29','','0020','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (134,3,50,450.00,'2012-12-07','','0044','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (135,3,50,450.00,'2013-01-08','','0058','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (136,3,50,450.00,'2013-02-12','','0088','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (137,3,50,450.00,'2013-03-02','','0103','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (138,3,50,0.00,'2013-04-30','','0000','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (139,3,50,0.00,'2013-05-31','','0000','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (140,3,50,0.00,'2013-06-30','','0000','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (141,3,50,1800.00,'2013-07-05','Remanente abr may y jun, actual','0199','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (142,3,50,0.00,'2013-08-31','','0000','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (143,3,50,900.00,'2013-09-10','','0258','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (144,3,50,450.00,'2013-10-25','','0309','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (145,4,50,1400.00,'2013-11-08','','0324','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (146,4,50,0.00,'2013-12-31','Pagado','0000','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (147,4,50,700.00,'2014-01-15','','0402','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (148,4,50,700.00,'2014-02-15','','0432','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (149,2,50,650.00,'2014-03-15','','0471','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (150,2,50,650.00,'2014-04-16','','0504','juan_fco_monter','2014-05-12',NULL,46);
INSERT INTO `payment` VALUES (151,3,51,900.00,'2012-11-20','actual y adelantado dic 2012','0016','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (152,3,52,900.00,'2012-11-20','Actual y adelantado','0016','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (153,3,51,0.00,'2012-12-31','pagado','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (154,3,52,0.00,'2012-12-31','pagado','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (155,3,51,0.00,'2013-01-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (156,3,51,0.00,'2013-02-28','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (157,3,51,0.00,'2013-03-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (158,3,51,0.00,'2013-04-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (159,3,51,0.00,'2013-05-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (160,3,51,0.00,'2013-06-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (161,3,51,0.00,'2013-07-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (162,3,51,0.00,'2013-08-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (163,3,51,0.00,'2013-09-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (164,3,51,0.00,'2013-10-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (165,3,51,0.00,'2013-11-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (166,3,51,2000.00,'2013-12-31','Remanente ene feb mar abr parcial mayo','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (167,3,51,1100.00,'2014-01-31','complemento mayo, jun y jul 2013','0416','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (168,3,51,0.00,'2014-02-28','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (169,3,51,0.00,'2014-03-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (170,3,51,0.00,'2014-04-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (171,3,52,0.00,'2013-01-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (172,3,52,0.00,'2013-02-28','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (173,3,52,0.00,'2013-03-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (174,3,52,0.00,'2013-04-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (175,3,52,0.00,'2013-05-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (176,3,52,0.00,'2013-06-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (177,3,52,0.00,'2013-07-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (178,3,52,0.00,'2013-08-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (179,3,52,0.00,'2013-09-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (180,3,52,0.00,'2013-10-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (181,3,52,0.00,'2013-11-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (182,3,52,2000.00,'2013-12-27','Remanente ene feb mar abr y parcial mayo','0380','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (183,3,52,900.00,'2014-01-31','complemento mayo, junio parcial de julio 2013','0416','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (184,3,52,0.00,'2014-02-28','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (185,3,52,0.00,'2014-03-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (186,3,52,0.00,'2014-04-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,47);
INSERT INTO `payment` VALUES (187,3,53,100.00,'2012-11-06','','0004','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (188,2,54,650.00,'2012-11-06','','0004','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (189,3,53,100.00,'2012-12-05','','0028','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (190,2,54,650.00,'2012-12-05','','0028','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (191,3,53,100.00,'2013-01-15','','0070','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (192,2,54,650.00,'2013-01-15','','0070','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (193,3,53,100.00,'2013-02-12','','0091','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (194,2,54,650.00,'2013-02-12','','0091','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (195,3,53,0.00,'2013-03-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (196,3,53,0.00,'2013-04-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (197,2,54,0.00,'2013-03-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (198,2,54,0.00,'2013-04-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (199,3,53,100.00,'2013-05-11','Remanente marzo','0158','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (200,2,54,650.00,'2013-05-11','Remanente marzo','0158','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (201,3,53,0.00,'2013-06-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (202,2,54,0.00,'2013-06-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (203,3,53,0.00,'2013-07-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (204,2,54,0.00,'2013-07-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (205,3,53,200.00,'2013-08-09','Remanente abr y may','0229','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (206,2,54,1300.00,'2013-08-09','Remanente abr y may','0229','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (208,3,53,100.00,'2013-08-10','Remanente junio','0231','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (209,3,53,100.00,'2013-08-10','Remanente julio','0232','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (210,2,54,650.00,'2013-08-10','Remanente junio','0231','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (211,2,54,650.00,'2013-08-10','Remanente julio','0232','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (212,3,53,0.00,'2013-08-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (213,2,54,0.00,'2013-08-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (214,3,53,100.00,'2013-09-20','Remanente agosto','0267','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (215,2,54,650.00,'2013-09-20','Remanente agosto','0267','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (216,3,53,100.00,'2013-09-20','','0269','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (217,2,54,650.00,'2013-09-20','','0269','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (218,3,53,100.00,'2013-10-22','','0307','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (219,2,54,650.00,'2013-10-22','','0307','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (220,3,53,100.00,'2013-11-15','','0337','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (221,2,54,650.00,'2013-11-15','','0337','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (222,2,54,50.00,'2013-11-15','','0339','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (223,3,53,100.00,'2013-12-16','','0377','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (224,2,54,600.00,'2013-12-16','','0377','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (225,3,53,100.00,'2014-01-15','','0398','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (226,2,54,650.00,'2014-01-15','','0398','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (227,3,53,100.00,'2014-02-15','','0434','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (228,2,54,650.00,'2014-02-15','','0434','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (229,3,53,100.00,'2014-03-15','','0466','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (230,2,54,650.00,'2014-03-15','','0466','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (231,3,53,100.00,'2014-04-15','','0502','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (232,2,54,650.00,'2014-04-15','','0502','juan_fco_monter','2014-05-13',NULL,48);
INSERT INTO `payment` VALUES (233,3,55,0.00,'2012-11-30','','0000','juan_fco_monter','2014-05-13',NULL,49);
INSERT INTO `payment` VALUES (234,3,55,900.00,'2012-12-13','','0047','juan_fco_monter','2014-05-13',NULL,49);
INSERT INTO `payment` VALUES (235,3,55,450.00,'2013-01-15','','0066','juan_fco_monter','2014-05-13',NULL,49);
INSERT INTO `payment` VALUES (236,3,55,450.00,'2013-02-26','','0099','juan_fco_monter','2014-05-13',NULL,49);
INSERT INTO `payment` VALUES (237,3,55,0.00,'2013-03-31','Debe','0000','juan_fco_monter','2014-05-13',NULL,49);
INSERT INTO `payment` VALUES (238,3,55,900.00,'2013-04-30','','0000','juan_fco_monter','2014-05-13',NULL,49);
INSERT INTO `payment` VALUES (239,3,55,450.00,'2013-05-13','','0161','juan_fco_monter','2014-05-13',NULL,49);
INSERT INTO `payment` VALUES (240,3,55,0.00,'2013-06-30','Debe','0000','juan_fco_monter','2014-05-13',NULL,49);
INSERT INTO `payment` VALUES (241,3,55,0.00,'2013-07-31','','0000','juan_fco_monter','2014-05-13',NULL,49);
INSERT INTO `payment` VALUES (242,3,55,900.00,'2013-08-05','Remanente jun y jul','0222','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (243,3,55,0.00,'2013-08-31','Debe','0000','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (244,3,55,0.00,'2013-09-30','Debe','0000','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (245,3,55,1350.00,'2013-10-02','Remanente ago sep y actual','0283','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (246,4,55,700.00,'2013-11-11','','0329','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (247,4,55,700.00,'2013-12-16','','0374','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (248,2,55,650.00,'2014-01-16','','0404','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (249,2,55,650.00,'2014-02-14','','0432','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (250,2,55,650.00,'2014-03-10','','0454','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (251,2,55,650.00,'2014-04-07','','0489','juan_fco_monter','2014-05-14',NULL,49);
INSERT INTO `payment` VALUES (252,2,56,0.00,'2012-11-30','Debe','0000','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (253,2,56,0.00,'2012-12-31','Debe','0000','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (254,2,56,0.00,'2013-01-31','Debe','0000','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (255,2,56,2550.00,'2013-02-19','Remanente nov y dic 2012, ene 2013 y actual','0095','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (256,2,56,1950.00,'2013-03-19','Actual y adelantado abr y mayo','0120','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (257,2,56,0.00,'2013-04-30','Pagado','0000','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (258,2,56,0.00,'2013-05-31','Pagado','0000','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (259,2,56,650.00,'2013-06-19','','0184','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (260,2,56,650.00,'2013-07-13','','0205','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (261,2,56,0.00,'2013-08-31','Debe','0000','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (262,2,56,650.00,'2013-09-19','','0268','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (263,2,56,650.00,'2013-10-15','','0306','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (264,2,56,650.00,'2013-11-14','','0327','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (265,2,56,650.00,'2013-12-11','','0373','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (266,2,56,650.00,'2014-01-14','','0403','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (267,2,56,650.00,'2014-02-04','','0444','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (268,2,56,650.00,'2014-03-14','','0477','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (269,2,56,650.00,'2014-04-14','','0503','juan_fco_monter','2014-05-14',NULL,50);
INSERT INTO `payment` VALUES (270,2,58,650.00,'2012-11-06','','0005','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (271,2,58,650.00,'2012-12-04','','0023','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (272,2,58,650.00,'2013-01-18','','0060','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (273,2,58,650.00,'2013-02-26','','0080','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (274,2,58,650.00,'2013-03-14','','0115','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (275,2,58,650.00,'2013-04-08','','0132','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (276,2,58,650.00,'2013-05-14','','0163','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (277,2,58,0.00,'2013-06-30','','0000','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (278,2,58,0.00,'2013-07-31','Debe','0000','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (279,2,58,1950.00,'2013-08-12','Remanente junio, julio y actual','0236','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (280,2,58,650.00,'2013-09-09','','0256','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (281,2,58,650.00,'2013-10-15','','0302','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (282,2,58,650.00,'2013-11-15','','0336','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (283,2,58,650.00,'2013-12-12','','0363','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (284,2,58,650.00,'2014-01-14','','0397','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (285,2,58,650.00,'2014-02-15','','0433','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (286,2,58,650.00,'2014-03-15','','0463','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (287,2,58,650.00,'2014-04-15','','0501','juan_fco_monter','2014-05-14',NULL,51);
INSERT INTO `payment` VALUES (288,3,59,900.00,'2012-11-15','actual y adelantado','0015','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (289,3,59,0.00,'2012-12-31','Pagado','0000','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (290,3,59,450.00,'2013-01-03','actual','0056','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (291,3,59,450.00,'2013-01-03','Adelantado febrero','0057','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (292,3,59,0.00,'2013-02-28','Pagado','0000','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (293,3,59,450.00,'2013-03-08','actual','0106','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (294,3,59,450.00,'2013-03-08','adelantado','0107','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (295,3,59,0.00,'2013-04-30','Pagado','0000','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (296,3,59,900.00,'2013-05-06','actual y adelantado','0152','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (297,3,59,0.00,'2013-06-30','Pagado','0000','juan_fco_monter','2014-05-14',NULL,52);
INSERT INTO `payment` VALUES (298,3,59,0.00,'2013-07-31','Debe','0000','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (299,3,59,900.00,'2013-08-01','Remanente julio y actual','0224','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (300,3,59,900.00,'2013-09-05','Actual y adelantado octubre','0254','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (301,3,59,0.00,'2013-10-31','Pagado','0000','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (302,3,59,900.00,'2013-11-04','Actual y adelantado diciembre','0345','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (303,3,59,0.00,'2013-12-31','Pagado','0000','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (304,3,59,900.00,'2014-01-02','Actual y adelantado febrero','0412','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (305,3,59,0.00,'2014-02-28','Pagado','0000','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (306,3,59,900.00,'2014-03-04','Actual y adelantado abril','0474','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (307,3,59,0.00,'2014-04-30','Pagado','0000','juan_fco_monter','2014-05-16',NULL,52);
INSERT INTO `payment` VALUES (308,2,60,0.00,'2012-11-30','Debe','0000','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (309,2,60,650.00,'2012-12-15','Remanente noviembre','0043','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (310,2,60,650.00,'2012-12-19','Actual','0051','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (311,2,60,0.00,'2013-01-31','Debe','0000','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (312,2,60,650.00,'2013-02-19','Renamente enero','0096','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (313,2,60,650.00,'2013-02-28','Actual','0102','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (314,2,60,0.00,'2013-03-31','Debe','0000','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (315,2,60,650.00,'2013-04-03','Remanente marzo','0127','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (316,2,60,0.00,'2013-04-30','Debe abril','0000','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (317,2,60,650.00,'2013-05-03','Remanente abril','0148','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (318,2,60,0.00,'2013-05-31','Debe mayo','0000','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (319,2,60,0.00,'2013-06-30','Debe junio','0000','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (320,2,60,1300.00,'2013-07-26','Remanente junio y actual','0215','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (321,2,60,650.00,'2013-08-16','','0239','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (322,2,60,650.00,'2013-09-18','','0265','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (323,2,60,650.00,'2013-10-15','','0305','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (324,2,60,650.00,'2013-11-14','','0335','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (325,2,60,650.00,'2013-12-13','','0372','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (326,2,60,650.00,'2014-01-15','','0410','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (327,2,60,650.00,'2014-02-15','','0442','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (328,2,60,650.00,'2014-03-18','','0481','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (329,2,60,650.00,'2014-04-15','','0511','juan_fco_monter','2014-05-16',NULL,53);
INSERT INTO `payment` VALUES (330,2,61,1300.00,'2012-11-24','Actual y adelantado diciembre','0017','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (331,2,61,0.00,'2012-12-31','Pagado','0000','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (332,2,61,650.00,'2013-01-16','','0072','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (333,2,61,650.00,'2013-02-23','','0101','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (334,2,61,0.00,'2013-03-31','Debe ','0000','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (335,2,61,1300.00,'2013-04-20','Remanente marzo y actual','0142','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (336,2,61,650.00,'2013-05-25','','0170','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (337,2,61,0.00,'2013-06-30','debe junio','0000','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (338,2,61,0.00,'2013-07-31','Debe julio','0000','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (339,2,61,1300.00,'2013-08-24','Remanente junio y julio','0245','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (340,2,61,650.00,'2013-08-24','Actual','0246','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (341,2,61,650.00,'2013-09-21','','0271','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (342,2,61,0.00,'2013-10-31','Debe','0000','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (343,2,61,650.00,'2013-11-09','Remanente octubre','0366','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (344,2,61,1300.00,'2013-12-12','Remanente noviembre y actual diciembre','0367','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (345,2,61,650.00,'2014-01-09','','0415','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (346,2,61,0.00,'2014-02-28','Debe febrero','0000','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (347,2,61,1300.00,'2014-03-12','Remanente febrero y actual ','0461','juan_fco_monter','2014-05-16',NULL,54);
INSERT INTO `payment` VALUES (348,2,62,650.00,'2012-11-15','','0014','juan_fco_monter','2014-05-16',NULL,55);
INSERT INTO `payment` VALUES (349,2,62,650.00,'2012-12-05','','0029','juan_fco_monter','2014-05-16',NULL,55);
INSERT INTO `payment` VALUES (350,2,62,650.00,'2013-01-05','','0055','juan_fco_monter','2014-05-16',NULL,55);
INSERT INTO `payment` VALUES (351,2,62,650.00,'2013-02-19','','0097','juan_fco_monter','2014-05-16',NULL,55);
INSERT INTO `payment` VALUES (352,2,62,650.00,'2013-03-01','','0125','juan_fco_monter','2014-05-16',NULL,55);
INSERT INTO `payment` VALUES (353,2,62,650.00,'2013-04-01','','0125','juan_fco_monter','2014-05-16',NULL,55);
INSERT INTO `payment` VALUES (354,2,62,650.00,'2013-05-01','','0169','juan_fco_monter','2014-05-16',NULL,55);
INSERT INTO `payment` VALUES (355,2,62,650.00,'2013-06-01','','0185','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (356,2,62,650.00,'2013-07-06','','0201','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (357,2,62,650.00,'2013-08-01','','0220','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (358,2,62,650.00,'2013-09-02','','0249','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (359,2,62,650.00,'2013-10-02','','0282','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (360,2,62,650.00,'2013-11-01','','0313','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (361,2,62,650.00,'2013-12-02','','0355','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (362,2,62,650.00,'2014-01-02','','0409','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (363,2,62,650.00,'2014-02-04','','0419','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (364,2,62,650.00,'2014-03-03','','0450','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (365,2,62,650.00,'2014-04-01','','0490','juan_fco_monter','2014-05-17',NULL,55);
INSERT INTO `payment` VALUES (366,2,63,650.00,'2012-11-01','','0002','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (367,2,63,650.00,'2012-12-15','','0041','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (368,2,63,1300.00,'2013-01-22','Actual y adelantado enero 2013','0074','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (369,2,63,0.00,'2013-02-28','Pagado','0000','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (370,2,63,650.00,'2013-03-08','','0108','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (371,2,63,650.00,'2013-04-05','','0124','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (372,2,63,650.00,'2013-05-07','','0151','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (373,2,63,650.00,'2013-06-06','','0174','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (374,2,63,650.00,'2013-07-26','','0214','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (375,2,63,650.00,'2013-08-12','','0237','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (376,2,63,650.00,'2013-09-20','','0266','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (377,2,63,650.00,'2013-10-03','','0284','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (378,2,63,650.00,'2013-11-12','','0331','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (379,2,63,650.00,'2013-12-14','','0368','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (380,2,63,650.00,'2014-01-10','','0394','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (381,2,63,650.00,'2014-02-16','','0439','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (382,2,63,650.00,'2014-03-15','','0467','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (383,2,63,650.00,'2014-04-10','','0494','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (384,2,63,650.00,'2014-05-15','','0527','juan_fco_monter','2014-05-17',NULL,56);
INSERT INTO `payment` VALUES (385,2,64,0.00,'2012-11-30','Debe','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (386,2,64,650.00,'2012-12-08','Remanente noviembre','0033','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (387,2,64,650.00,'2012-12-21','Actual','0050','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (388,2,64,0.00,'2013-01-31','Debe','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (389,2,64,650.00,'2013-02-02','Remanente enero','0079','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (390,2,64,0.00,'2013-02-28','Debe febrero','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (391,2,64,0.00,'2013-03-31','Debe marzo','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (392,2,64,0.00,'2013-04-30','Debe abril','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (393,2,64,0.00,'2013-05-31','Debe mayo','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (394,2,64,650.00,'2013-06-10','Remanente febrero','0194','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (395,2,64,650.00,'2013-06-17','Remanente marzo','0195','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (396,2,64,0.00,'2013-06-30','Debe junio','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (397,2,64,650.00,'2013-07-18','Remanente abril','0212','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (398,2,64,0.00,'2013-07-31','Debe julio','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (399,2,64,0.00,'2013-08-31','Debe agosto','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (400,2,64,0.00,'2013-09-30','debe septiembre','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (401,2,64,1950.00,'2013-10-14','remanente may jun y jul','0294','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (402,2,64,0.00,'2013-10-31','Debe octubre','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (403,2,64,2200.00,'2013-11-08','Remanente ago sep oct y pago parcial de nov','0325','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (404,2,64,1050.00,'2013-11-14','complemento noviembre y adelantado diciembre','0334','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (405,2,64,0.00,'2013-12-31','Pagado','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (406,2,64,1300.00,'2014-01-13','actual y adelantado febrero','0396','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (407,2,64,0.00,'2014-02-28','Pagado','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (408,2,64,0.00,'2014-03-31','Debe marzo','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (409,2,64,650.00,'2014-04-01','Remanente marzo','0483','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (410,2,64,0.00,'2014-04-30','Debe abril','0000','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (411,2,64,1300.00,'2014-05-15','Remanente abril y actual','0532','juan_fco_monter','2014-05-17',NULL,57);
INSERT INTO `payment` VALUES (412,3,65,0.00,'2012-11-30','Debe noviembre','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (413,3,65,900.00,'2012-12-04','Remanente noviembre y actual','0022','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (414,3,65,450.00,'2013-01-22','','0073','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (415,3,65,450.00,'2013-02-05','','0083','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (416,3,65,0.00,'2013-03-31','Debe marzo','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (417,3,65,0.00,'2013-04-30','Debe abril','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (418,3,65,0.00,'2013-05-31','Debe mayo','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (419,3,65,580.00,'2013-06-07','Remanentes','0176','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (420,3,65,0.00,'3013-06-30','Debe junio','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (421,3,65,0.00,'2013-07-31','Debe julio','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (422,3,65,0.00,'2013-08-31','Debe agosto','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (423,3,65,0.00,'2013-09-30','Debe septiembre','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (424,3,65,800.00,'2013-10-08','Remanentes','0291','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (425,3,65,0.00,'2013-10-31','Debe octubre','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (426,3,65,0.00,'2013-11-30','Debe noviembre','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (427,3,65,1000.00,'2013-12-13','Remanentes','0365','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (428,3,65,0.00,'2013-12-31','Debe diciembre','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (429,3,65,700.00,'2014-01-17','Remanentes','0406','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (430,3,65,0.00,'2014-01-31','Debe enero','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (431,3,65,0.00,'2014-02-28','Debe febrero','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (432,3,65,500.00,'2014-03-19','Remanentes','0479','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (433,3,65,0.00,'2014-03-31','Debe marzo','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (434,3,65,0.00,'2013-04-30','Debe abril','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (435,3,65,500.00,'2014-05-06','Remanentes','0515','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (436,3,65,150.00,'2014-05-15','Remanentes','0529','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (437,3,65,0.00,'2014-05-31','Debe mayo','0000','juan_fco_monter','2014-05-17',NULL,58);
INSERT INTO `payment` VALUES (438,2,66,0.00,'2012-11-30','Debe noviembre','0000','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (439,2,66,1300.00,'2012-12-04','Remanente noviembre y actual','0026','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (440,2,66,650.00,'2013-01-16','','0071','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (441,2,66,650.00,'2013-02-13','','0093','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (442,2,66,650.00,'2013-03-19','','0119','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (443,2,66,650.00,'2013-04-20','','0141','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (444,2,66,0.00,'2013-05-31','Debe mayo','0000','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (445,2,66,1300.00,'2013-06-07','Remanente mayo y actual junio','0175','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (446,2,66,650.00,'2013-07-22','','0208','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (447,2,66,650.00,'2013-08-26','','0247','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (448,2,66,650.00,'2013-09-24','','0273','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (449,2,66,650.00,'2013-10-15','','0303','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (450,2,66,650.00,'2013-11-15','','0341','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (451,2,66,650.00,'2013-11-15','','0341','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (452,2,66,650.00,'2013-12-17','','0376','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (453,2,66,650.00,'2014-01-15','','0401','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (454,2,66,650.00,'2014-02-16','','0440','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (455,2,66,650.00,'2014-03-15','','0472','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (456,2,66,650.00,'2014-04-22','','0507','juan_fco_monter','2014-05-17',NULL,59);
INSERT INTO `payment` VALUES (457,2,67,650.00,'2012-11-10','','0008','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (458,2,67,650.00,'2012-12-15','','0042','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (459,2,67,650.00,'2013-01-15','','0067','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (460,2,67,650.00,'2013-02-05','','0082','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (461,2,67,650.00,'2013-03-02','','0104','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (462,2,67,1300.00,'2013-04-27','Actual y adelantado','0143','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (463,2,67,0.00,'2013-05-31','Pagado','0000','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (464,2,67,650.00,'2013-06-08','','0177','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (465,2,67,650.00,'2013-07-26','','0213','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (466,2,67,0.00,'2013-08-31','','0000','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (467,2,67,1300.00,'2013-09-05','','0250','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (468,2,67,650.00,'2013-10-12','','0298','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (469,2,67,1300.00,'2013-11-06','Actual y adelantado diciembre','0320','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (470,2,67,0.00,'2013-12-31','Pagado','0000','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (471,2,67,650.00,'2014-01-03','','0384','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (472,2,67,650.00,'2014-02-05','','0421','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (473,2,67,650.00,'2014-03-06','','0449','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (474,2,67,650.00,'2013-04-05','','0488','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (475,2,67,650.00,'2014-05-06','','0516','juan_fco_monter','2014-05-17',NULL,60);
INSERT INTO `payment` VALUES (476,2,68,650.00,'2012-11-13','','0012','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (477,2,68,650.00,'2012-12-04','','0025','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (478,2,68,650.00,'2013-01-11','','0063','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (479,2,68,650.00,'2013-02-05','','0081','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (480,2,68,650.00,'2013-03-08','','0110','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (481,2,68,650.00,'2013-04-12','','0134','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (482,2,68,650.00,'2013-05-06','','0150','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (483,2,68,650.00,'2013-06-21','','0189','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (484,2,68,650.00,'2013-07-12','','0203','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (485,2,68,650.00,'2013-08-09','','0228','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (486,2,68,650.00,'2013-09-05','','0251','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (487,2,68,650.00,'2013-10-11','','0293','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (488,2,68,650.00,'2013-11-02','','0316','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (489,2,68,650.00,'2013-12-04','','0353','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (490,2,68,650.00,'2014-01-03','','0385','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (491,2,68,650.00,'2014-02-08','','0426','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (492,2,68,650.00,'2014-03-05','','0448','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (493,2,68,650.00,'2014-04-09','','0491','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (494,2,68,650.00,'2014-05-09','','0522','juan_fco_monter','2014-05-17',NULL,61);
INSERT INTO `payment` VALUES (495,3,69,450.00,'2012-11-16','','0013','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (496,3,70,450.00,'2012-11-16','','0013','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (497,3,71,450.00,'2012-11-16','','0013','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (498,3,69,450.00,'2012-12-15','','0039','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (499,3,70,450.00,'2012-12-15','','0039','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (500,3,71,450.00,'2012-12-15','','0039','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (501,3,69,450.00,'2013-01-25','','0075','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (502,3,71,450.00,'2013-01-25','','0075','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (503,3,70,450.00,'2013-01-25','','0075','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (504,3,69,0.00,'2013-02-28','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (505,3,70,0.00,'2013-02-28','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (506,3,71,0.00,'2013-02-28','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (507,3,69,900.00,'2013-03-23','Remanente feb y actual','0122','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (508,3,70,900.00,'2013-03-23','Remanente feb y actual','0122','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (509,3,71,900.00,'2013-03-23','Remanente feb y actual','0122','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (510,3,69,0.00,'2013-04-30','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (511,3,70,0.00,'2013-04-30','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (512,3,71,0.00,'2013-04-30','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (513,3,69,0.00,'2013-05-31','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (514,3,70,0.00,'2013-05-31','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (515,3,71,0.00,'2013-05-31','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (516,3,69,0.00,'2013-06-30','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (517,3,71,0.00,'2013-06-30','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (518,3,70,0.00,'2013-06-30','Debe','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (519,3,69,1350.00,'2013-07-29','Remanente abr, may y jun','0217','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (520,3,70,1350.00,'2013-07-29','Remanente abr, may y jun','0217','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (521,3,71,1350.00,'2013-07-29','Remanente abr, may y jun','0217','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (522,3,69,0.00,'2013-07-31','Debe julio','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (523,3,70,0.00,'2013-07-31','Debe julio','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (524,3,71,0.00,'2013-07-31','Debe julio','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (525,3,69,0.00,'2013-08-31','Debe agosto','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (526,3,70,0.00,'2013-08-31','Debe agosto','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (527,3,71,0.00,'2013-08-31','Debe agosto','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (528,3,69,0.00,'2013-09-30','Debe sepriembre','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (529,3,70,0.00,'2013-09-30','Debe septiembre','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (530,3,71,0.00,'2013-09-30','Debe septiembre','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (531,3,69,0.00,'2013-10-31','Debe octubre','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (532,3,70,0.00,'2013-10-31','Debe octubre','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (533,3,71,0.00,'2013-10-31','Debe octubre','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (534,3,69,2250.00,'2013-11-16','Remanente jul, ago, sep oct y nov','0343','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (535,3,70,2250.00,'2013-11-16','Remanente jul, ago, sep oct y nov','0343','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (536,3,71,2250.00,'2013-11-16','Remanente jul, ago, sep oct y nov','0343','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (537,3,69,0.00,'2013-12-31','Debe diciembre','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (538,3,70,0.00,'2013-12-31','Debe diciembre','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (539,3,71,0.00,'2013-12-31','Debe diciembre','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (540,3,69,900.00,'2014-01-04','Remanenten dic 2013 y actual','0387','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (541,3,70,900.00,'2014-01-04','Remanente dic 2013 y actual','0387','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (542,3,71,900.00,'2014-01-04','Remanenten dic 2013 y actual','0387','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (543,3,69,0.00,'2014-02-28','Debe febrero','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (544,3,70,0.00,'2014-02-28','Debe febrero','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (545,3,71,0.00,'2014-02-28','Debe febrero','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (546,3,69,0.00,'2014-03-31','Debe marzo','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (547,3,70,0.00,'2014-03-31','Debe marzo','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (548,3,71,0.00,'2014-03-31','Debe marzo','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (549,3,69,0.00,'2014-04-30','Debe abril','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (550,3,70,0.00,'2014-04-30','Debe abril','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (551,3,71,0.00,'2014-04-30','Debe abril','0000','juan_fco_monter','2014-05-21',NULL,62);
INSERT INTO `payment` VALUES (552,2,72,0.00,'2012-11-30','Debe','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (553,2,72,0.00,'2012-12-31','Debe','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (554,2,72,0.00,'2013-01-31','Debe','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (555,2,72,0.00,'2013-02-28','Debe','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (556,2,72,0.00,'2013-03-31','debe','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (557,2,72,0.00,'2013-04-30','Debe','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (558,2,72,0.00,'2013-05-31','Debe','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (559,2,72,4550.00,'2013-06-14','Remanentes nov, dic 2012, ene, feb, mar, abr, may y actual','0181','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (560,2,72,0.00,'2013-07-31','Debe julio','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (561,2,72,650.00,'2013-08-23','Remanente julio','0243','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (562,2,72,0.00,'2013-08-31','Debe agosto','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (563,2,72,650.00,'2013-09-07','Remanente agosto','0255','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (564,2,72,650.00,'2013-09-28','Actual','0278','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (565,2,72,0.00,'2013-10-31','Debe octubre','0000','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (566,2,72,1300.00,'2013-11-16','Remanenten octubre y actual','0342','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (567,2,72,650.00,'2013-12-16','','0375','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (568,2,72,650.00,'2014-01-17','','0411','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (569,2,72,650.00,'2014-02-15','','0437','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (570,2,72,650.00,'2014-03-15','','0468','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (571,2,72,650.00,'2014-04-21','','0506','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (572,2,72,650.00,'2014-05-16','','0538','juan_fco_monter','2014-05-21',NULL,63);
INSERT INTO `payment` VALUES (573,7,210,500.00,'2014-05-02','','3976','jose.manuel.tello','2014-05-21',NULL,200);
INSERT INTO `payment` VALUES (574,7,136,500.00,'2014-05-02','','3977','jose.manuel.tello','2014-05-21',NULL,126);
INSERT INTO `payment` VALUES (575,7,145,500.00,'2014-05-02','','3978','jose.manuel.tello','2014-05-21',NULL,135);
INSERT INTO `payment` VALUES (576,7,248,500.00,'2014-05-02','','3970','jose.manuel.tello','2014-05-21',NULL,238);
INSERT INTO `payment` VALUES (577,7,154,500.00,'2014-05-02','','3980','jose.manuel.tello','2014-05-21',NULL,144);
INSERT INTO `payment` VALUES (578,2,73,0.00,'2012-11-30','Debe nov 12','0000','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (579,2,73,0.00,'2012-12-31','Debe dic12','0000','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (580,2,73,0.00,'2013-01-31','Deb enero 13','0000','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (581,2,73,0.00,'2013-02-28','Debe feb 13','0000','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (582,2,73,3250.00,'2013-03-14','Remanente nov dic 12 ene feb y actual 13','0114','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (583,2,73,650.00,'2013-04-20','','0139','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (584,2,73,650.00,'2013-05-10','','0155','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (585,2,73,650.00,'2013-06-29','','0196','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (586,2,73,650.00,'2013-07-22','Actual','0206','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (587,2,73,650.00,'2013-07-22','','0207','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (588,2,73,0.00,'2013-08-31','Pagado','0000','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (589,2,73,650.00,'2013-09-07','','0253','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (590,2,73,650.00,'2013-10-05','','0288','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (591,2,73,650.00,'2013-11-08','','0323','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (592,2,73,650.00,'2013-12-07','','0359','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (593,2,73,650.00,'2014-01-04','','0388','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (594,2,73,650.00,'2014-02-04','','0418','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (595,2,73,650.00,'2014-03-08','','0452','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (596,2,73,650.00,'2014-04-14','','0497','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (597,2,73,650.00,'2014-05-12','','0526','juan_fco_monter','2014-05-22',NULL,64);
INSERT INTO `payment` VALUES (598,2,74,0.00,'2012-11-30','Debe nov 12','0000','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (599,2,74,0.00,'2012-12-31','Debe dic 12','0000','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (600,2,74,0.00,'2013-01-31','Debe ene 13','0000','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (601,2,74,0.00,'2013-02-28','Debe feb 2013','0000','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (602,2,74,3250.00,'2013-03-18','Remanente nov dic 12 ene feb y actual','0118','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (603,2,74,650.00,'2013-04-05','','0128','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (604,2,74,650.00,'2013-05-06','','0149','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (605,2,74,650.00,'2013-06-08','','0178','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (606,2,74,650.00,'2013-07-08','','0202','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (607,2,74,650.00,'2013-08-10','','0233','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (608,2,74,650.00,'2013-09-06','','0252','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (609,2,74,650.00,'2013-10-12','','0297','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (610,2,74,650.00,'2013-11-09','','0328','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (611,2,74,650.00,'2013-12-13','','0364','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (612,2,74,650.00,'2014-01-15','','0399','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (613,2,74,650.00,'2014-02-08','','0427','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (614,2,74,650.00,'2014-03-11','','0459','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (615,2,74,650.00,'2014-04-14','','0498','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (616,2,74,650.00,'2014-05-13','','0528','juan_fco_monter','2014-05-22',NULL,65);
INSERT INTO `payment` VALUES (617,2,75,650.00,'2012-11-13','','0011','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (618,2,75,650.00,'2012-12-07','','0032','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (619,2,75,650.00,'2013-01-08','','0061','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (620,2,75,650.00,'2013-02-09','','0085','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (621,2,75,650.00,'2013-03-08','','0109','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (622,2,75,650.00,'2013-04-15','','0135','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (623,2,75,650.00,'2013-05-18','','0165','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (624,2,75,650.00,'2013-06-21','','0190','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (625,2,75,1950.00,'2013-07-22','','0209','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (626,2,75,0.00,'2013-08-31','Pagado','0000','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (627,2,75,0.00,'2013-09-30','Pagado','0000','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (628,2,75,650.00,'2013-10-07','','0290','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (629,2,75,650.00,'2013-11-09','','0326','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (630,2,75,650.00,'2013-12-07','','0360','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (631,2,75,650.00,'2014-01-03','','0386','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (632,2,75,650.00,'2014-02-13','','0429','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (633,2,75,650.00,'2014-03-10','','0453','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (634,2,75,650.00,'2014-04-12','','0496','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (635,2,75,650.00,'2014-05-09','','0523','juan_fco_monter','2014-05-22',NULL,66);
INSERT INTO `payment` VALUES (636,3,76,0.00,'2012-11-30','Debe nov 12','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (637,3,76,0.00,'2012-12-31','Debe dic 12','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (638,3,76,0.00,'2013-01-31','Debe ene 13','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (639,3,76,0.00,'2013-02-28','Debe feb 2013','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (640,3,76,0.00,'2013-03-31','Debe mar 2013','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (641,3,76,0.00,'2013-04-30','Debe abr 2013','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (642,3,76,0.00,'2013-05-31','Debe may 2013','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (643,3,76,0.00,'2013-06-30','Debe jun 2013','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (644,3,76,0.00,'2013-07-31','Debe jul 2013','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (645,3,76,0.00,'2013-08-31','Debe ago  2013','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (646,3,76,0.00,'2013-09-30','Debe sep 2013','0000','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (647,4,76,4950.00,'2013-10-01','nov dic 2012 ene feb mar abr may jun jul ago seo y actual','0279','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (648,4,76,700.00,'2013-11-01','','0315','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (649,4,76,700.00,'2013-12-06','','0357','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (650,4,76,700.00,'2014-01-02','','0383','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (651,4,76,700.00,'2014-02-05','','0423','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (652,4,76,700.00,'2014-03-03','','0456','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (653,4,76,700.00,'2014-04-01','','0485','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (654,4,76,700.00,'2014-05-06','','0519','juan_fco_monter','2014-05-23',NULL,67);
INSERT INTO `payment` VALUES (655,3,77,0.00,'2012-11-30','Debe nov 12','0000','juan_fco_monter','2014-05-23',NULL,68);
INSERT INTO `payment` VALUES (656,3,77,900.00,'2012-12-13','Remanente nov y actual','0048','juan_fco_monter','2014-05-23',NULL,68);
INSERT INTO `payment` VALUES (657,4,77,700.00,'2013-01-15','','0065','juan_fco_monter','2014-05-23',NULL,68);
INSERT INTO `payment` VALUES (658,4,77,700.00,'2013-02-26','','0098','juan_fco_monter','2014-05-23',NULL,68);
INSERT INTO `payment` VALUES (659,4,77,0.00,'2013-03-31','Debe mar 2013','0000','juan_fco_monter','2014-05-23',NULL,68);
INSERT INTO `payment` VALUES (660,4,77,1400.00,'2013-04-30','Remanentea mar y actual','0147','juan_fco_monter','2014-05-23',NULL,68);
INSERT INTO `payment` VALUES (661,2,73,650.00,'2013-05-13','','0162','juan_fco_monter','2014-05-23',NULL,64);
INSERT INTO `payment` VALUES (662,2,77,650.00,'2013-05-13','','0162','juan_fco_monter','2014-05-23',NULL,68);
INSERT INTO `payment` VALUES (663,2,77,0.00,'2013-06-30','Debe jun 2013','0000','juan_fco_monter','2014-05-23',NULL,68);
INSERT INTO `payment` VALUES (664,2,77,0.00,'2013-07-31','Debe jul 2013','0000','juan_fco_monter','2014-05-23',NULL,68);
INSERT INTO `payment` VALUES (665,2,77,1300.00,'2013-08-05','Remanente jun y jul','0223','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (666,2,77,0.00,'2013-08-31','Debe agosto','0000','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (667,2,77,1300.00,'2013-09-13','Remanente agosto y actual','0260','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (668,2,77,650.00,'2013-10-14','','0299','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (669,2,77,650.00,'2013-11-13','','0333','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (670,2,77,650.00,'2013-12-06','','0351','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (671,2,77,650.00,'2014-01-08','','0390','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (672,2,77,650.00,'2014-02-12','','0428','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (673,2,77,650.00,'2014-03-07','','0451','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (674,2,77,650.00,'2014-04-10','','0493','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (675,2,77,650.00,'2014-05-14','','0525','juan_fco_monter','2014-05-27',NULL,68);
INSERT INTO `payment` VALUES (676,2,78,0.00,'2012-11-30','Debe nov 2012','0000','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (677,2,78,0.00,'2012-12-31','Debe dic 2012','0000','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (678,2,78,0.00,'2013-01-31','Debe ene 2013','0000','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (679,2,78,0.00,'2013-02-28','Debe feb 2013','0000','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (680,2,78,3250.00,'2013-03-12','remanente nov y dic 2012 ene feb y actual','0113','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (681,2,78,650.00,'2013-04-27','','0145','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (682,2,78,650.00,'2013-05-24','','0167','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (683,2,78,650.00,'2013-06-22','','0191','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (684,2,78,650.00,'2013-07-13','','0204','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (685,2,78,650.00,'2013-08-24','','0244','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (686,2,78,650.00,'2013-09-10','','0257','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (687,2,78,650.00,'2013-10-03','','0285','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (688,2,78,650.00,'2013-11-05','','0318','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (689,2,78,650.00,'2013-12-06','','0352','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (690,2,78,650.00,'2014-01-03','','0381','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (691,2,78,650.00,'2014-02-04','','0417','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (692,2,78,650.00,'2014-03-01','','0446','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (693,2,78,650.00,'2014-04-03','','0484','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (694,2,78,650.00,'2014-05-01','','0512','juan_fco_monter','2014-05-27',NULL,69);
INSERT INTO `payment` VALUES (695,3,79,0.00,'2012-11-30','Debe parcial de nov 2012','0000','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (696,3,79,0.00,'2012-12-31','Debe parcial de dic 2012','0000','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (697,3,79,0.00,'2013-01-31','Debe ene 2013','0000','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (698,3,79,0.00,'2013-02-28','Debe feb 2013','0000','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (699,3,79,1650.00,'2013-03-21','Remanente parcial de nov y dic 2012, ene feb y actual 2013','0121','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (700,3,79,450.00,'2013-04-19','','0138','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (701,3,79,450.00,'2013-05-08','','0154','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (702,3,79,450.00,'2013-06-05','','0173','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (703,3,79,0.00,'2013-07-31','Debe julio 2013','0000','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (704,3,79,450.00,'2013-08-01','Remanente julio','0218','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (705,3,79,450.00,'2013-08-23','','0242','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (706,3,79,400.00,'2013-09-02','deposito, pago parcial','0262','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (707,3,79,50.00,'2013-09-19','Complemento, efectivo','0263','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (708,3,79,450.00,'2013-10-07','','0296','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (709,3,79,450.00,'2013-11-05','','0317','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (710,3,79,450.00,'2013-12-06','','0358','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (711,3,79,450.00,'2014-01-06','','0389','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (712,3,79,450.00,'2014-02-06','','0425','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (713,3,79,450.00,'2014-03-21','','0458','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (714,3,79,0.00,'2014-04-30','Debe abril 2014','0000','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (715,3,79,900.00,'2014-05-06','Remanente abril y actual','0517','juan_fco_monter','2014-05-28',NULL,70);
INSERT INTO `payment` VALUES (716,2,80,0.00,'2012-11-30','Debe nov 2012','0000','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (717,2,80,650.00,'2012-12-01','Remanente nov 2012','0001','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (718,2,80,0.00,'2012-12-31','Debe dic 2012','0000','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (719,2,80,1300.00,'2013-01-26','Remanente dic 2012 y actual','0077','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (720,2,80,0.00,'2013-02-28','Debe feb 2013','0000','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (721,2,80,1300.00,'2013-03-16','Remanente feb y actual','0117','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (722,2,80,650.00,'2013-04-27','','0144','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (723,2,80,650.00,'2013-05-10','','0156','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (724,2,80,650.00,'2013-06-18','','0192','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (725,2,80,650.00,'2013-07-25','','0218','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (726,2,80,0.00,'2013-08-31','Debe ago 2013','0000','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (727,2,80,0.00,'2013-09-30','Debe sep 2013','0000','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (728,2,80,650.00,'2013-10-15','Actual','0301','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (729,2,80,650.00,'2013-08-30','Deposito identificado','0000','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (730,2,80,650.00,'2013-08-30','Deposito identificado pago sep 2013','0000','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (731,2,80,650.00,'2013-11-29','','0349','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (732,2,80,650.00,'2013-12-01','','0350','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (733,2,80,650.00,'2014-01-09','','0414','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (734,2,80,0.00,'2014-02-28','Debe feb 2014','0000','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (735,2,80,650.00,'2014-03-13','Remanente feb 2014','0469','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (736,2,80,650.00,'2014-03-14','Actual','0470','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (737,2,80,0.00,'2014-04-30','Debe abr 2014','0000','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (738,2,80,650.00,'2014-05-07','Remanente abr 2014','0520','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (739,2,80,650.00,'2014-05-16','Actual','0545','juan_fco_monter','2014-05-28',NULL,71);
INSERT INTO `payment` VALUES (740,2,81,0.00,'2012-11-30','Debe nov 2012','0000','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (741,2,81,1300.00,'2012-12-13','Remanente nov y actual','0036','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (742,2,81,0.00,'2013-01-31','Debe ene 2013','0000','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (743,2,81,1300.00,'2013-02-16','Remanente ene y actual','0094','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (744,2,81,0.00,'2013-03-31','Debe mar 2013, dice que no le correspodne por que llego a vivir despues del 20 del mes','00000','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (745,2,81,650.00,'2013-04-06','','0131','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (746,2,81,650.00,'2013-05-24','','0166','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (747,2,81,0.00,'2013-06-30','Debe jun 2013','0000','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (748,2,81,0.00,'2014-07-31','Debe jul 2013','0000','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (749,2,81,1950.00,'2013-08-10','Remanente jun jul y actual','0234','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (750,2,81,650.00,'2013-09-21','','0270','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (751,2,81,650.00,'2013-10-24','','0308','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (752,2,81,650.00,'2013-11-15','','0332','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (753,2,81,650.00,'2013-12-15','','0371','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (754,2,81,650.00,'2014-01-16','','0405','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (755,2,81,650.00,'2014-02-15','','0436','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (756,2,81,650.00,'2014-03-18','','0475','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (757,2,81,500.00,'2014-04-15','Pago parcial adeuda 150.00','0499','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (758,2,81,500.00,'2014-05-12','Pago parcial adeuda 150.00','0524','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (759,2,81,300.00,'2014-05-24','complemento de abr y may 2013','0547','juan_fco_monter','2014-05-29',NULL,72);
INSERT INTO `payment` VALUES (760,2,82,650.00,'2012-11-10','','0006','juan_fco_monter','2014-05-29',NULL,73);
INSERT INTO `payment` VALUES (761,2,82,650.00,'2012-12-15','','0040','juan_fco_monter','2014-05-29',NULL,73);
INSERT INTO `payment` VALUES (762,2,82,650.00,'2013-01-15','','0069','juan_fco_monter','2014-05-29',NULL,73);
INSERT INTO `payment` VALUES (763,2,82,650.00,'2013-02-09','','0086','juan_fco_monter','2014-05-29',NULL,73);
INSERT INTO `payment` VALUES (764,2,82,650.00,'2013-03-15','','0116','juan_fco_monter','2014-05-29',NULL,73);
INSERT INTO `payment` VALUES (765,2,82,650.00,'2013-04-20','','0140','juan_fco_monter','2014-05-29',NULL,73);
INSERT INTO `payment` VALUES (766,2,82,0.00,'2013-05-31','Debe mayo 2013','0000','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (767,2,82,650.00,'2013-06-21','Remanente mayo','0187','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (768,2,82,650.00,'2013-06-21','Actual','0188','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (769,2,82,0.00,'2013-07-31','Debe jul 2013','0000','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (770,2,82,650.00,'2013-08-05','Remanente jul 2013','0225','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (771,2,82,650.00,'2013-08-05','','0226','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (772,2,82,650.00,'2013-09-14','','0261','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (773,2,82,650.00,'2013-10-14','','0300','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (774,2,82,650.00,'2013-11-15','','0338','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (775,2,82,650.00,'2013-12-15','','0370.00','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (776,2,82,650.00,'2014-01-15','','0400','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (777,2,82,650.00,'2014-02-16','','0441','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (778,2,82,650.00,'2014-03-15','','0473','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (779,2,82,0.00,'2014-04-30','Debe abr 2014','0000','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (780,2,82,650.00,'2014-05-19','Remanente abr','0542','juan_fco_monter','2014-05-30',NULL,73);
INSERT INTO `payment` VALUES (781,2,83,0.00,'2012-11-30','Debe nov 2012','0000','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (782,2,83,650.00,'2012-12-04','Remanente nov ','0030','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (783,2,83,0.00,'2012-12-31','Debe dic 2013','0000','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (784,2,83,1300.00,'2013-01-26','Remanente dic 2012 y actual','0078','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (785,2,83,650.00,'2013-02-21','','0100','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (786,2,83,0.00,'2013-03-31','Debe mar 2013','0000','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (787,2,83,0.00,'2013-04-30','Debe abr 2013','0000','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (788,2,83,1950.00,'2013-05-23','Remanente mar abr y actual','0171','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (789,2,83,0.00,'2013-06-30','Debe jun 2013','0000','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (790,2,83,1300.00,'2013-07-23','Remanente jun y actual','0210','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (791,2,83,650.00,'2013-08-22','','0241','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (792,2,83,650.00,'2013-09-27','','0274','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (793,2,83,650.00,'2013-10-29','','0311','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (794,2,83,650.00,'2013-11-26','','0346','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (795,2,83,650.00,'2013-12-17','','0379','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (796,2,83,650.00,'2014-01-17','','0407','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (797,2,83,650.00,'2014-02-26','','0445','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (798,2,83,650.00,'2014-03-21','','0478','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (799,2,83,650.00,'2014-04-23','','0509','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (800,2,83,650.00,'2014-05-19','','0543','juan_fco_monter','2014-05-30',NULL,74);
INSERT INTO `payment` VALUES (801,2,84,650.00,'2012-11-13','','0009','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (802,3,85,450.00,'2012-11-13','','0009','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (803,3,86,450.00,'2012-11-13','','0009','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (804,2,84,650.00,'2012-12-11','','0034','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (805,3,85,450.00,'2012-12-11','','0034','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (806,3,86,450.00,'2012-12-11','','0034','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (807,2,84,650.00,'2013-01-11','','0062','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (808,3,85,450.00,'2013-01-11','','0062','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (809,3,86,450.00,'2013-01-11','','0062','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (810,2,84,650.00,'2013-02-13','','0092','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (811,3,85,450.00,'2013-02-13','','0092','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (812,3,86,450.00,'2013-02-13','','0092','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (813,2,84,650.00,'2013-03-08','','0105','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (814,3,85,450.00,'2013-03-08','','0105','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (815,3,86,450.00,'2013-03-08','','0102','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (816,2,84,650.00,'2013-04-01','','0123','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (817,3,85,450.00,'2013-04-01','','0123','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (818,3,86,450.00,'2013-04-01','','0123','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (819,2,84,650.00,'2013-05-08','','0153','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (820,3,85,450.00,'2013-05-08','','0153','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (821,3,86,450.00,'2013-05-08','','0153','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (822,2,84,650.00,'2013-06-05','','0172','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (823,3,85,450.00,'2013-06-05','','0172','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (824,3,86,450.00,'2013-06-05','','0172','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (825,2,84,650.00,'2013-07-05','','0198','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (826,3,85,450.00,'2013-07-05','','0198','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (827,3,86,450.00,'2013-07-05','','0198','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (828,2,84,650.00,'2013-08-02','','0219','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (829,3,85,450.00,'2013-08-02','','0198','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (830,3,86,450.00,'2013-08-02','','0219','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (831,2,84,650.00,'2013-09-02','','0248','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (832,3,85,450.00,'2013-09-02','','0248','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (833,3,86,450.00,'2013-09-02','','0248','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (834,2,84,650.00,'2013-10-04','','0295','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (835,3,85,450.00,'2013-10-04','','0295','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (836,3,86,450.00,'2013-10-04','','0295','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (837,2,84,650.00,'2013-11-01','','0314','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (838,3,85,450.00,'2013-11-01','','0314','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (839,3,86,450.00,'2013-11-01','','0314','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (840,2,84,650.00,'2013-12-06','','0356','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (841,3,85,450.00,'2013-12-06','','0356','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (842,3,86,450.00,'2013-12-06','','0356','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (843,2,84,650.00,'2014-01-02','','0382','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (844,3,85,450.00,'2014-01-02','','0382','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (845,3,86,450.00,'2014-01-02','','0382','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (846,2,84,650.00,'2014-02-05','','0424','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (847,3,85,450.00,'2014-02-05','','0424','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (848,3,86,450.00,'2014-02-05','','0424','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (849,2,84,650.00,'2014-03-03','','0457','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (850,3,85,450.00,'2014-03-03','','0457','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (851,3,86,450.00,'2014-03-03','','0457','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (852,2,84,650.00,'2014-04-01','','0486','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (853,3,85,450.00,'2014-04-01','','0486','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (854,3,86,450.00,'2014-04-01','','0486','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (855,2,84,650.00,'2014-05-06','','0518','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (856,3,85,450.00,'2014-05-06','','0518','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (857,3,86,450.00,'2014-05-06','','0518','juan_fco_monter','2014-05-30',NULL,75);
INSERT INTO `payment` VALUES (858,7,248,500.00,'2014-05-02','','3979','jose.manuel.tello','2014-06-11',NULL,238);
INSERT INTO `payment` VALUES (859,7,162,500.00,'2014-05-02','','3981','jose.manuel.tello','2014-06-11',NULL,152);
INSERT INTO `payment` VALUES (860,7,163,500.00,'2014-05-02','','3982','jose.manuel.tello','2014-06-11',NULL,153);
INSERT INTO `payment` VALUES (861,7,98,500.00,'2014-05-02','','3983','jose.manuel.tello','2014-06-11',NULL,87);
INSERT INTO `payment` VALUES (862,7,219,500.00,'2014-05-03','','3984','jose.manuel.tello','2014-06-11',NULL,209);
INSERT INTO `payment` VALUES (863,2,44,650.00,'2014-06-16','','0567','juan_fco_monter','2014-06-27',NULL,40);
INSERT INTO `payment` VALUES (865,5,299,270.00,'2013-02-01','','2177','juan_fco_monter','2014-07-08',NULL,283);
INSERT INTO `payment` VALUES (870,13,265,200.00,'2013-02-26','feb 2013','2171','juan_fco_monter','2014-08-03',0,255);
INSERT INTO `payment` VALUES (871,5,265,270.00,'2013-03-08','mar 2013','2276','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (872,5,265,270.00,'2013-04-04','Abr 2013','2358','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (873,5,265,270.00,'2013-05-06','Mayo 2013','2476','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (874,5,265,270.00,'2013-06-03','Junio 2013','2533','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (875,5,265,270.00,'2013-07-03','Julio 2013','2657','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (876,5,265,270.00,'2013-08-07','Agosto 2013','2823','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (877,5,265,320.00,'2013-09-10','Sep 2013','2969','juan_fco_monter','2014-08-03',0,255);
INSERT INTO `payment` VALUES (878,5,265,270.00,'2013-10-07','Octubre 2013','3082','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (879,5,265,270.00,'2013-11-08','Noviembre 2013','3238','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (880,5,265,270.00,'2013-12-08','Diciembre 2013','3340','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (881,5,265,270.00,'2014-01-07','Enero 2014','3464','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (882,5,265,270.00,'2014-02-06','Feb 2014','3546','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (883,5,265,270.00,'2014-03-08','Marzo 2014','3693','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (884,5,265,270.00,'2014-04-07','Abril 2014','3762','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (885,5,265,0.00,'2014-05-30','No Pago','0000','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (886,5,265,0.00,'2014-06-30','No Pago','0000','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (887,5,265,0.00,'2014-07-31','No Pago','0000','juan_fco_monter','2014-07-12',NULL,255);
INSERT INTO `payment` VALUES (889,5,266,270.00,'2013-03-04','Marzo 2013','2212','juan_fco_monter','2014-07-12',NULL,256);
INSERT INTO `payment` VALUES (890,5,266,270.00,'2013-05-06','Mayo 2013','2464','juan_fco_monter','2014-07-12',NULL,256);
INSERT INTO `payment` VALUES (891,5,266,270.00,'2013-04-01','Abril 2013','2302','juan_fco_monter','2014-07-12',NULL,256);
INSERT INTO `payment` VALUES (892,5,266,270.00,'2013-06-03','Junio 2013','2532','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (893,5,266,270.00,'2013-07-03','Julio 2013','2660','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (894,5,266,270.00,'2013-08-06','Agosto 2013','2815','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (895,5,266,270.00,'2013-09-09','Septiembre 2013','2956','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (896,5,266,270.00,'2013-10-03','Octubre 2013','3026','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (897,5,266,270.00,'2013-11-08','Noviembre 2013','3237','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (898,5,266,270.00,'2013-12-05','Diciembre 2013','3312','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (899,5,266,270.00,'2014-01-07','Enero 2014','3456','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (900,5,266,270.00,'2014-02-08','Febrero 2014','3578','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (901,5,266,270.00,'2014-03-10','Marzo 2014','3699','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (902,5,266,270.00,'2014-04-10','Abril 2014','3793','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (903,5,266,270.00,'2014-05-08','Mayo 2014','3885','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (904,5,266,270.00,'2014-06-14','Junio 2014','4010','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (905,5,266,270.00,'2014-07-04','Julio 2014','4061','juan_fco_monter','2014-07-13',NULL,256);
INSERT INTO `payment` VALUES (906,13,267,200.00,'2013-02-14','Febrero pagado','2150','juan_fco_monter','2014-08-05',0,257);
INSERT INTO `payment` VALUES (907,5,267,270.00,'2013-02-01','Marzo 2013','9186','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (908,5,267,270.00,'2013-04-08','Abril 2013','2388','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (909,5,267,270.00,'2013-05-02','Mayo 2013','2424','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (910,5,267,270.00,'2013-06-04','Junio 2013','2556','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (911,5,267,270.00,'2013-07-06','Julio 2013','2687','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (912,5,267,270.00,'2013-08-02','Agosto 2013','2757','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (913,5,267,270.00,'2013-09-07','Septiembre 2013\r\n','2937','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (914,5,267,270.00,'2013-10-03','Octubre 2013','3034','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (915,5,267,270.00,'2013-11-07','Noviembre 2013','3215','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (916,5,267,270.00,'2013-12-05','Diciembre 2013','3309','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (917,5,267,270.00,'2014-01-06','Enero 2014','3405','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (918,5,267,0.00,'2014-02-08','No Pago','0000','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (919,5,267,0.00,'2014-03-31','No Pago','0000','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (920,5,267,810.00,'2014-04-10','Abril 2014','3789','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (921,5,267,0.00,'2014-05-31','No Pago','0000','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (922,5,267,0.00,'2014-06-30','No Pago','0000','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (923,5,267,810.00,'2014-07-04','Mayo,Junio y Julio 2014','4067','juan_fco_monter','2014-07-13',NULL,257);
INSERT INTO `payment` VALUES (924,13,268,200.00,'2013-02-19','Febrero 2013','2161','juan_fco_monter','2014-08-05',0,258);
INSERT INTO `payment` VALUES (925,5,268,270.00,'2013-03-04','Marzo 2013','2217','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (926,5,268,270.00,'2013-00-04','Abril 2013','2352','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (927,5,268,0.00,'2013-05-31','No Pago','0000','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (928,5,268,640.00,'2013-06-20','Junio 2013','2620','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (929,5,268,270.00,'2013-07-08','Julio 2013','2717','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (930,5,268,270.00,'2013-08-08','Agosto 2013','2832','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (931,5,268,270.00,'2013-09-05','Septiembre 2013','2924','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (932,5,268,270.00,'2013-10-08','Octubre 2013','3100','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (933,5,268,270.00,'2013-11-07','Noviembre 2013','3214','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (934,5,268,270.00,'2013-12-06','Diciembre 2013','3314','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (935,5,268,270.00,'2014-01-07','Enero 2014','3457','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (936,5,268,270.00,'2014-02-08','Febrero 2014','3575','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (937,5,268,270.00,'2014-03-11','Marzo 2014','3751','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (938,5,268,270.00,'2014-04-10','Abril 2014','3798','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (939,5,268,320.00,'2014-05-12','Mayo 2014','3904','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (940,5,268,270.00,'2014-06-07','junio 2014\r\n','3981','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (941,5,268,270.00,'2014-07-08','Julio 2014','4103','juan_fco_monter','2014-07-13',NULL,258);
INSERT INTO `payment` VALUES (942,13,269,200.00,'2013-02-19','Febrero 2013','2162','juan_fco_monter','2014-08-05',0,259);
INSERT INTO `payment` VALUES (943,5,269,270.00,'2013-03-07','Marzo 2013','2263','juan_fco_monter','2014-07-14',NULL,259);
INSERT INTO `payment` VALUES (944,5,269,320.00,'2013-04-09','Abril 2013','2397','juan_fco_monter','2014-07-14',NULL,259);
INSERT INTO `payment` VALUES (945,5,269,270.00,'2013-05-04','Mayo 2013','2449','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (946,5,269,320.00,'2013-09-09','Septiembre 2013','2959','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (947,5,269,270.00,'2013-08-06','Agosto 2013','2814','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (948,5,269,270.00,'2013-07-06','Julio 2013','2696','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (949,5,269,270.00,'2013-06-01','Junio 2013','2527','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (950,5,269,270.00,'2013-10-07','Octubre 2013','3084','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (951,5,269,270.00,'2013-11-01','Noviembre 2013','3130','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (952,5,269,270.00,'2013-12-07','Diciembre 2013','3325','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (953,5,269,270.00,'2014-01-07','Enero 2013','3459','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (954,5,269,270.00,'2014-02-06','Febrero 2014','3544','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (955,5,269,270.00,'2014-03-08','Marzo 2013','3692','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (956,5,269,270.00,'2014-04-08','Abril 2014','3784','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (957,5,269,270.00,'2014-05-06','Mayo 2014','3847','juan_fco_monter','2014-08-23',0,259);
INSERT INTO `payment` VALUES (958,5,269,320.00,'2014-06-12','Junio 2014','4004','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (959,5,269,270.00,'2014-07-04','Julio 2014','4056','juan_fco_monter','2014-07-15',NULL,259);
INSERT INTO `payment` VALUES (961,5,270,270.00,'2013-03-06','Marzo 2013','2256','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (962,5,270,270.00,'2013-04-08','Abril 2013','2384','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (963,5,270,270.00,'2013-05-08','Mayo 2013','2503','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (964,5,270,270.00,'2013-06-08','Junio 2013','2607','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (965,5,270,270.00,'2013-07-08','Julio 2013','2718','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (966,5,270,320.00,'2013-08-13','Agosto 2013','2850','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (967,5,270,270.00,'2013-09-06','Septiembre 2013','2930','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (968,5,270,270.00,'2013-10-07','Octubre 2013','3086','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (969,5,270,270.00,'2013-11-08','Noviembre 2013','3228','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (970,5,270,270.00,'2013-12-07','Diciembre 2013','3331','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (971,5,270,270.00,'2014-01-07','Enero 2014','3474','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (972,5,270,270.00,'2014-02-07','Febrero 2014','3569','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (973,5,270,0.00,'2014-03-31','No Pago','0000','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (974,5,270,270.00,'2014-04-10','Abril 2014','3791','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (975,5,270,270.00,'2014-05-06','Mayo 2014','2856','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (976,5,270,270.00,'2014-06-07','Junio 2014','3978','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (977,5,270,270.00,'2014-07-07','Julio 2014','4071','juan_fco_monter','2014-07-15',NULL,260);
INSERT INTO `payment` VALUES (978,13,271,200.00,'2013-02-22','Febrero 2013','2172','juan_fco_monter','2014-08-16',0,261);
INSERT INTO `payment` VALUES (979,5,271,320.00,'2013-03-13','Marzo 2013','2282','juan_fco_monter','2014-07-15',NULL,261);
INSERT INTO `payment` VALUES (980,5,271,270.00,'2013-04-08','Abril 2013','2390','juan_fco_monter','2014-07-15',NULL,261);
INSERT INTO `payment` VALUES (981,5,271,320.00,'2013-05-13','Mayo 2013','2507','juan_fco_monter','2014-07-15',NULL,261);
INSERT INTO `payment` VALUES (982,5,271,0.00,'2013-06-30','No Pago\r\n','0000','juan_fco_monter','2014-07-15',NULL,261);
INSERT INTO `payment` VALUES (983,5,271,590.00,'2013-07-01','Julio 2013','2630','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (984,5,271,270.00,'2013-08-08','Agosto 2013\r\n','2842','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (985,5,271,320.00,'2013-09-09','Septiembre 2013','2952','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (986,5,271,320.00,'2013-10-15','Octubre 2013','3121','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (987,5,271,320.00,'2013-12-10','Noviembre 2013','3351','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (988,5,271,320.00,'2013-12-10','Diciembre 2013','3352','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (989,5,271,320.00,'2014-01-27','Enero 2014','3485','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (990,5,271,270.00,'2014-02-04','Febrero 2014','3516','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (991,5,271,320.00,'2014-04-08','Marzo 2014','3780','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (992,5,271,0.00,'2014-04-30','No Pago','0000','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (993,5,271,590.00,'2014-05-12','Abril Y Mayo 2014','3905','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (994,5,271,0.00,'2014-06-30','No Pago','0000','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (995,5,271,0.00,'2014-07-31','No Pago','0000','juan_fco_monter','2014-07-16',NULL,261);
INSERT INTO `payment` VALUES (997,5,272,270.00,'2013-03-04','Marzo 2013\r\n','2226','juan_fco_monter','2014-07-16',NULL,262);
INSERT INTO `payment` VALUES (999,5,272,270.00,'2013-04-04','Abril 2013\r\n','2356','juan_fco_monter','2014-07-16',NULL,262);
INSERT INTO `payment` VALUES (1000,5,272,270.00,'2013-05-02','Mayo 2013','2427','juan_fco_monter','2014-07-16',NULL,262);
INSERT INTO `payment` VALUES (1001,5,400,270.00,'2013-03-02','Marzo pagado','2199','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1003,5,400,270.00,'2013-04-04','Abril pagado','2350','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1004,5,272,270.00,'2013-06-06','junio 2013','2580','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1005,5,400,270.00,'2013-05-07','Mayo pagado','2493','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1006,5,271,0.00,'2014-08-31','Debe','0000','juan_fco_monter','2014-08-16',0,261);
INSERT INTO `payment` VALUES (1007,5,400,270.00,'2013-06-04','Junio pagado','2558','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1008,5,272,270.00,'2013-07-06','julio 2013','2699','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1009,5,400,270.00,'2013-07-02','Julio pagado','2646','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1010,5,272,270.00,'2013-08-05','agosto 2013','2794','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1011,5,400,270.00,'2013-08-03','Agosto pagado','2774','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1012,5,400,270.00,'2013-09-05','Septiembre pagado','2918','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1013,5,272,320.00,'2013-09-09','septiembre 2013\r\n ','2967','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1014,5,400,270.00,'2013-10-07','Octubre pagado','3095','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1015,5,272,270.00,'2013-10-07','octubre pagado','3072','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1016,5,400,270.00,'2013-11-06','Noviembre pagado\r\n','3203','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1017,5,400,270.00,'2013-12-07','Diciembre pagado','3333','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1018,5,272,270.00,'2013-11-12','noviembre pagado','3241','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1019,5,400,270.00,'2014-01-08','Enero pagado','3448','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1020,5,272,270.00,'2013-12-07','diciembre pagado','3333','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1021,5,400,270.00,'2014-02-06','Febrero pagado','3533','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1022,5,400,270.00,'2014-03-04','Marzo pagado','3650','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1023,5,272,270.00,'2014-01-07','enero pagado','3451','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1024,5,400,270.00,'2014-04-01','Abril pagado','3722','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1025,5,400,270.00,'2014-05-08','Mayo pagado','3886','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1026,5,272,270.00,'2014-02-08','febrero pagado','3576','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1027,5,400,270.00,'2014-06-06','Junio pagado','3976','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1028,5,272,270.00,'2014-03-08','marzo pagado','3690','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1029,5,400,270.00,'2014-07-04','Julio pagado','4053','juan_fco_monter','2014-07-16',NULL,382);
INSERT INTO `payment` VALUES (1030,5,272,270.00,'2014-04-04','abril pagado','3739','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1031,13,399,200.00,'2013-02-10','Febrero pagado','2138','juan_fco_monter','2014-08-19',0,381);
INSERT INTO `payment` VALUES (1032,5,272,270.00,'2014-05-08','mayo pagado','3884','juan_fco_monter','2014-08-24',0,262);
INSERT INTO `payment` VALUES (1033,5,399,270.00,'2013-03-08','Marzo pagado','2269','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1034,13,276,200.00,'2013-02-20','Febrero 2013','2165','juan_fco_monter','2014-08-16',0,263);
INSERT INTO `payment` VALUES (1035,5,399,270.00,'2013-04-15','Abril pagado','2401','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1036,5,276,270.00,'2013-03-08','Marzo 2013','2271','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1037,5,399,270.00,'2013-03-02','Marzo pagado','2423','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1038,5,276,270.00,'2013-04-08','Abril 2013','2381','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1039,5,399,270.00,'2013-06-07','Junio pagado','2598','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1040,5,399,270.00,'2013-07-08','Julio pagado','2721','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1041,5,276,270.00,'2013-05-04','Mayo 2013','2441','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1042,5,399,270.00,'2013-08-07','Agosto pagado','2828','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1043,5,276,270.00,'2013-06-01','Junio 2013','2528','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1044,5,399,270.00,'2013-09-09','Septiembre pagado','2955','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1045,5,276,270.00,'2013-07-08','Julio 2013\r\n','2715','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1046,5,399,270.00,'2013-10-07','Octubre pagado','3090','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1048,5,276,270.00,'2013-08-05','Agosto 2013','2806','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1049,5,276,320.00,'2013-09-09','Septiembre 2013','2950','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1050,5,276,270.00,'2013-10-07','Octubre 2013','3071','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1051,5,276,320.00,'2013-11-01','Noviembre 2013','3244','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1052,5,276,270.00,'2013-12-06','Diciembre 2013','3315','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1053,5,276,270.00,'2014-01-06','Enero 2014','3437','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1054,5,399,270.00,'2013-12-07','Diciembre pagado','3336','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1055,5,399,270.00,'2014-01-06','Enero pagado','3403','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1056,5,399,270.00,'2014-02-07','Febrero pagado','3560','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1057,5,276,270.00,'2014-02-08','Febrero 2014','3579','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1058,5,399,270.00,'2014-03-07','Marzo pagado','3670','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1059,5,276,270.00,'2014-03-10','Marzo 2014','3697','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1060,5,399,270.00,'2014-04-07','Abril pagado','3760','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1061,5,399,270.00,'2014-05-06','Mayo pagado','3859','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1062,5,399,270.00,'2014-06-05','Junio pagado','3965','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1064,5,399,270.00,'2014-07-04','Julio pagado','4054','juan_fco_monter','2014-07-16',NULL,381);
INSERT INTO `payment` VALUES (1066,5,398,270.00,'2013-03-02','Marzo pagado','2195','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1067,5,276,270.00,'2014-05-08','Mayo 2014','3906','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1068,5,398,270.00,'2013-04-03','Abril pagado','2342','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1069,5,398,270.00,'2013-05-06','Mayo pagado','2475','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1070,5,398,270.00,'2013-06-03','Junio pagado','2529','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1071,5,276,270.00,'2014-06-06','Junio 2014','3974','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1072,5,398,270.00,'2013-07-08','Julio pagado','2704','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1073,5,398,270.00,'2013-08-07','Julio pagado','2819','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1074,5,276,270.00,'2014-07-08','Julio 2014','4098','juan_fco_monter','2014-07-16',NULL,263);
INSERT INTO `payment` VALUES (1075,5,398,270.00,'2013-09-05','Septiembre pagado','2911','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1076,5,398,270.00,'2013-10-08','Octubre pagado','3107','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1077,5,398,270.00,'2013-11-08','Noviembre pagado','3226','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1078,5,398,320.00,'2013-12-09','Diciembre pagado','3346','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1079,5,398,270.00,'2014-01-07','Enero pagado','3469','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1080,5,398,270.00,'2014-02-08','Febrero pagado','3583','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1081,5,398,270.00,'2014-03-08','Marzo pagado','3688','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1082,5,398,270.00,'2014-04-10','Abril pagado','3796','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1083,5,398,270.00,'2014-05-08','Mayo pagado\r\n','3890','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1084,5,398,270.00,'2014-06-11','Junio pagado','4003','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1085,5,398,270.00,'2014-07-07','Julio pagado','4078','juan_fco_monter','2014-07-16',NULL,380);
INSERT INTO `payment` VALUES (1087,5,397,270.00,'2013-03-06','Marzo pagado','2252','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1088,5,397,270.00,'2013-04-01','Abril pagado','2308','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1089,5,397,270.00,'2013-05-03','Mayo pagado','2438','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1090,5,397,270.00,'2013-06-04','Junio pagado','2561','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1091,5,397,270.00,'2013-07-02','Julio pagado','2637','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1092,5,397,270.00,'2013-08-02','Agosto pagado','2753','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1093,5,397,270.00,'2013-09-05','Septiembre pagado','2906','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1094,5,397,270.00,'2013-10-03','Octubre pagado','3030','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1095,5,397,270.00,'2013-11-05','Noviembre pagado','3167','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1096,5,397,270.00,'2013-12-05','Diciembre pagado','3297','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1097,5,397,270.00,'2014-01-06','Enero pagado','3432','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1098,5,397,270.00,'2014-02-03','Febrero pagado','3515','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1099,5,397,270.00,'2014-03-01','Marzo pagado','3607','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1100,5,397,270.00,'2014-04-03','Abril pagado','3730','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1101,13,277,200.00,'2013-02-15','Febrero 2013','2152','juan_fco_monter','2014-08-16',0,264);
INSERT INTO `payment` VALUES (1102,5,397,270.00,'2014-05-06','Mayo pagado','3849','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1103,5,397,270.00,'2014-06-03','Junio pagado','3944','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1104,5,277,270.00,'2013-03-05','Marzo 2013','2247','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1105,5,397,270.00,'2014-07-01','Julio pagado','4025','juan_fco_monter','2014-07-16',NULL,379);
INSERT INTO `payment` VALUES (1107,5,277,270.00,'2013-04-01','Abril 2013','2311','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1108,5,396,270.00,'2013-03-04','Marzo pagado','2225','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1109,5,396,270.00,'2013-04-04','Abril pagado','2345','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1110,5,277,270.00,'2013-05-04','Mayo 2013','2451','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1111,5,396,270.00,'2013-05-08','Mayo pagado','2497','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1112,5,396,270.00,'2013-06-03','Junio pagado','2545','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1113,5,396,270.00,'2013-07-04','Julio pagado','2670','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1114,5,396,270.00,'2013-08-05','Agosto pagado','2788','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1115,5,396,270.00,'2013-09-03','Septiembre pagado','2890','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1116,5,396,270.00,'2013-10-07','Octubre pagado','3081','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1117,5,396,270.00,'2013-11-01','Noviembre pagado','3131','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1118,5,396,270.00,'2013-12-07','Diciembre pagado','3337','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1119,5,396,270.00,'2014-01-04','Enero pagado','3398','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1120,5,396,270.00,'2014-02-13','Febrero pagado','3590','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1121,5,396,270.00,'2014-03-06','Marzo pagado','3663','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1122,5,396,270.00,'2014-04-03','Abril pagado','3733','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1123,5,396,270.00,'2014-05-01','Mayo pagado','3816','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1124,5,396,270.00,'2014-06-05','Junio pagado\r\n','3961','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1125,5,396,270.00,'2014-06-27','Julio pagado','4015','juan_fco_monter','2014-07-16',NULL,378);
INSERT INTO `payment` VALUES (1127,5,395,270.00,'2013-03-04','Marzo pagado','2214','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1128,5,395,270.00,'2013-03-30','Abril pagado','2292','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1129,5,395,270.00,'2013-05-02','Mayo pagado','2431','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1130,5,395,270.00,'2013-06-01','Junio pagado','2519','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1131,5,395,270.00,'2013-07-02','Pagado','2647','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1132,5,395,270.00,'2013-08-01','Agosto 2013','2749','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1133,5,395,270.00,'2013-09-02','Septiembre pagado','2863','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1134,5,395,270.00,'2013-10-07','Octubre pagado','3092','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1135,5,395,270.00,'2013-11-07','Noviembre pagado','3223','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1136,5,395,320.00,'2013-12-19','Diciembre pagado','3363','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1137,5,395,320.00,'2014-01-31','Enero pagado','3488','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1138,5,395,270.00,'2014-01-31','Febrero pagado','3489','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1139,5,395,270.00,'2014-03-01','Marzo pagado','3612','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1140,5,395,270.00,'2014-04-03','Abril pagado','3731','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1141,5,395,270.00,'2014-05-08','Mayo pagado','3879','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1142,5,395,270.00,'2014-06-03','Junio pagado','3940','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1143,5,395,270.00,'2014-07-07','Julio pagado','4073','juan_fco_monter','2014-07-16',NULL,377);
INSERT INTO `payment` VALUES (1144,13,394,0.00,'2013-02-28','Febrero debe','0000','juan_fco_monter','2014-08-19',0,376);
INSERT INTO `payment` VALUES (1145,5,394,0.00,'2013-03-31','Marzo debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1146,5,394,0.00,'2013-04-30','Abril debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1147,5,394,0.00,'2013-05-31','Mayo debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1148,5,394,0.00,'2013-06-30','Junio debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1149,5,394,0.00,'2013-07-31','Julio debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1150,5,394,0.00,'2013-08-31','Agosto debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1151,5,394,0.00,'2013-09-30','Septiembre debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1152,5,394,0.00,'2013-10-31','Octubre debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1153,5,394,0.00,'2013-11-30','Noviembre debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1154,5,394,0.00,'2013-12-31','Diciembre debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1155,5,394,0.00,'2014-01-31','Enero debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1156,5,394,0.00,'2014-02-28','Febrero debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1157,5,394,0.00,'2014-03-31','Marzo debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1158,5,394,0.00,'2014-04-30','Abril debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1159,5,394,0.00,'2014-05-31','Mayo debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1160,5,394,0.00,'2014-06-30','Junio debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1161,5,394,0.00,'2014-07-31','Julio debe','0000','juan_fco_monter','2014-07-16',NULL,376);
INSERT INTO `payment` VALUES (1162,13,393,200.00,'2013-02-18','Febrero pagado','2150','juan_fco_monter','2014-08-19',0,375);
INSERT INTO `payment` VALUES (1163,5,393,270.00,'2013-03-07','Marzo pagado','2257','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1164,5,393,270.00,'2013-04-08','Abril pagado','2393','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1165,5,393,270.00,'2013-05-07','Mayo pagado','2490','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1166,5,393,270.00,'2013-06-07','Junio pagado','2599','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1167,5,393,270.00,'2013-07-08','Julio pagado\r\n','2701','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1168,5,393,270.00,'2013-08-08','Agosto pagado','2841','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1169,5,393,0.00,'2013-09-30','Septiembre debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1170,5,393,0.00,'2013-10-31','Octubre debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1171,5,393,0.00,'2013-11-30','Noviembre debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1172,5,393,0.00,'2013-12-31','Diciembre debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1173,5,393,0.00,'2014-01-31','Enero debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1174,5,393,0.00,'2014-02-28','Febrero debe\r\n','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1175,5,393,0.00,'2014-03-31','Marzo debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1176,5,393,0.00,'2014-04-30','Abril debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1177,5,393,0.00,'2014-05-31','Mayo debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1178,5,393,0.00,'2014-06-30','Junio debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1179,5,393,0.00,'2014-07-31','Julio debe','0000','juan_fco_monter','2014-07-16',NULL,375);
INSERT INTO `payment` VALUES (1180,5,277,270.00,'2013-06-07','junio 2013\r\n','2597','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1181,5,277,270.00,'2013-07-06','Julio 2013','2689','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1182,5,277,270.00,'2013-08-06','Agosto 2013','2811','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1183,5,277,270.00,'2013-09-07','Septiembre 2013','2940','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1184,5,277,270.00,'2013-10-07','Octubre 2013','3078','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1185,5,277,270.00,'2013-11-08','Noviembre 2013','3239','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1186,5,277,270.00,'2013-12-07','Diciembre 2013','3330','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1187,5,277,270.00,'2014-01-02','Enero 2014','3382','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1188,5,277,270.00,'2014-02-06','Febrero 2014','3539','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1189,5,277,270.00,'2014-03-04','Marzo 2014','3635','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1190,5,277,320.00,'2014-04-12','Abril 2014','3803','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1191,5,277,270.00,'2014-05-06','Mayo 2014','3845','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1192,5,277,0.00,'2014-06-30','No Pago','0000','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1193,5,277,0.00,'2014-07-31','Julio 2014','0000','juan_fco_monter','2014-07-16',NULL,264);
INSERT INTO `payment` VALUES (1195,5,392,270.00,'2013-03-04','Marzo pagado','2228','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1196,5,392,270.00,'2013-04-02','Abril pagado','2334','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1197,5,392,270.00,'2013-05-02','Mayo pagado','2419','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1198,5,392,270.00,'2013-06-04','Junio pagado','2564','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1199,5,392,270.00,'2013-07-08','Julio pagado','2724','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1200,5,392,270.00,'2013-08-05','Agosto pagado','2807','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1201,5,392,270.00,'2013-09-05','Septiembre pagado','2919','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1202,5,392,270.00,'2013-10-08','Octubre pagado','3109','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1203,5,392,270.00,'2013-11-01','Noviembre pagado','3133','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1204,5,392,270.00,'2013-12-02','Diciembre pagado','3268','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1205,5,392,270.00,'2014-01-02','Enero pagado','3381','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1206,5,392,270.00,'2014-02-03','Febrero pagado','3505','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1208,5,392,270.00,'2014-04-01','Abril pagado','3713','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1209,5,392,270.00,'2014-03-03','Marzo pagado','3622','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1210,5,392,270.00,'2014-05-12','Mayo pagado','3903','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1211,5,392,270.00,'2014-06-08','Junio pagado','3990','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1212,5,392,270.00,'2014-07-01','Julio pagado','4023','juan_fco_monter','2014-07-16',NULL,374);
INSERT INTO `payment` VALUES (1213,13,391,200.00,'2013-02-12','Fabrero pagado','2142','juan_fco_monter','2014-08-19',0,373);
INSERT INTO `payment` VALUES (1214,5,391,270.00,'2013-03-05','Marzo pagado','2239','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1215,5,391,270.00,'2013-04-08','Abril pagado','2395','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1216,5,391,270.00,'2013-05-07','Mayo pagado\r\n','2484','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1217,5,391,270.00,'2013-06-04','Junio pagado','2553','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1218,5,391,270.00,'2013-07-04','Julio pagado','2665','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1219,5,391,270.00,'2013-08-05','Agosto pagado','2793','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1220,5,391,270.00,'2013-09-03','Septiembre pagado','2875','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1221,5,391,270.00,'2013-10-03','Octubre pagado','3028','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1222,5,391,270.00,'2013-11-05','Noviembre pagado','3172','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1223,5,391,270.00,'2013-12-05','Diciembre pagado','3308','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1224,5,391,270.00,'2014-01-06','Enero pagado','3412','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1225,5,391,270.00,'2014-02-06','Febrero pagado','3534','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1226,5,391,270.00,'2014-03-04','Marzo pagado','3642','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1227,5,391,270.00,'2014-04-03','Abril pagado','3738','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1228,5,391,270.00,'2014-05-06','Mayo pagado','3854','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1229,5,391,270.00,'2014-06-06','Junio pagado','3967','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1230,5,391,270.00,'2014-07-03','Julio pagado','4034','juan_fco_monter','2014-07-16',NULL,373);
INSERT INTO `payment` VALUES (1232,5,390,270.00,'2013-03-06','Marzo pagado','2254','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1233,5,390,270.00,'2013-04-06','Abril pagado','2371','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1234,5,390,270.00,'2013-05-06','Mayo pagado','2458','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1235,5,390,270.00,'2013-06-13','Junio pagado','2613','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1236,5,390,270.00,'2013-07-04','Julio pagado','2668','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1237,5,390,270.00,'2013-08-06','Agosto pagado','2816','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1238,5,390,320.00,'2013-09-14','Septiembre pagado','2974','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1239,5,390,320.00,'2013-10-17','Octubre pagado','3122','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1240,5,390,270.00,'2013-11-01','Noviembre pagado','3247','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1241,5,390,270.00,'2013-12-14','Diciembre pagado','3358','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1242,5,390,270.00,'2014-01-17','Enero pagado','3481','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1243,5,390,270.00,'2014-02-17','Febrero pagado','3592','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1244,5,390,320.00,'2014-04-15','Marzo pagado','3805','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1245,5,390,320.00,'2014-04-26','Abril pagado','3812','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1246,5,390,270.00,'2014-05-17','Mayo pagado','3912','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1247,5,390,320.00,'2014-06-27','Junio pagado','4016','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1248,5,390,0.00,'2014-07-31','Julio debe','0000','juan_fco_monter','2014-07-16',NULL,372);
INSERT INTO `payment` VALUES (1249,13,389,200.00,'2013-02-21','Febrero pagado','2170','juan_fco_monter','2014-08-19',0,371);
INSERT INTO `payment` VALUES (1250,5,389,270.00,'2013-03-08','Marzo pagado','2278','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1251,5,389,270.00,'2013-04-08','Abril pagado','2391','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1252,5,389,270.00,'2013-05-08','Mayo pagado','2506','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1253,5,389,270.00,'2013-07-08','Julio pagado','2700','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1254,5,389,280.00,'2013-07-08','Junio pagado','2713','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1255,5,389,270.00,'2013-08-08','Agosto pagado','2838','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1256,5,389,270.00,'2013-09-09','Septiembre pagado','2949','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1257,5,389,270.00,'2013-10-04','Octubre pagado','3047','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1258,5,389,270.00,'2013-11-07','Noviembre pagado','3219','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1259,5,389,270.00,'2013-12-02','Diciembre pagado','3265','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1260,5,389,270.00,'2014-01-07','Enero pagado','3467','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1261,5,389,270.00,'2014-02-07','Febrero pagado','3562','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1262,5,389,270.00,'2014-03-01','Marzo pagado','3608','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1263,5,389,270.00,'2014-04-10','Abril pagado','3790','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1264,5,389,0.00,'2014-05-31','Mayo debe','0000','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1265,5,389,0.00,'2014-06-30','Junio debe','0000','juan_fco_monter','2014-07-16',NULL,371);
INSERT INTO `payment` VALUES (1266,5,389,590.00,'2014-07-21','junio y julio pagado','4119','juan_fco_monter','2014-08-22',0,371);
INSERT INTO `payment` VALUES (1268,5,388,0.00,'2013-03-31','Marzo debe','0000','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1269,5,388,0.00,'2013-04-30','Abril debe','0000','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1270,5,388,0.00,'2013-05-31','Mayo debe','0000','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1271,5,388,0.00,'2013-06-30','Julio debe','0000','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1272,5,388,0.00,'2013-07-31','Julio debe','0000','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1273,5,388,1620.00,'2013-09-01','Agosto pagado','2926','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1274,5,388,270.00,'2013-09-08','Septiembre pagado','2965','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1275,5,388,270.00,'2013-10-07','Octubre pagado','3073','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1276,5,388,270.00,'2013-11-06','Noviembre pagado','3199','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1277,5,388,270.00,'2013-12-05','Diciembre pagado','3296','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1278,5,388,320.00,'2014-01-17','Enero pagado','3482','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1279,5,388,270.00,'2014-02-05','Febrero pagado','3530','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1280,5,388,270.00,'2014-03-04','Marzo pagado','3634','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1281,5,388,0.00,'2014-04-30','Abril debe','0000','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1283,5,388,0.00,'2014-05-31','Mayo debe','0000','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1284,5,388,0.00,'2014-06-30','Junio debe','0000','juan_fco_monter','2014-07-16',NULL,370);
INSERT INTO `payment` VALUES (1285,5,388,640.00,'2014-07-31','Abril y mayo 2014 debe julio','4115','juan_fco_monter','2014-08-22',0,370);
INSERT INTO `payment` VALUES (1287,5,387,270.00,'2013-03-07','Marzo pagado','2264','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1288,5,387,270.00,'2013-04-06','Abril pagado','2375','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1289,5,387,270.00,'2013-05-06','Mayo pagado','2467','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1290,5,387,270.00,'2013-06-07','Junio pagado','2595','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1291,5,387,270.00,'2013-07-08','Julio pagado','2706','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1292,5,387,270.00,'2013-08-06','Agosto pagado','2810','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1293,5,387,270.00,'2013-09-07','Septiembre pagado','2947','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1294,5,387,270.00,'2013-10-07','Octubre pagado','3074','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1295,5,387,270.00,'2013-11-12','Noviembre pagado','3242','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1296,5,387,270.00,'2013-12-07','Diciembre pagado','3327','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1297,5,387,270.00,'2014-01-04','Enero pagado','3395','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1298,5,387,270.00,'2014-02-08','Febrero pagado','3573','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1299,5,387,270.00,'2014-03-03','Marzo pagado','3615','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1300,5,387,270.00,'2014-04-08','Abril pagado','3775','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1301,5,387,270.00,'2014-05-08','Mayo pagado','3874','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1302,5,387,270.00,'2014-06-09','Junio pagado','3999','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1303,5,387,270.00,'2014-07-03','Julio pagado','4049','juan_fco_monter','2014-07-17',NULL,369);
INSERT INTO `payment` VALUES (1305,5,386,270.00,'2013-03-04','Marzo pagado','2211','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1306,5,386,270.00,'2013-04-02','Abril pagado','2337','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1307,5,386,270.00,'2013-04-29','Abril pagado','2408','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1308,5,386,270.00,'2013-06-05','Junio pagado','2573','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1309,5,386,270.00,'2013-07-03','Julio pagado','2663','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1310,5,386,270.00,'2013-08-01','Agosto pagado','2751','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1311,5,386,270.00,'2013-09-03','Septiembre pagado','2886','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1312,5,386,270.00,'2013-09-30','Octubre pagado','2993','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1313,5,386,270.00,'2013-11-04','Noviembre pagado','3151','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1314,5,386,270.00,'2013-12-01','Diciembre pagado','3254','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1315,5,386,270.00,'2014-01-06','Enero pagado','3431','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1316,5,386,270.00,'2014-01-31','Febrero pagado','3191','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1317,5,386,270.00,'2014-03-04','Marzo pagado','3640','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1318,5,386,270.00,'2014-03-31','Abril pagado','3707','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1319,5,386,270.00,'2014-05-08','Mayo pagado','3897','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1320,5,386,270.00,'2014-06-02','Junio pagado','3926','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1321,5,386,270.00,'2014-07-07','Julio pagado','4080','juan_fco_monter','2014-07-17',NULL,368);
INSERT INTO `payment` VALUES (1322,13,385,200.00,'2013-03-23','Febrero pagado','2290','juan_fco_monter','2014-08-19',0,367);
INSERT INTO `payment` VALUES (1323,5,385,0.00,'2013-03-31','Marzo debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1324,5,385,0.00,'2013-04-30','Abril debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1325,5,385,0.00,'2013-05-31','Mayo debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1326,5,385,0.00,'2013-06-30','Junio debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1327,5,385,1600.00,'2013-07-11','mar, abr may jun y jul pagado','2727','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1328,5,385,0.00,'2013-08-31','Agosto debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1329,5,385,540.00,'2013-09-30','Ago y sep pagado','3118','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1330,5,385,0.00,'2013-10-31','Octubre debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1331,5,385,0.00,'2013-11-30','Noviembre debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1332,5,385,0.00,'2013-12-31','Diciembre debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1333,5,385,0.00,'2014-01-31','Enero debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1334,5,385,0.00,'2014-02-28','Febrero debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1335,5,385,0.00,'2014-03-31','Marzo debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1336,5,385,0.00,'2014-04-30','Abril debe','0000','juan_fco_monter','2014-07-17',NULL,367);
INSERT INTO `payment` VALUES (1338,5,385,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-08-23',0,367);
INSERT INTO `payment` VALUES (1339,5,385,0.00,'2014-06-30','Junio  debe','0000','juan_fco_monter','2014-08-23',0,367);
INSERT INTO `payment` VALUES (1341,5,384,270.00,'2013-03-02','Marzo pagado','2198','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1342,5,384,270.00,'2013-04-02','Abril pagado','2321','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1343,5,384,270.00,'2013-05-03','Mayo pagado','2432','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1344,5,384,270.00,'2013-06-05','Junio pagado','2571','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1345,5,384,270.00,'2013-07-01','Julio pagado','2628','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1346,5,384,270.00,'2013-07-30','Agosto pagado','2735','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1347,5,384,270.00,'2013-09-04','Septiembre pagado','2902','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1348,5,384,270.00,'2013-09-28','Octubre pagado','2990','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1349,5,384,270.00,'2013-11-02','Noviembre pagado','3141','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1350,5,384,270.00,'2013-12-02','Diciembre pagado','3259','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1351,5,384,270.00,'2013-12-26','Enero pagado','3366','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1353,5,384,270.00,'2014-01-25','Febrero pagado','3484','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1354,5,384,270.00,'2014-03-01','Marzo pagado','3613','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1355,5,384,270.00,'2014-04-01','Abril pagado','3712','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1356,5,384,270.00,'2014-05-02','Mayo pagado','3831','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1357,5,384,270.00,'2014-06-05','Junio pagado','3957','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1358,5,384,270.00,'2014-07-08','Julio pagado','4085','juan_fco_monter','2014-07-17',NULL,366);
INSERT INTO `payment` VALUES (1359,13,278,200.00,'2013-01-18','Febrero 2013','2157','juan_fco_monter','2014-08-16',0,265);
INSERT INTO `payment` VALUES (1360,5,278,270.00,'2013-03-06','Marzo 2013','2250','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1361,5,278,270.00,'2013-04-04','Abril 2013','2354','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1362,5,278,0.00,'2013-05-31','No Pago','0000','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1363,5,278,640.00,'2013-06-24','Mayo Y Junio 2013','2621','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1364,5,278,270.00,'2013-07-08','Julio 2013','2714','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1365,5,278,270.00,'2013-08-05','Agosto 2013','2805','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1366,5,278,270.00,'2013-09-07','Septiembre 2013','2942','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1367,5,278,270.00,'2013-09-28','Octubre 2013','2992','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1369,5,383,270.00,'2013-03-01','Marzo pagado','2178','juan_fco_monter','2014-08-24',0,365);
INSERT INTO `payment` VALUES (1370,5,383,270.00,'2013-04-18','Abril pagado','2403','juan_fco_monter','2014-07-18',NULL,365);
INSERT INTO `payment` VALUES (1371,5,383,270.00,'2013-05-15','Mayo pagado','2510','juan_fco_monter','2014-07-18',NULL,365);
INSERT INTO `payment` VALUES (1372,5,383,0.00,'2013-06-30','Junio debe','0000','juan_fco_monter','2014-07-18',NULL,365);
INSERT INTO `payment` VALUES (1373,5,383,0.00,'2013-07-31','Julio debe','0000','juan_fco_monter','2014-07-18',NULL,365);
INSERT INTO `payment` VALUES (1374,5,278,270.00,'2013-11-05','Noviembre 2013','3195','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1375,5,278,270.00,'2013-12-03','Diciembre 2013','3285','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1376,5,278,320.00,'2014-02-19','Enero 2014','3594','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1377,5,278,320.00,'2014-06-21','Febrero 2014','4013','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1378,5,278,270.00,'2014-03-08','Marzo 2014','3683','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1379,5,278,0.00,'2014-04-30','No Pago','0000','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1380,5,278,0.00,'2014-05-31','No Pago','0000','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1381,5,278,0.00,'2014-06-30','No Pago','0000','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1382,5,278,0.00,'2014-07-31','No Pago','0000','juan_fco_monter','2014-07-18',NULL,265);
INSERT INTO `payment` VALUES (1384,5,279,270.00,'2013-03-04','Marzo 2013','2215','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1385,5,279,270.00,'2013-04-08','Abril 2013','2380','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1386,5,279,270.00,'2013-05-07','Mayo 2013','2480','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1387,5,279,270.00,'2013-06-05','Junio 2013','2570','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1388,5,279,270.00,'2013-07-05','Julio 2013','2676','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1389,5,279,270.00,'2013-08-08','Agosto 2013','2837','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1390,5,279,270.00,'2013-08-03','Agosto 2013','2888','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1391,5,279,270.00,'2013-10-08','Octubre 2013','3102','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1392,5,279,270.00,'2013-11-08','Noviembre 2013','3232','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1393,5,279,270.00,'2013-12-04','Diciembre 2013','3289','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1394,5,279,270.00,'2014-01-07','Enero 2014','3470','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1395,5,279,270.00,'2014-02-07','Febrero 2014','3571','juan_fco_monter','2014-07-18',NULL,266);
INSERT INTO `payment` VALUES (1396,5,279,270.00,'2014-03-08','marzo 2014','3686','juan_fco_monter','2014-07-19',NULL,266);
INSERT INTO `payment` VALUES (1397,5,279,270.00,'2014-04-07','abril 2014','3767','juan_fco_monter','2014-07-19',NULL,266);
INSERT INTO `payment` VALUES (1398,5,279,270.00,'2014-05-08','Mayo 2014','3883','juan_fco_monter','2014-07-19',NULL,266);
INSERT INTO `payment` VALUES (1399,5,279,270.00,'2014-06-05','Junio 2014','3960','juan_fco_monter','2014-07-19',NULL,266);
INSERT INTO `payment` VALUES (1400,5,279,270.00,'2014-07-08','Julio 2014','4092','juan_fco_monter','2014-07-19',NULL,266);
INSERT INTO `payment` VALUES (1401,13,280,200.00,'2013-02-15','Febreo 2013','2151','juan_fco_monter','2014-08-16',0,267);
INSERT INTO `payment` VALUES (1402,5,280,0.00,'2013-03-31','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1403,5,280,0.00,'2013-04-30','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1404,5,280,0.00,'2013-05-31','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1405,5,280,0.00,'2014-06-30','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1406,5,280,0.00,'2014-07-31','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1407,5,280,0.00,'2013-08-31','No pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1408,5,280,0.00,'2013-09-30','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1409,5,280,2560.00,'2013-10-10','Marzo a Octubre 2013','3058','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1410,5,280,0.00,'2013-11-30','No Pago\r\n','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1411,5,280,0.00,'2013-12-31','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1412,5,280,0.00,'2014-01-31','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1413,5,280,640.00,'2013-02-13','Enero Febrero 2014','3112','juan_fco_monter','2014-08-23',0,267);
INSERT INTO `payment` VALUES (1414,5,280,0.00,'2014-03-31','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1415,5,280,0.00,'2014-04-30','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1416,5,280,2140.00,'2014-05-21','Noviembre Y Diciembre 2013, Marzo. Abril, Mayo, Junio, Julio 2014','3591','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1417,5,280,0.00,'2014-06-30','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1418,5,280,0.00,'2014-07-31','No Pago','0000','juan_fco_monter','2014-07-19',NULL,267);
INSERT INTO `payment` VALUES (1420,5,281,270.00,'2013-03-08','Marzo 2013','2267','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1421,5,281,270.00,'2013-04-05','Abril 2013','2364','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1422,5,281,270.00,'2013-05-08','Mayo 2013','2499','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1423,5,281,270.00,'2013-06-05','Junio 2013','2569','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1424,5,281,270.00,'2013-07-05','Julio 2013','2680','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1425,5,281,270.00,'2013-08-07','Agosto 2013','2827','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1426,5,281,270.00,'2013-09-05','Septiembre 2013','2909','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1427,5,281,270.00,'2013-10-05','Octubre 2013','3069','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1428,5,281,270.00,'2013-11-05','Noviembre 2013','3171','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1429,5,281,270.00,'2013-12-05','Diciembre 2013','3305','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1430,5,281,270.00,'2014-01-08','Enero 2014','3450','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1431,5,281,270.00,'2014-02-07','Febrero 2014','3567','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1432,5,281,270.00,'2014-03-07','Marzo 2014','3676','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1433,5,281,270.00,'2014-04-08','Abril 2014','3778','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1434,5,281,270.00,'2014-05-03','Mayo 2014','3835','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1435,5,281,270.00,'2014-06-01','Junio 2014','3922','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1436,5,281,270.00,'2014-07-08','Julio 2014','4094','juan_fco_monter','2014-07-19',NULL,268);
INSERT INTO `payment` VALUES (1438,5,282,270.00,'2013-03-07','marzo pagado','2258','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1439,5,282,270.00,'2013-04-06','abril pagado','2367','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1440,5,282,270.00,'2013-05-06','Mayo pagado','2459','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1441,5,282,270.00,'2013-06-08','Junio 2013','2609','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1442,5,282,270.00,'2013-07-08','Julio 2013','2702','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1443,5,282,270.00,'2013-08-08','Agosto 2013','2833','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1444,5,282,320.00,'2013-09-24','Septiembre 2013','298i1','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1445,5,282,270.00,'2013-09-24','Octubre 2013','2982','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1446,5,282,270.00,'2013-11-08','Noviembre 2013','3235','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1447,5,282,270.00,'2013-11-08','Diciembre 2013','3236','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1448,5,282,270.00,'2014-01-06','Enero 2014','3411','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1449,5,282,320.00,'2014-02-24','Febrero 2014','3597','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1450,5,282,270.00,'2014-02-24','Marzo 2014','3598','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1451,5,282,270.00,'2014-02-24','Abril 2014','3599','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1452,5,282,270.00,'2014-05-06','Mayo 2014','38i52','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1453,5,282,270.00,'2014-05-06','Junio 2014','3860','juan_fco_monter','2014-07-19',NULL,269);
INSERT INTO `payment` VALUES (1454,5,282,540.00,'2014-07-11','Julio agosto 2014','4101','juan_fco_monter','2014-08-17',0,269);
INSERT INTO `payment` VALUES (1455,5,383,540.00,'2013-08-01','Julio  y agosto pagado','2759','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1456,5,383,270.00,'2013-10-03','Septiembre pagado','3063','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1457,5,383,270.00,'2013-10-03','Octubre pagado','3029','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1458,5,383,0.00,'2013-11-30','Noviembre debe','0000','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1459,5,383,0.00,'2013-12-31','Diciembre debe','0000','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1460,5,383,0.00,'2014-01-31','Enero debe','0000','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1461,6,383,500.00,'2014-02-21','nov y dic 2014 pago parcial','3596','juan_fco_monter','2014-08-24',0,365);
INSERT INTO `payment` VALUES (1462,5,383,0.00,'2014-02-28','Febrero debe','0000','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1463,6,383,300.00,'2014-03-08','pago parcial a cuenta','3689','juan_fco_monter','2014-08-24',0,365);
INSERT INTO `payment` VALUES (1464,5,383,0.00,'2014-03-31','Marzo debe','0000','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1465,6,383,445.00,'2014-04-24','Pago parcial a cuenta','3814','juan_fco_monter','2014-08-24',0,365);
INSERT INTO `payment` VALUES (1466,5,383,0.00,'2014-04-14','Abril debe','0000','juan_fco_monter','2014-08-24',0,365);
INSERT INTO `payment` VALUES (1467,5,383,0.00,'2014-05-31','Mayo debe','0000','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1468,5,383,700.00,'2014-06-10','Pago a cuenta ','4012','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1469,5,383,0.00,'2014-07-31','Julio debe','0000','juan_fco_monter','2014-07-21',NULL,365);
INSERT INTO `payment` VALUES (1472,5,382,270.00,'2013-03-01','marzo pagado','2184','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1473,5,382,270.00,'2013-04-02','abril pagado','2327','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1474,5,382,270.00,'2013-05-02','mayo pagado','2414','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1476,5,382,270.00,'2013-05-31','junio pagado','2514','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1477,5,382,270.00,'2013-07-02','julio pagado','2638','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1479,5,382,270.00,'2013-08-05','agosto pagado','2787','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1480,5,382,270.00,'2013-09-03','septiembre pagado','2871','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1481,5,382,270.00,'2013-10-03','octubre pagado','3024','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1482,5,382,270.00,'2013-11-04','noviembre pagado','3150','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1483,5,382,270.00,'2013-12-07','diciembre pagado\r\n','3334','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1484,5,382,320.00,'2014-01-11','enero pagado','3477','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1485,5,382,270.00,'2014-02-06','febrero pagado','3542','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1486,5,382,270.00,'2014-03-01','marzo pagado','3606','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1487,5,382,270.00,'2014-04-07','abril pagado','3758','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (1488,13,381,200.00,'2013-02-28','Febrero pagado','2169','juan_fco_monter','2014-08-19',0,363);
INSERT INTO `payment` VALUES (1489,5,381,270.00,'2013-03-06','Marzo pagado','2251','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1490,5,381,270.00,'2013-04-01','Abril pagado','2315','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1491,5,381,270.00,'2013-05-07','Mayo pagado','2489','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1492,5,381,270.00,'2013-06-07','Junio pagado','2592','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1493,5,381,270.00,'2013-07-08','julio pagado','2722','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1494,5,381,270.00,'2013-08-03','Agosto pagado','2770','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1495,5,381,320.00,'2013-09-10','septiembre pagado','2971','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1496,5,381,270.00,'2013-10-05','octubre pagado','3066','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1497,5,381,270.00,'2013-11-06','noviembre pagado','3207','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1498,5,381,270.00,'2013-12-07','diciembre pagado','3328','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1499,5,381,270.00,'2014-01-08','enero pagado','3440','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1500,5,381,270.00,'2014-02-01','febrero pagado','3501','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1501,5,381,270.00,'2014-03-06','marzo pagado','3664','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1502,5,381,270.00,'2014-04-17','abril pagado','4001','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1503,5,381,270.00,'2014-05-03','mayo pagado','3820','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1504,5,381,270.00,'2014-06-07','junio pagado','3987','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1505,5,381,270.00,'2014-07-08','julio pagado','4099','juan_fco_monter','2014-07-21',NULL,363);
INSERT INTO `payment` VALUES (1506,13,380,200.00,'2013-02-10','febrero pagado','2140','juan_fco_monter','2014-08-19',0,362);
INSERT INTO `payment` VALUES (1507,5,380,270.00,'2013-03-08','marzo pagado','2270','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1508,5,380,270.00,'2013-04-08','abril pagado','2389','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1509,13,283,200.00,'2013-02-15','Febrero 2013','2153','juan_fco_monter','2014-08-17',0,270);
INSERT INTO `payment` VALUES (1510,5,283,270.00,'2013-03-04','Marzo 2013\r\n','2224','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1511,5,283,270.00,'2013-04-11','Abril 2013','2399','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1512,5,283,320.00,'2013-09-30','Mayo 2013','2994','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1513,5,283,320.00,'2013-07-01','Junio 2013','2629','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1514,5,283,270.00,'2013-07-08','Julio 2013','2707','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1515,5,283,270.00,'2013-08-07','Agosto 2013','2822','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1516,5,283,320.00,'2013-09-09','Septiembre 2013','2961','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1517,5,283,270.00,'2013-10-02','Octubre 2013','3022','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1518,5,283,270.00,'2013-11-05','Noviembre 2013','3186','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1519,5,283,270.00,'2013-12-06','Diciembre 2013','3318','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1520,5,283,270.00,'2014-01-06','Enero 2014','3415','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1521,5,283,320.00,'2014-02-10','Febrero 2014','3589','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1522,5,283,270.00,'2014-03-04','Marzo 2014','3645','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1523,5,283,270.00,'2014-04-07','Abril 2014','3764','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1524,5,283,270.00,'2014-05-22','Mayo 2014','3914','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1525,5,283,0.00,'2014-06-30','No Pago','0000','juan_fco_monter','2014-07-21',NULL,270);
INSERT INTO `payment` VALUES (1526,5,283,540.00,'2014-07-14','Junio y julio','4106','juan_fco_monter','2014-08-17',0,270);
INSERT INTO `payment` VALUES (1528,5,284,270.00,'2013-03-04','Marzo 2013','2221','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1529,5,284,270.00,'2013-04-01','Abril 2013','2319','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1530,5,284,270.00,'2013-05-02','Mayo 2013','2413','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1531,5,284,270.00,'2013-06-30','Pagado','2550','juan_fco_monter','2014-08-17',0,271);
INSERT INTO `payment` VALUES (1532,5,284,270.00,'2013-07-03','Julio 2013','2652','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1533,5,284,270.00,'2013-08-01','Agosto 2013','2743','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1534,5,284,270.00,'2013-09-04','Septiembre 2013','2904','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1535,5,284,270.00,'2013-10-01','Octubre 2013','270','juan_fco_monter','2014-08-17',0,271);
INSERT INTO `payment` VALUES (1536,5,284,270.00,'2013-11-05','Noviembre 2013','3192','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1537,5,284,270.00,'2013-12-03','Diciembre 2013','3280','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1538,5,284,270.00,'2014-01-06','Enero 2014','3413','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1539,5,284,270.00,'2014-02-04','Febrero 2014','3528','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1540,5,284,270.00,'2014-03-03','Marzo 2014','3617','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1541,5,284,270.00,'2014-03-31','Abril 2014','3711','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1542,5,284,270.00,'2014-05-06','Mayo 2014','3871','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1543,5,284,270.00,'2014-06-02','Junio 2014','3930','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1544,5,284,270.00,'2014-07-04','Julio 2014','4057','juan_fco_monter','2014-07-21',NULL,271);
INSERT INTO `payment` VALUES (1546,5,285,270.00,'2013-03-02','Marzo 2013','2194','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1547,5,285,270.00,'2013-04-05','Abril 2013','2362','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1548,5,285,270.00,'2013-05-01','Mayo 2013','2412','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1549,5,285,270.00,'2013-06-07','Junio 2013','2593','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1550,5,285,270.00,'2013-07-04','Julio 2013','2672','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1551,5,285,270.00,'2013-08-05','Agosto 2013','2797','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1552,5,285,270.00,'2013-09-07','Septiembre 2013','2944','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1553,5,285,320.00,'2013-10-10','Octubre 2013','3113','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1554,5,285,270.00,'2013-11-07','Noviembre 2013','3217','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1555,5,285,270.00,'2013-12-05','Diciembre 2013','3307','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1556,5,380,270.00,'2013-06-07','junio pagado','2590','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1557,5,380,320.00,'2013-06-07','mayo pagado','2601','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1558,5,285,270.00,'2014-01-08','Enero 2014','3441','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1559,5,380,320.00,'2013-07-11','julio pagado','2727','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1560,5,285,270.00,'2014-02-06','Febrero 2014','3556','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1561,5,380,320.00,'2013-08-12','agosto pagado','2849','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1562,5,285,270.00,'2014-03-04','Marzo 2014','3636','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1563,5,380,320.00,'2013-09-24','septiembre pagado','2983','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1564,5,285,270.00,'2014-04-05','Abril 2014','3746','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1565,5,380,320.00,'2013-10-14','octubra pagado','3117','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1566,5,285,270.00,'2014-05-08','Mayo 2014','3887','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1567,5,380,320.00,'2013-12-19','diciembre pagado','3364','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1568,5,380,320.00,'2013-12-19','diciembre pagado','3365','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1569,5,285,270.00,'2014-06-07','Junio 2014','3983','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1570,5,380,320.00,'2014-01-31','enero pagado','3487','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1571,5,380,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1572,5,285,270.00,'2014-07-08','Julio 2014','4089','juan_fco_monter','2014-07-21',NULL,272);
INSERT INTO `payment` VALUES (1573,5,380,590.00,'2014-03-01','febrero y marzo pagado','3416','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1575,5,286,270.00,'2013-03-04','Marzo 2013','2231','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1576,5,380,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1577,5,380,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1578,5,380,700.00,'2014-06-09','pago a cuenta de morosidad','4011','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1579,5,380,0.00,'2014-07-31','julio pagado','0000','juan_fco_monter','2014-07-21',NULL,362);
INSERT INTO `payment` VALUES (1580,5,286,270.00,'2013-03-30','Abril 2013','2293','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1582,5,286,270.00,'2013-05-04','Mayo 2013','2446','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1583,5,379,540.00,'2013-03-01','marzo abril pagado','2189','juan_fco_monter','2014-08-24',0,361);
INSERT INTO `payment` VALUES (1584,5,379,0.00,'2014-04-30','abril pagado','0000','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1585,5,379,540.00,'2013-05-03','mayo junio pagado','2443','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1586,5,286,270.00,'2013-05-31','Junio 2013','2516','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1587,5,379,0.00,'2014-06-30','junio pagado','0000','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1588,5,286,270.00,'2013-07-01','Julio 2013','2631','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1589,5,379,540.00,'2013-06-14','julio y agosto pagado','2614','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1590,5,379,0.00,'2013-08-31','agosto pagado','0000','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1591,5,286,270.00,'2013-08-02','Agosto 2013','2755','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1592,5,379,270.00,'2013-09-01','septiembre pagado','2746','juan_fco_monter','2014-08-24',0,361);
INSERT INTO `payment` VALUES (1593,5,286,270.00,'2013-08-02','Septiembre 2013','2865','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1594,5,379,270.00,'2013-09-30','octubre pagado','2860','juan_fco_monter','2014-08-24',0,361);
INSERT INTO `payment` VALUES (1595,5,286,270.00,'2013-10-07','Octubre 2013','3096','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1596,5,379,270.00,'2013-10-14','noviembre pagado','3119','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1597,5,286,270.00,'2013-11-04','Noviembre 2013','3162','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1598,5,379,270.00,'2013-12-02','diciembre pagado','3260','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1599,5,286,270.00,'2013-12-06','Diciembre 2013','3317','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1600,5,379,810.00,'2013-12-30','enero febrero marzo pagado','3374','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1601,5,286,270.00,'2014-01-07','Enero 2014','3461','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1603,5,379,0.00,'2014-02-28','febrero pagado','0000','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1604,5,286,270.00,'2014-02-01','Febrero 2014','3502','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1605,5,379,0.00,'2014-03-31','marzo pagado','0000','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1606,5,286,270.00,'2014-03-03','Marzo 2014','3632','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1607,5,379,540.00,'2014-03-06','abril mayo pagado','3673','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1608,5,379,0.00,'2014-05-31','mayo pagado','0000','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1609,5,286,270.00,'2014-04-07','Abril 2014','3768','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1610,5,286,270.00,'2014-05-06','Mayo 2014','3842','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1611,5,379,1080.00,'2014-05-09','junio julio agosto septiembre pagado','3898','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1612,5,379,270.00,'2014-07-14','octubre pagado','4105','juan_fco_monter','2014-07-21',NULL,361);
INSERT INTO `payment` VALUES (1613,5,286,270.00,'2014-06-07','Junio 2014','3986','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1614,5,286,270.00,'2014-07-07','Julio 2014','4076','juan_fco_monter','2014-07-21',NULL,273);
INSERT INTO `payment` VALUES (1615,13,378,200.00,'2013-02-21','febrero pagado','2168','juan_fco_monter','2014-08-19',0,360);
INSERT INTO `payment` VALUES (1616,5,378,270.00,'2013-03-08','marzo pagado','2265','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1617,5,378,270.00,'2013-04-08','abril pagado','2389','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1618,5,378,270.00,'2013-05-07','mayo pagado','2486','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1619,5,378,320.00,'2013-06-17','junio pagado','2616','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1620,5,378,270.00,'2013-07-08','julio pagado','2711','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1621,5,378,270.00,'2013-08-07','agosto pagado','2826','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1622,5,378,320.00,'2013-09-17','septiembre pagado','2976','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1623,5,378,270.00,'2013-10-08','octubre pagado','3098','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1624,5,378,270.00,'2013-11-08','noviembre pagado','3233','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1625,5,378,320.00,'2013-12-16','diciembre pagado','3359','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1626,5,378,270.00,'2014-01-07','enero pagado','3468','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1627,5,378,270.00,'2014-02-07','febrero pagado','3563','juan_fco_monter','2014-08-22',0,360);
INSERT INTO `payment` VALUES (1628,5,378,270.00,'2014-03-06','marzo pagado','3658','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1629,5,378,270.00,'2014-04-08','abril pagado','3785','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1630,5,378,270.00,'2014-05-08','mayo pagado','3894','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1631,5,378,320.00,'2014-07-03','junio pagado','4050','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1632,5,378,270.00,'2014-07-03','julio pagado','4047','juan_fco_monter','2014-07-21',NULL,360);
INSERT INTO `payment` VALUES (1633,13,377,200.00,'2013-02-21','febrero pagado','2167','juan_fco_monter','2014-08-19',0,359);
INSERT INTO `payment` VALUES (1634,5,377,270.00,'2013-03-02','marzo pagado','2202','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1635,5,377,270.00,'2013-04-04','abril pagado','2355','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1636,5,377,270.00,'2013-05-07','mayo pagado','2491','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1637,5,377,270.00,'2013-06-03','junio pagado','2537','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1638,5,377,270.00,'2013-07-04','julio pagado','2673','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1639,5,377,270.00,'2013-07-31','agosto pagado','2737','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1640,5,377,270.00,'2013-09-02','septiembre pagado','2867','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1641,5,377,270.00,'2013-10-04','octubre pagado','3046','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1642,5,377,270.00,'2013-11-08','noviembre pagado','3229','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1643,5,377,270.00,'2013-12-02','diciembre pagado','3275','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1644,5,377,270.00,'2014-01-08','enero pagado','3443','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1645,5,377,270.00,'2014-02-03','febrero pagado','3512','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1646,5,377,270.00,'2014-03-08','marzo pagadao','3685','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1647,5,377,270.00,'2014-04-11','abril pagado','3800','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1648,5,377,270.00,'2014-05-06','mayo pagado','3851','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1649,5,377,270.00,'2014-06-02','junio pagado','3939','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1650,5,377,270.00,'2014-07-08','julio pagado','4090','juan_fco_monter','2014-07-21',NULL,359);
INSERT INTO `payment` VALUES (1651,13,376,200.00,'2013-02-10','febrero pagado','2136','juan_fco_monter','2014-08-18',0,358);
INSERT INTO `payment` VALUES (1652,5,376,270.00,'2013-03-05','marzo pagado','2235','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1653,5,376,270.00,'2013-04-02','abril pagado','2324','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1654,5,376,270.00,'2013-05-06','mayo pagado','2466','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1655,5,376,270.00,'2013-06-04','junio pagado','2559','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1656,5,376,270.00,'2013-07-02','julio pagado','2641','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1657,5,376,270.00,'2013-08-03','agosto pagado','2777','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1658,5,376,270.00,'2013-09-04','septiembre pagado','2900','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1659,5,376,270.00,'2013-10-07','octubre pagado','3083','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1660,5,376,270.00,'2013-11-02','noviembre pagado','3143','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1661,5,376,270.00,'2013-12-01','diciembre pagado','3256','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1662,5,376,270.00,'2014-01-06','enero pagado','3436','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1663,5,376,270.00,'2014-02-04','febrero pagado','3523','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1664,5,376,270.00,'2014-03-03','marzo pagado','3631','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1665,5,376,270.00,'2014-04-03','abril pagado','3724','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1666,5,376,270.00,'2014-05-06','mayo pagado','3855','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1667,5,376,270.00,'2014-06-06','junio pagado','3973','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1668,5,376,270.00,'2014-07-03','julio pagado','4030','juan_fco_monter','2014-07-21',NULL,358);
INSERT INTO `payment` VALUES (1670,5,375,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1671,5,375,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1672,5,375,960.00,'2013-05-18','marzo abril mayo pagado','2511','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1673,5,375,270.00,'2013-06-12','junio pagado','2516','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1674,5,375,270.00,'2013-07-26','julio pagado','2733','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1675,5,375,270.00,'2013-07-26','agosto pagado','2734','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1676,5,375,270.00,'2013-08-30','septiembre pagado','2859','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1677,5,375,270.00,'2013-10-01','octubre pagado','3020','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1678,5,375,270.00,'2013-11-06','noviembre pagado','3201','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1679,5,375,320.00,'2013-12-13','diciembre pagado','3354','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1680,5,375,270.00,'2013-12-27','enero pagado','3367','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1681,5,375,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1682,5,375,540.00,'2014-03-04','febrero marzo pagado','3651','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1683,5,375,270.00,'2014-04-01','abril pagado','3723','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1684,5,375,320.00,'2014-05-13','mayo pagado','3907','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1685,5,375,270.00,'2014-06-03','junio pagado','3950','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1686,5,375,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-21',NULL,357);
INSERT INTO `payment` VALUES (1687,13,374,200.00,'2013-03-19','febrero pagado','2287','juan_fco_monter','2014-08-18',0,356);
INSERT INTO `payment` VALUES (1688,5,374,320.00,'2013-03-19','marzo pagado','2288','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1689,5,374,320.00,'2013-04-18','abril pagado','2404','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1690,5,374,320.00,'2013-05-15','mayo pagado','2509','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1691,5,374,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1692,5,374,640.00,'2013-07-12','junio y julio pagado','2728','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1693,5,374,270.00,'2013-08-08','agosto pagado','2835','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1694,5,374,270.00,'2013-09-09','septiembre pagado','2968','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1695,5,374,320.00,'2013-10-10','octubre pagado','3114','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1696,5,374,320.00,'2013-11-01','noviembre pagado','3423','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1697,5,374,320.00,'2013-12-19','diciembre pagado','3362','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1698,5,374,320.00,'2014-01-20','enero pagado','3483','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1699,5,374,320.00,'2014-02-21','febrero pagado','3595','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1700,5,374,0.00,'2014-03-31','marzo pagado','0000','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1701,5,374,640.00,'2014-04-22','marzo abril pagado','3813','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1702,5,374,270.00,'2014-05-07','mayo pagado','3915','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1703,5,374,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1704,5,374,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-21',NULL,356);
INSERT INTO `payment` VALUES (1706,5,373,270.00,'2013-03-02','marzo pagado','2206','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1707,5,373,270.00,'2013-04-08','abril pagado','2383','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1708,5,373,270.00,'2013-05-06','mayo pagado','2452','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1709,5,373,270.00,'2013-06-03','junio pagado','2546','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1710,5,373,270.00,'2013-08-05','agosto pagado','2785','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1711,5,373,320.00,'2013-08-05','julio pagado','2799','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1712,5,373,270.00,'2013-09-05','septiembre pagado','2913','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1713,5,373,270.00,'2013-10-03','octubre pagado','3041','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1714,5,373,270.00,'2013-11-08','noviembre pagado','3230','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1715,5,373,270.00,'2013-12-08','diciembre pagado','3338','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1716,5,373,270.00,'2014-01-07','enero pagado','3454','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1717,5,373,270.00,'2014-02-03','febrero pagado','3508','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1718,5,373,270.00,'2014-03-03','marzo pagado','3627','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1719,5,373,270.00,'2014-04-08','abril pagado','3782','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1720,5,373,270.00,'2014-05-08','mayo pagado','3896','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1721,5,373,270.00,'2014-06-02','junio pagado','3938','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1722,5,373,270.00,'2014-07-08','julio pagado','4102','juan_fco_monter','2014-07-21',NULL,355);
INSERT INTO `payment` VALUES (1724,5,372,270.00,'2013-03-01','marzo pagado','2185','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1725,5,372,270.00,'2013-04-02','abril pagado','2335','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1726,5,372,270.00,'2013-05-02','mayo pagado','2421','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1727,5,372,270.00,'2013-06-01','junio pagado','2523','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1728,5,372,270.00,'2013-07-02','julio pagado','2632','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1729,5,372,270.00,'2013-08-01','agosto pagado','2741','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1730,5,372,270.00,'2013-09-02','septiembre pagado','2861','juan_fco_monter','2014-08-22',0,354);
INSERT INTO `payment` VALUES (1731,5,372,270.00,'2013-10-03','octubre pagado','3035','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1732,5,372,270.00,'2013-11-02','noviembre pagado','3137','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1733,5,372,270.00,'2013-12-02','diciembre pagado','3272','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1734,5,372,270.00,'2014-01-07','enero pagado','3452','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1735,5,372,270.00,'2014-02-03','febrero pagado','3509','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1736,5,372,270.00,'2014-03-01','marzo pagado','3609','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1737,5,372,270.00,'2014-04-07','abril pagado','3770','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1738,5,372,270.00,'2014-05-03','mayo pagado','3840','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1739,5,372,270.00,'2014-06-03','junio pagado','3954','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1740,5,372,270.00,'2014-07-07','julio pagado','4074','juan_fco_monter','2014-07-21',NULL,354);
INSERT INTO `payment` VALUES (1742,5,371,270.00,'2013-03-08','marzo pagado','2266','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1743,5,371,270.00,'2013-04-05','abril pagado','2363','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1744,5,371,270.00,'2013-05-08','mayo pagado','2498','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1745,5,371,270.00,'2013-06-05','junio pagado','2568','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1746,5,371,270.00,'2013-07-05','julio pagado','2679','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1747,5,371,270.00,'2013-08-07','agosto pagado','2825','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1748,5,371,270.00,'2013-09-05','septiembre pagado','2908','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1749,5,371,270.00,'2013-10-05','octubre pagado','3068','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1750,5,371,270.00,'2013-11-05','noviembre pagado','3169','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1751,5,371,270.00,'2013-12-05','diciembre pagado','3304','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1752,5,371,270.00,'2014-01-08','enero pagado','3449','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1753,5,371,270.00,'2014-02-07','febrero pagado','3566','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1754,5,371,270.00,'2014-03-07','marzo pagado','3675','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1755,5,371,270.00,'2014-04-08','abril pagado','3777','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1756,5,371,270.00,'2014-05-03','mayo pagado','3863','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1757,5,371,270.00,'2014-06-01','junio pagado','3921','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1758,5,371,270.00,'2014-07-08','julio pagado','4093','juan_fco_monter','2014-07-21',NULL,353);
INSERT INTO `payment` VALUES (1760,5,370,270.00,'2013-03-14','marzo pagado','2284','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1761,5,370,270.00,'2013-04-02','abril pagado','2333','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1762,5,370,270.00,'2013-05-06','mayo pagado','2477','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1763,5,370,270.00,'2013-06-06','junio pagado','2583','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1764,5,370,270.00,'2013-07-08','julio pagado','2712','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1765,5,370,270.00,'2013-08-01','agosto pagado','2746','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1766,5,370,270.00,'2013-09-05','septiembre pagado','2920','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1767,5,370,270.00,'2013-09-30','octubre pagado','3002','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1768,5,370,270.00,'2013-11-07','noviembre pagado','3220','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1769,5,370,270.00,'2013-12-01','diciembre pagado','3252','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1770,5,370,270.00,'2014-01-06','enero pagado','3430','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1771,5,370,270.00,'2014-02-06','febrero pagado','3555','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1772,5,370,270.00,'2014-03-07','marzo pagado','3668','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1773,5,370,270.00,'2014-04-08','abril pagadao','3787','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1774,5,370,270.00,'2014-04-29','mayo pagado','3815','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1775,5,370,270.00,'2014-06-06','junio pagado','3975','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1776,5,370,270.00,'2014-07-08','julio pagado','4083','juan_fco_monter','2014-07-21',NULL,352);
INSERT INTO `payment` VALUES (1777,13,287,200.00,'2013-01-15','Febrero 2013','2154','juan_fco_monter','2014-08-17',0,274);
INSERT INTO `payment` VALUES (1778,5,287,270.00,'2013-03-08','Marzo 2013','2272','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1780,5,369,270.00,'2013-03-04','marzo pagado','2230','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1781,5,369,270.00,'2013-04-03','abril pagado','2350','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1782,5,369,270.00,'2013-05-07','mayo pagado','2492','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1783,5,369,270.00,'2013-06-03','junio pagado','2541','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1784,5,369,270.00,'2013-06-25','julio pagado','2622','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1785,5,369,270.00,'2013-08-05','agosto pagado','2792','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1786,5,369,270.00,'2013-09-02','septiembre pagado','2864','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1787,5,369,270.00,'2013-09-30','octubre pagado','3004','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1788,5,369,270.00,'2013-11-04','noviembre pagado','3155','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1789,5,369,270.00,'2013-12-09','diciembre pagado','3341','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1790,5,369,270.00,'2013-12-30','enero pagado','3375','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1791,5,369,270.00,'2014-02-01','febrero pagado','3494','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1792,5,369,270.00,'2014-02-28','marzo pagado','3602','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1793,5,369,270.00,'2014-03-25','abril pagado','3701','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1794,5,369,270.00,'2014-05-02','mayo pagado','3821','juan_fco_monter','2014-07-22',NULL,351);
INSERT INTO `payment` VALUES (1795,5,369,540.00,'2014-06-02','junio julio pagado\r\n','3935','juan_fco_monter','2014-08-24',0,351);
INSERT INTO `payment` VALUES (1796,5,369,270.00,'2014-07-31','agosto pagado','4118','juan_fco_monter','2014-08-24',0,351);
INSERT INTO `payment` VALUES (1798,5,368,270.00,'2013-03-01','marzo pagado','2182','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1799,5,368,270.00,'2013-04-01','abril pagado','2304','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1800,5,368,270.00,'2013-05-06','mayo pagado\r\n','2473','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1801,5,368,270.00,'2013-06-03','junio pagado','2530','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1802,5,368,270.00,'2013-07-02','julio pagado','2651','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1803,5,368,270.00,'2013-08-03','agosto pagado','2761','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1804,5,368,270.00,'2013-09-06','septiembre pagado','2928','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1805,5,368,270.00,'2013-09-28','octubre pagado','2988','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1806,5,368,270.00,'2013-11-08','noviembre pagado','3227','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1807,5,368,270.00,'2013-12-03','diciembre pagado','3282','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1808,5,368,270.00,'2014-01-02','enero pagado','3385','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1809,5,368,270.00,'2014-02-03','febrero pagado','3507','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1810,5,368,270.00,'2014-03-04','marzo pagado','3633','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1811,5,368,270.00,'2014-03-31','abril pagado','3710','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1812,5,368,270.00,'2014-05-02','mayo pagado','3832','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1813,5,368,270.00,'2014-06-02','junio pagado','3924','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1814,5,368,270.00,'2014-07-03','julio pagado','4027','juan_fco_monter','2014-07-22',NULL,350);
INSERT INTO `payment` VALUES (1815,5,287,270.00,'2013-05-04','Abril 2013','2361','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1816,5,287,270.00,'2013-05-04','Mayo 2013','2442','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1817,5,287,270.00,'2013-06-06','Junio 2013','2585','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1818,5,287,270.00,'2013-07-02','Julio 2013','2642','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1819,5,287,270.00,'2013-08-03','Agosto 2013','2763','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1820,5,287,270.00,'2013-08-03','Septiembre 2013','2885','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1821,5,287,270.00,'2013-10-08','Octubre 2013','3104','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1822,5,287,270.00,'2013-11-01','Noviembre 2013','3135','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1823,5,287,270.00,'2013-12-09','Diciembre 2013','3347','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1824,5,287,270.00,'2014-01-02','Enero 2014','3388','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1825,5,287,270.00,'2014-02-06','Febrero 2014','3550','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1827,5,367,270.00,'2013-03-02','marzo pagado','2197','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1828,5,367,270.00,'2013-04-02','abril pagado','2322','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1829,5,367,270.00,'2013-05-04','mayo junio','2450','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1830,5,367,270.00,'2013-06-03','junio pagado','2542','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1831,5,367,270.00,'2013-07-06','julio pagado','2693','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1832,5,367,270.00,'2013-08-05','agosto pagado','2778','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1833,5,367,270.00,'2013-09-06','septiembre pagado','2925','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1834,5,367,270.00,'2013-10-08','octubre pagado','3111','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1835,5,367,270.00,'2013-11-07','noviembre pagado','3210','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1836,5,367,270.00,'2013-12-03','diciembre pagado','3278','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1837,5,367,270.00,'2014-01-06','enero pagado','3420','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1838,5,367,270.00,'2014-02-07','febrero pagado','3572','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1839,5,367,270.00,'2014-03-07','marzo pagado','3674','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1840,5,367,270.00,'2014-04-08','abril pagado','3776','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1841,5,367,270.00,'2014-05-08','mayo pagado','3880','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1842,5,367,270.00,'2014-06-08','junio pagado','3992','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1843,5,367,270.00,'2014-07-07','julio pagado','4072','juan_fco_monter','2014-07-22',NULL,349);
INSERT INTO `payment` VALUES (1845,5,366,270.00,'2013-03-01','marzo pagado','2188','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1846,5,366,270.00,'2013-04-04','abril pagado','2349','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1847,5,366,270.00,'2013-05-04','mayo pagado','2445','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1848,5,366,270.00,'2013-06-06','junio pagado','2582','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1849,5,366,270.00,'2013-07-05','julio pagado','2683','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1850,5,366,320.00,'2013-08-10','agosto pagado','2846','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1851,5,366,270.00,'2013-09-04','noviembre pagado','2895','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1852,5,366,270.00,'2013-10-01','octubre pagado','3018','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1853,5,366,270.00,'2013-11-05','noviembre pagado','3188','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1854,5,366,270.00,'2013-12-04','diciembre pagado','3290','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1855,5,366,270.00,'2014-01-06','enero pagado','3423','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1856,5,366,270.00,'2014-02-04','febrero pagado','3527','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1857,5,366,270.00,'2014-03-08','marzo pagado','3687','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1858,5,366,270.00,'2014-04-05','abril pagado','3747','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1859,5,366,270.00,'2014-05-03','mayo pagado\r\n','3839','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1860,5,366,270.00,'2014-06-06','junio pagado','3977','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1861,5,366,270.00,'2014-07-03','julio pagado\r\n','4037','juan_fco_monter','2014-07-22',NULL,348);
INSERT INTO `payment` VALUES (1862,5,287,270.00,'2014-03-08','Marzo 2014','3694','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1863,5,287,270.00,'2014-04-10','Abril 2014','3794','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1864,5,287,270.00,'2014-05-08','Mayo 2014','3888','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1865,5,287,0.00,'2014-06-30','Junio 2014','0000','juan_fco_monter','2014-07-22',NULL,274);
INSERT INTO `payment` VALUES (1866,13,291,200.00,'2013-02-10','Febrero 2013','2137','juan_fco_monter','2014-08-17',0,275);
INSERT INTO `payment` VALUES (1867,5,291,270.00,'2013-03-02','Marzo 2013','2207','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1868,5,291,270.00,'2013-04-01','Abril 2013','2305','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1869,5,291,270.00,'2013-05-06','Mayo 2013','2461','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1870,5,291,270.00,'2013-06-04','Junio 2013','2549','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1871,5,291,270.00,'2013-07-06','Julio 2013','2691','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1872,5,291,270.00,'2013-08-03','Agosto 2013','2766','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1873,5,291,270.00,'2013-09-07','Septiembre 2013','2939','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1874,5,291,270.00,'2013-10-08','Octubre pagado','3105','juan_fco_monter','2014-08-24',0,275);
INSERT INTO `payment` VALUES (1875,5,291,270.00,'2013-11-04','Noviembre 2013','3157','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1877,5,365,270.00,'2013-03-01','marzo pagado','2176','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1878,5,291,270.00,'2013-12-03','Diciembre 2013','3283','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1880,5,365,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1881,5,365,270.00,'2013-05-04','mayo pagado','2448','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1882,5,365,270.00,'2013-06-06','junio pagado','2587','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1883,5,291,270.00,'2014-01-06','Enero 2014','3414','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1884,5,365,270.00,'2013-07-06','julio pagado','2694','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1885,5,365,270.00,'2013-08-05','agosto pagado','2796','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1886,5,291,270.00,'2014-02-04','Febrero 2014','3517','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1887,5,365,270.00,'2013-09-06','septiembre pagado','2936','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1888,5,365,270.00,'2013-10-07','octubre pagado','3089','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1889,5,291,270.00,'2014-03-04','Marzo 2014','3637','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1890,5,365,270.00,'2013-11-05','noviembre pagado\r\n','3191','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1891,5,365,270.00,'2013-12-05','diciembre pagado\r\n','3300','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1892,5,291,270.00,'2014-04-01','Abril 2014','3715','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1893,5,365,270.00,'2014-01-06','enero pagado\r\n','3410','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1894,5,291,270.00,'2014-05-06','Mayo 2014','3848','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1896,5,291,270.00,'2014-06-09','Junio 2014','3991','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1897,5,291,270.00,'2014-06-30','Julio 2014','4021','juan_fco_monter','2014-07-22',NULL,275);
INSERT INTO `payment` VALUES (1899,5,365,270.00,'2014-02-08','febrero pagado','3574','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1900,5,365,270.00,'2014-03-04','marzo pagado','3644','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1901,5,292,270.00,'2013-03-05','Marzo 2013','2244','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1902,5,365,320.00,'2014-04-12','abril pagado','3802','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1903,5,365,270.00,'2014-05-08','mayo pagado','3876','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1904,5,292,270.00,'2013-04-01','Abril 2013','2299','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1905,5,365,270.00,'2014-06-08','junio pagado\r\n','3997','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1907,5,365,270.00,'2014-07-21','julio pagado','4109','juan_fco_monter','2014-07-22',NULL,347);
INSERT INTO `payment` VALUES (1908,5,292,270.00,'2013-06-03','Junio 2013','2544','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1910,5,292,270.00,'2013-07-03','Julio 2014','2655','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1911,5,364,270.00,'2013-03-04','marzo pagado','2216','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1912,5,364,270.00,'2013-04-02','abril pagado','2323','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1913,5,292,270.00,'2013-08-01','Agosto 2013','2748','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1914,5,364,270.00,'2013-05-03','mayo pagado','2434','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1915,5,292,270.00,'2013-08-02','Septiembre 2013','2862','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1916,5,364,270.00,'2013-06-01','junio pagado','2522','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1917,5,292,270.00,'2013-10-07','Octubre 2013','3094','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1918,5,364,270.00,'2013-07-02','julio pagado','2643','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1919,5,364,270.00,'2013-08-07','agosto pagado','2821','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1920,5,292,270.00,'2013-11-04','Noviembre 2013','3159','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1921,5,292,270.00,'2013-12-04','Diciembre 2013','3292','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1922,5,364,270.00,'2013-08-03','septiembre pagado','2882','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1923,5,364,270.00,'2013-09-30','octubre pagado','2998','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1924,5,292,270.00,'2014-01-02','Enero 2014','3378','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1925,5,364,270.00,'2013-11-01','noviembre pagado','3126','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1926,5,364,270.00,'2013-12-03','diciembre pagado','3284','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1927,5,292,270.00,'2014-02-03','Febrero 2014','3506','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1928,5,364,270.00,'2014-01-02','enero pagado','3383','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1929,5,292,270.00,'2014-03-06','Marzo 2014','3659','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1930,5,364,270.00,'2014-02-04','febrero pagado','3522','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1931,5,292,270.00,'2014-04-04','Abril 2014','3741','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1932,5,364,270.00,'2014-03-04','marzo pagado','3646','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1933,5,364,270.00,'2014-04-01','abril pagado','3721','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1934,5,364,270.00,'2014-05-02','mayo pagado','3830','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1935,5,292,270.00,'2014-05-02','Mayo 2014','3825','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1936,5,364,270.00,'2014-06-03','junio pagado','3946','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1937,5,364,270.00,'2014-07-03','julio pagado','4044','juan_fco_monter','2014-07-22',NULL,346);
INSERT INTO `payment` VALUES (1939,5,363,270.00,'2013-03-05','marzo pagado','2248','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1940,5,363,270.00,'2013-03-30','abril pagado','2295','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1941,5,292,270.00,'2014-06-02','Junio 2014','3927','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1942,5,363,270.00,'2013-05-07','mayo pagado','2483','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1943,5,292,270.00,'2014-07-04','Julio 2014','4058','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (1944,5,363,270.00,'2013-06-01','junio pagado','2526','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1946,5,363,270.00,'2013-06-29','julio pagado','2624','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1947,5,363,270.00,'2013-08-03','agosto pagado','2767','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1948,5,363,270.00,'2013-09-03','septiembre pagado','2880','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1949,5,293,270.00,'2013-03-04','Marzo 2013','2209','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (1950,5,363,270.00,'2013-10-01','octubre pagado','3014','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1951,5,363,270.00,'2013-11-02','noviembre pagado','3146','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1952,5,293,270.00,'2013-04-03','Abril 2013','2343','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (1954,5,363,270.00,'2013-12-05','diciembre pagado','3303','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1955,5,363,270.00,'2014-01-04','enero pagado','3399','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1956,5,363,270.00,'2014-02-01','febrero pagado','3504','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1957,5,293,270.00,'2013-05-03','Mayo 2013','2440','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (1958,5,363,270.00,'2014-03-11','marzo pagado','3752','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1959,5,363,270.00,'2014-03-29','abril pagado','3706','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1960,5,363,270.00,'2014-05-03','mayo pagado','3834','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1961,5,363,270.00,'2014-06-03','junio pagado','3945','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1962,5,363,270.00,'2014-07-04','julio pagado','4066','juan_fco_monter','2014-07-22',NULL,345);
INSERT INTO `payment` VALUES (1963,5,293,270.00,'2013-06-03','Junio 2013','2543','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (1965,5,362,810.00,'2013-03-01','marzo abril mayo pagado','2181','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1966,5,362,0.00,'2013-04-30','abril pagado','0000','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1967,5,362,0.00,'2013-05-31','mayo pagado','0000','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1968,5,362,270.00,'2013-05-30','junio pagado','2513','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1969,5,293,270.00,'2013-07-02','Julio 2013','2645','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (1970,5,362,270.00,'2013-07-02','julio pagado','2644','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1971,5,362,270.00,'2013-08-01','agosto pagado','2745','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1972,5,293,270.00,'2013-08-05','Agosto 2013','2784','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (1973,5,362,270.00,'2013-09-03','septiembre pagado','2874','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1974,5,362,270.00,'2013-10-01','octubre pagado','3016','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1975,5,293,270.00,'2013-08-03','Septiembre','2881','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (1976,5,362,270.00,'2013-11-04','noviembre pagado','3149','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1977,5,362,270.00,'2013-12-02','diciembre pagado','3270','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1978,5,293,270.00,'2013-10-01','Octubre 2013','3012','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (1979,5,362,270.00,'2014-01-03','enero pagado','3390','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1980,5,362,270.00,'2014-02-03','febrero pagado','3519','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1981,5,362,270.00,'2014-03-03','marzo pagado','3629','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1982,5,362,270.00,'2014-04-01','abril pagado','3720','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1983,5,362,270.00,'2014-05-01','mayo pagado','3818','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1984,5,362,270.00,'2014-06-02','junio pagado','3928','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1985,5,362,270.00,'2014-07-03','julio pagado','4040','juan_fco_monter','2014-07-22',NULL,344);
INSERT INTO `payment` VALUES (1987,5,361,270.00,'2013-03-05','marzo pagado','2240','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1988,5,361,270.00,'2013-04-02','abril pagado','2326','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1989,5,361,270.00,'2013-05-06','mayo pagado','2465','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1990,5,361,270.00,'2013-06-03','junio pagado','2540','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1991,5,293,270.00,'2013-12-03','Diciembre 2013','3279','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (1992,5,361,270.00,'2013-07-04','julio pagado','2664','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1993,5,361,270.00,'2013-08-01','agosto pagado','2739','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1994,5,361,270.00,'2013-09-02','septiembre pagado','2866','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1995,5,361,270.00,'2013-10-01','octubre pagado','3010','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1996,5,361,270.00,'2013-11-05','noviembre pagado','3176','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1997,5,361,270.00,'2013-12-07','diciembre pagado\r\n','3322','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1998,5,361,270.00,'2014-01-08','enero pagado','3444','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (1999,5,361,270.00,'2014-02-06','febrero pagado','3537','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (2000,5,361,270.00,'2014-03-06','marzo pagado','3665','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (2001,5,361,270.00,'2014-04-07','abril pagado','3756','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (2002,5,361,270.00,'2014-05-06','mayo pagado','3846','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (2003,5,361,270.00,'2014-06-03','junio pagado','3941','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (2004,5,361,270.00,'2014-07-03','julio pagado','4045','juan_fco_monter','2014-07-22',NULL,343);
INSERT INTO `payment` VALUES (2006,5,360,270.00,'2013-03-08','marzo pagado','2273','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2007,5,360,270.00,'2013-04-06','abril pagado','2368','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2008,5,360,270.00,'2013-04-06','mayo pagado','2369','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2009,5,360,270.00,'2013-05-05','junio pagado','2572','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2010,5,360,270.00,'2013-06-05','julio pagado','2574','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2011,5,360,270.00,'2013-08-07','agosto pagado','2824','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2012,5,360,270.00,'2013-08-07','septiembre pagado','2830','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2013,5,360,860.00,'2013-10-26','octubre noviembre diciembre pagado','3125','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2014,5,360,0.00,'2013-11-30','noviembre pagado','0000','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2015,5,360,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2016,5,360,540.00,'2014-01-08','enero febrero pagado','3438','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2017,5,360,0.00,'2014-02-28','febrero pagado','0000','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2018,5,360,540.00,'2014-03-06','marzo abril pagado','3672','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2019,5,360,0.00,'2014-04-30','abril pagado','0000','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2020,5,360,540.00,'2014-05-15','mayo junio pagado','3910','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2021,5,360,0.00,'2014-06-30','junio pagado','0000','juan_fco_monter','2014-07-22',NULL,342);
INSERT INTO `payment` VALUES (2022,5,360,540.00,'2014-07-08','julio agosto pagado','4096','juan_fco_monter','2014-08-24',0,342);
INSERT INTO `payment` VALUES (2024,5,359,270.00,'2013-03-01','marzo pagado','2192','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2025,5,359,270.00,'2013-04-01','abril pagado','2298','juan_fco_monter','2014-08-23',0,341);
INSERT INTO `payment` VALUES (2026,5,359,270.00,'2013-05-02','mayo pagado','2425','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2027,5,359,270.00,'2013-06-03','junio pagado','2531','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2028,5,359,270.00,'2013-07-02','julio pagado','2635','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2029,5,359,270.00,'2013-08-05','agosto pagado','2802','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2030,5,359,270.00,'2013-09-03','septiembre pagado','2868','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2031,5,359,270.00,'2013-10-01','octubre pagado','3011','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2032,5,359,270.00,'2013-11-01','noviembre pagado','3134','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2033,5,359,270.00,'2013-12-01','diciembre pagado','3253','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2034,5,359,270.00,'2014-01-07','enero pagado','3453','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2035,5,359,270.00,'2014-01-31','febrero pagado','3490','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2036,5,359,270.00,'2014-02-28','marzo pagado','3604','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2037,5,359,270.00,'2014-03-31','abril pagado','3709','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2038,5,359,270.00,'2014-05-06','mayo pagado','3869','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2039,5,359,270.00,'2014-05-31','junio pagado','3920','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2040,5,359,270.00,'2014-07-03','julio pagado','4026','juan_fco_monter','2014-07-22',NULL,341);
INSERT INTO `payment` VALUES (2041,5,292,270.00,'2013-05-01','Mayo 2013','2411','juan_fco_monter','2014-07-22',NULL,276);
INSERT INTO `payment` VALUES (2042,5,293,270.00,'2013-11-04','Noviembre 2013','3153','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (2043,5,293,270.00,'2014-01-06','Enero 2014','3406','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (2044,5,293,270.00,'2014-02-03','Febrero 2014','3510','juan_fco_monter','2014-08-17',0,277);
INSERT INTO `payment` VALUES (2045,5,293,270.00,'2014-03-03','Marzo 2014','3620','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (2046,5,293,270.00,'2014-04-03','Abril 2014','3729','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (2047,5,293,270.00,'2014-05-06','Mayo 2014','3866','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (2048,5,293,270.00,'2014-06-03','Junio 2014','3942','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (2049,5,293,270.00,'2014-07-08','Julio 2014','4084','juan_fco_monter','2014-07-22',NULL,277);
INSERT INTO `payment` VALUES (2051,5,358,270.00,'2013-03-05','marzo pagado','2241','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2052,5,358,270.00,'2013-04-06','abril pagado','2366','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2053,5,358,270.00,'2013-05-06','mayo pagado','2455','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2054,5,358,270.00,'2013-06-06','junio pagado','2576','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2055,5,358,270.00,'2013-07-04','julio pagado','2671','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2056,5,358,270.00,'2013-08-06','agosto pagado','2813','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2057,5,358,270.00,'2013-09-07','septiembre pagado','2941','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2058,5,358,270.00,'2013-10-07','octubre pagado','3091','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2060,5,358,270.00,'2013-11-05','noviembre pagado','3181','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2061,5,358,270.00,'2013-12-02','diciembre pagado','3271','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2063,5,358,270.00,'2014-01-08','enero pagado','3445','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2064,5,358,270.00,'2014-02-06','febrero pagado','3545','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2065,5,294,270.00,'2013-03-01','Marzo 2013','2183','juan_fco_monter','2014-08-22',0,278);
INSERT INTO `payment` VALUES (2066,5,294,270.00,'2013-04-01','Abril 2013','2301','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2067,5,294,270.00,'2013-05-06','Mayo 2013','2470','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2068,5,294,270.00,'2013-06-04','Junio 2013','2552','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2069,5,294,270.00,'2013-07-05','Julio 2013','2684','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2070,5,294,270.00,'2013-08-06','Agosto 2013','2812','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2071,5,294,270.00,'2013-09-07','Septiembre 2013','2938','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2072,5,294,270.00,'2013-10-08','Octubre 2013','3108','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2073,5,294,270.00,'2013-11-06','Noviembre 2013','3205','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2074,5,294,270.00,'2013-12-07','Diciembre 2013','3320','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2075,5,294,270.00,'2014-01-06','Enero 2014','3407','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2076,5,294,270.00,'2014-02-04','Febrero 2014','3524','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2077,5,294,270.00,'2014-03-07','Marzo 2014','3677','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2078,5,294,270.00,'2014-04-07','Abril 2014','3761','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2079,5,294,270.00,'2014-05-10','Mayo 2014','3900','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2080,5,294,270.00,'2014-06-09','Junio 2014','3994','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2081,5,294,270.00,'2014-07-04','Julio 2014','4065','juan_fco_monter','2014-07-22',NULL,278);
INSERT INTO `payment` VALUES (2083,5,358,270.00,'2014-03-11','marzo pagado','3753','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2084,5,358,270.00,'2014-04-07','abril pagado','3759','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2085,5,358,270.00,'2014-05-08','mayo pagado','3895','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2086,5,358,270.00,'2014-07-01','julio pagado','4022','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2087,5,358,320.00,'2014-07-03','junio pagado','4031','juan_fco_monter','2014-07-22',NULL,340);
INSERT INTO `payment` VALUES (2089,5,357,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2090,5,357,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2091,5,357,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2092,5,357,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2093,5,357,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2094,5,357,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2095,5,357,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2096,5,357,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2097,5,357,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2098,5,357,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2099,5,357,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2100,5,357,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2101,5,357,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2102,5,357,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2103,5,357,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2104,5,357,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2105,5,357,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-22',NULL,339);
INSERT INTO `payment` VALUES (2107,5,356,270.00,'2013-03-04','marzo pagado','2232','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2108,5,356,270.00,'2013-04-04','abril pagado','2347','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2109,5,356,270.00,'2013-05-06','mayo pagado','2456','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2110,5,356,270.00,'2013-06-08','junio pagado','2603','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2111,5,356,270.00,'2013-07-06','julio pagado','2688','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2112,5,356,270.00,'2013-08-03','agosto pagado','2765','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2113,5,356,270.00,'2013-09-05','septiembre pagado','2910','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2114,5,356,270.00,'2013-10-05','octubre pagado','3070','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2115,5,356,270.00,'2013-11-05','noviembre pagado','3173','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2116,5,356,270.00,'2013-12-06','diciembre pagado','3316','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2117,5,356,270.00,'2014-01-06','enero pagado','3422','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2118,5,356,320.00,'2014-02-10','febrero pagado','3585','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2119,5,356,270.00,'2014-03-04','marzo pagado','3638','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2120,5,356,270.00,'2014-04-01','abril pagado','3719','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2121,5,356,270.00,'2014-05-02','mayo pagado','3828','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2122,5,356,270.00,'2014-06-03','junio pagado','3943','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2123,5,356,270.00,'2014-07-03','julio pagado','4038','juan_fco_monter','2014-07-23',NULL,338);
INSERT INTO `payment` VALUES (2125,5,355,270.00,'2013-03-06','marzo pagado','3272','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2126,5,355,270.00,'2013-04-06','abril pagado','2373','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2127,5,355,270.00,'2013-05-06','mayo pagado','2454','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2128,5,355,270.00,'2013-06-06','junio pagado','2579','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2129,5,355,270.00,'2013-07-06','julio pagado','2695','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2130,5,355,270.00,'2013-08-06','agosto pagado','2809','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2131,5,355,270.00,'2013-09-06','septiembre pagado','2927','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2132,5,355,270.00,'2013-10-05','octubre pagado','3049','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2133,5,355,270.00,'2013-11-02','noviembre pagado','3140','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2134,5,355,270.00,'2013-12-07','diciembre pagado','3326','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2135,5,355,270.00,'2014-01-02','enero pagado','3380','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2136,5,355,270.00,'2014-02-07','febrero pagado','3559','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2137,5,355,270.00,'2014-03-07','marzo pagado','3671','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2138,5,355,270.00,'2014-04-07','abril pagado','3748','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2139,5,355,270.00,'2014-05-03','mayo pagado','3838','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2140,5,355,270.00,'2014-06-07','junio pagado','3982','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2141,5,355,270.00,'2014-07-04','julio pagado','4064','juan_fco_monter','2014-07-23',NULL,337);
INSERT INTO `payment` VALUES (2143,5,354,270.00,'2013-03-08','marzo pagado','2274','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2144,5,354,270.00,'2013-04-03','abril pagado','2339','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2145,5,354,270.00,'2013-05-06','mayo pagado','2460','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2146,5,354,270.00,'2013-06-05','junio pagado','2575','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2147,5,354,270.00,'2013-07-03','julio pagado','2658','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2148,5,354,270.00,'2013-08-08','agosto pagado','2834','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2149,5,354,320.00,'2013-09-09','septiembre pagado','2951','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2150,5,354,270.00,'2013-10-01','octubre pagado','3019','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2151,5,354,270.00,'2013-11-07','noviembre pagado','3222','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2152,5,354,270.00,'2013-12-09','diciembre pagado','3342','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2153,5,354,270.00,'2014-01-07','enero pagado','3472','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2154,5,354,270.00,'2014-02-08','febreror pagado','3582','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2155,5,354,270.00,'2014-03-07','marzo pagado','3678','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2156,5,354,270.00,'2014-04-07','abril pagado','3773','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2157,5,354,270.00,'2014-05-06','mayo pagado','3870','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2158,5,354,270.00,'2014-06-05','junio pagado','3955','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2159,5,354,270.00,'2014-07-07','julio pagado','4075','juan_fco_monter','2014-07-23',NULL,336);
INSERT INTO `payment` VALUES (2160,5,295,270.00,'2013-03-05','Marzo 2013','2245','juan_fco_monter','2014-07-23',NULL,279);
INSERT INTO `payment` VALUES (2162,5,353,270.00,'2013-03-28','marzo pagado','2291','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2163,5,295,270.00,'2013-04-01','Abril 2013','2317','juan_fco_monter','2014-07-23',NULL,279);
INSERT INTO `payment` VALUES (2164,5,295,270.00,'2013-05-02','Mayo 2013','2428','juan_fco_monter','2014-07-23',NULL,279);
INSERT INTO `payment` VALUES (2165,5,353,270.00,'2013-04-07','abril pagado','2479','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2166,5,353,270.00,'2013-05-18','mayo pagado','2618','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2167,5,353,270.00,'2013-06-30','junio pagado','2736','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2168,5,295,270.00,'2013-06-06','Junio 2013','2588','juan_fco_monter','2014-07-23',NULL,279);
INSERT INTO `payment` VALUES (2169,5,353,0.00,'2013-07-31','julio pagado','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2170,5,353,0.00,'2013-08-31','agosto pagado','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2171,5,353,0.00,'2013-09-30','septiembre pagado','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2172,5,353,0.00,'2013-10-31','octubre pagado','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2173,5,353,0.00,'2013-11-30','noviembre pagado','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2174,5,353,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2175,5,353,0.00,'2014-01-31','enero pagado','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2176,5,353,0.00,'2014-02-28','febrero pagado','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2177,1,23,270.00,'2013-07-03','Julio 2013','2662','juan_fco_monter','2014-07-23',NULL,21);
INSERT INTO `payment` VALUES (2178,5,353,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2179,5,353,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2180,5,353,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2181,5,353,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2182,5,353,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,335);
INSERT INTO `payment` VALUES (2184,5,352,270.00,'2013-03-01','marzo pagado','2179','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2185,5,352,270.00,'2013-04-01','abril pagado','2312','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2186,5,352,270.00,'2013-05-06','mayo pagado','2453','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2187,5,295,270.00,'2013-07-03','julio pagado','2662','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2188,5,352,270.00,'2013-06-04','junio pagado','2557','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2189,5,352,270.00,'2013-07-03','julio pagado','2654','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2190,5,352,270.00,'2013-08-03','agosto pagado','2768','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2191,5,352,270.00,'2013-09-04','septiembre pagado','2897','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2192,5,352,270.00,'2013-10-03','octubre pagado','3032','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2193,5,295,270.00,'2013-08-07','agosto pagado','2829','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2194,5,352,270.00,'2013-11-04','noviembre pagado','3156','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2195,5,352,270.00,'2013-12-04','diciembre pagado','3293','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2196,5,352,270.00,'2014-01-02','enero pagado','3387','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2197,5,352,270.00,'2014-02-06','febrero pagado','3551','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2198,5,352,270.00,'2014-03-03','marzo pagado','3624','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2199,5,352,270.00,'2014-04-03','abril pagado','3728','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2200,5,295,270.00,'2013-09-04','septiembre pagado','2896','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2201,5,352,270.00,'2014-05-02','mayo pagado','3824','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2202,5,352,270.00,'2014-06-03','junio pagado','3952','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2203,5,352,270.00,'2014-07-04','julio pagado','4060','juan_fco_monter','2014-07-23',NULL,334);
INSERT INTO `payment` VALUES (2205,5,351,270.00,'2013-03-07','marzo pagado','2269','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2206,5,351,270.00,'2013-04-01','abril pagado','2297','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2207,5,351,270.00,'2013-04-30','mayo pagado','2409','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2208,5,351,270.00,'2013-05-30','junio pagado','2512','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2209,5,351,270.00,'2013-07-04','julio pagado','2666','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2210,5,295,270.00,'2013-10-05','octubre pagado','3067','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2211,5,351,270.00,'2013-08-08','agosto pagado','2844','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2212,5,295,270.00,'2013-11-02','noviembre pagado','3144','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2213,5,351,320.00,'2013-09-13','septiembre pagado','2973','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2214,5,351,270.00,'2013-10-04','octubre pagado','3045','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2215,5,295,270.00,'2013-12-01','diciembre pagado','3257','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2216,5,351,270.00,'2013-11-01','noviembre pagado','3128','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2217,5,351,270.00,'2013-12-03','diciembre pagado','3277','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2218,1,23,270.00,'2014-02-04','Febrero 2014','3525','juan_fco_monter','2014-07-23',NULL,21);
INSERT INTO `payment` VALUES (2219,5,351,270.00,'2013-12-28','enero pagado','3373','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2220,5,295,270.00,'2014-01-04','enero pagado','3397','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2221,5,351,270.00,'2014-02-07','febrero pagado','3565','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2222,5,351,270.00,'2014-02-28','marzo pagado','3605','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2223,5,295,270.00,'2014-02-04','febrero pagado','3525','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2224,5,351,270.00,'2014-03-29','abril pagado','3705','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2225,5,295,270.00,'2014-03-03','marzo pagado','3619','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2226,5,351,270.00,'2014-05-08','mayo pagado','3893','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2227,5,295,270.00,'2014-04-03','abril pagado','3736','juan_fco_monter','2014-08-24',0,279);
INSERT INTO `payment` VALUES (2228,5,351,270.00,'2014-05-29','junio pagado','3916','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2230,5,351,270.00,'2014-07-03','julio pagado','4032','juan_fco_monter','2014-07-23',NULL,333);
INSERT INTO `payment` VALUES (2231,5,296,270.00,'2013-03-05','Marzo 2014','2249','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2232,5,296,270.00,'2013-04-05','Abril 2013','2365','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2233,5,296,270.00,'2013-05-08','Mayo 2013','2500','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2234,5,296,270.00,'2013-06-06','Junio 2013','2584','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2235,5,296,270.00,'2013-07-05','Julio 2013','2685','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2236,5,296,270.00,'2013-08-05','Agosto 2013','2781','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2237,5,296,270.00,'2013-08-05','Septiembre 2013','2800','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2238,5,296,270.00,'2013-08-05','Octubre 2013','2803','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2239,5,296,270.00,'2013-11-05','Noviembre 2013','3164','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2240,5,296,270.00,'2013-12-09','Diciembre 2013','3344','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2241,5,296,270.00,'2014-01-06','Enero 2014','3417','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2243,5,350,270.00,'2013-03-07','marzo pagado','2261','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2244,5,350,320.00,'2013-04-22','abril pagado','2406','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2245,5,350,270.00,'2013-05-06','mayo pagado','2462','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2246,5,350,270.00,'2013-06-08','junio pagado','2606','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2247,5,350,270.00,'2013-07-08','julio pagado','2716','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2248,5,350,270.00,'2013-08-07','agosto pagado','2817','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2249,5,350,320.00,'2013-09-14','septiembre pagado','2975','juan_fco_monter','2014-08-22',0,332);
INSERT INTO `payment` VALUES (2250,5,350,270.00,'2013-10-07','octubre pagado','3080','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2251,5,350,270.00,'2013-11-04','noviembre pagado','3152','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2252,5,350,270.00,'2013-12-07','diciembre pagado','3321','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2253,5,350,320.00,'2014-02-06','enero pagado','3548','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2254,5,350,270.00,'2014-02-06','febrero pagado','3540','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2255,5,350,270.00,'2014-03-03','marzo pagado','3626','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2256,5,350,270.00,'2014-04-08','abril pagado','3779','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2257,5,350,320.00,'2014-06-06','mayo pagado','3969','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2258,5,350,270.00,'2014-06-05','junio pagado','3964','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2259,5,350,270.00,'2014-07-03','julio pagado','4039','juan_fco_monter','2014-07-23',NULL,332);
INSERT INTO `payment` VALUES (2260,13,349,400.00,'2013-02-22','enero febrero pagado admin anterior','2173','juan_fco_monter','2014-08-18',0,331);
INSERT INTO `payment` VALUES (2261,5,349,270.00,'2013-03-07','marzo pagado','2259','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2262,5,349,270.00,'2013-04-09','abril pagado','2398','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2264,5,349,250.00,'2013-05-09','mayo pagado parcial','2502','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2265,5,349,0.00,'2013-06-30','junio pagado','0000','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2266,5,349,250.00,'2013-07-06','julio pago parcial','3428','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2267,5,296,270.00,'2014-02-06','Febrero 2014','3552','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2268,5,349,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2269,5,296,270.00,'2014-03-04','Marzo 2014','3649','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2270,5,349,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2271,5,349,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2272,5,296,270.00,'2014-04-07','Abril 2014','3769','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2273,5,349,250.00,'2013-11-02','noviembre pagado','3148','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2274,5,349,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2275,5,349,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2276,5,296,270.00,'2014-05-06','Mayo 2014','3868','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2277,5,349,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2278,5,349,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2279,5,296,270.00,'2014-06-03','Junio 2014','3949','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2280,5,349,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,331);
INSERT INTO `payment` VALUES (2281,5,296,270.00,'2014-07-07','Julio 214','4079','juan_fco_monter','2014-07-23',NULL,280);
INSERT INTO `payment` VALUES (2282,5,349,2900.00,'2014-05-30','cuenta al corriente','3919','juan_fco_monter','2014-08-24',0,331);
INSERT INTO `payment` VALUES (2283,5,349,270.00,'2014-06-06','junio pagado','4112','juan_fco_monter','2014-08-22',0,331);
INSERT INTO `payment` VALUES (2285,5,349,270.00,'2014-07-18','julio pagado','4113','juan_fco_monter','2014-08-22',0,331);
INSERT INTO `payment` VALUES (2286,5,297,270.00,'2013-03-05','Marzo 2013','2233','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2287,5,297,270.00,'2013-04-02','Abril 2013','2325','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2288,13,348,200.00,'2013-02-12','febrero pagado','2148','juan_fco_monter','2014-08-18',0,330);
INSERT INTO `payment` VALUES (2291,5,348,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2292,5,348,540.00,'2013-06-30','mayo junio pagado','2710','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2293,5,348,0.00,'2013-07-31','julio debes\r\n','0000','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2294,5,297,270.00,'2013-05-03','Mayo 2013','2437','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2295,5,348,540.00,'2013-08-31','julio agosto pagado','3124','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2296,5,348,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2297,5,297,270.00,'2013-06-01','Junio 2013','2517','juan_fco_monter','2014-08-19',0,281);
INSERT INTO `payment` VALUES (2298,5,348,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2299,5,297,270.00,'2013-07-03','Julio 2013','2656','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2300,5,348,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2301,5,348,1000.00,'2013-12-28','abono a cuenta','3372','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2302,5,297,270.00,'2013-08-03','Agosto 2013','2756','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2303,5,348,270.00,'2014-03-01','enero pagado','3611','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2304,5,348,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2305,5,348,270.00,'2014-03-01','marzo pagado','3610','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2306,5,348,270.00,'2014-04-22','abril pagado','3807','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2307,5,297,270.00,'2013-09-06','Septiembre 2013','2934','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2308,5,348,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2309,5,348,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,330);
INSERT INTO `payment` VALUES (2310,5,348,1080.00,'2014-07-29','may jun jul y ago pagado','4116','juan_fco_monter','2014-08-23',0,330);
INSERT INTO `payment` VALUES (2311,13,347,200.00,'2013-02-12','febrero pagado','2147','juan_fco_monter','2014-08-18',0,329);
INSERT INTO `payment` VALUES (2312,5,347,270.00,'2013-03-12','marzo pagado','2281','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2313,5,347,270.00,'2013-04-02','abril pagado','2329','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2314,5,347,270.00,'2013-05-02','mayo pagado','2420','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2315,5,347,270.00,'2013-07-05','julio pagado','2681','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2316,5,347,270.00,'2013-07-05','junio pagado','2682','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2317,5,347,270.00,'2013-08-05','agosto pagado','2791','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2318,5,347,320.00,'2013-09-10','septiembre pagado','2970','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2319,5,347,270.00,'2013-10-07','octubre pagado','3079','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2320,5,347,270.00,'2013-11-06','noviembre pagado','3200','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2321,5,347,270.00,'2013-12-05','diciembre pagado','3302','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2322,5,347,270.00,'2014-01-07','enero pagado','3475','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2323,5,347,270.00,'2014-02-08','febrero pagado','3581','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2324,5,347,270.00,'2014-03-15','marzo pagado','3755','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2325,5,347,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2326,5,347,540.00,'2014-05-12','abril mayo pagado','3908','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2327,5,347,270.00,'2014-06-09','junio pagado','4002','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2328,5,347,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,329);
INSERT INTO `payment` VALUES (2331,5,346,270.00,'2013-03-13','marzo pagado','2283','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2332,5,346,270.00,'2013-04-01','abril pagado','2318','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2333,5,297,270.00,'2013-11-02','Noviembre 2013','3145','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2334,5,346,270.00,'2013-05-07','mayo pagado','2496','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2335,5,346,270.00,'2013-06-06','junio pagado','2577','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2336,5,297,270.00,'2013-12-02','Diciembre 2013','3264','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2337,5,346,270.00,'2013-07-02','julio pagado','2649','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2338,5,297,270.00,'2014-01-02','Enero 2014','3384','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2339,5,346,270.00,'2013-08-08','agosto pagado','2849','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2340,5,297,270.00,'2014-02-01','Febrero 2014','3503','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2341,5,346,300.00,'2013-09-09','septiembre pagado','2963','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2342,5,297,270.00,'2014-02-28','Marzo 2014','3603','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2343,5,346,270.00,'2013-10-05','octubre pagado','3050','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2344,5,297,270.00,'2014-04-01','Abril 2014','3714','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2345,5,346,270.00,'2013-11-07','noviembre pagado','3209','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2346,5,297,270.00,'2014-05-02','Mayo 2014','3822','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2347,5,346,270.00,'2013-12-07','diciembre pagado','3324','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2348,5,346,270.00,'2014-01-06','enero pagado','3421','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2349,5,297,270.00,'2014-06-05','Junio 2014','3958','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2350,5,346,270.00,'2014-02-07','febrero pagado','3564','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2351,5,346,270.00,'2014-03-06','marzo pagado','3656','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2352,1,25,270.00,'2014-07-03','Julio 2014','4041','juan_fco_monter','2014-07-23',NULL,23);
INSERT INTO `payment` VALUES (2353,5,346,270.00,'2014-04-08','abril pagado','3783','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2354,5,346,270.00,'2014-06-06','junio pagado','3972','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2355,5,346,270.00,'2014-06-08','mayo pagado','3996','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2356,5,346,270.00,'2014-07-03','julio pagado','4048','juan_fco_monter','2014-07-23',NULL,328);
INSERT INTO `payment` VALUES (2357,13,345,200.00,'2013-02-12','febrero pagado','2146','juan_fco_monter','2014-08-18',0,327);
INSERT INTO `payment` VALUES (2358,5,345,270.00,'2013-03-04','marzo pagado','2229','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2359,5,345,270.00,'2013-04-06','abril pagado','2374','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2360,5,345,270.00,'2013-05-03','mayo pagado','2435','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2361,5,345,270.00,'2013-06-06','junio pagado','2581','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2362,5,345,270.00,'2013-07-03','julio pagado','2661','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2363,5,345,270.00,'2013-08-05','agosto pagado','2786','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2364,5,345,270.00,'2014-09-07','septiembre pagado','2943','juan_fco_monter','2014-08-23',0,327);
INSERT INTO `payment` VALUES (2365,5,345,270.00,'2013-10-03','octubre pagado','3037','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2366,5,297,270.00,'2013-10-01','Octubre 2013','3015','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2367,5,345,270.00,'2013-11-05','noviembre pagado','3175','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2368,5,345,270.00,'2013-12-02','diciembre pagado','3269','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2369,5,345,270.00,'2014-01-06','enero pagado','3425','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2370,5,297,270.00,'2014-07-03','Julio 2014','4041','juan_fco_monter','2014-07-23',NULL,281);
INSERT INTO `payment` VALUES (2371,5,345,270.00,'2014-02-08','febrero pagado','3577','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2372,5,345,270.00,'2014-03-06','marzo pagado','3661','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2373,5,345,270.00,'2013-04-07','abril pagado','3749','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2374,5,345,270.00,'2014-05-06','mayo pagado','3864','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2375,5,345,270.00,'2014-06-02','junio pagado','3925','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2376,5,345,270.00,'2014-07-07','julio pagado','4077','juan_fco_monter','2014-07-23',NULL,327);
INSERT INTO `payment` VALUES (2378,5,344,270.00,'2013-03-04','marzo pagado','2227','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2379,5,344,270.00,'2013-04-02','abril pagado','2336','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2380,5,344,270.00,'2013-05-02','mayo pagado\r\n','2430','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2381,5,344,270.00,'2013-06-08','junio pagado','2608','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2382,5,344,270.00,'2013-07-01','julio pagado','2626','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2383,5,344,270.00,'2013-08-08','agosto pagado','2839','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2384,5,344,320.00,'2013-09-09','septiembre pagado','2960','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2385,5,344,270.00,'2013-10-08','octubre pagado','3099','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2386,5,344,270.00,'2013-11-05','noviembre pagado','3168','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2387,5,344,270.00,'2013-12-05','diciembre pagado','3306','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2388,5,344,320.00,'2013-01-10','enero pagado','3476','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2389,5,344,270.00,'2014-02-06','febrero pagado','3557','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2390,5,344,270.00,'2014-03-07','marzo pagado','3680','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2391,5,344,270.00,'2014-04-07','abril pagado','3772','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2392,5,344,270.00,'2014-05-08','mayo pagado','3881','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2393,5,344,270.00,'2014-06-07','junio pagado','3985','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2394,5,344,270.00,'2014-07-07','julio pagado','4082','juan_fco_monter','2014-07-23',NULL,326);
INSERT INTO `payment` VALUES (2395,13,343,200.00,'2013-02-26','febrero pagado','2175','juan_fco_monter','2014-08-18',0,325);
INSERT INTO `payment` VALUES (2396,5,343,270.00,'2013-03-05','marzo pagado','2237','juan_fco_monter','2014-07-23',NULL,325);
INSERT INTO `payment` VALUES (2397,5,343,270.00,'2013-04-05','abril pagado','2359','juan_fco_monter','2014-07-23',NULL,325);
INSERT INTO `payment` VALUES (2400,5,343,0.00,'2013-05-31','Debe mayo','0000','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2401,5,343,0.00,'2013-06-30','debe junio','0000','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2402,5,343,960.00,'2013-07-16','mayo junio julio pagado','2730','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2403,5,343,270.00,'2013-08-05','agosto pagado','2780','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2404,5,343,270.00,'2013-09-05','septiembre pagado','2905','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2405,5,343,320.00,'2013-10-14','octubre pagado','3116','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2406,5,343,270.00,'2013-11-07','noviembre pagado','3208','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2407,5,343,270.00,'2013-12-01','diciembre pagado','3258','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2408,5,343,270.00,'2014-01-04','enero pagdo','3394','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2409,5,343,270.00,'2014-02-01','febrero pagado','3497','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2410,5,343,270.00,'2014-03-03','marzo pagado','3630','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2411,5,343,320.00,'2014-04-11','abril pagado','3801','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2412,5,343,270.00,'2014-05-01','mayo pagado','3817','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2414,5,342,270.00,'2013-03-02','marzo pagado','2196','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2415,5,342,270.00,'2013-04-02','abril pagado','2330','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2416,5,342,270.00,'2013-05-07','mayo pagado','2481','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2417,5,342,270.00,'2013-06-06','junio pagado','2578','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2418,5,342,270.00,'2013-07-06','julio pagado','2697','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2419,5,342,270.00,'2013-08-05','agosto pagado','2798','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2420,5,342,270.00,'2013-09-07','septiembre pagado','2948','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2421,5,342,270.00,'2013-10-07','octubre pagado','3076','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2422,5,342,270.00,'2013-11-02','noviembre pagado','3142','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2423,5,342,270.00,'2013-12-05','diciembre pagado','3298','juan_fco_monter','2014-07-23',NULL,324);
INSERT INTO `payment` VALUES (2424,5,343,270.00,'2014-06-02','junio pagado','3923','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (2425,5,342,270.00,'2014-01-04','enero pagado','3393','juan_fco_monter','2014-08-24',0,324);
INSERT INTO `payment` VALUES (2426,5,342,270.00,'2014-02-04','febrero pagado','3521','juan_fco_monter','2014-08-24',0,324);
INSERT INTO `payment` VALUES (2427,5,342,270.00,'2014-03-04','marzo pagado','3652','juan_fco_monter','2014-08-24',0,324);
INSERT INTO `payment` VALUES (2428,5,342,270.00,'2014-04-07','abril pagado','3757','juan_fco_monter','2014-08-24',0,324);
INSERT INTO `payment` VALUES (2429,5,342,270.00,'2014-05-06','mayo pagado','3841','juan_fco_monter','2014-08-24',0,324);
INSERT INTO `payment` VALUES (2430,5,342,270.00,'2014-06-07','junio pagado','3989','juan_fco_monter','2014-08-24',0,324);
INSERT INTO `payment` VALUES (2432,5,341,270.00,'2013-03-08','marzo pagado','2280','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2433,5,341,320.00,'2013-06-01','abril pagado','2525','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2434,5,341,320.00,'2013-08-23','mayo pagado','2857','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2435,5,341,270.00,'2013-06-01','junio pagado','2524','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2436,5,341,270.00,'2013-07-06','julio pagado','2698','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2437,5,341,320.00,'2013-08-23','agosto pagado','2858','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2438,5,341,320.00,'2013-09-17','septiembre pagado','2977','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2439,5,341,270.00,'2013-10-03','octubre pagado','3039','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2440,5,341,270.00,'2013-11-05','noviembre pagado','3182','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2441,5,341,270.00,'2013-12-04','diciembre pagado','3287','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2442,5,341,320.00,'2014-01-16','enero pagado','3480','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2443,5,341,270.00,'2014-02-04','febrero pagado','3526','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2444,5,341,270.00,'2014-03-06','marzo pagado','3660','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2445,5,341,0.00,'2014-04-30','abril debe','3902','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2446,5,341,590.00,'2014-05-10','abril mayo pagado','3902','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2447,5,341,270.00,'2014-06-05','junio pagado','3959','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2448,5,341,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,323);
INSERT INTO `payment` VALUES (2449,13,340,200.00,'2013-02-19','febrero pagado','2164','juan_fco_monter','2014-08-18',0,322);
INSERT INTO `payment` VALUES (2450,5,340,270.00,'2013-03-02','marzo pagado','2205','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2451,5,340,270.00,'2013-04-01','abril pagado','2309','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2452,5,340,270.00,'2013-05-06','mayo pagado','2463','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2453,5,340,270.00,'2013-06-06','junio pagado','2586','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2454,5,340,270.00,'2013-07-06','julio pagado','2692','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2455,5,340,270.00,'2013-08-03','agosoto pagado','2772','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2456,5,340,270.00,'2013-09-06','septiembre pagado','2935','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2457,5,340,270.00,'2013-09-30','octubre pagado','3003','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2458,5,340,270.00,'2013-11-05','noviembre pagado','3190','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2459,5,340,270.00,'2013-12-05','diciembre pagado','3299','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2460,5,340,270.00,'2014-01-06','enero pagado','3409','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2461,5,340,270.00,'2014-02-10','febrero pagado','3587','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2462,5,340,270.00,'2014-03-06','marzo pagado','3655','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2463,5,340,270.00,'2014-04-05','abril pagado','3745','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2464,5,340,270.00,'2014-05-08','mayo pagado','3877','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2465,5,340,270.00,'2014-06-02','junio pagado','3931','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2466,5,340,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,322);
INSERT INTO `payment` VALUES (2468,5,339,270.00,'2013-03-04','marzo pagado','2213','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2469,5,339,270.00,'2013-04-06','abril pagado','2370','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2470,5,339,270.00,'2013-05-06','mayo pagado','2472','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2471,5,339,270.00,'2013-06-12','junio pagado','2617','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2472,5,339,270.00,'2013-07-02','julio pagado','2633','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2473,5,339,270.00,'2013-08-05','agosto pagado','2804','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2474,5,339,270.00,'2013-09-03','septiembre pagado','2873','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2475,5,339,270.00,'2013-10-04','octubre pagado','3048','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2476,5,339,270.00,'2013-11-05','noviembre pagado','3196','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2477,5,339,270.00,'2013-12-08','diciembre pagado','3339','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2478,5,339,270.00,'2014-01-06','enero pagado','3404','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2479,5,339,270.00,'2014-02-07','febrero pagado','3558','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2480,5,339,270.00,'2014-03-04','marzo pagado','3639','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2481,5,339,270.00,'2014-04-04','abril pagado','3742','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2482,5,339,270.00,'2014-05-06','mayo pagado','3844','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2483,5,339,270.00,'2014-06-05','junio pagado','3963','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2484,5,339,270.00,'2014-07-04','julio pagado','4063','juan_fco_monter','2014-07-23',NULL,321);
INSERT INTO `payment` VALUES (2485,13,338,350.00,'2013-02-19','febrero pagado','2160','juan_fco_monter','2014-08-20',0,320);
INSERT INTO `payment` VALUES (2486,5,338,270.00,'2013-03-08','marzo pagado','2277','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2487,5,338,270.00,'2013-04-08','abril pagado','2386','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2488,5,338,270.00,'2013-05-02','mayo pagado','2416','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2489,5,338,270.00,'2013-05-31','junio pagado','2515','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2490,5,338,270.00,'2013-07-02','julio pagado','2634','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2491,5,338,270.00,'2013-08-01','agosto pagado','2744','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2492,5,338,270.00,'2013-09-03','septiembre pagado','2879','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2493,5,338,270.00,'2013-09-30','octubre pagado','2995','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2494,5,338,270.00,'2013-11-11','noviembre pagado','3240','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2495,5,338,320.00,'2013-12-10','diciembre pagado','3353','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2496,5,338,270.00,'2014-01-06','enero pagado','3418','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2497,5,338,270.00,'2014-01-30','febrero pagado','3486','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2498,5,338,320.00,'2014-03-11','marzo pagado','3754','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2499,5,338,320.00,'2014-04-11','abril pagado','3799','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2500,5,338,320.00,'2014-05-13','mayo pagado','3909','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2502,5,338,320.00,'2014-06-12','junio pagado','4006','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2503,5,338,270.00,'2014-07-03','julio pagado','4036','juan_fco_monter','2014-07-23',NULL,320);
INSERT INTO `payment` VALUES (2505,5,337,270.00,'2013-03-04','marzo pagado','2219','juan_fco_monter','2014-07-23',NULL,319);
INSERT INTO `payment` VALUES (2508,5,337,270.00,'2013-04-05','abril pagado','2360','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2509,5,337,270.00,'2013-05-07','mayo pagado','2494','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2510,5,337,270.00,'2013-06-07','junio pagado','2591','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2511,5,337,270.00,'2013-07-04','julio pagado','2669','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2512,5,337,270.00,'2013-08-08','agosto pagado','2845','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2513,5,337,270.00,'2013-09-06','septiembre pagado','2993','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2514,5,337,270.00,'2013-10-03','octubre pagado','3036','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2515,5,337,270.00,'2013-11-01','noviembre pagado','3246','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2516,5,337,270.00,'2014-01-06','diciembre pagado','3435','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2517,5,337,270.00,'2014-01-06','enero pagado','3434','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2518,5,337,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2519,5,337,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2520,5,337,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2521,5,337,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-08-23',0,319);
INSERT INTO `payment` VALUES (2522,13,336,200.00,'2013-02-12','febrero pagado','2145','juan_fco_monter','2014-08-18',0,318);
INSERT INTO `payment` VALUES (2523,5,336,270.00,'2013-03-07','marzo pagado','2260','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2524,5,336,270.00,'2013-04-08','abril pagado','2392','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2525,5,336,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2526,5,336,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2527,5,336,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2528,5,336,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2529,5,336,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2530,5,336,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2531,5,336,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2532,5,336,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2533,5,336,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2534,5,336,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2535,5,336,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2536,5,336,2010.00,'2014-04-22','sep oct nov dic 13 ene feb mar abr 14 pagado','3809','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2537,5,336,3120.00,'2014-04-22','hasta noviembre 2014 pagado','3808 3810','juan_fco_monter','2014-08-24',0,318);
INSERT INTO `payment` VALUES (2539,5,336,0.00,'2014-06-30','junio pagado','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2540,5,336,0.00,'2014-07-31','julio pagado','0000','juan_fco_monter','2014-07-23',NULL,318);
INSERT INTO `payment` VALUES (2541,13,335,200.00,'2013-02-12','febrero pagado','2143','juan_fco_monter','2014-08-18',0,317);
INSERT INTO `payment` VALUES (2542,5,335,270.00,'2013-03-01','marzo pagado','2191','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2543,5,335,270.00,'2013-04-01','abril pagado','2296','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2544,5,335,270.00,'2013-04-30','mayo pagado','2410','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2545,5,335,270.00,'2013-06-04','junio pagado','2562','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2546,5,335,270.00,'2013-07-05','julio pagado','2674','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2547,5,335,270.00,'2013-08-05','agosto pagado','2789','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2548,5,335,270.00,'2013-09-09','septiembre pagado','2958','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2549,5,335,270.00,'2013-09-30','octubre pagado','2996','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2550,5,335,270.00,'2013-11-07','noviembre pagado','3225','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2551,5,335,270.00,'2013-12-05','diciembre pagado','3310','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2552,5,335,270.00,'2014-01-07','enero pagado','3463','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2553,5,335,270.00,'2014-02-01','febrero pagado','3495','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2554,5,335,270.00,'2014-03-03','marzo pagado','3623','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2555,5,335,270.00,'2014-04-10','abril pagado','3797','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2556,5,335,270.00,'2014-05-06','mayo pagado','3865','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2557,5,335,270.00,'2014-06-13','junio pagado','4007','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2558,5,335,270.00,'2014-07-21','julio pagado','4114','juan_fco_monter','2014-07-23',NULL,317);
INSERT INTO `payment` VALUES (2560,5,334,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2561,5,334,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2562,5,334,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2563,5,334,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2564,5,334,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2565,5,334,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2566,5,334,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2567,5,334,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2568,5,334,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2569,5,334,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2570,5,334,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2571,5,334,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2572,5,334,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2573,5,334,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2574,5,334,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2575,5,334,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2576,5,334,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,316);
INSERT INTO `payment` VALUES (2577,13,333,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-08-23',0,315);
INSERT INTO `payment` VALUES (2578,5,333,520.00,'2013-03-16','febrero marzo pagado','2285','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2579,5,333,320.00,'2013-04-20','abril pagado','2405','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2580,5,333,270.00,'2013-06-07','junio pagado','2600','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2581,5,333,320.00,'2013-06-08','mayo pagado','2602','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2582,5,333,320.00,'2013-07-20','julio pagado','2731','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2583,5,333,270.00,'2013-08-05','agosto pagado','2795','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2584,5,333,270.00,'2013-09-09','septiembre pagado','2957','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2585,5,333,270.00,'2013-10-08','octubre pagado','3106','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2586,5,333,320.00,'2013-12-02','noviembre pagado','3261','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2587,5,333,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2588,5,333,640.00,'2014-01-11','diciembre enero pagado','3478','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2589,5,333,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2590,5,333,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2591,5,333,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2592,5,333,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2593,5,333,1000.00,'2014-06-14','feb mar abri parcial may 2014','4009','juan_fco_monter','2014-08-23',0,315);
INSERT INTO `payment` VALUES (2595,5,333,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,315);
INSERT INTO `payment` VALUES (2597,5,332,270.00,'2013-03-02','marzo pagado','2201','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2598,5,332,270.00,'2013-04-04','abril pagado','2346','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2599,5,332,270.00,'2013-05-03','mayo pagado','2436','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2600,5,332,270.00,'2013-06-08','junio pagado','2604','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2601,5,332,270.00,'2013-06-08','julio pagado','270','juan_fco_monter','2014-08-19',0,314);
INSERT INTO `payment` VALUES (2602,5,332,270.00,'2013-08-05','agosto pagado','2783','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2603,5,332,270.00,'2013-09-05','septiembre pagado','2916','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2604,5,332,270.00,'2013-10-07','octubre pagado','3088','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2605,5,332,270.00,'2013-11-07','noviembre pagado','3224','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2606,5,332,270.00,'2013-12-05','diciembre pagado','3311','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2607,5,332,270.00,'2014-01-06','enero pagado','3424','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2608,5,332,270.00,'2014-02-07','febrero pagado','3570','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2609,5,332,270.00,'2014-03-06','marzo pagado','3667','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2610,5,332,270.00,'2014-04-07','abril pagado','3763','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2611,5,332,270.00,'2014-05-08','mayo pagado','3882','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2612,5,332,270.00,'2014-06-09','junio pagado','3993','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2613,5,332,270.00,'2014-07-08','julio pagado','4088','juan_fco_monter','2014-07-23',NULL,314);
INSERT INTO `payment` VALUES (2615,5,331,270.00,'2013-03-04','marzo pagado','2218','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2616,5,331,270.00,'2013-04-02','abril pagado','2328','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2617,5,331,270.00,'2013-05-08','mayo pagado','2505','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2618,5,331,270.00,'2013-06-04','junio pagado','2554','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2619,5,331,270.00,'2013-07-08','julio pagado','2709','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2620,5,331,270.00,'2013-08-01','agosto pagado','2750','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2621,5,331,270.00,'2013-08-03','septiembre pagado','2883','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2622,5,331,270.00,'2013-10-03','octubre pagado','3033','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2623,5,331,270.00,'2013-11-01','noviembre pagado','3132','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2625,5,331,270.00,'2013-12-03','diciembre pagado','3281','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2626,5,331,270.00,'2014-01-08','enero pagado','3447','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2627,5,331,320.00,'2014-02-10','febrero pagado','3586','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2628,5,331,270.00,'2014-03-06','marzo pagado','3657','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2629,5,331,270.00,'2014-04-04','abril pagado','3740','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2630,5,331,270.00,'2014-05-08','mayo pagado','3873','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2631,5,331,320.00,'2014-06-13','junio pagado','4008','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2632,5,331,270.00,'2014-07-07','julio pagado','4070','juan_fco_monter','2014-07-23',NULL,313);
INSERT INTO `payment` VALUES (2633,13,330,200.00,'2013-02-22','febrero pagado','2174','juan_fco_monter','2014-08-18',0,312);
INSERT INTO `payment` VALUES (2634,5,330,270.00,'2013-03-08','marzo pagado','2268','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2635,5,330,270.00,'2013-04-03','abril pagado','2341','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2636,5,330,270.00,'2013-05-07','mayo pagado','2482','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2637,5,330,270.00,'2013-06-04','junio pagado','2563','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2638,5,330,270.00,'2013-07-03','julio pagado','2659','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2639,5,330,270.00,'2013-08-01','agosto pagado','2747','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2640,5,330,270.00,'2013-09-05','septiembre pagado','2923','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2641,5,330,270.00,'2013-09-30','octubre pagado','3008','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2642,5,330,270.00,'2013-11-05','noviembre pagado','3179','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2643,5,330,270.00,'2013-12-05','diciembre pagado','3313','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2644,5,330,270.00,'2013-12-30','enero pagado','3376','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2645,5,330,270.00,'2014-02-04','febrero pagado','3529','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2646,5,330,270.00,'2014-02-25','marzo pagado','3600','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2647,5,330,270.00,'2014-04-03','abril pagado','3737','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2648,5,330,270.00,'2014-05-02','mayo pagado','3827','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2649,5,330,270.00,'2014-06-03','junio pagado','3953','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2650,5,330,270.00,'2014-07-04','julio pagado','4055','juan_fco_monter','2014-07-23',NULL,312);
INSERT INTO `payment` VALUES (2652,5,329,270.00,'2013-03-02','marzo pagado','2193','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2653,5,329,270.00,'2013-04-04','abril pagado','2344','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2654,5,329,270.00,'2013-05-03','mayo pagado','2439','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2655,5,329,270.00,'2013-06-01','junio pagado','2521','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2656,5,329,270.00,'2013-07-06','julio pagado','2686','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2657,5,329,320.00,'2013-08-10','agosto pagado','2847','juan_fco_monter','2014-08-19',0,311);
INSERT INTO `payment` VALUES (2658,5,329,270.00,'2013-09-05','septiembre pagado','2921','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2659,5,329,270.00,'2013-10-07','octubre pagado','3097','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2660,5,329,270.00,'2013-11-05','noviembre pagado','3187','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2661,5,329,270.00,'2013-12-05','diciembre pagado','3295','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2662,5,329,270.00,'2014-01-06','enero pagado','3433','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2663,5,329,270.00,'2014-02-06','febrero pagado','3541','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2664,5,329,270.00,'2014-03-06','marzo pagado','3666','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2665,5,329,270.00,'2014-04-08','abril pagado','3774','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2666,5,329,270.00,'2014-05-06','mayo pagado','3862','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2667,5,329,270.00,'2014-06-07','junio pagado','3979','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2668,5,329,270.00,'2014-07-07','julio pagado','4069','juan_fco_monter','2014-07-23',NULL,311);
INSERT INTO `payment` VALUES (2669,13,328,200.00,'2013-04-06','febrero pagado','2378','juan_fco_monter','2014-08-24',0,310);
INSERT INTO `payment` VALUES (2670,5,328,270.00,'2013-04-06','marzo pagado','2377','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2671,5,328,270.00,'2013-04-06','abril pagado','2376','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2672,5,328,270.00,'2013-05-07','mayo pagado','2495','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2673,5,328,270.00,'2013-08-23','junio pagado','2854','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2674,5,328,270.00,'2013-08-23','julio pagado','2855','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2675,5,328,270.00,'2013-08-23','agosto pagado','2856','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2676,5,328,270.00,'2013-09-06','septiembre pagado','2932','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2677,5,328,270.00,'2013-10-09','octubre pagado','3702','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2678,5,328,540.00,'2013-11-14','noviembre diciembre pagado','3703','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2679,5,328,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2680,5,328,540.00,'2013-11-27','enero febrero pagado','3704','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2681,5,328,0.00,'2014-02-28','febrero pagado','0000','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2682,5,328,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2683,5,328,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2684,5,328,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2685,5,328,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2686,5,328,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,310);
INSERT INTO `payment` VALUES (2688,5,327,270.00,'2013-03-05','marzo pagado','2236','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2689,5,327,270.00,'2013-03-30','abril pagado','2294','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2690,5,327,540.00,'2013-05-07','mayo junio pagado','2487','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2691,5,327,0.00,'2014-06-30','junio pagado','0000','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2692,5,327,270.00,'2013-07-08','julio pagado','2708','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2693,5,327,270.00,'2013-08-07','agosto pagado','2831','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2695,5,327,320.00,'2013-09-09','septiembre pagado','2953','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2696,5,327,270.00,'2013-10-07','octubre pagado','3075','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2697,5,327,270.00,'2013-11-07','noviembre pagado','3216','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2698,5,327,270.00,'2013-12-03','diciembre pagado','3274','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2699,5,327,270.00,'2014-01-07','enero pagado','3460','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2700,5,327,270.00,'2014-02-07','febrero pagado','3561','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2701,5,327,270.00,'2014-03-07','marzo pagado','3682','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2702,5,327,270.00,'2014-04-07','abril pagado','3771','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2703,5,327,270.00,'2014-04-26','mayo pagado','3811','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2704,5,327,270.00,'2014-06-07','junio pagado','3984','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2705,5,327,270.00,'2014-07-04','julio pagado','4062','juan_fco_monter','2014-07-23',NULL,309);
INSERT INTO `payment` VALUES (2706,13,326,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-08-23',0,308);
INSERT INTO `payment` VALUES (2707,5,326,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2708,5,326,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2709,5,326,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2710,5,326,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2711,5,326,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2712,5,326,2120.00,'2013-08-22','feb a agosto pagado','2853','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2713,5,326,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2714,5,326,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2715,5,326,1180.00,'2013-11-02','sep a dic pagado','3139','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2716,5,326,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2717,5,326,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2718,5,326,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2719,5,326,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2720,5,326,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2721,5,326,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2722,5,326,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2723,5,326,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,308);
INSERT INTO `payment` VALUES (2725,5,325,270.00,'2013-03-04','marzo pagado\r\n','2222','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2726,5,325,270.00,'2013-04-01','abril pagado','2306','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2727,5,325,270.00,'2013-05-02','mayo pagado','2415','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2728,5,325,270.00,'2013-06-05','junio pagado','2566','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2729,5,325,270.00,'2013-07-05','julio pagado','2677','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2730,5,325,270.00,'2013-08-01','agosto pagado','2752','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2731,5,325,270.00,'2013-09-03','septiembre pagado','2878','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2732,5,325,270.00,'2013-10-03','octubre pagado','3043','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2733,5,325,270.00,'2013-11-03','noviembre pagado','3183','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2734,5,325,320.00,'2013-12-16','diciembre pagado','3360','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2735,5,325,270.00,'2014-01-07','enero pagado','3462','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2736,5,325,270.00,'2014-02-03','febrero pagado','5311','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2737,5,325,270.00,'2014-03-03','marzo pagado','3621','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2738,5,325,270.00,'2014-04-03','abril pagado','3735','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2739,5,325,270.00,'2014-05-02','mayo pagado','3826','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2740,5,325,270.00,'2014-06-09','junio pagado','3995','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2741,5,325,270.00,'2014-07-08','julio pagado','4095','juan_fco_monter','2014-07-23',NULL,307);
INSERT INTO `payment` VALUES (2743,5,324,270.00,'2013-03-06','marzo pagado','2253','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2744,5,324,270.00,'2013-04-01','abril pagado','2307','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2745,5,324,270.00,'2013-05-02','mayo pagado','2422','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2746,5,324,270.00,'2013-06-03','junio pagado','2534','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2747,5,324,270.00,'2013-07-02','julio pagado','2640','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2748,5,324,270.00,'2013-08-05','agosto pagado','2779','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2749,5,324,270.00,'2013-09-04','septiembre pagado','2899','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2750,5,324,270.00,'2013-09-30','octubre pagado','2997','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2751,5,324,270.00,'2013-11-04','noviembre pagado','3160','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2752,5,324,320.00,'2013-12-10','diciembre pagado','3350','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2753,5,324,270.00,'2014-01-04','enero pagado','3402','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2754,5,324,270.00,'2014-02-01','febrero pagado','3499','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2755,5,324,270.00,'2014-03-04','marzo pagado','3641','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2756,5,324,270.00,'2014-04-03','abril pagado','3727','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2757,5,324,270.00,'2014-05-10','mayo pagado','3899','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2758,5,324,270.00,'2014-06-07','junio pagado','3980','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2759,5,324,270.00,'2014-07-08','julio pagado','4101','juan_fco_monter','2014-07-23',NULL,306);
INSERT INTO `payment` VALUES (2761,5,323,270.00,'2013-03-05','marzo pagado','2238','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2762,5,323,270.00,'2013-04-04','abril pagado','2348','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2763,5,323,270.00,'2013-05-06','mayo pagado','2474','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2764,5,323,270.00,'2013-06-07','junio pagado','2594','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2765,5,323,270.00,'2013-07-02','julio pagado','2648','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2766,5,323,270.00,'2013-08-03','agosto pagado','2776','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2767,5,323,270.00,'2013-09-05','septiembre pagado','2922','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2768,5,323,270.00,'2013-10-08','octubre pagado','3103','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2769,5,323,270.00,'2013-11-05','noviembre pagado','3193','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2770,5,323,270.00,'2013-12-03','diciembre pagado','3286','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2771,5,323,270.00,'2014-01-06','enero pagado','3429','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2772,5,323,270.00,'2014-02-06','febrero pagado','3553','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2773,5,323,270.00,'2014-03-10','marzo pagado','3698','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2774,5,323,270.00,'2014-04-07','abril pagado','3750','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2775,5,323,270.00,'2014-05-06','mayo pagado','3863','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2776,5,323,270.00,'2014-06-09','junio pagado','3998','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2777,5,323,270.00,'2014-07-03','julio pagado','4028','juan_fco_monter','2014-07-23',NULL,305);
INSERT INTO `payment` VALUES (2779,5,322,270.00,'2013-03-05','marzo pagado','2241','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2780,5,322,270.00,'2013-04-01','abril pagado','2300','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2781,5,322,270.00,'2013-05-04','mayo pagado','2444','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2782,5,322,270.00,'2013-06-03','junio pagado','2539','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2783,5,322,270.00,'2013-07-02','julio pagado','2636','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2784,5,322,270.00,'2013-08-03','agosto pagado','2762','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2785,5,322,270.00,'2013-09-03','septiembre pagado','2877','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2786,5,322,270.00,'2013-10-01','octubre pagado','3013','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2787,5,322,270.00,'2013-11-07','noviembre pagado','3212','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2788,5,322,270.00,'2013-12-02','diciembre pagado','3263','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2789,5,322,270.00,'2014-01-03','enero pagado','3391','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2790,5,322,270.00,'2014-02-06','febrero pagado','3531','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2791,5,322,270.00,'2014-03-08','marzo pagado','3691','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2792,5,322,320.00,'2014-04-10','abril pagado','3788','juan_fco_monter','2014-08-19',0,304);
INSERT INTO `payment` VALUES (2793,5,322,270.00,'2014-05-10','mayo pagado','3901','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2794,5,322,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,304);
INSERT INTO `payment` VALUES (2795,5,322,270.00,'2014-07-08','junio julio pagado','4086','juan_fco_monter','2014-08-23',0,304);
INSERT INTO `payment` VALUES (2797,5,321,270.00,'2013-03-04','marzo pagado','2208','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2798,5,321,270.00,'2013-04-01','abril pagado','2310','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2799,5,321,270.00,'2013-05-04','mayo pagado','2447','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2800,5,321,270.00,'2013-06-01','junio pagado','2520','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2801,5,321,270.00,'2013-07-06','julio pagado','2690','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2802,5,321,270.00,'2013-07-31','julio pagado','2738','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2803,5,321,270.00,'2013-08-03','agosto pagado','2884','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2804,5,321,270.00,'2013-10-01','octubre pagado','3710','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2805,5,321,270.00,'2013-11-01','noviembre pagado','3129','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2806,5,321,270.00,'2013-12-01','diciembre pagado','3255','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2807,5,321,270.00,'2014-01-03','enero pagado','3389','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2808,5,321,270.00,'2014-02-01','febrero pagado','3498','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2809,5,321,270.00,'2014-03-04','marzo pagado','3648','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2810,5,321,270.00,'2014-04-03','abril pagado','3732','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2811,5,321,270.00,'2014-05-06','mayo pagado','3858','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2812,5,321,270.00,'2014-06-02','junio pagado','3937','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2813,5,321,270.00,'2014-06-30','julio pagado','4019','juan_fco_monter','2014-07-23',NULL,303);
INSERT INTO `payment` VALUES (2814,13,320,200.00,'2013-02-15','febrero pagado','2156','juan_fco_monter','2014-08-18',0,302);
INSERT INTO `payment` VALUES (2815,5,320,270.00,'2013-03-18','marzo pagado','2286','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2816,5,320,270.00,'2013-04-16','abril pagado','2402','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2817,5,320,270.00,'2013-05-15','mayo pagado','2508','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2818,5,320,270.00,'2013-06-19','junio pagado','2619','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2819,5,320,270.00,'2013-07-15','junio pagado','2729','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2820,5,320,270.00,'2013-08-16','agosto pagado','2852','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2821,5,320,200.00,'2013-10-03','pago parcial','3062','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2822,5,320,200.00,'2013-11-04','pago parcial','31661','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2823,5,320,200.00,'2013-12-05','pago parcial','3301','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2824,5,320,200.00,'2014-01-06','pago parcial','3426','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2825,5,320,200.00,'2014-01-06','pago parcial','3427','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2826,5,320,200.00,'2014-02-10','pago parcial','3588','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2827,5,320,200.00,'2014-03-10','pago parical marzo','3696','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2828,5,320,200.00,'2014-04-21','abril pagado','3806','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2829,5,320,200.00,'2014-05-30','pago parcial mayo','3917','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2830,5,320,200.00,'2014-06-26','pago parcial junio','4014','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2831,5,320,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,302);
INSERT INTO `payment` VALUES (2833,5,319,270.00,'2013-03-02','marzo pagado','2203','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2834,5,319,270.00,'2013-04-02','abril pagado','2338','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2835,5,319,270.00,'2013-05-02','mayo pagado','2417','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2836,5,319,270.00,'2013-06-03','junio pagado','2535','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2837,5,319,270.00,'2013-07-05','julio pagado','2675','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2838,5,319,270.00,'2013-08-08','agosto pagado','2836','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2839,5,319,270.00,'2013-09-03','septiembre pagado','2889','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2840,5,319,270.00,'2013-10-03','octubre pagado','3042','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2841,5,319,270.00,'2013-11-08','noviembre pagado','3231','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2842,5,319,270.00,'2013-12-04','diciembre pagado','3288','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2843,5,319,270.00,'2014-01-08','enero pagado','3439','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2844,5,319,270.00,'2014-02-03','febrero pagado','3515','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2845,5,319,270.00,'2014-03-07','marzo pagado','3681','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2846,5,319,270.00,'2014-04-07','abril pagado','3766','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2847,5,319,270.00,'2014-05-08','mayo pagado','3875','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2848,5,319,270.00,'2014-05-31','junio pagado','3918','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2849,5,319,270.00,'2014-07-04','julio pagado','4059','juan_fco_monter','2014-07-23',NULL,301);
INSERT INTO `payment` VALUES (2851,5,318,270.00,'2013-03-04','marzo pagado','2220','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2852,5,318,270.00,'2013-04-11','abril pagado','2400','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2853,5,318,270.00,'2013-05-06','mayo pagado','2469','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2854,5,318,270.00,'2013-06-04','junio pagado','2555','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2855,5,318,270.00,'2013-07-01','julio pagado','2627','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2856,5,318,270.00,'2013-08-05','agosto pagado','2790','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2857,5,318,270.00,'2013-09-05','septiembre pagado','2907','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2858,5,318,270.00,'2013-09-30','octubre pagado','3006','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2859,5,318,270.00,'2013-11-06','noviembre pagado','3204','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2860,5,318,270.00,'2013-12-02','diciembre pagado','3262','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2861,5,318,270.00,'2014-01-08','enero pagado','3442','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2862,5,318,270.00,'2014-02-06','febrero pagado','3543','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2863,5,318,270.00,'2014-02-25','marzo pagado','3601','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2864,5,318,270.00,'2014-04-03','abril pagado','3734','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2865,5,318,270.00,'2014-05-06','mayo pagado','3843','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2866,5,318,270.00,'2014-06-03','junio pagado','3951','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2867,5,318,270.00,'2014-07-03','julio pagado','4042','juan_fco_monter','2014-07-23',NULL,300);
INSERT INTO `payment` VALUES (2869,5,317,270.00,'2013-03-02','marzo pagado','2204','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2870,5,317,270.00,'2013-04-01','abril pagado','2314','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2871,5,317,270.00,'2013-05-02','mayo pagado','2426','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2873,5,317,270.00,'2013-06-03','junio pagado','2536','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2874,5,317,270.00,'2013-07-05','julio pagado','2678','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2875,5,317,270.00,'2013-08-05','agosto pagado','2782','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2876,5,317,270.00,'2013-09-07','septiembre pagado','2945','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2877,5,317,270.00,'2013-10-03','octubre pagado','3031','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2878,5,317,270.00,'2013-11-05','noviembre pagado','3184','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2879,5,317,270.00,'2013-12-07','diciembre pagado','3329','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2880,5,317,270.00,'2014-01-02','enero pagado','3379','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2881,5,317,270.00,'2014-02-04','febrero pagado','3518','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2882,5,317,270.00,'2014-03-03','marzo pagado','3628','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2883,5,317,270.00,'2014-04-01','abril pagado','3718','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2884,5,317,270.00,'2014-05-06','mayo pagado','3853','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2885,5,317,270.00,'2014-06-02','junio pagado','3934','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2886,5,317,270.00,'2014-06-30','julio pagado','4020','juan_fco_monter','2014-07-23',NULL,299);
INSERT INTO `payment` VALUES (2887,13,316,200.00,'2013-02-10','febrero pagado','2139','juan_fco_monter','2014-08-18',0,298);
INSERT INTO `payment` VALUES (2888,5,316,270.00,'2013-03-04','marzo pagado','2223','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2889,5,316,270.00,'2013-04-01','abril pagado','2303','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2890,5,316,270.00,'2013-05-07','mayo pagado','2448','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2891,5,316,270.00,'2013-06-05','junio pagado','2567','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2892,5,316,320.00,'2013-07-11','julio pagado','2725','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2893,5,316,320.00,'2013-08-13','agosto pagado','2851','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2894,5,316,270.00,'2013-09-03','septiembre pagado','2869','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2895,5,316,270.00,'2013-10-08','octubre pagado','3110','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2896,5,316,270.00,'2013-11-07','noviembre pagado','3211','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2897,5,316,270.00,'2013-12-09','diciembre pagado','3345','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2898,5,316,270.00,'2014-01-07','enero pagado','3465','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2899,5,316,320.00,'2014-02-19','febrero pagado','3593','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2900,5,316,270.00,'2014-03-06','marzo pagado','3654','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2901,5,316,270.00,'2014-04-03','abril pagado','3725','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2902,5,316,320.00,'2014-06-02','mayo pagado','3936','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2903,5,316,270.00,'2014-06-02','junio pagado','3933','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2904,5,316,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,298);
INSERT INTO `payment` VALUES (2906,5,315,270.00,'2013-03-05','marzo pagado','2243','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2907,5,315,270.00,'2013-04-08','abril pagado','2382','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2908,5,315,270.00,'2013-05-08','mayo pagado','2504','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2909,5,315,270.00,'2013-06-12','junio pagado','2611','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2910,5,315,270.00,'2013-07-22','julio pagado','2732','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2911,5,315,270.00,'2013-08-06','agosto pagado','2808','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2912,5,315,270.00,'2013-09-04','septiembre pagado','29093','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2913,5,315,270.00,'2013-10-07','octubre pagado','2954','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2914,5,315,270.00,'2013-11-07','noviembre pagado','3213','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2915,5,315,320.00,'2013-12-09','diciembre pagado','3348','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2916,5,315,270.00,'2014-01-07','enero pagado','3455','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2917,5,315,270.00,'2014-02-08','febrero pagado','3580','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2918,5,315,270.00,'2014-03-06','marzo pagado','3662','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2919,5,315,270.00,'2014-04-03','abril pagado','3726','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2920,5,315,270.00,'2014-05-06','mayo pagado','3850','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2921,5,315,270.00,'2014-06-06','junio pagado','3970','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2922,5,315,270.00,'2014-07-19','julio pagado','4108','juan_fco_monter','2014-07-23',NULL,297);
INSERT INTO `payment` VALUES (2923,13,313,200.00,'2013-02-15','febrero pagado','2155','juan_fco_monter','2014-08-18',0,296);
INSERT INTO `payment` VALUES (2924,5,313,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2925,5,313,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2926,5,313,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2927,5,313,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2928,5,313,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2929,5,313,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2930,5,313,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2931,5,313,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2932,5,313,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2933,5,313,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2934,5,313,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2935,5,313,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2936,5,313,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2937,5,313,0.00,'2014-04-30','abril debe\r\n','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2938,5,313,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2939,5,313,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2940,5,313,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-23',NULL,296);
INSERT INTO `payment` VALUES (2941,13,312,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-08-24',0,295);
INSERT INTO `payment` VALUES (2942,5,312,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2943,5,312,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2944,5,312,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2945,5,312,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2946,5,312,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2947,5,312,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2948,5,312,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2949,5,312,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2950,5,312,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2951,5,312,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2952,5,312,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2953,5,312,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2954,5,312,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2955,5,312,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2956,5,312,0.00,'2014-05-31','mayo debe','00000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2957,5,312,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2958,5,312,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-24',NULL,295);
INSERT INTO `payment` VALUES (2959,13,298,200.00,'2013-02-18','febrero pagado','2158','juan_fco_monter','2014-08-18',0,282);
INSERT INTO `payment` VALUES (2960,5,298,270.00,'2013-03-06','marzo pagado','2255','juan_fco_monter','2014-07-24',NULL,282);
INSERT INTO `payment` VALUES (2961,5,298,270.00,'2013-04-04','abril pagado','2351','juan_fco_monter','2014-07-24',NULL,282);
INSERT INTO `payment` VALUES (2962,5,298,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-24',NULL,282);
INSERT INTO `payment` VALUES (2963,5,298,320.00,'2013-06-10','junio debe','2610','juan_fco_monter','2014-07-24',NULL,282);
INSERT INTO `payment` VALUES (2964,5,298,270.00,'2013-07-03','julio pagado','2653','juan_fco_monter','2014-07-24',NULL,282);
INSERT INTO `payment` VALUES (2965,5,297,270.00,'2013-08-05','agosto debe','2801','juan_fco_monter','2014-07-24',NULL,281);
INSERT INTO `payment` VALUES (2966,5,298,270.00,'2013-08-05','agosto pagado','2801','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2967,5,298,270.00,'2013-09-05','septiembre pagado','2917','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2968,5,298,270.00,'2013-10-02','octubre pagado','3023','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2969,5,298,270.00,'2013-11-05','noviembre pagado','3198','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2970,5,298,270.00,'2013-12-01','diciembre pagado','3251','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2971,5,298,270.00,'2013-12-28','enero pagado','3368','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2972,5,298,270.00,'2014-02-06','febrero pagado','3536','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2973,5,298,270.00,'2014-03-04','marzo pagado','3653','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2974,5,298,270.00,'2014-04-05','abril pagado','3744','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2975,5,298,270.00,'2014-05-06','mayo pagado','3857','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2976,5,298,270.00,'2014-06-05','junio pagado','3962','juan_fco_monter','2014-08-24',0,282);
INSERT INTO `payment` VALUES (2977,5,299,270.00,'2013-02-19','febrero pagado','2163','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2978,5,299,270.00,'2013-03-01','marzo pagado','2177','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2979,5,299,270.00,'2013-04-04','abril pagado','2357','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2980,5,299,270.00,'2013-05-02','mayo pagado','2418','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2981,5,299,270.00,'2013-06-05','junio pagado','2565','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2982,5,299,270.00,'2013-07-02','julio pagado','2639','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2983,5,299,270.00,'2013-08-02','agosto pagado','2754','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2984,5,299,270.00,'2013-09-05','septiembre pagado','2912','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2985,5,299,270.00,'2013-09-30','octubre pagado','2999','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2986,5,299,270.00,'2013-11-05','noviembre pagado','3163','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2987,5,299,320.00,'2013-12-10','diciembre pagado','3349','juan_fco_monter','2014-08-19',0,283);
INSERT INTO `payment` VALUES (2988,5,299,270.00,'2014-01-03','enero pagado','3392','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2989,5,299,270.00,'2014-02-04','febrero pagado','3520','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2990,5,299,270.00,'2014-03-03','marzo pagado','3625','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2991,5,299,270.00,'2014-04-07','abril pagado','3765','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2992,5,299,270.00,'2014-05-08','mayo pagado','3891','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2993,5,299,270.00,'2014-06-06','junio pagado','3966','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2994,5,299,270.00,'2014-07-04','julio pagado','4052','juan_fco_monter','2014-07-24',NULL,283);
INSERT INTO `payment` VALUES (2996,5,300,270.00,'2013-03-04','marzo pagado','2210','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (2997,5,300,270.00,'2013-04-02','abril pagado','2331','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (2998,5,300,270.00,'2013-05-02','mayo pagado','2429','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (2999,5,300,270.00,'2013-06-03','junio pagado','2538','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3000,5,300,270.00,'2013-07-08','julio pagado','2705','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3001,5,300,270.00,'2013-08-03','agosto pagado','2764','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3002,5,300,270.00,'2013-09-05','septiembre pagado','2914','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3003,5,300,270.00,'2013-10-03','octubre pagado','3038','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3004,5,300,270.00,'2013-11-05','noviembre pagado','3180','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3005,5,300,270.00,'2013-12-02','diciembre pagado','3276','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3006,5,300,270.00,'2014-01-06','enero pagado','3408','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3007,5,300,270.00,'2014-02-01','febrero pagado','3492','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3008,5,300,270.00,'2014-03-03','marzo pagado','3616','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3009,5,300,270.00,'2014-03-31','marzo pagado','3708','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3010,5,300,270.00,'2014-05-03','mayo pagado','3837','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3011,5,300,270.00,'2014-06-05','junio pagado','3956','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3012,5,300,270.00,'2014-07-03','julio pagado','4029','juan_fco_monter','2014-07-24',NULL,284);
INSERT INTO `payment` VALUES (3013,13,301,200.00,'2013-02-10','febrero pagado','2141','juan_fco_monter','2014-08-18',0,285);
INSERT INTO `payment` VALUES (3014,5,301,270.00,'2013-03-01','marzo pagado','2187','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3015,5,301,270.00,'2013-04-08','abril pagado','2394','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3016,5,301,0.00,'2013-05-30','mayo debe','0000','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3017,5,301,270.00,'2013-06-03','junio pagado','2547','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3018,5,301,320.00,'2013-08-03','julio pagado','2771','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3019,5,301,270.00,'2013-08-03','agosto pagado','2760','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3020,5,301,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3021,5,301,540.00,'2013-10-15','septiembre octubre pagado','3120','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3022,5,301,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3023,5,301,540.00,'2013-12-14','noviembre diciembre pagado','3356','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3024,5,301,270.00,'2013-12-14','enero pagado','3357','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3025,5,301,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3026,5,301,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3027,5,301,500.00,'2014-05-17','pago a cuenta','3911','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3028,5,301,310.00,'2014-05-06','pago a cuenta','3867','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3029,5,301,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3030,5,301,540.00,'2014-07-15','pago a cuenta','4107','juan_fco_monter','2014-07-24',NULL,285);
INSERT INTO `payment` VALUES (3032,5,302,270.00,'2013-03-01','marzo pagado','2190','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3033,5,302,270.00,'2013-04-02','abril pagado','2332','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3034,5,302,270.00,'2013-05-06','mayo pagado','2471','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3035,5,302,270.00,'2013-06-04','junio pagado','2560','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3036,5,302,270.00,'2013-07-08','julio pagado','2703','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3037,5,302,270.00,'2013-08-07','agosto pagado','2818','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3038,5,302,270.00,'2013-09-03','septiembre pagado','2876','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3039,5,302,270.00,'2013-10-03','octubre pagado','3025','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3040,5,302,270.00,'2013-11-04','noviembre pagado','3161','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3041,5,302,590.00,'2013-12-28','diciembre enero pagado','3370','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3042,5,302,0.00,'2014-01-31','enero pagado','0000','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3043,5,302,270.00,'2014-02-06','febrero pagado','3538','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3044,5,302,270.00,'2014-02-06','marzo pagado','3547','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3045,5,302,540.00,'2014-04-15','abril mayo pagado','3804','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3046,5,302,0.00,'2014-04-15','mayo pagado','0000','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3047,5,302,810.00,'2014-06-06','jun jul ago pagado','3968','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3048,5,302,0.00,'2014-07-31','julio pagado','0000','juan_fco_monter','2014-07-24',NULL,286);
INSERT INTO `payment` VALUES (3049,13,303,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-08-23',0,287);
INSERT INTO `payment` VALUES (3050,5,303,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3051,5,303,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3052,5,303,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3053,5,303,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3054,5,303,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3055,5,303,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3056,5,303,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3057,5,303,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3058,5,303,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3059,5,303,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3060,5,303,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3061,5,303,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3062,5,303,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3063,5,303,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3064,5,303,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3065,5,303,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3066,5,303,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-24',NULL,287);
INSERT INTO `payment` VALUES (3067,13,304,200.00,'2013-02-20','febrero pagado','2166','juan_fco_monter','2014-08-18',0,288);
INSERT INTO `payment` VALUES (3068,5,304,270.00,'2013-03-02','marzo pagado','2200','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3069,5,304,320.00,'2013-04-09','abril pagado','3396','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3070,5,304,270.00,'2013-05-06','mayo pagado','2468','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3071,5,304,270.00,'2013-06-07','junio pagado','2589','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3072,5,304,270.00,'2013-07-08','julio pagado','2720','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3073,5,304,270.00,'2013-08-03','agosto pagado','2773','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3074,5,304,270.00,'2013-09-03','septiembre pagado','2872','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3075,5,304,270.00,'2013-10-03','octubre pagado','3040','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3076,5,304,270.00,'2013-11-02','noviembre pagado','3147','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3077,5,304,270.00,'2013-12-07','diciembre pagado','3323','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3078,5,304,270.00,'2013-12-28','enero pagado','3369','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3079,5,304,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3080,5,304,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3081,5,304,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3082,5,304,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3083,5,304,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3084,5,304,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-24',NULL,288);
INSERT INTO `payment` VALUES (3085,13,305,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-08-24',0,289);
INSERT INTO `payment` VALUES (3087,5,305,470.00,'2013-03-05','febrero marzo pagado','2246','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3088,5,305,270.00,'2013-04-01','abril pagado','2313','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3089,5,305,270.00,'2013-05-06','mayo pagado','2478','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3090,5,305,320.00,'2013-06-12','junio pagado','2612','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3091,5,305,270.00,'2013-07-02','julio pagado','2650','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3092,5,305,270.00,'2013-08-07','julio pagado','2820','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3093,5,305,270.00,'2013-09-05','septiembre pagado','2915','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3095,5,305,270.00,'2013-11-05','Noviembre pagado','3178','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3096,5,305,270.00,'2013-12-06','diciembre pagado','3319','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3097,5,305,270.00,'2014-01-02','enero pagado','3377','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3098,5,305,270.00,'2014-02-10','febrero pagado','3584','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3099,5,305,270.00,'2014-03-04','marzo pagado','3643','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3100,5,305,320.00,'2014-04-10','abril pagado','3792','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3101,5,305,270.00,'2014-05-08','mayo pagado','3892','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3102,5,305,320.00,'2014-06-12','junio pagado','4005','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3103,5,305,270.00,'2014-07-04','julio pagado','4051','juan_fco_monter','2014-07-24',NULL,289);
INSERT INTO `payment` VALUES (3105,5,306,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3107,5,306,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3108,5,306,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3109,5,306,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3110,5,306,270.00,'2013-07-08','julio pagado','2719','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3111,5,306,270.00,'2013-08-08','agosto pagado','2843','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3112,5,306,270.00,'2013-09-06','septiembre pagado','2929','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3113,5,306,320.00,'2013-11-28','noviembre pagado','3250','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3114,5,306,270.00,'2013-12-07','diciembre pagado','3335','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3115,5,306,270.00,'2014-01-04','enero pagado','3396','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3116,5,306,270.00,'2014-02-01','febrero pagado','3493','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3117,5,306,270.00,'2014-03-07','marzo pagado','3679','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3118,5,306,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3119,5,306,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3120,5,306,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3121,5,306,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-24',NULL,290);
INSERT INTO `payment` VALUES (3123,5,307,270.00,'2013-03-08','marzo pagado','2279','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3124,5,307,270.00,'2013-04-01','abril pagado','2316','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3125,5,307,320.00,'2013-05-09','mayo pagado','2501','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3126,5,307,270.00,'2013-06-07','junio pagado','2596','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3127,5,307,270.00,'2013-07-08','julio pagado','2723','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3128,5,307,320.00,'2013-08-12','agosto pagado','2848','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3129,5,307,320.00,'2013-09-09','septiembre pagado','2966','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3130,5,307,270.00,'2013-10-07','octubre pagado','3085','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3131,5,307,270.00,'2013-11-08','noviembre pagado','3234','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3132,5,307,320.00,'2013-12-14','diciembre pagado','3355','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3133,5,307,270.00,'2014-01-07','enero pagado','3471','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3134,5,307,270.00,'2014-02-03','febrero pagado','3514','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3135,5,307,270.00,'2014-03-10','marzo pagado','3700','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3136,5,307,270.00,'2014-04-05','abril pagado','3743','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3137,5,307,270.00,'2014-05-08','mayo pagado','3878','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3138,5,307,270.00,'2014-06-06','junio pagado','3971','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3139,5,307,270.00,'2014-07-07','julio pagado','4081','juan_fco_monter','2014-07-24',NULL,291);
INSERT INTO `payment` VALUES (3141,5,308,270.00,'2013-03-01','marzo pagado','2180','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3142,5,308,270.00,'2013-04-02','abril pagado','2320','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3143,5,308,270.00,'2013-05-03','mayo pagado','2433','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3144,5,308,270.00,'2013-06-04','junio pagado','2551','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3145,5,308,270.00,'2013-07-04','julio pagado','2667','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3146,5,308,270.00,'2013-08-01','agosto pagado','2740','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3147,5,308,270.00,'2013-09-03','septiembre pagado','2870','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3148,5,308,270.00,'2013-10-03','octubre pagado','3027','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3149,5,308,270.00,'2013-11-01','noviembre pagado','3127','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3150,5,308,270.00,'2013-12-02','diciembre pagado','3267','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3151,5,308,270.00,'2014-01-07','enero pagado','3458','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3152,5,308,270.00,'2014-02-01','febrero pagado','3500','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3153,5,308,270.00,'2014-03-03','marzo pagado','3618','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3154,5,308,270.00,'2014-04-01','abril pagado','3716','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3155,5,308,270.00,'2014-05-02','mayo pagado','3823','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3156,5,308,270.00,'2014-06-02','junio pagado','3929','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3157,5,308,270.00,'2014-07-03','julio pagado','4033','juan_fco_monter','2014-07-24',NULL,292);
INSERT INTO `payment` VALUES (3159,5,310,270.00,'2013-03-05','marzo pagado','2234','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3160,5,310,270.00,'2013-04-04','abril pagado','2353','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3161,5,310,270.00,'2013-04-29','mayo pagado','2407','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3162,5,310,270.00,'2013-06-01','junio pagado','2518','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3163,5,310,270.00,'2013-06-29','julio pagado','2625','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3164,5,310,270.00,'2013-08-03','agosto pagado','2775','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3165,5,310,270.00,'2013-09-06','septiembre pagado','2931','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3166,5,310,270.00,'2013-10-04','octubre pagado','3044','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3167,5,310,270.00,'2013-11-07','noviembre pagado','3218','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3168,5,310,270.00,'2013-12-09','diciembre pagado','3245','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3169,5,310,270.00,'2014-01-02','enero pagado','3343','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3170,5,310,270.00,'2014-02-01','febrero pagado','3386','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3171,5,310,270.00,'2014-03-04','marzo pagado','3496','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3172,5,310,270.00,'2014-04-01','abril pagado','3647','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3173,5,310,270.00,'2014-05-02','mayo pagado','3717','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3174,5,310,270.00,'2014-06-03','junio pagado','3829','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3175,5,310,270.00,'2014-07-03','julio pagado','3948','juan_fco_monter','2014-07-24',NULL,293);
INSERT INTO `payment` VALUES (3176,13,311,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-08-23',0,294);
INSERT INTO `payment` VALUES (3177,5,311,520.00,'2013-03-23','febrero marzo pagado','2289','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3178,5,311,270.00,'2013-04-08','abril pagado','2379','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3179,5,311,270.00,'2013-05-06','mayo pagado','2457','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3180,5,311,270.00,'2013-06-03','junio pagado','2548','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3181,5,311,270.00,'2013-06-29','julio pagado','2623','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3182,5,311,270.00,'2013-08-03','agosto pagado','2769','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3183,5,311,270.00,'2013-09-03','septiembre pagado','2887','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3184,5,311,270.00,'2013-10-02','octubre pagado','3021','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3185,5,311,270.00,'2013-11-02','noviembre pagado','3138','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3186,5,311,270.00,'2013-12-05','diciembre pagado','3294','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3187,5,311,270.00,'2014-01-08','enero pagado','3446','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3188,5,311,270.00,'2014-02-06','febrero pagado','3532','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3189,5,311,270.00,'2014-03-07','marzo pagado','3669','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3190,5,311,270.00,'2014-04-08','abril pagado','3786','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3191,5,311,270.00,'2014-05-06','mayo pagado','3861','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3192,5,311,270.00,'2014-06-03','junio pagado','3947','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3193,5,311,270.00,'2014-07-03','julio pagado','4046','juan_fco_monter','2014-07-24',NULL,294);
INSERT INTO `payment` VALUES (3194,1,2,600.00,'2013-01-05','enero pagado','0004','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3195,1,2,600.00,'2013-02-07','febrero pagado','0037','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3196,1,2,600.00,'2013-03-07','marzo pagado','0064','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3197,1,2,600.00,'2013-04-04','abril pagado','0084','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3198,1,2,600.00,'2013-05-07','mayo pagado','0113','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3199,1,2,600.00,'2013-06-06','junio pagado','0146','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3200,1,2,600.00,'2013-07-09','julio pagado','0173','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3201,1,2,600.00,'2013-08-01','agosto pagado','0195','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3202,1,2,600.00,'2013-09-05','septiembre pagado','0227','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3203,1,2,600.00,'2013-10-07','octubre pagado','0253','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3204,1,2,600.00,'2013-11-05','noviembre pagado','0279','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3205,1,2,600.00,'2013-12-11','diciembre pagado','0317','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3206,1,2,600.00,'2014-01-06','enero pagado','0340','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3207,1,2,600.00,'2014-02-05','pagado','0370','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3208,1,2,600.00,'2014-03-05','pagado','0405','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3209,1,2,600.00,'2014-04-10','pagado','0437','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3210,1,2,600.00,'2014-05-07','pagado','0470','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3211,1,2,600.00,'2014-06-09','pagado','0495','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3212,1,2,600.00,'2014-07-08','pagado','0525','juan_fco_monter','2014-07-26',NULL,2);
INSERT INTO `payment` VALUES (3213,1,3,600.00,'2013-01-19','pagado','0026','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3214,1,3,600.00,'2013-02-07','pagado','0038','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3215,1,3,600.00,'2013-03-15','pagado','0071','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3216,1,3,600.00,'2013-04-18','pagado','0100','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3217,1,3,600.00,'2013-05-09','pagado','0117','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3218,1,3,600.00,'2013-06-17','pagado','0149','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3219,1,3,600.00,'2013-07-23','pagado','0188','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3221,1,3,600.00,'2013-09-19','pagado','0239','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3222,1,3,600.00,'2013-10-15','pagado','0264','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3223,1,3,600.00,'2013-11-28','pagado','0296','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3224,1,3,600.00,'2013-12-29','pagado','0308','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3225,1,3,600.00,'2013-12-17','enero 2014 pagado','0332','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3226,1,3,600.00,'2014-02-18','pagado','0390','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3227,1,3,600.00,'2014-03-17','pagado','0415','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3228,1,3,600.00,'2014-04-21','pagado','0448','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3229,1,3,0.00,'2014-05-14','pagado','0485','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3230,1,3,600.00,'2014-06-18','pagado','0513','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3231,1,3,600.00,'2014-07-16','pagado','0530','juan_fco_monter','2014-07-26',NULL,3);
INSERT INTO `payment` VALUES (3232,11,4,0.00,'2013-01-31','pagado admin anterior','0000','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3233,1,4,600.00,'2013-02-05','febrero pagado','0034','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3234,1,4,600.00,'2013-02-28','marzo pagado','0058','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3235,1,4,600.00,'2013-04-12','abril pagado','0090','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3236,1,4,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3237,1,4,1200.00,'2013-06-06','mayo junio pagado','0142','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3238,1,4,600.00,'2013-07-02','julio pagado','0168','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3239,1,4,600.00,'2013-08-01','agosto pagado','0196','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3240,1,4,600.00,'2013-09-11','septiembre pagado','0229','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3241,1,4,600.00,'2013-10-15','octubre pagado','0258','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3242,1,4,600.00,'2013-11-12','noviembre pagado','0283','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3243,1,4,600.00,'2013-12-12','diciembre pagado','0320','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3244,1,4,400.00,'2013-12-12','pago parcial enero 2014','0321','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3245,10,4,200.00,'2014-01-02','pago complementario enero','0338','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3246,1,4,600.00,'2014-02-11','febrero pagado','0375','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3247,1,4,600.00,'2014-03-17','marzo pagado','0419','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3248,1,4,600.00,'2014-04-09','abril pagado','0436','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3249,1,4,600.00,'2014-05-08','mayo pagado','0471','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3250,1,4,600.00,'2014-06-12','junio pagado','0498','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3251,1,4,600.00,'2014-07-13','julio pagado','0529','juan_fco_monter','2014-07-26',NULL,4);
INSERT INTO `payment` VALUES (3252,11,5,0.00,'2013-01-11','pagado admin anterior','0000','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3253,1,5,600.00,'2013-02-14','febrero pagado','0045','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3254,1,5,600.00,'2013-05-02','marzo pagado','0109','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3255,1,5,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3256,1,5,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3257,1,5,1800.00,'2013-06-20','myo junio julio pagado','0161','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3258,1,5,0.00,'2013-07-31','julio pagado','0000','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3259,1,5,600.00,'2013-08-01','agosto pagado','0193','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3260,1,5,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3261,1,5,600.00,'2013-10-16','octubre pagado','0262','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3262,1,5,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3263,1,5,600.00,'2013-12-10','diciembre pagado','0315','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3264,1,5,600.00,'2014-01-15','enero pagado','0352','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3265,1,5,600.00,'2014-02-11','febrero pagado','0376','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3266,1,5,600.00,'2014-03-11','marzo pagado','0407','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3267,1,5,600.00,'2014-04-15','abril pagado','0438','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3268,1,5,600.00,'2014-05-06','mayo pagado','0466','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3269,1,5,600.00,'2014-06-10','junio pagado','0496','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3270,1,5,600.00,'2014-07-14','julio pagado','0528','juan_fco_monter','2014-07-26',NULL,5);
INSERT INTO `payment` VALUES (3271,1,6,600.00,'2013-01-05','enero pagado','0005','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3272,1,6,600.00,'2013-02-05','febrero pagado','0032','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3273,1,6,600.00,'2013-03-07','marzo pagado','0063','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3274,1,6,600.00,'2013-04-10','abril pagado','0086','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3275,1,6,600.00,'2013-05-02','mayo pagado','0108','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3276,1,6,600.00,'2013-06-04','junio pagado','0139','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3277,1,6,600.00,'2013-07-02','julio pagado','0166','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3278,1,6,600.00,'2013-08-01','agosto pagado','0194','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3279,1,6,600.00,'2013-09-11','septiembre pagado','0228','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3280,1,6,600.00,'2013-10-01','octubre pagado','0250','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3281,1,6,600.00,'2013-11-05','noviembre pagado','0282','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3282,1,6,600.00,'2013-12-03','diciembre pagado','0302','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3283,1,6,600.00,'2014-01-07','enero pagado','0341','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3284,1,6,600.00,'2014-02-06','febrero pagado','0372','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3285,1,6,600.00,'2014-03-04','marzo pagado','0403','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3286,1,6,600.00,'2014-04-03','abril pagado','0433','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3287,1,6,600.00,'2014-05-06','mayo pagado','0467','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3288,1,6,600.00,'2014-06-10','junio pagado','0497','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3289,1,6,600.00,'2014-07-03','julio pagado','0523','juan_fco_monter','2014-07-26',NULL,6);
INSERT INTO `payment` VALUES (3290,1,7,600.00,'2013-01-15','enero pagado','0015','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3291,1,7,600.00,'2013-02-21','febrero pagado','0052','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3292,1,7,600.00,'2013-03-19','marzo pagado','0075','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3293,1,7,600.00,'2013-04-11','abril pagado','0089','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3294,1,7,600.00,'2013-05-09','mayo pagado','0116','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3295,1,7,600.00,'2013-06-06','junio pagado','0144','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3296,1,7,600.00,'2013-07-16','julio pagado','0183','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3297,1,7,600.00,'2013-08-12','agosto pagado','0200','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3298,1,7,600.00,'2013-09-17','septiembre pagado','0232','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3299,1,7,600.00,'2013-10-10','octubre pagado','0256','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3300,1,7,600.00,'2013-11-07','noviembre pagado','0278','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3301,1,7,600.00,'2013-12-11','diciembre pagado','0318','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3302,1,7,600.00,'2014-01-14','enero pagado','0346','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3303,1,7,600.00,'2014-02-11','febrero pagado','0374','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3304,1,7,600.00,'2014-03-06','marzo pagado','0406','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3305,1,7,600.00,'2014-04-10','abril pagado','0434','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3306,1,7,600.00,'2014-05-13','mayo pagado','0476','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3307,1,7,600.00,'2014-06-17','junio pagado','0502','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3308,1,7,600.00,'2014-07-08','julio pagado','0526','juan_fco_monter','2014-07-26',NULL,7);
INSERT INTO `payment` VALUES (3309,1,8,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3310,1,8,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3311,1,8,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3312,1,8,3000.00,'2013-04-12','ene feb mar abr may pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3313,1,8,0.00,'2013-05-31','mayo pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3314,1,8,2400.00,'2013-06-13','jun jul ago sep pagado','0158','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3315,1,8,0.00,'2013-07-31','julio pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3316,1,8,0.00,'2013-08-31','agosto pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3317,1,8,0.00,'2013-09-30','septiembre pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3318,1,8,1800.00,'2013-10-17','oct nov dic pagado','0266','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3319,1,8,0.00,'2013-11-30','noviembre pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3320,1,8,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3321,1,8,3600.00,'2014-01-15','ene feb mar abr may jun pagado','0350','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3322,1,8,0.00,'2014-02-28','febrero pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3323,1,8,0.00,'2014-03-31','marzo pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3324,1,8,0.00,'2014-04-30','abril pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3325,1,8,0.00,'2014-05-31','mayo pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3326,1,8,0.00,'2014-06-30','junio pagado','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3327,1,8,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-26',NULL,8);
INSERT INTO `payment` VALUES (3328,11,9,0.00,'2013-01-31','enero pagado admin anterior','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3329,1,9,1200.00,'2013-02-08','febrero marzo pagado','0042','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3330,1,9,0.00,'2013-03-31','marzo pagado','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3331,1,9,600.00,'2013-04-30','abril pagado','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3332,1,9,600.00,'2013-05-31','mayo pagado','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3333,1,9,600.00,'2013-06-30','junio pagado','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3334,1,9,600.00,'2013-07-31','julio pagado','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3335,1,9,600.00,'2013-08-07','agosto pagado','0222','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3336,1,9,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3337,1,9,1200.00,'2013-10-22','septiembre octubre pagado','0267','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3338,1,9,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3339,1,9,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3340,1,9,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3341,1,9,1400.00,'2014-02-26','nov dic ene pago parcial','0398','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3342,1,9,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3343,10,9,1600.00,'2014-04-02','ene complemento feb mar  pagado','0432','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3344,1,9,600.00,'2014-04-25','abril pagado','0459','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3345,1,9,600.00,'2014-04-30','mayo pagado','0516','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3346,1,9,600.00,'2014-05-30','junio pagado','0517','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3347,1,9,600.00,'2014-06-19','julio pagado','0518','juan_fco_monter','2014-07-26',NULL,9);
INSERT INTO `payment` VALUES (3348,12,10,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3349,12,10,0.00,'2013-02-28','febrero debe\r\n','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3350,12,10,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3351,12,10,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3352,12,10,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3353,12,10,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3354,12,10,0.00,'2013-07-31','julio  debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3355,12,10,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3356,12,10,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3357,12,10,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3358,12,10,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3359,12,10,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3360,12,10,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3361,12,10,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3362,12,10,0.00,'2014-03-31','maro debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3363,12,10,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3364,12,10,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3365,12,10,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3366,12,10,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3367,1,11,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3368,1,11,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3369,1,11,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3370,1,11,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3371,1,11,3000.00,'2013-05-21','ene feb mar abr may pagado','0132','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3372,1,11,4200.00,'2013-06-20','jun jul ago sep oct nov dic pagado','0163','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3373,1,11,0.00,'2013-07-31','julio pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3374,1,11,0.00,'2013-08-31','agosto pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3375,1,11,0.00,'2013-09-30','septiembre pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3376,1,11,0.00,'2013-10-31','octubre pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3377,1,11,0.00,'2013-11-30','noviembre pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3378,1,11,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3379,1,11,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3380,1,11,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3381,1,11,7200.00,'2014-03-19','ene feb mar abr may jun jul ago sep oct nov dic pagado','0422','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3382,1,11,0.00,'2014-04-30','abril pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3383,1,11,0.00,'2014-05-31','mayo pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3384,1,11,0.00,'2014-06-30','junio pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3385,1,11,0.00,'2014-07-31','julio pagado','0000','juan_fco_monter','2014-07-26',NULL,10);
INSERT INTO `payment` VALUES (3386,12,12,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3387,12,12,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3388,12,12,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3389,12,12,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3390,12,12,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3391,12,12,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3392,12,12,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3393,12,12,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3394,12,12,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3395,12,12,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3396,12,12,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3397,12,12,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3398,12,12,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3399,12,12,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3400,12,12,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3401,12,12,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3402,12,12,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3403,12,12,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3404,12,12,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-26',NULL,11);
INSERT INTO `payment` VALUES (3405,1,13,600.00,'2013-01-10','enero pagado','0010','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3406,1,13,600.00,'2013-02-13','febrero pagado','0043','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3407,1,13,600.00,'2013-03-13','marzo pagado','0069','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3408,1,13,600.00,'2013-04-15','abril pagado','0092','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3409,12,13,300.00,'2013-05-15','mayo pagado','0123','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3410,12,13,300.00,'2013-05-15','junio pagado','0124','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3411,12,13,600.00,'2013-07-09','julio agosto pagado','0170','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3412,12,13,0.00,'2013-08-31','agosto pagado','0000','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3413,12,13,300.00,'2013-09-27','septiembre pagado','0243','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3414,12,13,300.00,'2013-09-27','octubre pagado','0244','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3415,1,13,600.00,'2013-11-05','noviembre pagado','0281','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3416,1,13,600.00,'2013-12-06','diciembre pagado','0310','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3417,1,13,600.00,'2014-01-10','enero pagado','0343','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3418,1,13,600.00,'2014-02-11','febrero pagado','0373','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3419,1,13,600.00,'2014-03-13','marzo pagado','0412','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3420,1,13,600.00,'2014-04-17','abril pagado','0443','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3421,1,13,600.00,'2014-05-13','mayo pagado','0473','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3422,1,13,600.00,'2014-06-18','junio pagado','0512','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3423,1,13,600.00,'2014-07-17','julio pagado','0534','juan_fco_monter','2014-07-26',NULL,12);
INSERT INTO `payment` VALUES (3424,1,14,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3425,1,14,1200.00,'2013-02-28','enero febrero pagado','0059','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3426,1,14,600.00,'2013-03-11','marzo pagado','0066','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3427,1,14,600.00,'2013-04-09','abril pagado','0085','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3428,1,14,600.00,'2013-05-24','mayo pagado','0135','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3429,1,14,600.00,'2013-06-25','junio pagado','0164','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3430,1,14,600.00,'2013-09-30','julio pagado','0248','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3431,1,14,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3432,1,14,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3433,1,14,0.00,'2013-10-31','octubre pagado','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3434,1,14,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3435,1,14,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3436,1,14,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3437,1,14,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3438,1,14,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3439,1,14,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3440,1,14,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3441,1,14,5000.00,'2014-06-20','pago por acuerdo en morosidad','0515','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3442,1,14,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-26',NULL,13);
INSERT INTO `payment` VALUES (3443,1,15,600.00,'2013-01-17','enero pagado','0024','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3444,1,15,600.00,'2013-02-21','febrero pagado','0055','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3445,1,15,600.00,'2013-03-27','marzo pagado','0083','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3446,1,15,600.00,'2013-04-18','abril pagado','0099','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3447,1,15,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3448,1,15,1200.00,'2013-06-18','mayo junio pagado','0160','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3449,1,15,600.00,'2013-07-25','julio pagado','0190','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3450,1,15,600.00,'2013-08-16','agosto pagado','0205','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3451,1,15,600.00,'2013-09-18','septiembre pagado','0234','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3452,1,15,600.00,'2013-10-24','octubre pagado','0268','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3453,1,15,600.00,'2013-12-05','noviembre pagado','0309','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3454,1,15,600.00,'2013-12-16','diciembre pagado','0326','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3455,1,15,600.00,'2014-01-14','enero pagado','0348','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3456,1,15,600.00,'2014-02-06','febrero pagado','0371','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3457,1,15,600.00,'2014-03-11','marzo pagado','0408','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3458,1,15,600.00,'2014-04-18','abril pagado','0445','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3459,1,15,600.00,'2014-05-16','mayo pagado','0479','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3460,1,15,600.00,'2014-06-17','junio pagado','0505','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3461,1,15,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-26',NULL,14);
INSERT INTO `payment` VALUES (3462,1,16,600.00,'2013-01-15','enero pagado','0016','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3463,1,16,6600.00,'2013-02-07','fer mar abr may jun jul ago sep oct nov dic pagado','0040','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3464,1,16,0.00,'2013-03-31','marzo pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3465,1,16,0.00,'2013-04-30','abril pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3466,1,16,0.00,'2013-05-31','mayo pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3467,1,16,0.00,'2013-06-30','junio pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3468,1,16,0.00,'2013-07-31','julio pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3469,1,16,0.00,'2013-08-31','agosto pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3470,1,16,0.00,'2013-09-30','septiembre pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3471,1,16,0.00,'2013-10-31','octubre pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3472,1,16,0.00,'2013-11-30','noviembre pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3473,1,16,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3474,1,16,600.00,'2014-02-04','enero pagado','0367','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3475,1,16,600.00,'2014-02-18','febrero pagado','0388','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3476,1,16,600.00,'2014-03-01','marzo pagado','0401','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3477,1,16,600.00,'2014-04-15','abril pagado','0440','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3478,1,16,600.00,'2014-05-07','mayo pagado','0469','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3479,1,16,600.00,'2014-06-05','junio pagado','0493','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3480,1,16,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-26',NULL,15);
INSERT INTO `payment` VALUES (3481,1,17,1800.00,'2013-01-23','ene feb mar pagado','0028','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3482,1,17,0.00,'2013-02-28','febrero pagado','0000','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3483,1,17,0.00,'2013-03-31','marzo pagado','0000','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3484,1,17,600.00,'2013-04-18','abril pagado','0098','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3485,1,17,600.00,'2013-05-14','mayo pagado','0122','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3486,1,17,600.00,'2013-06-18','junio pagado','0156','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3487,1,17,600.00,'2013-07-09','julio pagado','0174','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3488,1,17,600.00,'2013-08-28','agosto pagado','0221','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3489,1,17,600.00,'2013-09-24','septiembre pagado','0241','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3490,1,17,600.00,'2013-10-24','octubre pagado','0269','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3491,1,17,600.00,'2013-11-28','noviembre pagado','0295','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3492,1,17,600.00,'2013-12-17','diciembre pagado','0327','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3493,1,17,600.00,'2014-01-21','enero pagado','0361','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3494,1,17,600.00,'2014-02-19','febrero pagado','0389','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3495,1,17,600.00,'2014-03-20','marzo pagado','0425','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3496,1,17,600.00,'2014-04-24','abril pagado','0453','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3497,1,17,600.00,'2014-05-16','mayo pagado','0480','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3498,1,17,600.00,'2014-06-17','junio pagado','0509','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3499,1,17,0.00,'2014-07-31','julio pagado','0000','juan_fco_monter','2014-07-27',NULL,16);
INSERT INTO `payment` VALUES (3500,12,18,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3501,12,18,0.00,'2013-02-28','febrero pagado','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3502,12,18,0.00,'2013-03-31','maro debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3503,12,18,0.00,'2013-04-30','abril pagado','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3504,12,18,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3505,12,18,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3506,12,18,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3507,12,18,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3508,12,18,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3509,12,18,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3510,12,18,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3511,12,18,0.00,'2013-12-31','diciembre debe\r\n','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3512,12,18,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3513,12,18,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3514,12,18,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3515,12,18,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3516,12,18,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3517,12,18,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3518,12,18,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3519,12,19,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3520,12,19,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3521,12,19,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3522,12,19,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3523,12,19,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3524,12,19,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3525,12,19,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3526,12,19,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3527,12,19,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3528,12,19,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3529,12,19,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3530,12,19,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3531,12,19,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3532,12,19,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3533,12,19,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3534,12,19,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3535,12,19,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3536,12,19,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3537,12,19,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-27',NULL,17);
INSERT INTO `payment` VALUES (3538,12,20,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3539,12,20,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3540,12,20,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3541,12,20,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3542,12,20,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3543,12,20,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3544,12,20,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3545,12,20,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3546,12,20,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3547,12,20,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3548,12,20,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3549,12,20,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3550,12,20,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3551,12,20,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3552,12,20,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3553,12,20,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3554,12,20,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3555,12,20,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3556,12,20,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-27',NULL,18);
INSERT INTO `payment` VALUES (3557,1,21,600.00,'2013-01-03','enero pagado','00002','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3558,1,21,600.00,'2013-02-05','febrero pagado','0031','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3559,1,21,600.00,'2013-03-05','marzo pagado','0062','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3560,1,21,600.00,'2013-04-30','abril pagado','0107','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3561,1,21,600.00,'2013-05-28','mayo pagado','0136','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3562,1,21,600.00,'2013-06-18','junio pagado','0153','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3563,1,21,600.00,'2013-07-16','julio pagado','0180','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3564,1,21,600.00,'2013-08-15','agosto pagado','204','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3565,1,21,600.00,'2013-09-17','septiembre pagado','0231','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3566,1,21,600.00,'2013-10-15','octubre pagado','0261','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3567,1,21,600.00,'2013-11-19','noviembre pagado','0286','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3568,1,21,600.00,'2013-12-09','diciembre pagado','0312','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3570,1,21,600.00,'2014-01-13','enero pagado','0344','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3571,1,21,600.00,'2014-02-17','febrero pagado','0381','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3572,1,21,600.00,'2014-03-17','marzo pagado','0417','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3573,1,21,2014.00,'2014-04-22','abril pagado','600','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3574,1,21,600.00,'2014-05-13','mayo pagado','0475','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3575,1,21,600.00,'2014-06-13','junio pagado','0499','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3576,1,21,600.00,'2014-07-17','julio pagado','0533','juan_fco_monter','2014-07-27',NULL,19);
INSERT INTO `payment` VALUES (3577,1,22,7200.00,'2013-01-08','ene feb mar abr may jun jul ago sep oct nov dic pagado','0007','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3578,1,22,0.00,'2013-02-28','febrero pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3579,1,22,0.00,'2013-03-31','marzo pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3580,1,22,0.00,'2013-04-30','abril págado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3581,1,22,0.00,'2013-05-31','mayo pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3582,1,22,0.00,'2013-06-30','junio pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3583,1,22,0.00,'2013-07-31','julio pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3584,1,22,0.00,'2013-08-31','agosto pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3585,1,22,0.00,'2013-09-30','septiembre pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3586,1,22,0.00,'2013-10-31','octubre pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3587,1,22,0.00,'2013-11-30','noviembre pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3588,1,22,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3589,1,22,7200.00,'2014-01-15','ene feb mar abr may jun jul ago sep oct nov dic pagado','0353','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3590,1,22,0.00,'2014-02-28','febrero pagado','000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3591,1,22,0.00,'2014-03-31','marzo pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3592,1,22,0.00,'2014-04-30','abril pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3593,1,22,0.00,'2014-05-31','mayo pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3594,1,22,0.00,'2014-06-30','junio pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3595,1,22,0.00,'2014-07-31','julio pagado','0000','juan_fco_monter','2014-07-28',NULL,20);
INSERT INTO `payment` VALUES (3596,1,23,600.00,'2013-01-17','enero pagado','0022','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3597,1,23,600.00,'2013-02-14','febrero pagado','0044','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3598,1,23,600.00,'2013-03-19','marzo pagado','0073','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3599,1,23,600.00,'2013-04-19','abril pagado','0103','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3600,1,23,600.00,'2013-05-17','mayo pagado','0128','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3601,1,23,600.00,'2013-06-18','junio pagado','0159','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3602,1,23,600.00,'2013-07-18','julio pagado','0187','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3603,1,23,600.00,'2013-08-13','agosto pagado','0201','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3604,1,23,600.00,'2013-09-19','septiembre pagado','0236','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3605,1,23,600.00,'2013-10-10','octubre pagado','0255','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3606,1,23,600.00,'2013-12-04','noviembre pagado','0304','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3607,1,23,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3608,1,23,1200.00,'2014-01-21','diciembre enero pagado','0363','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3609,1,23,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3610,1,23,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3611,1,23,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3612,1,23,2400.00,'2014-05-02','feb mar abri may pagado','0464','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3613,1,23,600.00,'2014-07-03','junio pagado','0522','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3614,1,23,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,21);
INSERT INTO `payment` VALUES (3615,1,24,600.00,'2013-02-08','enero pagado','0041','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3616,1,24,600.00,'2013-03-05','febrero pagado','0061','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3617,1,24,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3618,1,24,1200.00,'2013-05-07','marzo abril pagado','01111','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3619,1,24,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3620,1,24,1200.00,'2013-08-09','mayo junio pagado','0199','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3621,1,24,600.00,'2013-08-20','julio pagado','0211','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3622,1,24,600.00,'2013-09-26','agosto pagado','0242','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3623,1,24,600.00,'2013-10-31','septiembre pagado','0274','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3624,1,24,600.00,'2013-11-29','octubre pagado','0297','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3625,1,24,600.00,'2013-12-10','noviembre pagado','0316','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3626,1,24,600.00,'2013-12-19','diciembre pagado','0335','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3627,1,24,600.00,'2014-01-30','enero pagado','0366','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3628,1,24,600.00,'2014-03-03','febrero pagado','0400','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3629,1,24,600.00,'2014-04-30','marzo pagado','0458','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3630,1,24,600.00,'2014-05-31','abril pagado','0488','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3631,1,24,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3632,1,24,0.00,'2014-06-30','junio pagado','0000','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3633,1,24,0.00,'2014-07-31','julio pagado','0000','juan_fco_monter','2014-07-28',NULL,22);
INSERT INTO `payment` VALUES (3634,1,25,600.00,'2013-01-15','enero pagado','0421','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3635,1,25,600.00,'2013-02-07','febrero pagado','0036','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3636,1,25,600.00,'2013-03-22','marzo pagado','0082','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3637,1,25,600.00,'2013-04-18','abril pagado','0096','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3638,1,25,600.00,'2013-05-17','mayo pagado','0130','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3639,1,25,600.00,'2013-06-18','junio pagado','0157','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3640,1,25,600.00,'2013-07-11','julio pagado','0176','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3641,1,25,600.00,'2013-08-22','agosto pagado','0217','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3642,1,25,600.00,'2013-09-24','septiembre pagado','0240','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3643,1,25,600.00,'2013-10-17','octubre pagado','0265','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3644,1,25,600.00,'2013-11-21','noviembre pagado','0290','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3645,1,25,600.00,'2013-12-17','diciembre pagado','0331','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3646,1,25,600.00,'2014-01-13','enero pagado','0357','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3647,1,25,600.00,'2014-02-18','febrero pagado','0391','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3648,1,25,600.00,'2014-03-18','marzo pagado','0421','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3649,1,25,600.00,'2014-05-05','abril pagado','0465','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3650,1,25,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3651,1,25,1200.00,'2014-06-26','mayo junio pagado','0519','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3652,1,25,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,23);
INSERT INTO `payment` VALUES (3653,1,401,600.00,'2013-01-15','enero pagado','0018','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3654,1,401,600.00,'2013-02-07','febrero pagado','0039','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3655,1,401,600.00,'2013-03-07','marzo pagado','0065','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3656,1,401,600.00,'2013-04-11','abril pagado','0088','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3657,1,401,600.00,'2013-05-17','mayo pagado','0129','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3658,1,401,600.00,'2013-06-18','junio pagado','0154','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3659,1,401,600.00,'2013-07-18','julio pagado','0186','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3660,1,401,600.00,'2013-08-20','agosto pagado','0212','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3661,1,401,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3662,1,401,1200.00,'2013-10-01','septiembre octubre pagado','0249','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3663,1,401,600.00,'2013-11-28','noviembre pagado','0299','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3664,1,401,600.00,'2013-12-09','diciembre pagado','0311','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3665,1,401,600.00,'2014-01-15','enero pagado','0349','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3666,1,401,600.00,'2014-02-18','febrero pagado','0384','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3667,1,401,600.00,'2014-03-20','marzo pagado','0423','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3668,1,401,600.00,'2014-04-10','abril pagado','0435','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3669,1,401,600.00,'2014-05-13','mayo pagado','0477','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3670,1,401,600.00,'2014-06-17','junio pagado','0503','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3671,1,401,600.00,'2014-07-08','julio pagado','0527','juan_fco_monter','2014-07-28',NULL,7);
INSERT INTO `payment` VALUES (3672,1,26,600.00,'2013-01-15','enero pagado','0017','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3673,1,26,600.00,'2013-02-21','febrero pagado','0054','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3674,1,26,600.00,'2013-03-22','marzo pagado','0078','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3675,1,26,600.00,'2013-04-18','abril pagado','0102','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3676,1,26,500.00,'2013-05-23','mayo pagado','0134','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3677,1,26,500.00,'2013-06-20','junio pagado','0162','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3678,1,26,600.00,'2013-07-25','julio pagado','0191','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3679,1,26,600.00,'2013-08-22','agosto pagado','0220','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3680,1,26,600.00,'2013-10-01','septiembre pagado','0252','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3681,1,26,600.00,'2013-10-21','octubre pagado','0270','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3682,1,26,600.00,'2013-11-30','noviembre pagado','0300','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3683,1,26,600.00,'2013-12-19','diciembre pagado','0337','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3684,1,26,600.00,'2014-01-17','enero pagado','0356','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3685,1,26,600.00,'2014-02-18','febrero pagado','0395','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3686,1,26,500.00,'2014-03-24','marzo pagado','0428','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3687,1,26,600.00,'2014-04-21','abril pagado','0446','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3688,1,26,600.00,'2014-05-31','mayo pagado','0489','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3689,1,26,600.00,'2014-06-30','junio pagado','0520','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3690,1,26,0.00,'2014-07-31','julio pagado','0000','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3691,10,26,100.00,'2014-03-11','mayo 2013','0409','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3692,10,26,100.00,'2014-03-11','julio 2013','0410','juan_fco_monter','2014-07-28',NULL,24);
INSERT INTO `payment` VALUES (3693,1,27,600.00,'2013-01-15','enero pagado','0020','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3694,1,27,600.00,'2013-02-19','febrero pagado','0050','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3695,1,27,600.00,'2013-03-14','marzo pagado','0070','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3696,1,27,600.00,'2013-04-18','abril pagado','0097','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3697,1,27,600.00,'2013-05-14','mayo pagado','0119','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3698,1,27,600.00,'2013-06-18','junio pagado','0155','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3699,1,27,600.00,'2013-07-16','julio pagado','0184','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3700,1,27,600.00,'2013-08-15','agosto pagado','0202','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3701,1,27,600.00,'2013-09-17','septiembre pagado','0233','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3702,1,27,600.00,'2013-10-15','octubre pagado','0260','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3703,1,27,600.00,'2013-11-16','noviembre pagado','0285','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3704,1,27,600.00,'2013-12-16','diciembre pagado','0324','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3705,1,27,600.00,'2014-01-14','enero pagado','0345','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3706,1,27,600.00,'2014-02-17','febrero pagado','0379','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3707,1,27,600.00,'2014-03-17','marzo pagado','0416','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3708,1,27,600.00,'2014-04-16','abril pagado','0442','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3709,1,27,600.00,'2014-05-13','mayo pagado','0474','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3710,1,27,600.00,'2014-06-17','junio pagado','0500','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3711,1,27,600.00,'2014-07-17','julio pagado','0531','juan_fco_monter','2014-07-28',NULL,25);
INSERT INTO `payment` VALUES (3712,1,29,600.00,'2013-01-17','enero pagado','0021','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3713,1,29,600.00,'2013-02-19','febrero pagado','0049','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3714,1,29,600.00,'2013-03-21','marzo pagado','0076','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3715,1,29,600.00,'2013-04-18','abril pagado','0104','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3716,1,29,600.00,'2013-05-20','mayo pagado','0131','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3717,1,29,600.00,'2013-06-17','junio pagado','0148','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3718,1,29,600.00,'2013-07-25','julio pagado','0189','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3719,1,29,600.00,'2013-08-15','agosto pagado','0203','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3720,1,29,600.00,'2013-09-18','septiembre pagado','0235','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3721,1,29,600.00,'2013-10-31','octubre pagado','0275','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3722,1,29,600.00,'2013-11-21','noviembre pagado','0289','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3723,1,29,600.00,'2013-12-17','diciembre pagado','0328','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3724,1,29,600.00,'2014-01-21','enero pagado','0362','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3725,1,29,600.00,'2014-02-18','febrero pagado','0383','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3726,1,29,600.00,'2014-03-17','marzo pagado','0414','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3727,1,29,600.00,'2014-04-25','abril pagado','0455','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3728,1,29,600.00,'2014-05-15','mayo pagado','0478','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3729,1,29,600.00,'2014-06-17','junio pagado','0511','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3730,1,29,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,26);
INSERT INTO `payment` VALUES (3731,1,30,600.00,'2013-01-03','enero pagado','0003','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3732,1,30,600.00,'2013-02-05','febrero pagado','0033','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3733,1,30,600.00,'2013-03-05','marzo pagado','0060','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3734,1,30,600.00,'2013-04-11','abril pagado','0087','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3735,1,30,600.00,'2013-05-02','mayo pagado','0110','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3736,1,30,600.00,'2013-06-04','junio pagado','0140','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3737,1,30,600.00,'2013-07-02','julio pagado','0167','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3738,1,30,600.00,'2013-08-01','agosto pagado','0197','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3739,1,30,600.00,'2013-09-03','septiembre pagado','0255','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3740,1,30,600.00,'2013-10-01','octubre pagado','0251','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3741,1,30,600.00,'2013-11-05','noviembre pagado','0284','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3742,1,30,600.00,'2013-12-03','diciembre pagado','0301','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3743,1,30,600.00,'2014-01-07','enero pagado','0342','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3744,1,30,600.00,'2014-02-04','febrero pagado','0368','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3745,1,30,600.00,'2014-03-04','marzo pagado','0404','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3746,1,30,600.00,'2014-04-01','abril pagado','0431','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3747,1,30,600.00,'2014-05-06','mayo pagado','0468','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3748,1,30,600.00,'2014-06-03','junio pagado','0492','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3749,1,30,600.00,'2014-07-01','julio pagado','0521','juan_fco_monter','2014-07-28',NULL,27);
INSERT INTO `payment` VALUES (3750,1,31,600.00,'2013-07-11','enero pagado','0175','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3751,12,31,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3752,12,31,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3753,12,31,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3754,12,31,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3755,12,31,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3756,12,31,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3757,1,31,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3758,1,31,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3759,1,31,3000.00,'2013-10-04','ago sep oct nov dic pagado','0276','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3760,1,31,0.00,'2013-11-30','noviembre pagado','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3761,1,31,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3762,1,31,310.00,'2014-02-14','enero parcial','0394','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3763,1,31,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3764,1,31,600.00,'2014-03-20','marzo pagado','0424','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3765,10,31,290.00,'2014-04-14','enero complemento','0439','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3766,1,31,600.00,'2014-04-23','abril pagado','0450','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3767,1,31,600.00,'2014-05-16','mayo pagado','0481','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3768,1,31,600.00,'2014-06-17','junio pagado','0510','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3769,1,31,600.00,'2014-07-23','julio pagado','0537','juan_fco_monter','2014-07-28',NULL,28);
INSERT INTO `payment` VALUES (3770,1,32,0.00,'2013-01-31','enero pagado arq Alfredo','0000','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3771,1,32,600.00,'2013-08-19','enero pagado','0209','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3772,1,32,600.00,'2013-09-27','marzo pagado','0247','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3773,1,32,600.00,'2013-10-31','abril pagado','0373','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3774,1,32,600.00,'2013-11-28','mayo pagado','0293','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3775,1,32,600.00,'2013-12-10','junio pagado','0313','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3776,1,32,600.00,'2014-01-17','julio pagado','0358','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3777,1,32,600.00,'2013-08-19','agosto pagado','0208','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3778,1,32,600.00,'2013-09-27','septiembre pagado','0246','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3779,1,32,600.00,'2013-10-31','octubre pagado','0272','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3780,1,32,600.00,'2013-12-10','noviembre pagado','0314','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3781,1,32,600.00,'2013-12-18','diciembre pagado','0333','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3782,1,32,600.00,'2014-02-18','enero pagado','0386','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3783,1,32,600.00,'2014-03-18','febrero pagado','0420','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3784,1,32,600.00,'2014-04-17','marzo pagado','0444','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3785,1,32,600.00,'2014-05-20','abril pagado','0483','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3786,1,32,600.00,'2014-06-17','mayo pagado','0507','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3787,1,32,600.00,'2014-07-19','junio pagado','0535','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3788,1,32,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,29);
INSERT INTO `payment` VALUES (3789,1,33,600.00,'2013-01-11','enero pagado','0012','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3790,1,33,600.00,'2013-02-26','febrero pagado','0056','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3791,1,33,600.00,'2013-03-19','marzo pagado','0074','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3792,1,33,600.00,'2013-05-09','abril pagado','0115','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3793,1,33,1200.00,'2013-05-30','mayo junio pagado','0137','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3794,1,33,0.00,'2013-06-30','junio pagado','0000','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3795,1,33,600.00,'2013-07-09','julio pagado','0172','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3796,1,33,600.00,'2013-08-01','agosto pagado','0198','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3797,1,33,600.00,'2013-09-20','septiembre pagado','0238','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3798,1,33,600.00,'2013-10-16','octubre pagado','0263','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3799,1,33,600.00,'2013-11-21','noviembre pagado','0288','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3800,1,33,0.00,'2014-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3801,1,33,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3802,1,33,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3803,1,33,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3804,1,33,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3805,1,33,4200.00,'2014-05-12','dic ene feb mar abr may pagado','0486','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3806,1,33,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3807,1,33,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,30);
INSERT INTO `payment` VALUES (3808,1,34,600.00,'2013-01-07','enero pagado','0009','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3809,1,34,600.00,'2013-02-05','febrero pagado','0035','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3810,1,34,600.00,'2013-03-08','marzo pagado','0067','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3811,1,34,600.00,'2013-04-16','abril pagado','0094','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3812,1,34,600.00,'2013-05-07','mayo pagado','0114','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3813,1,34,600.00,'2013-06-04','junio pagado','0138','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3814,1,34,600.00,'2013-07-04','julio pagado','0169','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3815,1,34,600.00,'2013-08-16','agosto pagado','0206','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3816,1,34,600.00,'2013-09-19','septiembre pagado','0237','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3817,1,34,600.00,'2013-10-15','octubre pagado','0259','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3818,1,34,600.00,'2013-11-21','noviembre pagado','0287','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3819,1,34,600.00,'2013-12-12','diciembre pagado','0322','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3820,1,34,600.00,'2014-01-14','enero pagado','0347','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3821,1,34,600.00,'2014-02-16','febrero pagado','0380','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3822,1,34,600.00,'2014-03-14','marzo pagado','0413','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3823,1,34,600.00,'2014-04-15','abril pagado','0441','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3824,1,34,600.00,'2014-05-16','mayo pagado','0482','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3825,1,34,600.00,'2014-06-17','junio pagado','0501','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3826,1,34,600.00,'2014-07-17','julio pagado','0532','juan_fco_monter','2014-07-28',NULL,31);
INSERT INTO `payment` VALUES (3827,1,35,600.00,'2013-01-15','enero pagado','0014','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3828,1,35,600.00,'2013-02-14','febrero pagado','0048','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3829,1,35,600.00,'2013-03-21','marzo pagado','0077','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3830,1,35,600.00,'2013-05-07','abril pagado','0112','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3831,1,35,600.00,'2013-06-06','mayo pagado','0143','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3832,1,35,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3833,1,35,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3834,1,35,1800.00,'2013-08-14','junio julio agosto pagado','0216','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3835,1,35,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3836,1,35,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3837,1,35,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3838,1,35,1200.00,'2013-12-03','noviembre diciembre pagado','0303','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3839,1,35,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3840,1,35,1200.00,'2014-02-10','enero febrero pagado','0378','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3841,1,35,600.00,'2014-03-20','marzo pagado','0426','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3842,1,35,600.00,'2014-04-30','abril pagado','0463','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3843,1,35,600.00,'2014-05-20','mayo pagado','0484','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3844,1,35,600.00,'2014-06-26','junio pagado','0524','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3845,1,35,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,32);
INSERT INTO `payment` VALUES (3846,1,36,600.00,'2013-01-08','enero pagado','0006','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3847,1,36,600.00,'2013-02-21','febrero pagado','0053','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3848,12,36,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3849,12,36,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3850,1,36,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3851,12,36,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3852,1,36,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3853,1,36,0.00,'2013-08-31','agosto pagado','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3854,1,36,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3855,1,36,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3856,1,36,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3857,1,36,3000.00,'2013-12-06','mar abr may jun jul ago sep oct nov dic pagado','0325','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3858,1,36,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3859,1,36,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3860,1,36,0.00,'2014-03-31','marzo debe\r\n','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3861,1,36,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3862,1,36,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3863,1,36,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3864,1,36,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-28',NULL,33);
INSERT INTO `payment` VALUES (3865,1,37,600.00,'2013-01-03','enero pagado','0001','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3866,1,37,600.00,'2013-01-31','febrero pagado','0029','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3867,1,37,600.00,'2013-02-28','marzo pagado','0057','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3868,1,37,600.00,'2013-04-15','abril pagado','0093','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3869,1,37,600.00,'2013-05-14','mayo pagado','0121','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3870,1,37,600.00,'2013-06-11','junio pagado','0145','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3871,1,37,600.00,'2013-07-15','julio pagado','0178','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3872,1,37,600.00,'2013-08-16','agosto pagado','0207','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3873,1,37,600.00,'2013-09-03','septiembre pagado','0223','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3874,1,37,600.00,'2013-10-08','octubre pagado','0254','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3875,1,37,600.00,'2013-11-05','noviembre pagado','0280','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3876,1,37,600.00,'2013-12-12','diciembre pagado','0319','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3877,1,37,600.00,'2014-01-02','enero pagado','0339','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3878,1,37,600.00,'2014-02-04','febrero pagado','0369','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3879,1,37,600.00,'2014-03-11','marzo pagado','0411','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3880,1,37,1200.00,'2014-04-01','abril mayo pagado','0430','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3881,1,37,0.00,'2014-05-31','mayo pagado','0000','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3882,1,37,1200.00,'2014-06-02','junio julio pagado','0490','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3883,1,37,0.00,'2014-07-31','julio pagado','0000','juan_fco_monter','2014-07-29',NULL,34);
INSERT INTO `payment` VALUES (3884,1,38,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3885,1,38,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3886,1,38,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3887,1,38,2400.00,'2013-04-22','ene feb mar abr pagado','0106','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3888,1,38,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3889,1,38,1200.00,'2013-06-06','mayo junio pagado','0147','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3890,1,38,600.00,'2013-07-16','julio pagado','0182','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3891,1,38,1800.00,'2013-08-17','ago sep oct pagado','0215','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3892,1,38,0.00,'2013-09-30','septiembre pagado','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3893,1,38,0.00,'2013-10-31','octubre pagado','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3894,1,38,1200.00,'2013-11-12','noviembre diciembre pagado','0291','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3895,1,38,0.00,'2013-12-31','diciembre pagado','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3896,1,38,1800.00,'2014-01-17','ene feb mar pagado','0360','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3897,1,38,0.00,'2014-02-28','febrero pagado','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3898,1,38,0.00,'2014-03-31','marzo pagado','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3899,1,38,1800.00,'2014-04-22','abr may jun pagado','0451','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3900,1,38,0.00,'2014-05-31','mayo pagado','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3901,1,38,0.00,'2014-06-30','junio pagado','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3902,1,38,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-29',NULL,35);
INSERT INTO `payment` VALUES (3903,1,39,600.00,'2013-01-17','enero pagado','0023','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3904,1,39,600.00,'2013-02-19','febrero pagado','0051','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3905,1,39,600.00,'2013-03-19','marzo pagado','0072','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3906,1,39,600.00,'2013-04-19','abril pagado','0105','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3907,1,39,600.00,'2013-05-15','mayo pagado','0125','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3908,1,39,600.00,'2013-06-27','junio pagado','0165','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3909,1,39,600.00,'2013-07-09','julio pagado','0171','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3910,1,39,600.00,'2013-08-20','agosto pagado','0213','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3911,1,39,600.00,'2013-09-05','septiembre pagado','0226','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3912,1,39,600.00,'2013-12-05','octubre pagado','0305','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3913,1,39,600.00,'2013-12-05','noviembre pagado','0306','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3914,1,39,600.00,'2013-12-05','diciembre pagado','0307','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3915,1,39,600.00,'2014-01-16','enero pagado','0355','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3916,1,39,600.00,'2014-02-12','febrero pagado','0377','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3917,1,39,600.00,'2014-03-04','marzo pagado','0402','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3918,1,39,600.00,'2014-04-21','abril pagado','0447','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3919,1,39,600.00,'2014-05-12','mayo pagado','0472','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3920,1,39,600.00,'2014-06-09','junio pagado','0494','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3921,1,39,600.00,'2014-07-18','julio pagado','0536','juan_fco_monter','2014-07-29',NULL,36);
INSERT INTO `payment` VALUES (3922,1,40,600.00,'2013-01-08','enero pagado','0008','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3923,1,40,600.00,'2013-02-05','febrero pagado','0030','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3924,1,40,600.00,'2013-03-12','marzo pagado','0068','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3925,1,40,600.00,'2013-04-16','abril pagado','0095','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3926,1,40,600.00,'2013-05-16','mayo pagado','0127','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3927,1,40,600.00,'2013-06-17','junio pagado','0150','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3928,1,40,600.00,'2013-07-11','julio pagado','0177','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3929,1,40,600.00,'2013-08-22','agosto pagado','0219','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3930,1,40,600.00,'2013-09-27','septiembre pagado','0245','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3931,1,40,600.00,'2013-11-05','octubre pagado','0277','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3932,1,40,600.00,'2013-11-29','noviembre pagado','0298','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3933,1,40,600.00,'2013-12-15','diciembre pagado','0323','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3934,1,40,600.00,'2014-01-15','enero pagado','0351','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3935,1,40,600.00,'2014-02-17','febrero pagado','0382','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3936,1,40,600.00,'2014-03-20','marzo pagado','0427','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3937,1,40,600.00,'2014-04-24','abril pagado','0452','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3938,1,40,600.00,'2014-05-22','mayo pagado','0487','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3939,1,40,600.00,'2014-06-17','junio pagado','0508','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3940,1,40,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-29',NULL,37);
INSERT INTO `payment` VALUES (3941,1,41,600.00,'2013-01-19','enero pagado','0027','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3942,1,41,600.00,'2013-02-14','febrero pagado','0046','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3943,1,41,300.00,'2013-03-22','marzo pago parcial','0080','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3944,10,41,300.00,'2013-04-12','complemento marzo','0091','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3945,1,41,0.00,'2013-04-30','abril pagado','0000','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3946,1,41,1200.00,'2013-05-14','abril mayo pagado','0120','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3947,1,41,600.00,'2013-06-18','junio pagado','0151','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3948,1,41,600.00,'2013-07-16','julio pagado','0181','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3949,1,41,600.00,'2013-08-20','agosto pagado','0214','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3950,1,41,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3951,1,41,1200.00,'2013-10-10','septiembre octubre pagado','0257','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3952,1,41,600.00,'2013-12-17','noviembre pagado','0329','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3953,1,41,600.00,'2013-12-17','diciembre pagado','0336','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3954,1,41,600.00,'2014-01-22','enero pagado','0365','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3955,1,41,600.00,'2014-02-21','febrero pagado','0397','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3956,1,41,600.00,'2014-03-24','marzo pagado','0429','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3957,1,41,600.00,'2014-06-03','abril pagado','0491','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3958,1,41,600.00,'2014-06-17','abril pagado','0491','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3959,1,41,0.00,'2014-06-17','junio debe','0000','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3960,1,41,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-29',NULL,38);
INSERT INTO `payment` VALUES (3961,1,42,600.00,'2013-01-17','enero pagado','0025','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3962,1,42,600.00,'2013-02-14','febrero pagado','0047','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3963,1,42,600.00,'2013-03-22','marzo pagado','0079','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3964,1,42,600.00,'2013-04-18','abril pagado','0101','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3965,1,42,600.00,'2013-05-16','mayo pagado','0126','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3966,1,42,600.00,'2013-06-04','junio pagado','0141','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3967,1,42,600.00,'2013-07-18','julio pagado','0185','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3968,1,42,600.00,'2013-08-22','agosto pagado','0218','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3969,1,42,600.00,'2013-09-13','septiembre pagado','0230','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3970,1,42,600.00,'2013-10-29','octubre pagado','0271','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3971,1,42,600.00,'2013-11-28','noviembre pagado','0294','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3972,1,42,600.00,'2013-12-19','diciembre pagado','0334','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3973,1,42,600.00,'2014-01-16','enero pagado','0354','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3974,1,42,600.00,'2014-02-18','febrero pagado','0385','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3975,1,42,600.00,'2014-03-17','marzo pagado','0418','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3976,1,42,600.00,'2014-04-25','abril pagado','0454','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3977,1,42,600.00,'2014-04-26','mayo pagado','0456','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3978,1,42,600.00,'2014-06-17','junio pagado','0506','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3979,10,42,600.00,'2014-06-18','julio pagado','0514','juan_fco_monter','2014-07-29',NULL,39);
INSERT INTO `payment` VALUES (3980,12,43,0.00,'2013-01-31','enero debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3981,12,43,0.00,'2013-02-28','febrero debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3982,12,43,0.00,'2013-03-31','marzo debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3983,12,43,0.00,'2013-04-30','abril debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3984,12,43,0.00,'2013-05-31','mayo debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3985,12,43,0.00,'2013-06-30','junio debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3986,12,43,0.00,'2013-07-31','julio debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3987,12,43,0.00,'2013-08-31','agosto debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3988,12,43,0.00,'2013-09-30','septiembre debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3989,12,43,0.00,'2013-10-31','octubre debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3990,12,43,0.00,'2013-11-30','noviembre debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3991,12,43,0.00,'2013-12-31','diciembre debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3992,12,43,0.00,'2014-01-31','enero debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3993,12,43,0.00,'2014-02-28','febrero debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3994,12,43,0.00,'2014-03-31','marzo debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3995,12,43,0.00,'2014-04-30','abril debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3996,12,43,0.00,'2014-05-31','mayo debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3997,12,43,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3998,12,43,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-07-29',NULL,18);
INSERT INTO `payment` VALUES (3999,1,2,0.00,'2014-08-01','','0000','juan_fco_monter','2014-08-02',0,2);
INSERT INTO `payment` VALUES (4001,5,287,0.00,'2014-07-31','Julio debe','0000','juan_fco_monter','2014-08-16',0,274);
INSERT INTO `payment` VALUES (4003,5,389,270.00,'2014-07-21','agosto pagado','4111','juan_fco_monter','2014-08-22',0,371);
INSERT INTO `payment` VALUES (4005,5,343,270.00,'2014-07-01','julio pagado','4024','juan_fco_monter','2014-08-22',0,325);
INSERT INTO `payment` VALUES (4006,5,337,0.00,'2014-06-30','junio debe','0000','juan_fco_monter','2014-08-22',0,319);
INSERT INTO `payment` VALUES (4007,5,337,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-08-22',0,319);
INSERT INTO `payment` VALUES (4008,5,382,270.00,'2014-05-03','mayo pagado','3833','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (4009,5,382,270.00,'2014-06-07','junio pagado','3988','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (4010,5,382,270.00,'2014-06-28','julio pagado','4017','juan_fco_monter','2014-08-23',0,364);
INSERT INTO `payment` VALUES (4011,5,385,0.00,'2014-07-31','julio debe','0000','juan_fco_monter','2014-08-23',0,367);
INSERT INTO `payment` VALUES (4012,5,272,270.00,'2014-06-09','junio pagado','4000','juan_fco_monter','2014-08-23',0,262);
INSERT INTO `payment` VALUES (4013,5,272,270.00,'2014-07-08','julio pagado','4100','juan_fco_monter','2014-08-23',0,262);
INSERT INTO `payment` VALUES (4014,5,295,270.00,'2014-05-01','mayo pagado','3819','juan_fco_monter','2014-08-23',0,279);
INSERT INTO `payment` VALUES (4015,5,295,270.00,'2014-06-02','junio pagado\r\n','3932','juan_fco_monter','2014-08-23',0,279);
INSERT INTO `payment` VALUES (4016,5,295,270.00,'2014-07-07','julio pagado','4068','juan_fco_monter','2014-08-23',0,279);
INSERT INTO `payment` VALUES (4017,5,298,270.00,'2014-07-04','julio pagado','4052','juan_fco_monter','2014-08-23',0,282);
INSERT INTO `payment` VALUES (4018,5,342,270.00,'2014-07-08','julio pagado','4091','juan_fco_monter','2014-08-23',0,324);

INSERT INTO `surcharge` (`surcharge_id`, `payment_id`, `fixed`, `surcharge_type`) VALUES
(1, 877, 50, 'M'),
(2, 928, 100, 'M'),
(3, 939, 50, 'M'),
(4, 944, 50, 'M'),
(5, 946, 50, 'M'),
(6, 958, 50, 'M'),
(8, 966, 50, 'M'),
(9, 979, 50, 'M'),
(10, 983, 50, 'M'),
(11, 985, 50, 'M'),
(12, 986, 50, 'M'),
(13, 987, 50, 'M'),
(14, 989, 50, 'M'),
(15, 993, 50, 'M'),
(16, 991, 50, 'M'),
(17, 981, 50, 'M'),
(18, 988, 50, 'M'),
(20, 1049, 50, 'M'),
(21, 1051, 50, 'M'),
(22, 1190, 50, 'M'),
(23, 1363, 100, 'M'),
(24, 1376, 50, 'M'),
(25, 1377, 50, 'M'),
(26, 1409, 400, 'M'),
(27, 1413, 100, 'M'),
(28, 1416, 250, 'M'),
(29, 1444, 50, 'M'),
(30, 1449, 50, 'M'),
(31, 1512, 50, 'M'),
(32, 1513, 50, 'M'),
(33, 1516, 50, 'M'),
(34, 1521, 50, 'M'),
(35, 1553, 50, 'M'),
(36, 2963, 50, 'M'),
(37, 2987, 50, 'M'),
(38, 3018, 50, 'M'),
(39, 3041, 50, 'M'),
(40, 3069, 50, 'M'),
(41, 3090, 50, 'M'),
(42, 3100, 50, 'M'),
(43, 3102, 50, 'M'),
(44, 3113, 50, 'M'),
(45, 3125, 50, 'M'),
(46, 3128, 50, 'M'),
(47, 3129, 50, 'M'),
(48, 3132, 50, 'M'),
(49, 3177, 50, 'M'),
(50, 2915, 50, 'M'),
(51, 2892, 50, 'M'),
(52, 2893, 50, 'M'),
(53, 2899, 50, 'M'),
(54, 2902, 50, 'M'),
(55, 2792, 50, 'M'),
(56, 2752, 50, 'M'),
(57, 2734, 50, 'M'),
(58, 2712, 300, 'M'),
(59, 2715, 100, 'M'),
(60, 2695, 50, 'M'),
(61, 2657, 50, 'M'),
(62, 2627, 50, 'M'),
(63, 2631, 50, 'M'),
(64, 2578, 50, 'M'),
(65, 2579, 50, 'M'),
(66, 2581, 50, 'M'),
(67, 2582, 50, 'M'),
(68, 2586, 50, 'M'),
(69, 2588, 100, 'M'),
(70, 2593, 150, 'M'),
(71, 2495, 50, 'M'),
(72, 2498, 50, 'M'),
(73, 2499, 50, 'M'),
(74, 2502, 50, 'M'),
(75, 2500, 50, 'M'),
(76, 2433, 50, 'M'),
(77, 2434, 50, 'M'),
(78, 2437, 50, 'M'),
(79, 2438, 50, 'M'),
(80, 2442, 50, 'M'),
(81, 2446, 50, 'M'),
(83, 2384, 50, 'M'),
(84, 2388, 50, 'M'),
(85, 2341, 30, 'M'),
(86, 2318, 50, 'M'),
(87, 2282, 200, 'M'),
(88, 2244, 50, 'M'),
(89, 2253, 50, 'M'),
(90, 2249, 50, 'M'),
(91, 2257, 50, 'M'),
(92, 2213, 50, 'M'),
(93, 2149, 50, 'M'),
(94, 2118, 50, 'M'),
(95, 2087, 50, 'M'),
(96, 2013, 50, 'M'),
(97, 1902, 50, 'M'),
(98, 1850, 50, 'M'),
(99, 1711, 50, 'M'),
(100, 1688, 50, 'M'),
(101, 1689, 50, 'M'),
(102, 1690, 50, 'M'),
(103, 1692, 100, 'M'),
(104, 1695, 50, 'M'),
(105, 1696, 50, 'M'),
(106, 1697, 50, 'M'),
(107, 1698, 50, 'M'),
(108, 1699, 50, 'M'),
(109, 1701, 100, 'M'),
(110, 1672, 150, 'M'),
(111, 1679, 50, 'M'),
(112, 1684, 50, 'M'),
(114, 1619, 50, 'M'),
(115, 1622, 50, 'M'),
(116, 1625, 50, 'M'),
(117, 1631, 50, 'M'),
(118, 1557, 50, 'M'),
(119, 1559, 50, 'M'),
(120, 1561, 50, 'M'),
(121, 1563, 50, 'M'),
(122, 1565, 50, 'M'),
(123, 1570, 50, 'M'),
(124, 1567, 50, 'M'),
(125, 1568, 50, 'M'),
(126, 1573, 50, 'M'),
(127, 1578, 100, 'M'),
(128, 1495, 50, 'M'),
(130, 1327, 250, 'M'),
(131, 1278, 50, 'M'),
(132, 1285, 100, 'M'),
(133, 1254, 50, 'M'),
(134, 1266, 50, 'M'),
(135, 1238, 50, 'M'),
(136, 1239, 50, 'M'),
(137, 1244, 50, 'M'),
(138, 1245, 50, 'M'),
(139, 1247, 50, 'M'),
(140, 1136, 50, 'M'),
(141, 1137, 50, 'M'),
(142, 1078, 50, 'M'),
(145, 2402, 150, 'M'),
(146, 2405, 50, 'M'),
(147, 2411, 50, 'M'),
(148, 1484, 50, 'M'),
(149, 1013, 50, 'M');
