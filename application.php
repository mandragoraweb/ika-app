<?php
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\Config;
use \Mandragora\Mvc\Application;

require 'vendor/autoload.php';

$configuration = new Config(require 'config/di.config.php');
$sm = new ServiceManager($configuration);

$app = new Application($sm);
$app->setEnvironment(require 'config/app.config.php');
$app->run();