<?php
namespace Mandragora\Password;

use \Zend\Crypt\Password\Bcrypt;

class ZendBcryptPassword extends Bcrypt implements SecurePasswordGenerator
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;
}
