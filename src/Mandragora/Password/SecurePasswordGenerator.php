<?php
namespace Mandragora\Password;

interface SecurePasswordGenerator
{
    /**
     * @param string $password
     */
    public function create($password);

    /**
     * @param string $password
     * @param string $hash
     */
    public function verify($password, $hash);
}
