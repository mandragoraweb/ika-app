<?php
namespace Mandragora\Twig\Extension;

use \Pagerfanta\View\Template\TwitterBootstrapTemplate;
use \Pagerfanta\PagerfantaInterface;
use \Mandragora\View\Helper\UrlHelper;
use \Pagerfanta\View\TwitterBootstrapView;
use \Pagerfanta\View\ViewFactory;
use \Twig_Extension as TwigExtension;
use \Twig_Function_Method as TwigFunctionMethod;

class PagerfantaExtension extends TwigExtension
{
    /**
     * @var UrlHelper
     */
    protected $urlHelper;

    /**
     * @param UrlHelper $urlHelper
     */
    public function __construct(UrlHelper $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    /**
     * @return UrlHelper
     */
    protected function getUrlHelper()
    {
        return $this->urlHelper;
    }

    /**
     * @see Twig_Extension::getFunctions()
     */
    public function getFunctions()
    {
        return array(
            'pagerfanta' => new TwigFunctionMethod(
                $this, 'renderPagerfanta', array('is_safe' => array('html'))
            ),
        );
    }

    /**
     * Renders a pagerfanta.
     *
     * @param PagerfantaInterface $pagerfanta The pagerfanta.
     * @param string              $viewName   The view name.
     * @param array               $options    An array of options (optional).
     *
     * @return string The pagerfanta rendered.
     */
    public function renderPagerfanta(PagerfantaInterface $pagerfanta, $options = array())
    {
        if (!$pagerfanta->havetoPaginate()) {
            return;
        }

        $factory = new ViewFactory();
        $template = new TwitterBootstrapTemplate();
        //TODO: pass this options trough the SM
        $template->setOptions(array(
            'prev_message'        => '&larr; Anterior',
            'next_message'        => 'Siguiente &rarr;',)
        );
        $factory->add(array(
            'twitter_bootstrap' => new TwitterBootstrapView($template),
        ));

        $router = $this->getUrlHelper();
        $routeName = $options['routeName'];
        $routeParams = isset($options['routeParams']) ? $options['routeParams']: array();
        $pageParameter = isset($options['pageParameter']) ? $options['pageParameter'] : 'page';

        $routeGenerator = function($page) use ($router, $routeName, $routeParams, $pageParameter) {
            return sprintf(
                '%s?%s',
                $router->url($routeName, $routeParams),
                http_build_query(array($pageParameter => $page))
            );
        };

        return $factory->get('twitter_bootstrap')
                       ->render($pagerfanta, $routeGenerator, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'pagerfanta';
    }
}
