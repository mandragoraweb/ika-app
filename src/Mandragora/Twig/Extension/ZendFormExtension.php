<?php
namespace Mandragora\Twig\Extension;

use Zend\View\Renderer\RendererInterface;
use Twig_SimpleFunction as SimpleFunction;
use Twig_Extension as Extension;

class ZendFormExtension extends Extension
{
    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * @param RendererInterface $renderer
     */
    public function __construct(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * Register the following Zend form view helpers
     *  - 'form', 'formElement', 'formLabel', 'formElementErrors', 'formButton'
     */
    public function getFunctions()
    {
        return [
            new SimpleFunction('form', [$this->renderer, 'form'], ['is_safe' => ['html']]),
            new SimpleFunction('formElement', [$this->renderer, 'formElement'], ['is_safe' => ['html']]),
            new SimpleFunction('formLabel', [$this->renderer, 'formLabel'], ['is_safe' => ['html']]),
            new SimpleFunction('formElementErrors', [$this->renderer, 'formElementErrors'], ['is_safe' => ['html']]),
            new SimpleFunction('formButton', [$this->renderer, 'formButton'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zend_form';
    }
}
