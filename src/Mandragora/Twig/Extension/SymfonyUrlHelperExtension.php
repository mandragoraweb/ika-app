<?php
namespace Mandragora\Twig\Extension;

use Mandragora\View\Helper\UrlHelper;
use Twig_Extension as TwigExtension;
use Twig_Function_Method as TwigFunctionMethod;

class SymfonyUrlHelperExtension extends TwigExtension
{
    /** @var UrlHelper */
    protected $urlHelper;

    /**
     * @param UrlHelper $urlHelper
     */
    public function __construct(UrlHelper $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    /**
     * @return UrlHelper
     */
    protected function getUrlHelper()
    {
        return $this->urlHelper;
    }

    /**
     * @see Twig_Extension::getFunctions()
     */
    public function getFunctions()
    {
        return [
            'url' => new TwigFunctionMethod($this, 'url', ['is_safe' => ['html']]),
            'serverUrl' => new TwigFunctionMethod($this, 'serverUrl', ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param string $routeName
     * @param array  $params
     */
    public function url($routeName, array $params = array())
    {
        return $this->urlHelper->url($routeName, $params);
    }

    /**
     * @return string
     */
    public function serverUrl()
    {
        return $this->urlHelper->serverUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'symfony-url-helper';
    }
}
