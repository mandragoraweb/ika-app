<?php
namespace Mandragora\Form;

use Zend\Form\Form;

class CrudForm extends Form
{
    /**
     * Initialize the form elements before persisting an entity to the data store
     *
     * @return CrudForm
     */
    public function prepareForCreating()
    {
        return $this;
    }

    /**
     * Initialize the form elements before persisting an entity to the data store
     *
     * @return CrudForm
     */
    public function prepareForEditing()
    {
        return $this;
    }
}
