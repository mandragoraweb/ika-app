<?php
/**
 * Adapter for Doctrine 2 connections
 *
 * PHP version 5.3
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 * @license    MIT
 */
namespace Mandragora\Database;

use \Doctrine\DBAL\Configuration;
use \Doctrine\DBAL\DriverManager;
use \ReflectionClass;

/**
 * Adapter for Doctrine 2 connections
 */
class DbalConnection implements ConnectionInterface
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        $configuration = $this->createConfiguration($options['config']);
        $this->connection = DriverManager::getConnection($options, $configuration);
    }

    /**
     * @param  array                        $options
     * @return \Doctrine\DBAL\Configuration
     */
    protected function createConfiguration(array $options)
    {
        $config = new Configuration();
        if (!empty($options['cache'])) {
            $class = new ReflectionClass($options['cache']['class']);
            $cache = $class->newInstanceArgs($options['cache']['params']);
            $config->setResultCacheImpl($cache);
        }

        return $config;
    }

    /**
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param  string $tableName
     * @param  array  $identifier
     * @return int    The number of affected rows
     */
    public function delete($tableName, array $identifier)
    {
        return $this->getConnection()->delete($tableName, $identifier);
    }

    /**
     * @param  string $tableName
     * @param  array  $data
     * @return int    The last insert ID
     */
    public function insert($tableName, array $data)
    {
        $this->getConnection()->insert($tableName, $data);

        return $this->lastInsertId();
    }

    /**
     * @param  unknown $tableName
     * @param  array   $data
     * @param  array   $identifier
     * @return int     The number of affected rows
     */
    public function update($tableName, array $data, array $identifier)
    {
        return $this->getConnection()->update($tableName, $data, $identifier);
    }

    /**
     * @param  unknown $sql
     * @param  array   $params
     * @return array
     */
    public function fetchAll($sql, array $params = array())
    {
        return $this->getConnection()->fetchAll($sql, $params);
    }

    /**
     * @param string $sql
     * @param array  $params
     */
    public function fetch($sql, array $params = array())
    {
        return $this->getConnection()->fetchAssoc($sql, $params);
    }

    /**
     * Retrieves an scalar from a query
     *
     * @param  string $sql
     * @return string
     */
    public function fetchScalar($sql, array $params = array())
    {
        return $this->getConnection()->executeQuery($sql, $params)->fetchColumn(0);
    }

    /**
     * @param  string                          $query
     * @param  array                           $params
     * @return \Doctrine\DBAL\Driver\Statement The executed statement.
     */
    public function executeQuery($query, array $params = array())
    {
        return $this->getConnection()->executeQuery($query, $params);
    }

    /**
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function createQueryBuilder()
    {
        return $this->getConnection()->createQueryBuilder();
    }

    /**
     * @return int
     */
    public function lastInsertId()
    {
        return $this->getConnection()->lastInsertId();
    }

    /**
     * @param string $identifier
     */
    public function quoteIdentifier($identifier)
    {
        $this->getConnection()->quoteIdentifier($identifier);
    }
}
