<?php
/**
 * Interface for database connections
 *
 * PHP version 5.3
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 * @license    MIT
 */
namespace Mandragora\Database;

/**
 * Interface for database connections
 */
interface ConnectionInterface
{
    /**
     * @param string $tableName
     * @param array  $identifier
     */
    public function delete($tableName, array $identifier);

    /**
     * @param  string $tableName
     * @param  array  $data
     * @return int    The last insert ID
     */
    public function insert($tableName, array $data);

    /**
     * @param string $tableName
     * @param array  $data
     * @param array  $identifier
     */
    public function update($tableName, array $data, array $identifier);

    /**
     * @param string $sql
     * @param array  $params
     */
    public function fetchAll($sql, array $params = []);

    /**
     * Retrieves an scalar from a query
     *
     * @param  string $sql
     * @return string
     */
    public function fetchScalar($sql, array $params = []);

    /**
     * Fecth a single row
     *
     * @param string $sql
     * @param array  $params
     */
    public function fetch($sql, array $params = []);

    /**
     * @param string $query
     * @param array  $params
     */
    public function executeQuery($query, array $params = []);

    /**
     * @return int
     */
    public function lastInsertId();
}
