<?php
namespace Mandragora\View\Helper;

use \Symfony\Component\Routing\RequestContext;
use \Symfony\Component\Routing\RouteCollection;
use \Symfony\Component\Routing\Generator\UrlGenerator;

class SymfonyUrlHelper implements UrlHelper
{
    /**
     * @var UrlGenerator
     */
    protected $urlGenerator;

    /**
     * @var RouteCollection
     */
    protected $routes;

    /**
     * @param RouteCollection $routes
     * @param RequestContext  $context
     */
    public function __construct(RouteCollection $routes, RequestContext $context)
    {
        $this->routes = $routes;
        $this->urlGenerator = new UrlGenerator($routes, $context);
    }

    /**
     * @param string $name
     * @param array  $params
     */
    public function url($name, array $params = array())
    {
        if (null !== $route = clone $this->routes->get($name)) {
            $variables = $route->compile()->getVariables();
            $params = array_intersect_key($params, array_flip($variables));
        }

        return $this->urlGenerator->generate($name, $params);
    }

    /**
     * @param  string $url
     * @return string
     */
    public function serverUrl()
    {
        $serverUrl = $this->urlGenerator->getContext()->getScheme().'://' .
                     $this->urlGenerator->getContext()->getHost();

        return $serverUrl;

    }
}
