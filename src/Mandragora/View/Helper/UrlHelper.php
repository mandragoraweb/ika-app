<?php
/**
 * URL helper interface for adding hypermedia to resources
 *
 * PHP version 5.3
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 * @license    MIT
 */
namespace Mandragora\View\Helper;

/**
 * URL helper interface for adding hypermedia to resources
 */
interface UrlHelper
{
    /**
     * @param string $name
     * @param array  $params
     */
    public function url($name, array $params = array());
}
