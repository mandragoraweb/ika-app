<?php
namespace Mandragora\View;

interface Renderer
{
    /**
     * @return string
     */
    public function render();

    /**
     * @param string $path
     */
    public function setTemplatePath($path);

    /**
     * @param array $data
     */
    public function setData(array $data);
}
