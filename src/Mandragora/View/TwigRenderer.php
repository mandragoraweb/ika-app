<?php
namespace Mandragora\View;

use \Twig_Environment as Environment;

class TwigRenderer implements Renderer
{
    /**
     * @var Environment
     */
    protected $twig;

    /**
     * @var string
     */
    protected $templatePath;

    /**
     * @var array
     */
    protected $data;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
        $this->data = array();
    }

    /**
     * @see \Mandragora\View\Renderer::render()
     */
    public function render()
    {
        return $this->twig->render($this->getTemplatePath(), $this->getData());
    }

    /**
     * @param string $path
     */
    public function setTemplatePath($path)
    {
        $this->templatePath = $path;
    }

    /**
     * @return string
     */
    protected function getTemplatePath()
    {
        return $this->templatePath;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    protected function getData()
    {
        return $this->data;
    }
}
