<?php
namespace Mandragora\Service;

use \Mandragora\View\Helper\UrlHelper;

class CrudService extends AbstractService
{
    /**
     * @var \Mandragora\Form\CrudForm
     */
    protected $form;

    /**
     * @var string
     */
    protected $formOptionsPath;

    /**
     * @var array
     */
    protected $routes;

    /**
     * @var array
     */
    protected $scripts;

    /**
     * Get the route name to be passed to the URL helper for redirecting or setting the
     * action form's attribute
     *
     * @param  string $key
     * @return string
     */
    public function getRoute($key)
    {
        return $this->routes[$key];
    }

    /**
     * @param  string $key
     * @return string
     */
    public function getScript($key)
    {
        return $this->scripts[$key];
    }

    /**
     * @return \Mandragora\Form\CrudForm
     */
    public function getForm()
    {
        if (!$this->form) {
            $this->form = $this->buildForm(require $this->formOptionsPath);
        }

        return $this->form;
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $form = $this->getForm()->prepareForCreating();
        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return \Mandragora\Form\CrudForm
     */
    public function getFormForEditing()
    {
        return $this->getForm()->prepareForEditing();
    }

    /**
     * @param  UrlHelper $urlHelper
     * @return array
     */
    public function populateEditingForm(UrlHelper $urlHelper)
    {
        $form = $this->getFormForEditing();

        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($this->getParams());

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $form = $this->getForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return array
     */
    public function fetchAll()
    {
        $items = $this->getRepository()->findAll($this->getParams());
        $count = $this->getRepository()->count($this->getParams());

        $this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));

        return array('paginator' => $this->getPaginator());
    }

    /**
     * Save a new entity to the dta source
     */
    public function save()
    {
        $this->getRepository()->insert($this->getForm()->getData());
    }

    /**
     * @return array
     */
    public function retrieveOne()
    {
        return $this->getRepository()->find($this->getParams());
    }

    /**
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());

        $form = $this->getForm()->prepareForEditing();
        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return void
     */
    public function update()
    {
        $this->getRepository()->update($this->getForm()->getData());
    }

    /**
     * @return array
     */
    public function show()
    {
        return array('entity' => $this->retrieveOne());
    }

    /**
     * @return int The number of affected rows
     */
    public function delete()
    {
        return $this->getRepository()->delete($this->getParams());
    }
}
