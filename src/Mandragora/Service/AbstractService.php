<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Mandragora\Service;

use \Zend\Form\Factory;
use \Mandragora\Repository\Repository;
use \Mandragora\Paginator\PaginatorInterface;

abstract class AbstractService
{
    /**
     * @var Factory
     */
    protected $formFactory;

    /**
     * @var array
     */
    protected $params;

    /**
     * @var \Mandragora\Repository\Repository
     */
    protected $repository;

    /**
     * @var \Mandragora\Paginator\PaginatorInterface
     */
    protected $paginator;

    /**
     * @return \Mandragora\Repository\Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param \Mandragora\Repository\Repository $repository
     */
    public function setRepository(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Factory $factory
     */
    public function setFormFactory(Factory $factory)
    {
        $this->formFactory = $factory;
    }

    /**
     * @param  array                    $spec
     * @return \Zend\Form\FormInterface
     */
    protected function buildForm(array $spec)
    {
        if (!$this->formFactory) {
            $this->formFactory = new Factory();
        }

        return $this->formFactory->create($spec);
    }

    /**
     * @return \Mandragora\Paginator\PaginatorInterface
     */
    protected function getPaginator()
    {
        return $this->paginator;
    }

    /**
     * @param \Mandragora\Paginator\PaginatorInterface $paginator
     */
    public function setPaginator(PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param  string $name
     * @param  mixed  $default
     * @return mixed
     */
    public function getParam($name, $default = null)
    {
        return isset($this->params[$name]) ? $this->params[$name] : $default;
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function setParam($name, $value)
    {
        $this->params[$name] = $value;
    }
}
