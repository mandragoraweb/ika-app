<?php
namespace Mandragora\Service;

use \Mandragora\Service\CrudService;

interface ServiceAware
{
    /**
     * @param CrudService $user
     */
    public function setService(CrudService $service);

    /**
     * @return CrudService
     */
    public function getService();
}
