<?php
namespace Mandragora\EventListener;

use \Zend\EventManager\EventInterface;
use \Mandragora\View\Renderer;

class ViewRenderingEventListener
{
    /**
     * @var Renderer
     */
    protected $renderer;

    /**
     * @param Renderer $renderer
     */
    public function __construct(Renderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @param EventInterface $event
     */
    public function __invoke(EventInterface $event)
    {
        $this->render($event);
    }

    /**
     * @param EventInterface $event
     */
    public function render(EventInterface $event)
    {
        $controller = $event->getTarget();

        $response = $controller->getResponse();
        if ($response->isRedirect() || $response->getContent()) {
            return;
        }

        $this->renderer->setData($controller->getData());
        $this->renderer->setTemplatePath($controller->getViewScript());
        $response->setContent($this->renderer->render());
    }
}
