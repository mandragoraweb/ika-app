<?php
namespace Mandragora\EventListener;

use \Mandragora\Service\ServiceAware;
use \Zend\EventManager\EventInterface;

class ServiceParamsEventListener
{
    /**
     * @param EventInterface $e
     */
    public function __invoke(EventInterface $event)
    {
        $this->initServiceParams($event);
    }

    /**
     * @param EventInterface $e
     */
    public function initServiceParams(EventInterface $event)
    {
        $controller = $event->getTarget();
        $controller->initParams();
        if ($controller instanceof ServiceAware) {

            $service = $controller->getService();
            $service->setParams($controller->getParams());
        }
    }
}
