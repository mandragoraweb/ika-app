<?php
namespace Mandragora\EventListener;

use \Zend\EventManager\EventInterface;
use \Zend\Authentication\AuthenticationService;

class AuthenticationEventListener
{
    /**
     * @var AuthenticationService
     */
    protected $authenticationService;

    /**
     * @param AuthenticationService $authenticationService
     */
    public function __construct(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param EventInterface $e
     */
    public function isAuthenticated(EventInterface $event)
    {
        $controller  = $event->getTarget();

        if ('authenticateAction' === $controller->getAction()) {
            return; //The user is being authenticated
        }

        if (!$this->authenticationService->hasIdentity()) {
            $controller->redirect('index_index');

            return;
        }

        $user = $this->authenticationService->getIdentity();
        $controller->assign(['role' => $user['role']]);
        $controller->setParam('role', $user['role']);
        $controller->setParam('username', $user['username']);
    }

    /**
     * @param EventInterface $event
     */
    public function __invoke(EventInterface $event)
    {
        $this->isAuthenticated($event);
    }
}
