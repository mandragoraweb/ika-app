<?php
namespace Mandragora\EventListener;

use \Zend\EventManager\EventInterface;
use \Mandragora\Repository\Specification\AbstractSpecification;

class QuerySpecificationEventListener
{
    /**
     * @var AbstractSpecification
     */
    protected $specification;

    /**
     * @param AbstractSpecification $specification
     */
    public function __construct(AbstractSpecification $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @param EventInterface $event
     */
    public function __invoke(EventInterface $event)
    {
        $this->matchQuery($event);
    }

    /**
     * @param EventInterface $event
     */
    public function matchQuery(EventInterface $event)
    {
        $criteria = $event->getParam('criteria');
        $qb = $event->getParam('qb');

        $this->specification->setCriteria($criteria);
        $this->specification->match($qb);
    }
}
