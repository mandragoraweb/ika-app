<?php
namespace Mandragora\Controller;

use \Mandragora\Service\ServiceAware;
use \Mandragora\Service\CrudService;

class CrudController extends HttpController implements ServiceAware
{
    /**
     * @var CrudService
     */
    protected $service;

    /**
     * @param CrudService $user
     */
    public function setService(CrudService $service)
    {
        $this->service = $service;
    }

    /**
     * @return CrudService
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Retrieve a paginated list of entities
     */
    public function listAction()
    {
        $this->assign($this->getService()->fetchAll());
    }

    /**
     * Retrieve the form for creating a new entity
     */
    public function createAction()
    {
        $this->assign($this->getService()->getFormForCreating($this->getUrlHelper()));
    }

    /**
     * Save an entity to the data source, if validation fails show the form with the
     * corresponding error messages
     */
    public function saveAction()
    {
        if ($this->getService()->isValid()) {

            $this->getService()->save();
            $this->redirect($this->getService()->getRoute('list'));
        }

        $this->createAction();
        $this->renderScript($this->getService()->getScript('create'));
    }

    /**
     * Show the form for editing an entity, if the entity cannot be found redirect to the
     * list entites action
     */
    public function editAction()
    {
        if ($this->retrieveOne()) {

            $this->assign($this->getService()->edit($this->getUrlHelper()));
        }
    }

    /**
     * Update the information of a given resource
     *
     * @return void
     */
    public function updateAction()
    {
        if ($this->retrieveOne() && $this->getService()->isValid()) {

            $this->getService()->update();
            $this->redirect($this->getService()->getRoute('list'));
        }
        $this->assign($this->getService()->populateEditingForm($this->getUrlHelper()));
        $this->renderScript($this->getService()->getScript('edit'));
    }

    /**
     * Delete the information of a given resource
     *
     * @return void
     */
    public function removeAction()
    {
        $this->assign($this->getService()->show());
    }

    /**
     * Delete the information of a given resource
     *
     * @return void
     */
    public function deleteAction()
    {
        if ($this->retrieveOne()) {

            $this->getService()->delete();
        }

        $this->redirect($this->getService()->getRoute('list'));
    }

    /**
     * Retrieve the entity by its ID, if not found redirect to the list entities action
     *
     * @param  array $id
     * @return array
     */
    protected function retrieveOne()
    {
        $values = $this->getService()->retrieveOne();

        if (!$values) {

            $this->redirect($this->getService()->getRoute('list'));
        }

        return $values;
    }
}
