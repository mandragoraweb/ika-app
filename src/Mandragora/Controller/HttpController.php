<?php
namespace Mandragora\Controller;

use \Mandragora\View\Helper\UrlHelper;
use \Zend\EventManager\EventManager;
use \Zend\EventManager\EventManagerInterface;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\RedirectResponse;
use \Symfony\Component\HttpFoundation\Request;

class HttpController
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var string
     */
    protected $action;

    /**
     * Current $_POST, $_GET and route params
     *
     * @var array
     */
    protected $params;

    /**
     * @var array
     */
    protected $routeParams;

    /**
     * @var string
     */
    protected $viewScript;

    /**
     * @var \Mandragora\View\Helper\UrlHelper
     */
    protected $urlHelper;

    /**
     * @param Request  $request
     * @param Response $response
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->data = array();
        $this->routeParams = array();
        $this->params = array();
    }

    /**
     * @param  EventManagerInterface $events
     * @return HttpController
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    /**
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (!$this->hasEventManager()) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

    /**
     * @return boolean
     */
    public function hasEventManager()
    {
        return !(null === $this->events);
    }

    /**
     * @param UrlHelper $urlHelper
     */
    public function setUrlHelper(UrlHelper $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    /**
     * @return \Mandragora\View\Helper\UrlHelper
     */
    protected function getUrlHelper()
    {
        return $this->urlHelper;
    }

    /**
     * @param array $data
     */
    public function assign(array $data)
    {
        $this->data = array_merge($this->data, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response $response
     */
    protected function setResponse(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param array $params
     */
    public function setRouteParams(array $params)
    {
        $this->routeParams = $params;
    }

    /**
     * @return array
     */
    protected function getRouteParams()
    {
        return $this->routeParams;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setParam($key, $value)
    {
        $this->params[$key] = $value;
    }

    /**
     * @param  string $key
     * @param  string $default
     * @return string | null
     */
    public function getParam($key, $default = null)
    {
        return isset($this->params[$key]) ? $this->params[$key] : $default;
    }

    /**
     * Initialize, $_POST, $_GET and route params
     *
     * @return void
     */
    public function initParams()
    {
        $this->params += $this->getRouteParams();
        if (is_array($this->getRequest()->request->all())) {
            $this->params += $this->getRequest()->request->all();
        }
        if (is_array($this->getRequest()->query->all())) {
            $this->params += $this->getRequest()->query->all();
        }
    }

    /**
     * @param string $path
     */
    public function renderScript($path)
    {
        $this->viewScript = $path;
    }

    /**
     * @return boolean
     */
    public function hasViewScript()
    {
        return !empty($this->viewScript);
    }

    /**
     * @return string
     */
    public function getViewScript()
    {
        return $this->viewScript;
    }

    /**
     * @param string $routeName
     */
    public function redirect($routeName)
    {
        $url = $this->getUrlHelper()->url($routeName, $this->getParams());

        $this->setResponse(new RedirectResponse($url));
    }

    /**
     * Dispatch the current action
     *
     * @return Response
     */
    public function dispatch()
    {
        $this->getEventManager()->trigger('preDispatch', $this);
        $this->{$this->getAction()}();
        $this->getEventManager()->trigger('postDispatch', $this);

        return $this->getResponse();
    }

    /**
     * @param string $type
     */
    public function setContentType($type)
    {
        $this->response->headers->set('Content-Type', $type);
    }
}
