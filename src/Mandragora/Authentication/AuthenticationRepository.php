<?php
namespace Mandragora\Authentication;

interface AuthenticationRepository
{
    /**
     * @param  string $username
     * @return array
     */
    public function findOneByUsername($username);
}
