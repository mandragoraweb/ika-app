<?php
namespace Mandragora\Authentication;

use \Zend\Authentication\Result;
use \Zend\Authentication\Adapter\AdapterInterface;
use \Mandragora\Password\SecurePasswordGenerator;
use \Mandragora\Authentication\AuthenticationRepository;

class ZendAuthenticationAdapter implements AdapterInterface
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var \Mandragora\Password\SecurePasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @var \Mandragora\Authentication\AuthenticationRepository
     */
    protected $repository;

    /**
     * @param \Mandragora\Password\SecurePasswordGenerator $passwordGenerator
     */
    public function setPasswordGenerator(SecurePasswordGenerator $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
    }

    /**
     * @param \Mandragora\Authentication\AuthenticationRepository
     */
    public function setRepository(AuthenticationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $username
     * @param string $password
     */
    public function setCredentials($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @see \Zend\Authentication\Adapter\AdapterInterface::authenticate()
     */
    public function authenticate()
    {
        $user = $this->repository->findOneByUsername($this->username);

        if ($user) {

            if ($this->passwordGenerator->verify($this->password, $user['password'])) {
                $user['password'] = null;

                return new Result(Result::SUCCESS, $user);
            }

            return new Result(
                Result::FAILURE_CREDENTIAL_INVALID,
                null,
                ['password' => ['Su contraseña es incorrecta.']]
            );
        }

        return new Result(
            Result::FAILURE_IDENTITY_NOT_FOUND,
            null,
            ['username' => ['Su nombre de usuario o contraseña son incorrectos']]
        );
    }
}
