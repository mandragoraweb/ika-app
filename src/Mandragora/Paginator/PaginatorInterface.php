<?php
namespace Mandragora\Paginator;

use \IteratorAggregate;
use \Countable;

interface PaginatorInterface extends IteratorAggregate, Countable
{
    /**
     * @param  integer            $count
     * @param  array              $items
     * @return PaginatorInterface
     */
    public function setCurrentItems($count, array $items);

    /**
     * @param int $currentPage
     */
    public function setCurrentPage($currentPage);

    /**
     * @param int $maxPerPage
     */
    public function setMaxPerPage($maxPerPage);
}
