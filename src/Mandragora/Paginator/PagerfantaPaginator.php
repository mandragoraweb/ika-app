<?php
namespace Mandragora\Paginator;

use \Pagerfanta\PagerfantaInterface;
use \Pagerfanta\Adapter\FixedAdapter;
use \Pagerfanta\Pagerfanta;

class PagerfantaPaginator implements PaginatorInterface, PagerfantaInterface
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @var \Pagerfanta\Pagerfanta
     */
    protected $pagerFanta;

    /**
     * @var int
     */
    protected $maxPerPage;

    /**
     * @param  int                                      $maxPerPage
     * @return \Mandragora\Paginator\PaginatorInterface
     */
    public function setMaxPerPage($maxPerPage)
    {
        $this->maxPerPage = $maxPerPage;

        return $this;
    }

    /**
     * @see \Mandragora\Paginator\PaginatorInterface::setCurrentItems()
     */
    public function setCurrentItems($count, array $items)
    {
        $this->pagerFanta = new Pagerfanta(new FixedAdapter($count, $items));
        $this->pagerFanta->setMaxPerPage($this->maxPerPage);

        return $this;
    }

    /**
     * @return \Pagerfanta\Adapter\AdapterInterface
     */
    public function getAdapter()
    {
        return $this->pagerFanta->getAdapter();
    }

    /**
     * @return boolean
     */
    public function haveToPaginate()
    {
        return $this->pagerFanta->haveToPaginate();
    }

    /**
     * @return number
     */
    public function getCurrentPage()
    {
        return $this->pagerFanta->getCurrentPage();
    }

    /**
     * @return number
     */
    public function getNbResults()
    {
        return $this->pagerFanta->getNbResults();
    }

    /**
     * @return number
     */
    public function getNbPages()
    {
        return $this->pagerFanta->getNbPages();
    }

    /**
     * @return \Traversable
     */
    public function getCurrentPageResults()
    {
        return $this->pagerFanta->getCurrentPageResults();
    }

    /**
     * @return boolean
     */
    public function hasPreviousPage()
    {
        return $this->pagerFanta->hasPreviousPage();
    }

    /**
     * @return boolean
     */
    public function hasNextPage()
    {
        return $this->pagerFanta->hasNextPage();
    }

    /**
     * @return number
     */
    public function getNextPage()
    {
        return $this->pagerFanta->getNextPage();
    }

    /**
     * @return number
     */
    public function getPreviousPage()
    {
        return $this->pagerFanta->getPreviousPage();
    }

    /**
     * @see \Mandragora\Paginator\PaginatorInterface::setCurrentPage()
     */
    public function setCurrentPage($currentPage)
    {
        $this->pagerFanta->setCurrentPage($currentPage);
    }

    /**
     * @see IteratorAggregate::getIterator()
     */
    public function getIterator()
    {
        return $this->pagerFanta->getIterator();
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->pagerFanta->count();
    }
}
