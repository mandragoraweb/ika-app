<?php
/**
 * Runs several query specifications
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Mandragora\Repository\Specification;

use Doctrine\DBAL\Query\QueryBuilder;

/**
 * Runs several query specifications
 */
class ChainedSpecification extends AbstractSpecification
{
    /** @type Specification[] */
    protected $specifications;

    /**
     * @param Specification[] $specifications
     */
    public function __construct(array $specifications)
    {
        $this->specifications = $specifications;
    }

    /**
     * @see \Mandragora\Repository\Specification\AbstractSpecification::setCriteria()
     */
    public function setCriteria(array $criteria)
    {
        foreach ($this->specifications as $specification) {
            $specification->setCriteria($criteria);
        }
    }

    /**
     * @see \ZendApplication\Doctrine\QuerySpecifications\Specification::match()
     */
    public function match(QueryBuilder $query)
    {
        foreach ($this->specifications as $specification) {
            $specification->match($query);
        }
    }
}
