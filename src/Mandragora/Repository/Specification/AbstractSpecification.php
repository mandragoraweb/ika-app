<?php
namespace Mandragora\Repository\Specification;

use \Doctrine\DBAL\Query\QueryBuilder;

abstract class AbstractSpecification
{
    /**
     * @var array
     */
    protected $criteria;

    /**
     * @param  array                                                            $criteria
     * @return \Application\Repository\Specification\EventCategorySpecification
     */
    public function setCriteria(array $criteria)
    {
        $this->criteria = $criteria;

        return $this;
    }

    /**
     * @return array
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param QueryBuilder $qb
     */
    abstract public function match(QueryBuilder $qb);
}
