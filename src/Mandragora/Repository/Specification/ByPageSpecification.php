<?php
/**
 * Specification for retrieving paginated results in a query.
 *
 * PHP version 5.3
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 * @license    MIT
 */
namespace Mandragora\Repository\Specification;

use \Doctrine\DBAL\Query\QueryBuilder;

/**
 * Specification for retrieving paginated results in a query.
 */
class ByPageSpecification extends AbstractSpecification
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * The amount of items per page
     *
     * @var int
     */
    protected $limit;

    /**
     * The number of the page that the user wants to retrieve
     *
     * @var int
     */
    protected $page;

    /**
     * @param  array               $criteria
     * @return ByPageSpecification
     */
    public function setCriteria(array $criteria)
    {
        $this->setPage(isset($criteria['page']) ? $criteria['page'] : 1);

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->getLimit() * ($this->getPage() - 1);
    }

    /**
     * Add both first and max results clauses
     *
     * @see \Mandragora\Repository\Specification\SpecificationInterface::match()
     */
    public function match(QueryBuilder $qb)
    {
        if ($this->isQueryCount($qb)) {
            return;
        }

        $qb->setFirstResult($this->getOffset());
        $qb->setMaxResults($this->getLimit());
    }

    /**
     * @param  QueryBuilder $query
     * @return boolean
     */
    protected function isQueryCount(QueryBuilder $query)
    {
        $select = $query->getQueryPart('select');

        foreach ($select as $selectPart) {
            if (preg_match('/COUNT/i', $selectPart)) {
                return true;
            }
        }

        return false;
    }
}
