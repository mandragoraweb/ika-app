<?php
/**
 * Interface for repositories
 *
 * PHP version 5.3
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2013
 * @license    MIT
 */
namespace Mandragora\Repository;

use \Countable;

/**
 * Interface for repositories
 */
interface RepositoryInterface extends Countable
{
    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria);

    /**
     * @param  array $id
     * @return array
     */
    public function find(array $id);

    /**
     * @param  array $values
     * @return int   The number of affected rows
     */
    public function insert(array $values);

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
     */
    public function update(array $values);

    /**
     * @param  array $id
     * @return int   The number of affected rows
     */
    public function delete(array $id);

    /**
     * @see Countable::count()
     */
    public function count(array $criteria = array());
}
