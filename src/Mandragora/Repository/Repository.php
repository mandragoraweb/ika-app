<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Mandragora\Repository;

use \Mandragora\Database\ConnectionInterface;
use \Zend\EventManager\EventManagerInterface;
use \Zend\EventManager\EventManager;

/**
 * Base class for repositories
 */
abstract class Repository
{
    /**
     * @var \Mandragora\Database\ConnectionInterface
     */
    protected $connection;

    /**
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * @return \Mandragora\Database\ConnectionInterface
     */
    protected function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param \Mandragora\Database\ConnectionInterface $connection
     */
    public function setConnection(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param  EventManagerInterface $events
     * @return Repository
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $eventManager->setIdentifiers([__CLASS__, get_called_class()]);
        $this->eventManager = $eventManager;

        return $this;
    }

    /**
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (!$this->hasEventManager()) {
            $this->setEventManager(new EventManager());
        }

        return $this->eventManager;
    }

    /**
     * @return boolean
     */
    public function hasEventManager()
    {
        return !(null === $this->eventManager);
    }

    /**
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function createQueryBuilder()
    {
        return $this->getConnection()->createQueryBuilder();
    }

    /**
     * @param string $query
     * @param array  $params
     */
    public function executeQuery($query, array $params = array())
    {
        return $this->getConnection()->executeQuery($query, $params);
    }
}
