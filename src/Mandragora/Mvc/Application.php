<?php
namespace Mandragora\Mvc;

use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\Config;
use \Symfony\Component\Routing\Exception\ResourceNotFoundException;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\Request;
use \ErrorException;
use \Exception;

class Application
{
    /**
     * @var ServiceManager
     */
    protected $sm;

    /**
     * @var string
     */
    protected $environment;

    /**
     * @param ServiceManager $sm
     */
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        $this->environment = 'production';
        set_error_handler(array($this, 'errorExceptionHandler'));
    }

    /**
     * @return ServiceManager
     */
    protected function getServiceManager()
    {
        return $this->sm;
    }

    /**
     * @return string
     */
    protected function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param string $environment
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;
    }

    /**
     * Dispatch the current request
     */
    public function run()
    {
        try {

            $router = $this->getServiceManager()->get('router');
            $parameters = $router->match($router->getContext()->getPathInfo());

            $configuration = new Config(require $parameters['config']);
            $configuration->configureServiceManager($this->getServiceManager());

            $controller = $this->initController($parameters);

            $response = $controller->dispatch();

        } catch (ResourceNotFoundException $e) {

            $response = $this->handleNotFound($router->getContext()->getPathInfo());

        } catch (Exception $e) {

            $response = $this->handleException($e);
        }

        $response->send();
    }

    /**
     * @param  array                                 $params
     * @return \Mandragora\Controller\HttpController
     */
    protected function initController(array $params)
    {
        $controller = $this->getServiceManager()->get($params['controller']);
        $controller->setAction($params['action']);

        $viewScript = str_replace('_', '/', $params['_route']) . '.html.twig';
        $controller->renderScript($viewScript);

        unset($params['controller']);
        unset($params['action']);
        unset($params['config']);
        unset($params['_route']);

        $controller->setRouteParams($params);

        return $controller;
    }

    /**
     * Show the not found template
     *
     * @param  string   $pathinfo
     * @return Response
     */
    protected function handleNotFound($pathinfo)
    {
        $view = $this->getServiceManager()->get('view');

        $content = $view->render('error/notfound.html.twig', array('url' => $pathinfo));

        return new Response($content, 404);
    }

    /**
     * Show the error template with the exception stack trace if the dev environment is on
     *
     * @param  Exception $e
     * @return Response
     */
    protected function handleException(Exception $e)
    {
        $view = $this->getServiceManager()->get('view');

        $content = $view->render(
            'error/error.html.twig',
            array('exception' => $e, 'environment' => $this->getEnvironment())
        );

        return new Response($content, 500);
    }

    /**
     * @param  int            $errno
     * @param  string         $errstr
     * @param  string         $errfile
     * @param  int            $errline
     * @throws ErrorException
     */
    public function errorExceptionHandler($errno, $errstr, $errfile, $errline)
    {
        throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
    }
}
