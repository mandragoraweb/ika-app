<?php
namespace Mandragora\Mvc;

use \Symfony\Component\Routing\Route;
use \Symfony\Component\Routing\RouteCollection;

class RouteProvider
{
    /**
     * @var array
     */
    protected $routesPaths;

    public function __construct(array $routesPaths)
    {
        $this->routesPaths = $routesPaths;
    }

    /**
     * @return \Symfony\Component\Routing\RouteCollection
     */
    public function getRoutes()
    {
        $collection = new RouteCollection();

        foreach ($this->routesPaths as $routesPath) {

            $this->loadRoutes(require $routesPath, $collection);
        }

        return $collection;
    }

    /**
     * @param array           $routes
     * @param RouteCollection $collection
     */
    public function loadRoutes(array $routes, RouteCollection $collection)
    {
        foreach ($routes as $routeName => $route) {
            $collection->add($routeName, new Route($route['path'], $route['defaults']));
        }
    }
}
