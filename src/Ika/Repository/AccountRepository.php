<?php
namespace Ika\Repository;

use Mandragora\Repository\Repository;

class AccountRepository extends Repository
{
    /** @var string */
    public static $CLASS = __CLASS__;

    /**
     * @return array
     */
    public function findAccountStateByUsername(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $select = <<<SELECT
        p.first_name,
        p.paternal_surname,
        p.maternal_surname,
        cl.client_id,
        cl.rfc,
        CASE
            WHEN pr.opening_balance <  0 THEN pr.opening_balance + SUM(pa.amount_paid)
            ELSE pr.opening_balance - SUM(pa.amount_paid)
        END AS opening_balance
SELECT;
        $qb
            ->select($select)
            ->from('user', 'u')
            ->innerJoin('u', 'profile', 'p', 'u.profile_id = p.profile_id')
            ->leftJoin('p', 'client', 'cl', 'cl.profile_id = p.profile_id')
            ->leftJoin('cl', 'property', 'pr', 'cl.client_id = pr.client_id')
            ->leftJoin('pr', 'payment', 'pa', 'pr.property_id = pa.property_id AND pa.is_opening_balance = "1" AND pa.date < :startDate')
            ->where('u.username = :username');

        $client = $this->getConnection()->fetch($qb->getSQL(), [
            'username' => $criteria['username'], 'startDate' => $criteria['start_date'],
        ]);
        $criteria['client_id'] = $client['client_id'];

        $client['payments'] = $this->findAllPaymentsByClient($criteria);

        return $client;
    }

    /**
     * @return array
     */
    public function findAccountStateByClient(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $select = <<<SELECT
        p.first_name,
        p.paternal_surname,
        p.maternal_surname,
        cl.client_id,
        cl.rfc,
        CASE
            WHEN pr.opening_balance <  0 THEN pr.opening_balance + COALESCE(SUM(pa.amount_paid), 0)
            ELSE pr.opening_balance - COALESCE(SUM(pa.amount_paid), 0)
        END AS opening_balance
SELECT;
        $qb
            ->select($select)
            ->from('client', 'cl')
            ->innerJoin('cl', 'profile', 'p', 'cl.profile_id = p.profile_id')
            ->leftJoin('cl', 'property', 'pr', 'cl.client_id = pr.client_id')
            ->leftJoin('pr', 'payment', 'pa', 'pr.property_id = pa.property_id AND pa.is_opening_balance = "1" AND pa.date < :startDate')
            ->where('cl.client_id = :clientId')
            ->andWhere('pr.property_id = :propertyId');

        $client = $this->getConnection()->fetch($qb->getSQL(), [
            'clientId' => $criteria['client_id'], 'propertyId' => $criteria['property_id'], 'startDate' => $criteria['start_date'],
        ]);

        $client['payments'] = $this->findAllPaymentsByClient($criteria);

        return $client;
    }

    /**
     * @param array $criteria
     */
    public function findAllPaymentsByClient(array $criteria)
    {
        $qbProperty  = $this->createQueryBuilder();
        $select = <<<SELECT
            pa.payment_id,
            pa.is_opening_balance,
            pa.client_id AS payor_client_id,
            pa.date AS payment_date,
            p.street,
            p.number,
            c.concept,
            t.description,
            f.amount,
            su.surcharge_id,
            SUM(
                CASE
                    WHEN su.surcharge_type = 'M' THEN su.fixed
                    ELSE su.fixed / 100 * f.amount
                END
            ) AS surcharge_amount,
            pa.amount_paid,
            pa.date
SELECT;
        $qbProperty
            ->select($select)
            ->from('property', 'p')
            ->innerJoin('p', 'payment', 'pa', 'pa.property_id = p.property_id')
            ->leftJoin('pa', 'fee', 'f', 'f.fee_id = pa.fee_id')
            ->leftJoin('f', 'concept', 'c', 'c.concept_id = f.concept_id')
            ->innerJoin('f', 'type', 't', 't.type_id = f.type_id')
            ->leftJoin('pa', 'surcharge', 'su', 'su.payment_id = pa.payment_id')
            ->where('pa.date BETWEEN :startDate and :stopDate')
            ->andWhere('p.property_id = :propertyId')
            ->groupBy('pa.payment_id');

        $params = [
            'propertyId' => $criteria['property_id'],
            'startDate' => $criteria['start_date'],
            'stopDate' => $criteria['stop_date']
        ];

        return $this->getConnection()->fetchAll($qbProperty->getSQL(), $params);

    }
}
