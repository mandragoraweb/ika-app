<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class ClientRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $builder = $this->createQueryBuilder();
        $builder
            ->select('DISTINCT(cl.client_id), pr.first_name, pr.paternal_surname, pr.maternal_surname, u.username')
            ->from('client', 'cl')
            ->innerJoin('cl', 'profile', 'pr', 'cl.profile_id = pr.profile_id')
            ->innerJoin('pr', 'user', 'u', 'pr.profile_id = u.profile_id')
            ->orderBy('pr.first_name, pr.paternal_surname, pr.maternal_surname');

        if ('webmaster' !== $criteria['role']) {
            $builder
                ->leftJoin('cl', 'property', 'p', 'cl.client_id = p.client_id')
                ->leftJoin('p', 'zone', 'z', 'p.zone_id = z.zone_id')
                ->leftJoin('z', 'user_zone', 'uz', 'z.zone_id = uz.zone_id')
                ->where('uz.username = :username')
                ->setParameter('username', $criteria['username']);
        }

        $eventParams = ['criteria' => $criteria, 'qb' => $builder];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($builder->getSql(), $builder->getParameters());
    }

    /**
     * @array $criteria
     * @return array
     */
    public function findAllWithoutFilter(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('DISTINCT(cl.client_id), pr.first_name, pr.paternal_surname, pr.maternal_surname')
           ->from('client', 'cl')
           ->innerJoin('cl', 'profile', 'pr', 'cl.profile_id = pr.profile_id')
           ->innerJoin('pr', 'user', 'u', 'pr.profile_id = u.profile_id');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('filterByRole', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql(), $qb->getParameters());
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
            ->from('client', 'cl')
            ->innerJoin('cl', 'profile', 'pr', 'cl.profile_id = pr.profile_id')
            ->innerJoin('pr', 'user', 'u', 'pr.profile_id = u.profile_id')
            ->where('client_id = :clientId');
        $params = array('clientId' => $criteria['clientId']);

        return $this->getConnection()->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $this->getConnection()->insert('client', $values);

        return $this->getConnection()->lastInsertId();
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('client_id' => $values['client_id']);

        return $this->getConnection()->update('client', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('client', array('client_id' => $criteria['clientId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $builder = $this->createQueryBuilder();

        $builder
            ->select('COUNT(DISTINCT(cl.client_id))')
            ->from('client', 'cl')
            ->innerJoin('cl', 'profile', 'pr', 'cl.profile_id = pr.profile_id')
            ->innerJoin('pr', 'user', 'u', 'pr.profile_id = u.profile_id');

        if ('webmaster' !== $criteria['role']) {
            $builder
                ->leftJoin('cl', 'property', 'p', 'cl.client_id = p.client_id')
                ->leftJoin('p', 'zone', 'z', 'p.zone_id = z.zone_id')
                ->leftJoin('z', 'user_zone', 'uz', 'z.zone_id = uz.zone_id')
                ->where('uz.username = :username')
                ->setParameter('username', $criteria['username']);
        }

        return $this->getConnection()->fetchScalar($builder->getSql(), $builder->getParameters());
    }

    /**
     * @param  array $id
     * @return array
     */
    public function findAllWithUserAndProfileInfo(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
           ->from('client', 'cl')
           ->innerJoin('cl', 'profile', 'pr', 'cl.profile_id = pr.profile_id')
           ->innerJoin('pr', 'user', 'u', 'pr.profile_id = u.profile_id')
           ->where('client_id = :clientId');
        $params = array('clientId' => $criteria['client_id']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $id
     * @return array
     */
    public function findZoneAdministrator(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
           ->from('zone', 'z')
           ->innerJoin('z', 'user_zone', 'uz', 'uz.zone_id = z.zone_id')
           ->innerJoin('uz', 'user', 'u', 'u.username = uz.username')
           ->innerJoin('u', 'profile', 'p', 'u.profile_id = p.profile_id')
           ->where('z.zone_id = :zoneId')
           ->andWhere('u.role = "admin"');

        $params = array('zoneId' => $criteria['zone_id']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param integer $zoneId
     */
    public function inSubdivision($zoneId)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->select('*')
            ->from('client', 'c')
            ->innerJoin('c', 'profile', 'p', 'c.profile_id = p.profile_id')
            ->innerJoin('c', 'property', 'pr', 'pr.client_id = c.client_id')
            ->innerJoin('pr', 'zone', 'z', 'pr.zone_id = z.zone_id')
            ->innerJoin('pr', 'user', 'u', 'p.profile_id = u.profile_id')
            ->where('z.zone_id = :zoneId')
            ->orderBy('p.first_name, p.paternal_surname, p.maternal_surname');

        return $this->executeQuery($qb->getSQL(), ['zoneId' => $zoneId])->fetchAll();
    }
}
