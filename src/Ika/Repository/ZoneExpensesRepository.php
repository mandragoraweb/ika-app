<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class ZoneExpensesRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('zone_expenses', 'ze')
           ->innerJoin('ze', 'concept', 'c', 'ze.concept_id = c.concept_id')
           ->where('ze.zone_id = :zoneId');

        $params = ['zoneId' => $criteria['zoneId']];
        /*$eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);*/

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @return array
     */
    public function findAllWithoutFilter()
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('zone_expenses', 'ze');

        return $this->getConnection()->fetchAll($qb->getSql());
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
            ->from('zone_expenses', 'ze')
            ->innerJoin('ze', 'concept', 'c', 'ze.concept_id = c.concept_id')
            ->where('zone_expenses_id = :zoneExpensesId');
        $params = array('zoneExpensesId' => $criteria['zoneExpensesId']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $this->getConnection()->insert('zone_expenses', $values);

        return $this->getConnection()->lastInsertId();
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('zone_expenses_id' => $values['zone_expenses_id']);

        return $this->getConnection()->update('zone_expenses', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('zone_expenses', array('zone_expenses_id' => $criteria['zoneExpensesId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('zone_expenses', 'ze');

        return $this->getConnection()->fetchScalar($qb->getSql());
    }
}
