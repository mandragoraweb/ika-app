<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class FeeRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('fee', 'f')
           ->innerJoin('f', 'concept', 'c', 'f.concept_id = c.concept_id')
           ->where('f.zone_id = :zoneId');

        $params = ['zoneId' => $criteria['zoneId']];
        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @todo Is there a good reason to pass an array instead of the zoneId value?
     * @array $criteria
     * @return array
     */
    public function findAllByZone(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('fee', 'f')
           ->innerJoin('f', 'concept', 'c', 'f.concept_id = c.concept_id')
           ->innerJoin('f', 'type', 't', 'f.type_id = t.type_id')
           ->where('f.zone_id = :zoneId');

        $params = array('zoneId' => $criteria['zoneId']);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @return array
     */
    public function findAllWithoutFilter()
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('fee', 'f')
           ->innerJoin('f', 'concept', 'c', 'f.concept_id = c.concept_id');

        return $this->getConnection()->fetchAll($qb->getSql());
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
            ->from('fee', 'f')
            ->where('fee_id = :feeId');
        $params = array('feeId' => $criteria['feeId']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        return $this->getConnection()->insert('fee', $values);
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('fee_id' => $values['fee_id']);

        return $this->getConnection()->update('fee', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('fee', array('fee_id' => $criteria['feeId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')
           ->from('fee', 'f')
           ->innerJoin('f', 'concept', 'c', 'f.concept_id = c.concept_id')
           ->where('f.zone_id = :zoneId');

        $params = array('zoneId' => $criteria['zoneId']);;

        return $this->getConnection()->fetchScalar($qb->getSql(), $params);
    }
}
