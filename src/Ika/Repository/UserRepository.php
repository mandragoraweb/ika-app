<?php
namespace Ika\Repository;

use \Mandragora\Authentication\AuthenticationRepository;
use \Mandragora\Repository\CrudRepository;

class UserRepository extends CrudRepository implements AuthenticationRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @param  string $username
     * @return array
     */
    public function findOneByUsername($username)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('user', 'u')
           ->where('u.username = :username')
           ->setParameter('username', $username);

        return $this->executeQuery($qb->getSQL(), $qb->getParameters())->fetch();
    }

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('u.*, p.*')->from('user', 'u');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql(), $qb->getParameters());
    }

    /**
     * @array $criteria
     * @return array
     */
    public function findAllWithoutFilter()
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('user', 'u')
           ->innerJoin('u', 'profile', 'pr', 'u.profile_id = pr.profile_id');

        return $this->getConnection()->fetchAll($qb->getSql());
    }

    /**
     * @array $criteria
     * @return array
     */
    public function findAllByProfile(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
            ->from('user', 'u')
            ->where('profile_id = :profileId');
        $params = array('profileId' => $criteria['profileId']);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @return array
     */
    public function findAllByAdminRole()
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('user', 'u')
           ->innerJoin('u', 'profile', 'pr', 'u.profile_id = pr.profile_id')
           ->where('u.role = :role');
        $params = array('role' => 'admin');

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @return array
     */
    public function findState($username)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('user', 'u')
           ->innerJoin('u', 'profile', 'pr', 'u.profile_id = pr.profile_id')
           ->leftJoin('pr', 'client', 'cl', 'cl.profile_id = pr.profile_id')
           ->where('u.username = :username');
        $params = array('username' => $username);

        $clients = $this->getConnection()->fetchAll($qb->getSql(), $params);

        $repository = $this;
        $qbProperty  = $this->createQueryBuilder();
        $clients = array_map(function($client) use ($repository, $qbProperty) {
            $qbProperty->select('*')
                       ->from('property', 'p')
                       ->innerJoin('p', 'payment', 'pa', 'pa.property_id = p.property_id')
                       ->leftJoin('pa', 'fee', 'f', 'f.fee_id = pa.fee_id')
                       ->leftJoin('f', 'concept', 'c', 'c.concept_id = f.concept_id')
                       ->where('p.client_id = :client_id');

            $params = array('client_id' => $client['client_id']);
            $property['property'] = $repository->getConnection()
                                               ->fetchAll($qbProperty->getSQL(), $params);

            return $client + $property;
        }, $clients);

        return $clients;
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
            ->from('user', 'u')
            ->where('username = :username');
        $params = array('username' => $criteria['username']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $id
     * @return array
     */
    public function findByUserZone(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
           ->from('user_zone', 'uz')
           ->innerJoin('uz', 'user', 'u', 'u.username = uz.username')
           ->innerJoin('u', 'profile', 'p', 'p.profile_id = u.profile_id')
           ->where('uz.username = :username')
           ->andWhere('uz.zone_id = :zoneId');
        $params = array(
            'username' => $criteria['user'],
            'zoneId' => $criteria['zoneId']
        );

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        return $this->getConnection()->insert('user', $values);
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
     */
    public function insertUserZone(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        return $this->getConnection()->insert('user_zone', $values);
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('username' => $values['username']);

        return $this->getConnection()->update('user', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('user', array('username' => $criteria['username']));
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
     */
    public function deleteUserZone(array $criteria)
    {
        $this->getConnection()->delete('user_zone', array('username' => $criteria['user'], 'zone_id' => $criteria['zoneId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('user', 'u');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchScalar($qb->getSql(), $qb->getParameters());
    }

    /**
     * @see Countable::count()
     */
    public function countByProfile(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('user', 'u')
           ->where('profile_id = :profileId');
        $params = array('profileId' => $criteria['profileId']);

        return $this->getConnection()->fetchScalar($qb->getSql(), $params);
    }
}
