<?php
namespace Ika\Repository\Specification;

use Mandragora\Repository\Specification\AbstractSpecification;
use Doctrine\DBAL\Query\QueryBuilder;

class CommitteeInZoneSpecification extends AbstractSpecification
{
    public function match(QueryBuilder $qb)
    {
        if (!empty($this->criteria['zoneId'])) {
            $qb->innerJoin('u', 'user_zone', 'uz', 'u.username = uz.username')
               ->innerJoin('u', 'profile', 'p', 'u.profile_id = p.profile_id')
               ->where('uz.zone_id = :zoneId')
               ->andWhere('u.role = "committee"');
            $qb->setParameter('zoneId', $this->criteria['zoneId']);
        }
    }
}
