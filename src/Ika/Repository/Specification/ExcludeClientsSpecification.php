<?php
namespace Ika\Repository\Specification;

use Mandragora\Repository\Specification\AbstractSpecification;
use Doctrine\DBAL\Query\QueryBuilder;

class ExcludeClientsSpecification extends AbstractSpecification
{
    public function match(QueryBuilder $qb)
    {
        $qb->innerJoin('po', 'user', 'u', 'po.profile_id = u.profile_id')
           ->where('u.role != :role');

        $qb->setParameter('role', 'client');
    }
}
