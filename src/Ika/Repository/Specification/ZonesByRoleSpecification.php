<?php
namespace Ika\Repository\Specification;

use Mandragora\Repository\Specification\AbstractSpecification;
use Doctrine\DBAL\Query\QueryBuilder;

class ZonesByRoleSpecification extends AbstractSpecification
{
    public function match(QueryBuilder $qb)
    {
        if (!empty($this->criteria['role']) && $this->criteria['role'] !== 'webmaster') {
            $qb->leftJoin('z', 'user_zone', 'uz', 'z.zone_id = uz.zone_id');
            $qb->andWhere('uz.username = :username');
            $qb->setParameter('username', $this->criteria['username']);
        }
    }
}
