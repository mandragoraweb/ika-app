<?php
namespace Ika\Repository\Specification;

use Mandragora\Repository\Specification\AbstractSpecification;
use Doctrine\DBAL\Query\QueryBuilder;

class ExcludeCommitteeByZoneRegisterSpecification extends AbstractSpecification
{
    public function match(QueryBuilder $qb)
    {
        if (!empty($this->criteria['zoneId'])) {
            $qb->leftJoin('u', 'user_zone', 'uz', 'u.username = uz.username')
               ->innerJoin('u', 'profile', 'p', 'u.profile_id = p.profile_id')
               ->where('uz.username IS NULL')
               ->andWhere('u.role = "committee"');
        }
    }
}
