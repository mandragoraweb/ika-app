<?php
namespace Ika\Repository\Specification;

use Mandragora\Repository\Specification\AbstractSpecification;
use Doctrine\DBAL\Query\QueryBuilder;
use Ika\Repository\ClientRepository;

class ClientsByZoneSpecification extends AbstractSpecification
{
    protected $repository;

    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }

    public function match(QueryBuilder $qb)
    {
        if (!empty($this->criteria['role']) && $this->criteria['role'] !== 'webmaster') {
            $onlyOwnedClientsQuery = $this->repository->createQueryBuilder();
            $onlyOwnedClientsQuery->select('uz.zone_id')
                                  ->from('user_zone', 'uz')
                                  ->where('uz.username = :username');

            $qb->leftJoin('cl', 'property', 'p', 'cl.client_id = p.client_id');
            $qb->leftJoin('p', 'zone', 'z', 'p.zone_id = z.zone_id');
            $qb->where("z.zone_id IN ({$onlyOwnedClientsQuery->getSQL()})");

            $qb->setParameter('username', $this->criteria['username']);
        }
    }
}
