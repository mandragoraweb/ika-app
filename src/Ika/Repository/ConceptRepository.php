<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;
use \Doctrine\DBAL\DBALException;

class ConceptRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('concept', 'c')
           ->orderBy('c.code');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql());
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
            ->from('concept', 'c')
            ->where('concept_id = :conceptId');
        $params = array('conceptId' => $criteria['conceptId']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        return $this->getConnection()->insert('concept', $values);
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('concept_id' => $values['concept_id']);

        return $this->getConnection()->update('concept', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        try {
            $this->getConnection()->delete('concept', array('concept_id' => $criteria['conceptId']));
        } catch (DBALException $e) {
            return array ('message' => $e->getMessage());
        }
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')
           ->from('concept', 'c');

        return $this->getConnection()->fetchScalar($qb->getSql());
    }

    /**
     * @return array
     */
    public function findAllWithoutFilter()
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('concept', 'c')->where('type = :type');

        $params = array('type' => 'I');

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @return array
     */
    public function findAllWithExpense()
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('concept', 'c')->where('type = :type');

        $params = array('type' => 'O');

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }
}
