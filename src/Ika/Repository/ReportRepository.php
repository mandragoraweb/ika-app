<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Repository;

use \Mandragora\Repository\Repository;

class ReportRepository extends Repository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @return array
     */
    public function findStateByClients(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*','SUM(f.amount) AS resultAmount', 'SUM(pa.amount_paid) AS resultAmountPaid', 'SUM(su.fixed) as resultSurcharge')
           ->from('client', 'cl')
           ->innerJoin('cl', 'profile', 'pr', 'pr.profile_id = cl.profile_id')
           ->innerJoin('cl', 'property', 'p', 'p.client_id = cl.client_id')
           ->innerJoin('p', 'zone', 'z', 'z.zone_id = p.zone_id')
           ->innerJoin('p', 'payment', 'pa', 'pa.property_id = p.property_id')
           ->leftJoin('pa', 'fee', 'f', 'pa.fee_id = f.fee_id')
           ->leftJoin('f', 'concept', 'c', 'f.concept_id = c.concept_id')
           ->innerJoin('f', 'type', 't', 't.type_id = f.type_id')
           ->leftJoin('pa', 'surcharge', 'su', 'su.payment_id = pa.payment_id')
           ->where('pa.date BETWEEN :startDate and :stopDate')
           ->andWhere('z.zone_id = :zoneId')
           ->groupBy('cl.client_id');

        $params = array(
            'startDate' => $criteria['start_date'],
            'stopDate' => $criteria['stop_date'],
            'zoneId' => $criteria['zone_id']
        );

        return $this->getConnection()->fetchAll($qb->getSQL(), $params);
    }

    /**
     * @return array
     */
    public function findStateByZones(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qbExpenses = $this->createQueryBuilder();
        $qbExpenses->select('z.zone_id, z.name, SUM(e.amount_paid + e.iva) AS total_expenses')
                   ->from('zone', 'z')
                   ->leftJoin('z', 'zone_expenses', 'e', 'e.zone_id = z.zone_id')
                   ->leftJoin('e', 'concept', 'ce', 'ce.concept_id = e.concept_id')
                   ->where('e.date BETWEEN :startDate AND :stopDate')
                   ->andWhere('z.zone_id = :zoneId')
                   ->groupBy('z.zone_id');

        $qbIncome = $this->createQueryBuilder();
        $qbIncome->select('z.zone_id, z.name, SUM(f.amount) AS total_payment, SUM(p.amount_paid) AS received_payment_total')
                 ->from('zone', 'z')
                 ->leftJoin('z', 'zone_payment', 'p', 'p.zone_id = z.zone_id')
                 ->leftJoin('p', 'fee', 'f', 'f.fee_id = p.fee_id')
                 ->leftJoin('f', 'concept', 'cp', 'cp.concept_id = f.concept_id')
                 ->where('p.date BETWEEN :startDate AND :stopDate')
                 ->andWhere('z.zone_id = :zoneId')
                 ->groupBy('z.zone_id');

        $select = <<<SELECT
            IFNULL(income.zone_id, expenses.zone_id) AS zone_id,
            IFNULL(income.name, expenses.name) AS name,
            expenses.total_expenses,
            income.total_payment,
            income.received_payment_total
SELECT;

        $qb->select($select)
           ->from("({$qbExpenses->getSQL()})", 'expenses')
           ->leftJoin(
               'expenses',
               "({$qbIncome->getSQL()})",
               'income',
               'expenses.zone_id = income.zone_id'
           );

        $params = array(
            'startDate' => $criteria['start_date'],
            'stopDate' => $criteria['stop_date'],
            'zoneId' => $criteria['zone_id']
        );

        $zones = $this->getConnection()->fetchAll($qb->getSql(), $params);

        return $zones;
    }

    /**
     * @param  array $criteria
     * @return array
     */
    public function findStateByClientsMonth(array $criteria)
    {
        $builder = $this->createQueryBuilder();
        $builder
            ->select(
                'z.zone_id',
                'z.name',
                'pa.date',
                'SUM(f.amount) AS generated_fees',
                'SUM(pa.amount_paid) AS generated_payments'
            )
            ->from('payment', 'pa')
            ->leftJoin('pa', 'property', 'p', 'p.property_id = pa.property_id')
            ->leftJoin('p', 'zone', 'z', 'z.zone_id = p.zone_id')
            ->leftJoin('pa', 'fee', 'f', 'f.fee_id = pa.fee_id')
            ->leftJoin('f', 'concept', 'c', 'c.concept_id = f.concept_id')
            ->leftJoin('f', 'type', 't', 't.type_id = f.type_id')
            ->leftJoin('pa', 'surcharge', 'su', 'su.payment_id = pa.payment_id')
            ->where('pa.date BETWEEN :startDate and :stopDate')
            ->andWhere('z.zone_id = :zoneId')
            ->groupBy('MONTH(pa.date)')
            ->setParameters([
                'startDate' => $criteria['start_date'],
                'stopDate' => $criteria['stop_date'],
                'zoneId' => $criteria['zone_id']
            ]);

        return $this->getConnection()->fetchAll($builder->getSQL(), $builder->getParameters());
    }

    /**
     * @param  array $criteria
     * @return array
     */
    public function findStateByZonesMonth(array $criteria)
    {
        $qbExpenses = $this->createQueryBuilder();
        $qbExpenses
            ->select('z.zone_id, z.name, e.date, SUM(e.amount_paid + e.iva) AS total_expenses')
            ->from('zone', 'z')
            ->leftJoin('z', 'zone_expenses', 'e', 'e.zone_id = z.zone_id')
            ->leftJoin('e', 'concept', 'ce', 'ce.concept_id = e.concept_id')
            ->where('e.date BETWEEN :startDate AND :stopDate')
            ->andWhere('z.zone_id = :zoneId')
            ->groupBy('MONTH(e.date)');

        $qbIncome = $this->createQueryBuilder();
        $qbIncome
            ->select(
                'z.zone_id',
                'z.name',
                'p.date',
                'SUM(f.amount) AS total_payment',
                'SUM(p.amount_paid) AS received_payment_total'
            )
            ->from('zone', 'z')
            ->leftJoin('z', 'zone_payment', 'p', 'p.zone_id = z.zone_id')
            ->leftJoin('p', 'fee', 'f', 'f.fee_id = p.fee_id')
            ->leftJoin('f', 'concept', 'cp', 'cp.concept_id = f.concept_id')
            ->where('p.date BETWEEN :startDate AND :stopDate')
            ->andWhere('z.zone_id = :zoneId')
            ->groupBy('MONTH(p.date)');

        $qb = $this->createQueryBuilder();
        $qb
            ->select(
                'IFNULL(income.zone_id, expenses.zone_id) AS zone_id',
                'IFNULL(income.name, expenses.name) AS name',
                'IFNULL(income.date, expenses.date) AS date',
                'expenses.total_expenses',
                'income.total_payment',
                'income.received_payment_total'
            )
            ->from("({$qbExpenses->getSQL()})", 'expenses')
            ->leftJoin('expenses', "({$qbIncome->getSQL()})", 'income', 'expenses.zone_id = income.zone_id')
            ->setParameters([
                'startDate' => $criteria['start_date'],
                'stopDate' => $criteria['stop_date'],
                'zoneId' => $criteria['zone_id']
            ]);

        return $this->getConnection()->fetchAll($qb->getSQL(), $qb->getParameters());
    }
}
