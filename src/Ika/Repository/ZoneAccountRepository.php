<?php
namespace Ika\Repository;

use \Mandragora\Repository\Repository;

class ZoneAccountRepository extends Repository
{
    /** @var string */
    public static $CLASS = __CLASS__;

    /**
     * @param  string $subdivision
     * @param  string $startDate
     * @param  string $stopDate
     * @return array
     */
    public function ofSubdivisionInRange($subdivision, $startDate, $stopDate)
    {
        $builder = $this->createQueryBuilder();

        $builder
            ->select('*')
            ->from('zone', 'z')
            ->innerJoin('z', 'zone_payment', 'zp', 'z.zone_id = zp.zone_id')
            ->innerJoin('zp', 'fee', 'f', 'f.fee_id = zp.fee_id')
            ->innerJoin('f', 'concept', 'c', 'c.concept_id = f.concept_id')
            ->where('zp.zone_id = :zoneId')
            ->andWhere('zp.date BETWEEN :startDate and :stopDate');

        return $this->getConnection()->fetchAll($builder->getSql(), [
            'zoneId' => $subdivision,
            'startDate' => $startDate,
            'stopDate' => $stopDate,
        ]);
    }

    /**
     * @return array
     */
    public function findStateByClient(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('client', 'cl')
           ->innerJoin('cl', 'profile', 'pr', 'cl.profile_id = pr.profile_id')
           ->where('cl.client_id = :clientId');
        $params = array('clientId' => $criteria['client_id']);

        $clients = $this->getConnection()->fetchAll($qb->getSQL(), $params);

        $clients[0]['property'] = $this->findAllPropertiesByClient($criteria);

        return $clients;
    }

    /**
     * @param array $criteria
     */
    public function findAllPropertiesByClient(array $criteria)
    {
        $qbProperty  = $this->createQueryBuilder();
        $qbProperty->select('*')
                   ->from('property', 'p')
                   ->innerJoin('p', 'payment', 'pa', 'pa.property_id = p.property_id')
                   ->leftJoin('pa', 'fee', 'f', 'f.fee_id = pa.fee_id')
                   ->leftJoin('f', 'concept', 'c', 'c.concept_id = f.concept_id')
                   ->innerJoin('f', 'type', 't', 't.type_id = f.type_id')
                   ->leftJoin('pa', 'surcharge', 'su', 'su.payment_id = pa.payment_id')
                   ->where('p.client_id = :client_id')
                   ->andWhere('pa.date BETWEEN :startDate and :stopDate');

        $params = array(
            'client_id' => $criteria['client_id'],
            'startDate' => $criteria['start_date'],
            'stopDate' => $criteria['stop_date']
        );

        return $this->getConnection()->fetchAll($qbProperty->getSQL(), $params);

    }

    /**
     * @return array
     */
    public function findStateByZone(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('user', 'u')
           ->innerJoin('u', 'profile', 'pr', 'u.profile_id = pr.profile_id')
           ->leftJoin('u', 'zone', 'z', 'z.username = u.username')
           ->where('u.username = :username');
        $params = array('username' => $criteria['username']);

        $clients = $this->getConnection()->fetchAll($qb->getSql(), $params);

        $clients[0]['property'] = $this->findAllPropertiesByZone($clients, $criteria);

        return $clients;
    }

    /**
     * @param array $client
     * @param array $criteria
     */
    public function findAllPropertiesByZone(array $client, array $criteria)
    {
        $qbProperty  = $this->createQueryBuilder();
        $qbProperty->select('*')
                   ->from('property', 'p')
                   ->innerJoin('p', 'payment', 'pa', 'pa.property_id = p.property_id')
                   ->leftJoin('pa', 'fee', 'f', 'f.fee_id = pa.fee_id')
                   ->leftJoin('f', 'concept', 'c', 'c.concept_id = f.concept_id')
                   ->leftJoin('pa', 'surcharge', 'su', 'su.payment_id = pa.payment_id')
                   ->where('p.zone_id = :zone_id')
                   ->andWhere('pa.date BETWEEN :startDate and :stopDate');

        $params = array(
            'zone_id' => $client[0]['zone_id'],
            'startDate' => $criteria['start_date'],
            'stopDate' => $criteria['stop_date']
        );

        return  $this->getConnection()->fetchAll($qbProperty->getSQL(), $params);
    }
}
