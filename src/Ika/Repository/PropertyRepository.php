<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class PropertyRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('property', 'p')
           ->innerJoin('p', 'client', 'cl', 'p.client_id = cl.client_id')
           ->innerJoin('cl', 'profile', 'pr', 'cl.profile_id = pr.profile_id')
           ->where('p.zone_Id = :zoneId')
           ->orderBy('p.street', 'ASC');

        $params = ['zoneId' => $criteria['zoneId']];
        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @array $criteria
     * @return array
     */
    public function findAllByUsername(array $criteria )
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('property', 'p')
           ->innerJoin('p', 'client', 'cl', 'p.client_id = cl.client_id')
           ->innerJoin('cl', 'profile', 'pr', 'cl.profile_id = pr.profile_id')
           ->leftJoin('pr', 'user', 'u', 'pr.profile_id = u.profile_id')
           ->where('u.username = :username');

        $params = ['username' => $criteria['username']];
        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @param  array $criteria
     * @return array
     */
    public function findAllByIdentity(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('property', 'p')
           ->innerJoin('p', 'zone', 'z', 'p.zone_id = z.zone_id')
           ->innerJoin('z', 'user', 'u', 'z.username = u.username')
           ->where('u.username = :username');

        $params = array('username' => $criteria['username']);
        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @para int $zoneId
     * @return array
     */
    public function findAllByZone($zoneId)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('property', 'p')
           ->where('p.zone_id = :zoneId');

        $params = array('zoneId' => $zoneId);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @array $criteria
     * @return array
     */
    public function findAllPropertiesByZone(array $criteria )
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('property', 'p')
           ->where('p.zone_id = :zoneId');

        $params = ['zoneId' => $criteria['zoneId']];

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @param $clientId
     * @return array
     */
    public function toClient($clientId)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('property', 'p')
            ->where('p.client_id = :clientId');

        $params = array('clientId' => $clientId);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    public function myProperties($profileId)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('property', 'p')
           ->innerJoin('p', 'client', 'c', 'c.client_id = p.client_id')
           ->innerJoin('c', 'profile', 'pr', 'pr.profile_id = c.profile_id')
           ->where('pr.profile_id = :profileId');

        $params = array('profileId' => $profileId);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @return array
     */
    public function findAllWithoutFilter()
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('property', 'p');

        return $this->getConnection()->fetchAll($qb->getSql());
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
            ->from('property', 'p')
            ->where('property_id = :propertyId');
        $params = array('propertyId' => $criteria['propertyId']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param $propertyId
     * @return array
     */
    public function retrieveClientOfProperty($propertyId)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('client_id')
           ->from('property', 'p')
           ->where('property_id = :propertyId');
        $params = array('propertyId' => $propertyId);

        return $this->getConnection()->fetchScalar($qb->getSQL(), $params);
    }

    /**
     * @param array $criteria {
     *     @type integer $propertyId
     * }
     * @return array
     */
    public function findOpeningBalance(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $select = <<<SELECT
        p.property_id,
        CASE
            WHEN p.opening_balance <  0 THEN p.opening_balance + COALESCE(SUM(pa.amount_paid), 0)
            ELSE p.opening_balance - COALESCE(SUM(pa.amount_paid), 0)
        END AS opening_balance
SELECT;
        $qb
            ->select($select)
            ->from('property', 'p')
            ->leftJoin('p', 'payment', 'pa', 'p.property_id = pa.property_id AND pa.is_opening_balance = 1')
            ->where('p.property_id = :propertyId')
            ->groupBy('p.property_id');

        return $this->executeQuery($qb->getSql(), ['propertyId' => $criteria['propertyId']])->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        return $this->getConnection()->insert('property', $values);
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('property_id' => $values['property_id']);

        return $this->getConnection()->update('property', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('property', array('property_id' => $criteria['propertyId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')
           ->from('property', 'p')
           ->where('p.zone_Id = :zoneId');

        $params = array('zoneId' => $criteria['zoneId']);;

        return $this->getConnection()->fetchScalar($qb->getSql(), $params);
    }

    /**
     * @see Countable::count()
     */
    public function countByUsername($username)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('property', 'p')
           ->innerJoin('p', 'zone', 'z', 'p.zone_id = z.zone_id')
           ->innerJoin('z', 'user_zone', 'uz', 'uz.zone_id = z.zone_id')
           ->where('uz.username = :username');

        $params = array('username' => $username);

        return $this->getConnection()->fetchScalar($qb->getSql(), $params);
    }

    /**
     * @see Countable::count()
     */
    public function countByZone($username)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('property', 'p')
        ->innerJoin('p', 'client', 'cl', 'p.client_id = cl.client_id')
        ->innerJoin('cl', 'profile', 'pr', 'cl.profile_id = pr.profile_id')
        ->leftJoin('pr', 'user', 'u', 'pr.profile_id = u.profile_id')
        ->where('u.username = :username');

        $params = array('username' => $username);

        return $this->getConnection()->fetchScalar($qb->getSql(), $params);
    }
}
