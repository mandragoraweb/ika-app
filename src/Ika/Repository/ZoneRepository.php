<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class ZoneRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('zone', 'z');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql(), $qb->getParameters());
    }

    /**
     * Filter the subdivisions to which a role has access to.
     *
     * A webmaster has access to all the subdivisions, administrators have access only to the
     * subdivisions assigned to them. Subdivision's committee members only have access to the
     * subvdivision associated to their committee.
     *
     * @param  array $identity
     * @return array
     */
    public function supervisedBy(array $identity)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->select('*')
            ->from('zone', 'z')
            ->leftJoin('z', 'user_zone', 'uz', 'z.zone_id = uz.zone_id')
            ->leftJoin('uz', 'user', 'u', 'uz.username = u.username');

        if ('webmaster' !== $identity['role']) {
            $qb
                ->where('u.role = :role')
                ->andWhere('u.username = :username')
                ->setParameter('role', $identity['role'])
                ->setParameter('username', $identity['username']);
        }

        return $this->connection->fetchAll($qb->getSQL(), $qb->getParameters());
    }

    /**
     * @return array
     */
    public function findAllWithoutFilter(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('zone', 'z');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('filterByRole', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql(), $qb->getParameters());
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
           ->from('zone', 'z')
           ->leftJoin('z', 'user_zone', 'uz', 'z.zone_id = uz.zone_id')
           ->leftJoin('uz', 'user', 'u', 'uz.username = u.username')
           ->where('z.zone_id = :zoneId')
           ->andWhere('u.role = "admin"');

        $params = array('zoneId' => $criteria['zoneId']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        return $this->getConnection()->insert('zone', $values);
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
     */
    public function insertUserZone(array $values)
    {
        return $this->getConnection()->insert('user_zone', $values);
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
     */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('zone_id' => $values['zone_id']);

        return $this->getConnection()->update('zone', $values, $id);
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
     */
    public function updateUserZone(array $values, array $id)
    {
        return $this->getConnection()->update('user_zone', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('zone', array('zone_id' => $criteria['zoneId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('zone', 'z');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchScalar($qb->getSql(), $qb->getParameters());
    }
}
