<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class TypeRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria = [])
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')->from('type', 't');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql());
    }

    /**
     * @param  array $id
     * @return array
     */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
           ->from('type', 't')
           ->where('type_id = :typeId')
           ->setParameters(array('typeId' => $criteria['typeId']));

        return $this->executeQuery($qb->getSQL(), $qb->getParameters())->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        return $this->getConnection()->insert('type', $values);
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('type_id' => $values['type_id']);

        return $this->getConnection()->update('type', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('type', array('type_id' => $criteria['typeId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('type', 't');

        return $this->getConnection()->fetchScalar($qb->getSql());
    }
}
