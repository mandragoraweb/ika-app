<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Repository;

use Ika\CondominiumManagement\AnnualExpensesStatement;
use Ika\CondominiumManagement\AnnualIncomeStatement;
use Mandragora\Repository\Repository;

class AnnualStatementRepository extends Repository
{
    /** @var string */
    public static $CLASS = __CLASS__;

    /**
     * @param  integer               $year
     * @param  integer               $subdivision
     * @return AnnualIncomeStatement
     */
    public function incomeStatement($year, $subdivision)
    {
        $builder = $this->createQueryBuilder();
        $builder
            ->select(<<<SELECT
                c.client_id,
                z.name,
                pro.first_name,
                pro.paternal_surname,
                pro.maternal_surname,
                SUM(CASE WHEN MONTH(p.date) = 1 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS jan,
                SUM(CASE WHEN MONTH(p.date) = 2 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS feb,
                SUM(CASE WHEN MONTH(p.date) = 3 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS mar,
                SUM(CASE WHEN MONTH(p.date) = 4 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS apr,
                SUM(CASE WHEN MONTH(p.date) = 5 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS may,
                SUM(CASE WHEN MONTH(p.date) = 6 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS jun,
                SUM(CASE WHEN MONTH(p.date) = 7 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS jul,
                SUM(CASE WHEN MONTH(p.date) = 8 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS aug,
                SUM(CASE WHEN MONTH(p.date) = 9 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS sep,
                SUM(CASE WHEN MONTH(p.date) = 10 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS oct,
                SUM(CASE WHEN MONTH(p.date) = 11 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS nov,
                SUM(CASE WHEN MONTH(p.date) = 12 THEN COALESCE(p.amount_paid, 0) ELSE 0 END) AS `dec`
SELECT
            )
            ->from('payment', 'p')
            ->innerJoin('p', 'client', 'c', 'p.client_id = c.client_id')
            ->innerJoin('c', 'profile', 'pro', 'c.profile_id = pro.profile_id')
            ->innerJoin('p', 'property', 'pr', 'p.property_id = pr.property_id')
            ->innerJoin('pr', 'zone', 'z', 'pr.zone_id = z.zone_id')
            ->where('pr.zone_id = :zoneId')
            ->andWhere('YEAR(p.date) = :year')
            ->groupBy('client_id')
            ->setParameters(['zoneId' => $subdivision, 'year' => $year]);

        $rows = $this->connection->fetchAll($builder->getSQL(), $builder->getParameters());

        return new AnnualIncomeStatement($rows);
    }

    public function expensesStatement($year, $subdivision)
    {
        $builder = $this->createQueryBuilder();
        $builder
            ->select(<<<SELECT
                z.name,
                c.concept_id,
                c.concept,
                c.code,
                SUM(CASE WHEN MONTH(ze.date) = 1 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS jan,
                SUM(CASE WHEN MONTH(ze.date) = 2 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS feb,
                SUM(CASE WHEN MONTH(ze.date) = 3 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS mar,
                SUM(CASE WHEN MONTH(ze.date) = 4 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS apr,
                SUM(CASE WHEN MONTH(ze.date) = 5 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS may,
                SUM(CASE WHEN MONTH(ze.date) = 6 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS jun,
                SUM(CASE WHEN MONTH(ze.date) = 7 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS jul,
                SUM(CASE WHEN MONTH(ze.date) = 8 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS aug,
                SUM(CASE WHEN MONTH(ze.date) = 9 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS sep,
                SUM(CASE WHEN MONTH(ze.date) = 10 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS oct,
                SUM(CASE WHEN MONTH(ze.date) = 11 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS nov,
                SUM(CASE WHEN MONTH(ze.date) = 12 THEN COALESCE(ze.amount_paid, 0) ELSE 0 END) AS `dec`
SELECT
            )
            ->from('zone_expenses', 'ze')
            ->innerJoin('ze', 'concept', 'c', 'ze.concept_id = c.concept_id')
            ->innerJoin('ze', 'zone', 'z', 'ze.zone_id = z.zone_id')
            ->where('ze.zone_id = :zoneId')
            ->andWhere('YEAR(ze.date) = :year')
            ->groupBy('concept_id')
            ->setParameters(['zoneId' => $subdivision, 'year' => $year]);

        $rows = $this->connection->fetchAll($builder->getSQL(), $builder->getParameters());

        return new AnnualExpensesStatement($rows);
    }
}
