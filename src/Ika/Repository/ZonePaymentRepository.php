<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class ZonePaymentRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('zone_payment', 'zp')
           ->innerJoin('zp', 'fee', 'f', 'zp.fee_id = f.fee_id')
           ->innerJoin('f', 'concept', 'c', 'f.concept_id = c.concept_id')
           ->where('zp.zone_id = :zoneId');

        $params = ['zoneId' => $criteria['zoneId']];

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @return array
     */
    public function findAllWithoutFilter()
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('zone_payment', 'zp');

        return $this->getConnection()->fetchAll($qb->getSql());
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
            ->from('zone_payment', 'zp')
            ->where('zone_payment_id = :zonePaymentId');
        $params = array('zonePaymentId' => $criteria['zonePaymentId']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $this->getConnection()->insert('zone_payment', $values);

        return $this->getConnection()->lastInsertId();
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('zone_payment_id' => $values['zone_payment_id']);

        return $this->getConnection()->update('zone_payment', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('zone_payment', array('zone_payment_id' => $criteria['zonePaymentId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('zone_payment', 'zp');

        return $this->getConnection()->fetchScalar($qb->getSql());
    }
}
