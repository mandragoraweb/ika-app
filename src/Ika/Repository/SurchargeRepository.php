<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class SurchargeRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('surcharge', 'sc')
           ->where('payment_id = :paymentId');

        $params = ['paymentId' => $criteria['paymentId']];
        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
            ->from('surcharge', 'sc')
            ->innerJoin('sc', 'payment', 'p', 'sc.payment_id = p.payment_id')
            ->innerJoin('p', 'fee', 'f', 'p.fee_id = f.fee_id')
            ->where('sc.surcharge_id = :surchargeId');
        $params = array('surchargeId' => $criteria['surchargeId']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        return $this->getConnection()->insert('surcharge', $values);
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('surcharge_id' => $values['surcharge_id']);

        return $this->getConnection()->update('surcharge', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('surcharge', array('surcharge_id' => $criteria['surchargeId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')
           ->from('surcharge', 'sc')
           ->where('payment_id = :paymentId');

        $params = array('paymentId' => $criteria['paymentId']);;

        return $this->getConnection()->fetchScalar($qb->getSql(), $params);
    }
}
