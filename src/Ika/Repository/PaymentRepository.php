<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class PaymentRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
           ->from('payment', 'p')
           ->innerJoin('p', 'fee', 'f', 'p.fee_id = f.fee_id')
           ->innerJoin('f', 'concept', 'c', 'f.concept_id = c.concept_id')
           ->where('p.property_id = :propertyId');

        $params = ['propertyId' => $criteria['propertyId']];
        /*$eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);*/

        return $this->getConnection()->fetchAll($qb->getSql(), $params);
    }

    /**
     * @return array
     */
    public function findAllWithoutFilter()
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('payment', 'pa');

        return $this->getConnection()->fetchAll($qb->getSql());
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('p.*, pr.zone_id')
            ->from('payment', 'p')
            ->where('payment_id = :paymentId')
            ->innerJoin('p', 'property', 'pr', 'p.property_id = pr.property_id');

        $params = array('paymentId' => $criteria['paymentId']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $this->getConnection()->insert('payment', $values);

        return $this->getConnection()->lastInsertId();
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('payment_id' => $values['payment_id']);

        return $this->getConnection()->update('payment', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('payment', array('payment_id' => $criteria['paymentId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('payment', 'pa');

        return $this->getConnection()->fetchScalar($qb->getSql());
    }

    public function retrieveFolio($folio, $zone)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
            ->from('payment', 'p')
            ->innerJoin('p', 'property', 'pr', 'p.property_id = pr.property_id')
            ->innerJoin('pr', 'zone', 'z', 'pr.zone_id = z.zone_id')
            ->where('p.folio_receipt = :folio')
            ->andWhere('z.zone_id = :zoneId');
        $params = array('folio' => $folio, 'zoneId' => $zone['zone_id']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }
}
