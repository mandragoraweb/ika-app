<?php
namespace Ika\Repository;

use \Mandragora\Repository\CrudRepository;

class ProfileRepository extends CrudRepository
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @array $criteria
     * @return array
     */
    public function findAll(array $criteria)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')->from('profile', 'po');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchAll($qb->getSQL(), $qb->getParameters());
    }

    /**
     * @param  array $id
     * @return array
    */
    public function find(array $criteria)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
           ->from('profile', 'po')
           ->innerJoin('po', 'user', 'u', 'po.profile_id = u.profile_id')
           ->where('po.profile_id = :profileId');
        $params = array('profileId' => $criteria['profileId']);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }

    /**
     * @param  array $values
     * @return int   The number of affected rows
    */
    public function insert(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $this->getConnection()->insert('profile', $values);

        return $this->getConnection()->lastInsertId();
    }

    /**
     * @param  array $values
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function update(array $values)
    {
        unset($values['send']);
        unset($values['csrf']);

        $id = array('profile_id' => $values['profile_id']);

        return $this->getConnection()->update('profile', $values, $id);
    }

    /**
     * @param  array $id
     * @return int   The number of affected rows
    */
    public function delete(array $criteria)
    {
        $this->getConnection()->delete('profile', array('profile_id' => $criteria['profileId']));
    }

    /**
     * @see Countable::count()
    */
    public function count(array $criteria = array())
    {
        $qb = $this->createQueryBuilder();

        $qb->select('COUNT(*)')->from('profile', 'po');

        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $this->getEventManager()->trigger('onFindAll', $this, $eventParams);

        return $this->getConnection()->fetchScalar($qb->getSql(), $qb->getParameters());
    }

    /**
     * @param  array $id
     * @return array
     */
    public function findAllWithUserAndProfileInfo($profileId)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('*')
           ->from('profile', 'po')
           ->innerJoin('po', 'user', 'u', 'po.profile_id = u.profile_id')
           ->where('po.profile_id = :profileId');
        $params = array('profileId' => $profileId);

        return $this->executeQuery($qb->getSql(), $params)->fetch();
    }
}
