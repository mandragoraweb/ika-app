<?php
namespace Ika\Controller;

use Ika\Repository\PropertyRepository;
use Mandragora\Controller\HttpController;

class CustomerPropertiesController extends HttpController
{
    /** @type PropertyRepository  */
    protected $allProperties;

    /**
     * @param PropertyRepository $allProperties
     */
    public function setPropertiesRepository(PropertyRepository $allProperties)
    {
        $this->allProperties = $allProperties;
    }

    /**
     * Return all properties to client
     */
    public function showCustomerProperties()
    {
        $clientId = $this->getParam('clientId');
        $this->assign(['properties' => $this->allProperties->toClient($clientId)]);
        $this->renderScript('property/customer-properties.html.twig');
    }
}
