<?php
namespace Ika\Controller;

use \Mandragora\Controller\CrudController;

class ProfileController extends CrudController
{
    /**
     * Save an entity to the data source, if validation fails show the form with the
     * corresponding error messages
     */
    public function saveAction()
    {
        if ($this->getService()->isValid()) {

            $profileId = $this->getService()->save();
            $this->getService()->sendMessage($profileId);

            $this->redirect($this->getService()->getRoute('list'));
        }

        $this->createAction();
        $this->renderScript($this->getService()->getScript('create'));
    }

    /**
     * Retrieve form for change password
     */
    public function formPasswordAction()
    {
        $this->assign($this->getService()->getFormChangePassword($this->getUrlHelper()));
    }

    /**
     * send params to change password
     */
    public function changePasswordAction()
    {
        if ($this->getService()->isValidChangeForm()) {
            if (is_array($this->getService()->changePassword())) {
                $this->assign($this->getService()->changePassword());
                $this->renderScript($this->getService()->getScript('change_password'));
            } else {
                $this->redirect($this->getService()->getRoute('list'));
            }
        }
        $this->formPasswordAction();
        $this->renderScript($this->getService()->getScript('change_password'));
    }
}
