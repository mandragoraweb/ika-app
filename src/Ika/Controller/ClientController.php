<?php
namespace Ika\Controller;

use \Mandragora\Controller\CrudController;

class ClientController extends CrudController
{
    /**
     * After saving a client redirect to create property
     */
    public function saveAction()
    {
        if ($this->getService()->isCreationValid()) {

            $clientId = $this->getService()->save();
            $this->getService()->getPropertyService()->setParam('clientId', $clientId);

            $values = $this->getService()->getParams();

            if ((integer) $values['existing_property'] == 1) {
                $this->redirect($this->getService()->getRoute('list'));

                return;
            }

            $this->createProperty();

            return;
        }

        $this->createAction();
        $this->renderScript($this->getService()->getScript('create'));
    }

    /**
     * before saving client, create property
     */
    public function createProperty()
    {
        $propertyService = $this->getService()->getPropertyService();
        $this->assign($propertyService->getFormForClientProperty($this->getUrlHelper()));
        $this->renderScript($this->getService()->getScript('create_property'));
    }

    /**
     * Retrieve form for change password
     */
    public function formPasswordAction()
    {
        $this->assign($this->getService()->initChangePasswordForm($this->getUrlHelper()));
    }

    /**
     * send params to change password
     */
    public function changePasswordAction()
    {
        $client = $this->getService();
        if ($client->isChangePasswordFormValid() && $client->clientExist()) {
            $client->changePassword();
            $this->redirect($client->getRoute('list'));

            return;
        }
        $this->formPasswordAction();
        $this->renderScript($client->getScript('change_password'));
    }
}
