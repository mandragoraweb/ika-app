<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Controller;

use Ika\CondominiumManagement\ViewHomeowners;
use Mandragora\Controller\HttpController;

class ViewHomeownersController extends HttpController
{
    /** @var ViewHomeowners */
    protected $useCase;

    /**
     * @param ViewHomeowners $useCase
     */
    public function setUseCase(ViewHomeowners $useCase)
    {
        $this->useCase = $useCase;
    }

    /**
     * Show the homeowners filtering by role and subdivision
     */
    public function showHomeowners()
    {
        $this->assign(['paginator' => $this->useCase->viewHomeowners(['page' => $this->getParam('page', 1)])]);
        $this->renderScript('client/list.html.twig');
    }
}
