<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Controller;

use Zend\Authentication\AuthenticationService;
use Mandragora\Controller\HttpController;
use Ika\Form\AccountForm;
use Ika\Form\AccountFormHydrator;
use Ika\Service\AccountService;

class AccountBalanceController extends HttpController
{
    /** @type AccountForm */
    protected $accountBalanceForm;

    /** @type AccountFormHydrator */
    protected $formHydrator;

    /** @type AuthenticationService */
    protected $authenticationService;

    /** @type AccountService  */
    protected $accountBalanceService;

    /**
     * @param AccountService $service
     */
    public function setAccountBalanceService(AccountService $service)
    {
        $this->accountBalanceService = $service;
    }

    /**
     * @param AccountForm $form
     */
    public function setAccountBalanceForm(AccountForm $form)
    {
        $this->accountBalanceForm = $form;
    }

    /**
     * @param AccountFormHydrator $hydrator
     */
    public function setFormHydrator(AccountFormHydrator $hydrator)
    {
        $this->formHydrator = $hydrator;
    }

    /**
     * @param AuthenticationService $auth
     */
    public function setAuthenticationService(AuthenticationService $auth)
    {
        $this->authenticationService = $auth;
    }

    /**
     * Show the account balance form
     */
    public function showBalanceForm()
    {
        $this->initAccountBalanceForm();

        $this->assign(['form' => $this->accountBalanceForm]);
        $this->renderScript('account/balance-form.html.twig');
    }

    /**
     * It shows the account balance of a given client in a specific period.
     *
     * The account balance contains all the payments related to all client's properties.
     * It summarizes the payments in order to know wether the client owes money or not.
     */
    public function showAccountBalance()
    {
        $this->accountBalanceForm->setData($this->getParams());
        $this->initAccountBalanceForm();

        if ($this->accountBalanceForm->isValid()) {
            $identity = $this->authenticationService->getIdentity();
            $criteria = $this->accountBalanceForm->getData();
            $this->assign($this->accountBalanceService->retrieveAccountBalance($identity, $criteria));
            $this->renderScript('account/show-balance.html.twig');

            return;
        }

        $this->showBalanceForm();
    }

    /**
     * Setup form elements according to the current user's role
     */
    protected function initAccountBalanceForm()
    {
        $identity = $this->authenticationService->getIdentity();
        $this->formHydrator->setCurrentUserIdentity($identity);
        $this->accountBalanceForm->hydrate($this->formHydrator);
        $this->accountBalanceForm->setAttribute(
            'action', $this->getUrlHelper()->url('account_show-balance', $this->getParams())
        );
    }
}
