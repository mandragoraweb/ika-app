<?php
namespace Ika\Controller;

use \Mandragora\Controller\CrudController;

class PaymentController extends CrudController
{
    /**
     * Save an entity to the data source, if validation fails show the form with the
     * corresponding error messages
     */
    public function saveAction()
    {
        if ($this->getService()->isValid()) {

            $paymentId = $this->getService()->save();

            $this->setParam('paymentId', $paymentId);
            $this->redirect($this->getService()->getRoute('create'));
        }

        $this->createAction();
        $this->renderScript($this->getService()->getScript('create'));
    }

    /**
     * Update the information of a given resource
     *
     * @return void
     */
    public function updateAction()
    {
        if ($this->retrieveOne() && $this->getService()->isEditingValid()) {

            $this->getService()->update();
            $this->redirect($this->getService()->getRoute('list'));
        }

        $this->editAction();
        $this->renderScript($this->getService()->getScript('edit'));
    }

    /**
     * Retrieve the form for creating params with date ranges
     */
    public function customizeAction()
    {
        $this->assign($this->getService()->getPersonalizeForm($this->getUrlHelper()));
    }

    /**
     * Retrieve a paginated list of entities
     */
    public function listAction()
    {
        $this->assign($this->getService()->getPersonalizeForm($this->getUrlHelper()));
    }

    public function propertyByZoneAction()
    {
        $this->assign($this->getService()->retrievePropertyByZone());
    }

    public function paymentsByPropertyAction()
    {
        $this->assign($this->getService()->fetchAll());
    }

    /**
     * Retrieve opening balance
     */
    public function openingBalanceByPropertyAction()
    {
        $this->setContentType('application/json');
        $this->assign($this->getService()->retrieveOpeningBalanceByProperty());
    }
}
