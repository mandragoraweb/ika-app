<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Controller;

use Ika\Form\ZonePaymentForm;
use Mandragora\Controller\HttpController;

class NewSubdivisionPaymentController extends HttpController
{
    /** @var  ZonePaymentForm */
    protected $form;

    /**
     * @param ZonePaymentForm $form
     */
    public function setZonePaymentForm(ZonePaymentForm $form)
    {
        $this->form = $form;
    }

    public function showPaymentZoneForm()
    {
        $this->form->setAttribute('action', $this->urlHelper->url('zone-payment_save'));
        $this->assign(['form' => $this->form]);
        $this->renderScript('zone-payment/create.html.twig');
    }
}
