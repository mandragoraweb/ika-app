<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Controller;

use Ika\CondominiumManagement\CalculateSubdivisionAccountBalance;
use Ika\Form\ZoneAccountForm;
use Mandragora\Controller\HttpController;

class ZoneAccountController extends HttpController
{
    /** @var ZoneAccountForm */
    protected $form;

    /** @var CalculateSubdivisionAccountBalance */
    protected $useCase;

    /**
     * @param ZoneAccountForm $form
     */
    public function setZoneAccountForm(ZoneAccountForm $form)
    {
        $this->form = $form;
    }

    /**
     * @param CalculateSubdivisionAccountBalance $useCase
     */
    public function setUseCase(CalculateSubdivisionAccountBalance $useCase)
    {
        $this->useCase = $useCase;
    }

    /**
     * Show the form to select the date range to calculate the account balance
     */
    public function showAccountBalanceForm()
    {
        $this->form->setAttribute('action', $this->urlHelper->url('zone-account_show-balance'));
        $this->assign(['form' => $this->form]);
        $this->renderScript('zone-account/customize.html.twig');
    }

    /**
     * Calculate the account balance for a given subdivision in a specific date range
     */
    public function showBalanceAction()
    {
        $this->form->setData($this->getParams());

        if ($this->form->isValid()) {

            $criteria = $this->form->getData();
            $this->assign([
                'items' => $this->useCase->calculateAccountBalance(
                    $criteria['zone_id'], $criteria['start_date'], $criteria['stop_date']
                ),
            ]);
            $this->renderScript('zone-account/show-balance.html.twig');

            return;
        }

        $this->showAccountBalanceForm();
    }
}
