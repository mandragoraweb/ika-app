<?php
namespace Ika\Controller;

use \Mandragora\Controller\CrudController;

class ReportController extends CrudController
{
    /**
     * Retrieve the form for creating params with date ranges
     */
    public function customizeClientAction()
    {
        $this->assign($this->getService()->getCustomizeClientForm($this->getUrlHelper()));
    }

    /**
     * Retrieve the form for creating params with date ranges
     */
    public function customizeZoneAction()
    {
        $this->assign($this->getService()->getCustomizeZoneForm($this->getUrlHelper()));
    }

    /**
     * Retrieve the form for creating params with date ranges
     */
    public function customizeClientGeneralAction()
    {
        $this->assign($this->getService()->getCustomizeClientGeneralForm($this->getUrlHelper()));
    }

    /**
     * Retrieve the form for creating params with date ranges
     */
    public function customizeZoneGeneralAction()
    {
        $this->assign($this->getService()->getCustomizeZoneGeneralForm($this->getUrlHelper()));
    }

    /**
     * retrieve all dates to clients balance
     */
    public function showClientBalanceAction()
    {
        if ($this->getService()->isValid()) {
            $this->assign($this->getService()->retrieveClientsBalance());

            return;
        }

        $this->customizeClientAction();
        $this->renderScript($this->getService()->getScript('clientCostumize'));
    }

    /**
     * retrieve all dates to clients balance
     */
    public function showZoneBalanceAction()
    {
        if ($this->getService()->isValid()) {
            $this->assign($this->getService()->retrieveZonesBalance());

            return;
        }

        $this->customizeZoneAction();
        $this->renderScript($this->getService()->getScript('zoneCostumize'));
    }

    /**
     * retrieve all dates to clients balance
     */
    public function showClientBalanceMonthAction()
    {
        if ($this->getService()->isValid()) {
            $this->assign($this->getService()->retrieveClientsBalanceMonth());

            return;
        }

        $this->customizeClientGeneralAction();
        $this->renderScript($this->getService()->getScript('clientCostumizeMonth'));
    }

    /**
     * retrieve all dates to clients balance
     */
    public function showZoneBalanceMonthAction()
    {
        if ($this->getService()->isValid()) {
            $this->assign($this->getService()->retrieveZonesBalanceMonth());

            return;
        }

        $this->customizeZoneGeneralAction();
        $this->renderScript($this->getService()->getScript('zoneCostumizeMonth'));
    }

    /**
     * generate excel file for download
     */
    public function generateReportAction()
    {
        $filename = $this->getService()->generateFileExcel();
        $this->getResponse()->headers->set("Content-type", "application/vnd.ms-excel");
        $this->getResponse()->headers->set("Content-Disposition", "attachment; filename=\"$filename\"");
        $this->getResponse()->headers->set("Expires", "0");
        $this->getResponse()->headers->set("Pragma", "public");

        $this->getResponse()->sendHeaders();

        $this->getResponse()->setContent(readfile($this->getService()->getFilepath(). $filename));

        $this->getResponse()->send();

        die();
    }

    /**
     * generate excel file for download
     */
    public function reportMonthAction()
    {
        $filename = $this->getService()->generateFileExcelMonth();
        $this->getResponse()->headers->set("Content-type", "application/vnd.ms-excel");
        $this->getResponse()->headers->set("Content-Disposition", "attachment; filename=\"$filename\"");
        $this->getResponse()->headers->set("Expires", "0");
        $this->getResponse()->headers->set("Pragma", "public");

        $this->getResponse()->sendHeaders();

        $this->getResponse()->setContent(readfile($this->getService()->getFilepath(). $filename));

        $this->getResponse()->send();

        die();
    }
}
