<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Controller;

use Ika\Form\AnnualStatementForm;
use Ika\Repository\AnnualStatementRepository;
use Mandragora\Controller\HttpController;

class AnnualIncomeStatementController extends HttpController
{
    /** @var AnnualStatementForm */
    protected $form;

    /** @var AnnualStatementRepository */
    protected $statements;

    /**
     * @param AnnualStatementForm $form
     */
    public function setAnnualStatementForm(AnnualStatementForm $form)
    {
        $this->form = $form;
    }

    /**
     * @param AnnualStatementRepository $repository
     */
    public function setAnnualStatementRepository(AnnualStatementRepository $repository)
    {
        $this->statements = $repository;
    }

    /**
     * Show the form to choose both the subdivision and year to calculate the income statement
     */
    public function viewIncomeStatementForm()
    {
        $this->form->setAttribute('action', $this->urlHelper->url('reports.annual_income_statement'));

        $this->assign(['form' => $this->form]);
        $this->renderScript('report/view-annual-balance-form.html.twig');
    }

    /**
     * Show the annual income statement
     */
    public function viewAnnualIncomeStatement()
    {
        $this->form->setData($this->getParams());

        if ($this->form->isValid()) {
            $criteria = $this->form->getData();
            $this->assign(['statement' => $this->statements->incomeStatement($criteria['year'], $criteria['zone_id'])]);
            $this->renderScript('report/view-annual-income-balance.html.twig');

            return;
        }
        $this->viewIncomeStatementForm();
    }
}
