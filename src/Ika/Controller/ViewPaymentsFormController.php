<?php

namespace Ika\Controller;

use Ika\Form\ZonePaymentPersonalizeForm;
use Mandragora\Controller\HttpController;

class ViewPaymentsFormController extends HttpController
{
    /** @var  ZonePaymentPersonalizeForm */
    protected $form;

    /**
     * @param ZonePaymentPersonalizeForm $form
     */
    public function setZonePaymentForm(ZonePaymentPersonalizeForm $form)
    {
        $this->form = $form;
    }

    public function showPaymentZoneForm()
    {
        $this->assign(['form' => $this->form]);
        $this->renderScript('zone-payment/customize.html.twig');
    }
}
