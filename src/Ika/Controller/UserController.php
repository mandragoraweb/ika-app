<?php
namespace Ika\Controller;

use \Mandragora\Controller\CrudController;

class UserController extends CrudController
{
    /**
     * If the user's credentials are correct redirect to the dashboard, otherwise generate
     * the corresponding error messages
     */
    public function authenticateAction()
    {
        if (!$this->getService()->isLoginValid()) {
            $this->assign(array('form' => $this->getService()->getLoginForm()));
            $this->renderScript($this->getService()->getScript('home'));

            return;
        }

        $result = $this->getService()->authenticate();

        if (!$result->isValid()) {
            $this->assign(array(
                'form' => $this->getService()->getLoginForm($result->getMessages())
            ));
            $this->renderScript($this->getService()->getScript('home'));

            return;
        }

        $user = $this->getService()->getAuthService()->getIdentity();
        $this->redirect($this->getService()->getRoute($user['role']));
    }

    /**
     * Ends the current user session
     */
    public function logoutAction()
    {
        $this->getService()->logout();
        $this->redirect($this->getService()->getRoute('home'));
    }

    /**
     * Retrieve all profiles
     */
    public function listProfileAction()
    {
        $this->assign($this->getService()->retrieveAllByProfile());
    }

    /**
     * Retrieve form for change password
     */
    public function formPasswordAction()
    {
        $this->assign($this->getService()->getFormChangePassword($this->getUrlHelper()));
    }

    /**
     * send params to change password
     */
    public function changePasswordAction()
    {
        if ($this->getService()->isValidChangeForm()) {
            if (is_array($this->getService()->changePassword())) {
                $this->assign($this->getService()->changePassword());
                $this->renderScript($this->getService()->getScript('change_password'));
            } else {
                $this->redirect($this->getService()->getRoute('list'));
            }
        }
        $this->formPasswordAction();
        $this->renderScript($this->getService()->getScript('change_password'));
    }
}
