<?php
namespace Ika\Controller;

use \Mandragora\Controller\CrudController;

class ZoneExpensesController extends CrudController
{
    /**
     * Save an entity to the data source, if validation fails show the form with the
     * corresponding error messages
     */
    public function saveAction()
    {
        if ($this->getService()->isValid()) {

            $expenseId = $this->getService()->save();

            $this->setParam('expenseId', $expenseId);

            $this->redirect($this->getService()->getRoute('list'));
        }

        $this->createAction();
        $this->renderScript($this->getService()->getScript('create'));
    }

    /**
     * Update the information of a given resource
     *
     * @return void
     */
    public function updateAction()
    {
        if ($this->retrieveOne() && $this->getService()->isValid()) {

            $this->getService()->update();
            $this->redirect($this->getService()->getRoute('list'));
        }

        $this->assign($this->getService()->populateEditingForm($this->getUrlHelper()));
        $this->renderScript($this->getService()->getScript('edit'));
    }

    /**
     * Retrieve the form for creating params with date ranges
     */
    public function customizeAction()
    {
        $this->assign($this->getService()->getPersonalizeForm($this->getUrlHelper()));
    }

    /**
     * Retrieve a paginated list of entities
     */
    public function listAction()
    {
        $this->assign($this->getService()->getPersonalizeForm($this->getUrlHelper()));
    }

    /**
     * return array
     */
    public function expensesByZoneAction()
    {
        $this->assign($this->getService()->fetchAll());
    }
}
