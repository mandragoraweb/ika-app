<?php
namespace Ika\Controller;

use \Mandragora\Controller\CrudController;

class PropertyController extends CrudController
{
    /**
     * Retrieve all properties by username client
     */
    public function listClientAction()
    {
        $this->assign($this->getService()->fetchAllByUsername());
    }

    /**
     * Retrieve all properties by zone
     */
    public function listZoneAction()
    {
        $this->assign($this->getService()->fetchAllByZone());
    }

    public function saveClientPropertyAction()
    {
        if ($this->getService()->isClientPropertyValid()) {

            $this->getService()->save();
            $this->getService()->sendMessage($this->getParams());

            $this->redirect($this->getService()->getRoute('client_list'));
        }

        $this->assign($this->getService()->getFormForClientProperty($this->getUrlHelper()));
        $this->renderScript($this->getService()->getScript('create_property'));
    }
}
