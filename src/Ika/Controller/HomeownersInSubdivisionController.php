<?php
namespace Ika\Controller;

use Mandragora\Controller\HttpController;
use Ika\Repository\ClientRepository;

class HomeownersInSubdivisionController extends HttpController
{
    protected $allHomeowners;

    public function setClientRepository(ClientRepository $allHomeowners)
    {
        $this->allHomeowners = $allHomeowners;
    }

    public function showHomeownersInSubdivision()
    {
        $zoneId = $this->getParam('zoneId');
        $this->assign(['homeowners' => $this->allHomeowners->inSubDivision($zoneId)]);
        $this->renderScript('client/homeowners-in-subdivision.html.twig');
    }
}
