<?php
namespace Ika\Controller;

use \Mandragora\Controller\CrudController;

class ZonePaymentController extends CrudController
{
    /**
     * Save an entity to the data source, if validation fails show the form with the
     * corresponding error messages
     */
    public function saveAction()
    {
        if ($this->getService()->isValid()) {

            $paymentId = $this->getService()->save();

            $this->setParam('paymentId', $paymentId);

            $this->redirect($this->getService()->getRoute('list'));
        }

        $this->assign(['form' => $this->getService()->getForm()]);
        $this->renderScript($this->getService()->getScript('create'));
    }

    /**
     * Update the information of a given resource
     *
     * @return void
     */
    public function updateAction()
    {
        if ($this->retrieveOne() && $this->getService()->isValid()) {

            $this->getService()->update();
            $this->redirect($this->getService()->getRoute('list'));
        }

        $this->assign($this->getService()->populateEditingForm($this->getUrlHelper()));
        $this->renderScript($this->getService()->getScript('edit'));
    }

    public function propertyByZoneAction()
    {
        $this->assign($this->getService()->retrievePropertyByZone());
    }

    public function paymentsByZoneAction()
    {
        $this->assign($this->getService()->fetchAll());
    }
}
