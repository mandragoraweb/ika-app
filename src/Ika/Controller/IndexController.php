<?php
namespace Ika\Controller;

use \Ika\Service\UserService;
use \Ika\Service\ContactService;
use \Mandragora\Controller\HttpController;

class IndexController extends HttpController
{
    /**
     * @var UserService
     */
    protected $user;

    /**
     * @var ContactService
     */
    protected $contact;

    /**
     * @param UserService $user
     */
    public function setUser(UserService $user)
    {
        $this->user = $user;
    }

    /**
     * @return UserService
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * return ContactService
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param ContactService $contact
     */
    public function setContact(ContactService $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * @return void
     */
    public function aboutAction()
    {
    }

    /**
     * @return void
     */
    public function serviceAction()
    {
    }

    /**
     * @return void
     */
    public function saleAction()
    {
    }

    /**
     * @return void
     */
    public function sellItemAction()
    {
        $slug = $this->getParam('slug');
        $this->renderScript("sales/$slug.html.twig");
    }

    /**
     * @return void
     */
    public function contactAction()
    {
        $this->assign(array('contactForm' => $this->getContact()->getContactForm()));
    }

    /**
     * return void
     */
    public function sendEmailAction()
    {
        if ($this->getContact()->isContactValid($this->getParams())) {

            $this->assign($this->getContact()->sendMessage());

            return;
        }
        $this->contactAction();
        $this->renderScript($this->getContact()->getScript('contact'));
    }
}
