<?php
namespace Ika\Validator;

use Ika\Repository\UserRepository;

class UniqueUsernameValidator
{
    protected $userRepository;

    public function __construct(UserRepository $repository)
    {
        $this->userRepository = $repository;
    }

    public function __invoke($username)
    {
        $user = $this->userRepository->find(['username' => $username]);

        return ! (boolean) $user;
    }
}
