<?php
namespace Ika\Validator;

use Ika\Repository\PaymentRepository;

class UniqueZoneFolioValidator
{
    /** @var PaymentRepository */
    protected $allProperties;

    /**
     * @param PaymentRepository $allProperties
     */
    public function __construct(PaymentRepository $allProperties)
    {
        $this->allProperties = $allProperties;
    }

    /**
     * @param $folio
     * @param $zone
     * @return bool
     */
    public function __invoke($folio, $zone)
    {
        if ($folio == '') {
            return true;
        }

        $property = $this->allProperties->retrieveFolio($folio, $zone);

        return ! (boolean) $property;
    }
}
