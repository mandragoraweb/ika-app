<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\CondominiumManagement;

use ArrayObject;

class AnnualExpensesStatement extends ArrayObject
{
    /** @var array */
    protected $conceptsTotal;

    public function __construct(array $statement)
    {
        parent::__construct($statement);
        $this->setFlags(ArrayObject::ARRAY_AS_PROPS);
        $this->conceptsTotal = [];
    }

    /**
     * @return string
     */
    public function subdivisionName()
    {
        $conceptStatement = $this->getIterator()->current();

        return $conceptStatement['name'];
    }

    /**
     * Sum all the monthly expenses of a specific concept
     *
     * @param  array $conceptExpenses
     * @return float
     */
    public function conceptTotal(array $conceptExpenses)
    {
        return $conceptExpenses['jan'] + $conceptExpenses['feb'] + $conceptExpenses['mar'] + $conceptExpenses['apr']
            + $conceptExpenses['may'] + $conceptExpenses['jun'] + $conceptExpenses['jul'] + $conceptExpenses['aug']
            + $conceptExpenses['sep'] + $conceptExpenses['oct'] + $conceptExpenses['nov'] + $conceptExpenses['dec'];
    }

    /**
     * Sum all the concepts expenses of a specific month
     *
     * @param  string  $month
     * @return integer
     */
    public function monthTotal($month)
    {
        $conceptExpenses = $this->getIterator();
        $this->conceptsTotal[$month] = 0;
        foreach ($conceptExpenses as $expenses) {
            $this->conceptsTotal[$month] += $expenses[$month];
        }

        return $this->conceptsTotal[$month];
    }

    /**
     * Sum all the expenses of a given year
     *
     * @return float
     */
    public function total()
    {
        $total = 0;
        foreach ($this->conceptsTotal as $month => $monthTotal) {
            $total += $this->conceptsTotal[$month];
        }

        return $total;
    }
}
