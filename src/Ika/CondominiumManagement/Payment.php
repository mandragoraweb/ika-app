<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\CondominiumManagement;

use ArrayObject;

class Payment extends ArrayObject
{
    /** @type float */
    protected $total;

    /**
     * @param array $payment
     */
    public function __construct(array $payment)
    {
        parent::__construct($payment);
        $this->setFlags(ArrayObject::ARRAY_AS_PROPS);
    }

    /**
     * If the value for the key 'is_opening_balance' is '1' it means the payment is either added or
     * subtracted to the client's opening balance
     *
     * @return boolean
     */
    public function isOpeningBalance()
    {
        return $this->offsetGet('is_opening_balance') === '1';
    }

    /**
     * @return number
     */
    public function totalBalance()
    {
        if (!$this->total) {
            $this->total = $this->offsetGet('amount_paid') - (
                $this->offsetGet('amount') + $this->offsetGet('surcharge_amount')
            );
        }

        return $this->total;
    }

    /**
     * @return float
     */
    public function amountPaid()
    {
        return (float) $this->offsetGet('amount_paid');
    }
}
