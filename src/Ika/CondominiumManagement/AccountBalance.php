<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\CondominiumManagement;

use ArrayObject;

class AccountBalance extends ArrayObject
{
    /** @type float */
    protected $totalOpeningBalance;

    /**
     * This value is updated as payments are iterated
     *
     * @type float
     */
    protected $partialBalance;

    /**
     * @param array $clientPayments {
     *     @type array      $client
     *     @type Payment[]  $payments
     * }
     */
    public function __construct(array $clientPayments)
    {
        $this->partialBalance = 0;
        $account = [
            'payments' => array_map(function($payment) {
                return new Payment($payment);
            }, $clientPayments['payments'])
        ];
        unset($clientPayments['payments']);

        $account['client'] = $clientPayments;

        parent::__construct($account);
        $this->setFlags(ArrayObject::ARRAY_AS_PROPS);
    }

    /**
     * A partial balance reflects whether the client owes money or not.
     *
     * If this method returns a positive number means a client has a positive credit balance,
     * otherwise the client owes that amount.
     *
     * @param  Payment $payment
     * @return float
     */
    public function partialBalance(Payment $payment)
    {
        $amount = $payment->totalBalance();

        if ($this->partialBalance < 0) {
            $this->applyToNegativeBalance($amount);
        } else {
            $this->partialBalance = $this->partialBalance + $amount;
        }

        return $this->partialBalance;
    }

    /**
     * @param $amount
     */
    protected function applyToNegativeBalance($amount)
    {
        if ($amount > 0) {
            $this->partialBalance = $amount + $this->partialBalance;
        } else {
            $this->partialBalance = $this->partialBalance + $amount;
        }
    }

    /**
     * Sums all the payments that are related to the client's opening balance
     *
     * @return number
     */
    public function totalOpeningBalance()
    {
        if (!$this->totalOpeningBalance) {
            $total = 0;
            /** @var Payment $payment */
            foreach ($this->offsetGet('payments') as $payment) {
                if ($payment->isOpeningBalance()) {
                    $total += $payment->amountPaid();
                }
            }
            $this->totalOpeningBalance = $total;
        }

        return $this->totalOpeningBalance;
    }

    /**
     * Sums all the client's regular payments (those which are not related to the opening balance)
     */
    public function totalPayments()
    {
        $total = 0;
        /** @var Payment $payment */
        foreach ($this->offsetGet('payments') as $payment) {
            $total += $payment->totalBalance();
        }

        return $total;
    }

    /**
     * The current opening balance is the opening balance amount prior to the start date of this
     * account balance minus the payments generated during this period
     *
     * @return float
     */
    public function currentOpeningBalance()
    {
        $currentOpeningBalance = 0;
        if ($this->offsetGet('client')['opening_balance'] < 0) {
            $currentOpeningBalance = $this->offsetGet('client')['opening_balance'] + $this->totalOpeningBalance();
        }
        if ($this->offsetGet('client')['opening_balance'] > 0 ) {
            $currentOpeningBalance = $this->offsetGet('client')['opening_balance'] - $this->totalOpeningBalance();
        }

        return $currentOpeningBalance;
    }

    /**
     * The account balance total is the arithmetic sum of the current opening balance and the
     * payments generated during this period
     *
     * @return float
     */
    public function total()
    {
        return $this->currentOpeningBalance() + $this->totalPayments();
    }
}
