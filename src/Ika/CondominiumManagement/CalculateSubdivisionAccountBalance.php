<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\CondominiumManagement;

use Ika\Repository\ZoneAccountRepository;

class CalculateSubdivisionAccountBalance
{
    /** @var ZoneAccountRepository */
    protected $allAccountBalances;

    /**
     * @param ZoneAccountRepository $allAccountBalances
     */
    public function __construct(ZoneAccountRepository $allAccountBalances)
    {
        $this->allAccountBalances = $allAccountBalances;
    }

    /**
     * @param  string $subdivision
     * @param  string $startDate
     * @param  string $stopDate
     * @return array
     */
    public function calculateAccountBalance($subdivision, $startDate, $stopDate)
    {
        return $this->allAccountBalances->ofSubdivisionInRange($subdivision, $startDate, $stopDate);
    }
}
