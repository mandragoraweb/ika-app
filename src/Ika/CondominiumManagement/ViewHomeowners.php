<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\CondominiumManagement;

use Ika\Repository\ClientRepository;
use Mandragora\Paginator\PaginatorInterface;
use Zend\Authentication\AuthenticationService;

class ViewHomeowners
{
    /** @var AuthenticationService */
    protected $authentication;

    /** @var ClientRepository */
    protected $allHomeowners;

    /** @var  PaginatorInterface */
    protected $paginator;

    /**
     * @param AuthenticationService $authentication
     * @param ClientRepository      $allHomeowners
     * @param PaginatorInterface    $paginator
     */
    public function __construct(
        AuthenticationService $authentication,
        ClientRepository $allHomeowners,
        PaginatorInterface $paginator
    )
    {
        $this->authentication = $authentication;
        $this->allHomeowners = $allHomeowners;
        $this->paginator = $paginator;
    }

    /**
     * @param  array              $parameters
     * @return PaginatorInterface
     */
    public function viewHomeowners(array $parameters)
    {
        $homeowners = $this->allHomeowners->findAll($parameters + $this->authentication->getIdentity());
        $count = $this->allHomeowners->count($parameters + $this->authentication->getIdentity());

        $this->paginator->setCurrentItems($count, $homeowners)->setCurrentPage($parameters['page']);

        return $this->paginator;
    }
}
