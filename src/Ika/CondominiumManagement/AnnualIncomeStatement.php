<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\CondominiumManagement;

use ArrayObject;

class AnnualIncomeStatement extends ArrayObject
{
    /** @var array */
    protected $homeownersTotal;

    public function __construct(array $statement)
    {
        parent::__construct($statement);
        $this->setFlags(ArrayObject::ARRAY_AS_PROPS);
        $this->homeownersTotal = [];
    }

    /**
     * @return string
     */
    public function subdivisionName()
    {
        $homeownerStatement = $this->getIterator()->current();

        return $homeownerStatement['name'];
    }

    /**
     * Sum all the monthly payments of a specific homeowner
     *
     * @param  array $homeownerIncome
     * @return float
     */
    public function homeownerTotal(array $homeownerIncome)
    {
        return $homeownerIncome['jan'] + $homeownerIncome['feb'] + $homeownerIncome['mar'] + $homeownerIncome['apr']
            + $homeownerIncome['may'] + $homeownerIncome['jun'] + $homeownerIncome['jul'] + $homeownerIncome['aug']
            + $homeownerIncome['sep'] + $homeownerIncome['oct'] + $homeownerIncome['nov'] + $homeownerIncome['dec'];
    }

    /**
     * Sum all the homeowners payments of a specific month
     *
     * @param  string  $month
     * @return integer
     */
    public function monthTotal($month)
    {
        $homeownersIncome = $this->getIterator();
        $this->homeownersTotal[$month] = 0;
        foreach ($homeownersIncome as $income) {
            $this->homeownersTotal[$month] += $income[$month];
        }

        return $this->homeownersTotal[$month];
    }

    /**
     * Sum all the payments of a given year
     *
     * @return float
     */
    public function total()
    {
        $total = 0;
        foreach ($this->homeownersTotal as $month => $monthTotal) {
            $total += $this->homeownersTotal[$month];
        }

        return $total;
    }
}
