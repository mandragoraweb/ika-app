<?php
namespace Ika\Service;

use \Mandragora\View\Helper\UrlHelper;
use \Mandragora\Service\CrudService;
use \Zend\Authentication\AuthenticationService;
use \Ika\Service\ConceptService;
use \Ika\Service\ZoneService;

class ZoneExpensesService extends CrudService
{

    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @var FeeService
     */
    protected $serviceConcept;

    /**
     * @var ZoneService
     */
    protected $serviceZone;

    /**
     * @var \Zend\Form\FormInterface
     */
    protected $personalizeForm;

    /**
     * @return FeeService
     */
    public function getServiceConcept()
    {
        return $this->serviceConcept;
    }

    /**
     * @param FeeService $serviceFee
     */
    public function setServiceConcept(ConceptService $serviceConcept)
    {
        $this->serviceConcept = $serviceConcept;
    }

    /**
     * @param AbstractService $user
     */
    public function setServiceZone(ZoneService $service)
    {
        $this->serviceZone = $service;
    }

    /**
     * @return AbstractService
     */
    public function getServiceZone()
    {
        return $this->serviceZone;
    }

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/zone-expenses.php';
        $this->routes = array(
            'list' => 'zone-expenses_list',
            'save' => 'zone-expenses_save',
            'update' => 'zone-expenses_update',
            'edit' => 'zone-expenses_edit',
        );
        $this->scripts = array(
            'create' => 'zone-expenses/create.html.twig',
            'edit' => 'zone-expenses/edit.html.twig',
        );
    }

    /**
     * @return \Zend\Form\FormInterface
     */
    public function getFormPersonalize(array $messages = null)
    {
        if (!$this->personalizeForm) {
            $this->personalizeForm = $this->buildForm(require 'config/forms/zone-expenses-personalize.php');
        }

        if (is_array($messages)) {
            $this->personalizeForm->setMessages($messages);
        }

        return $this->personalizeForm;
    }

    /**
     * @return array
     */
    public function getPersonalizeForm(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getFormPersonalize();
        $form->setZones($zones);

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('list'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();
        $concepts = $this->getServiceConcept()->retrieveAllConceptsByExpense();

        $this->getForm()->setZones($zones);
        $this->getForm()->setConcepts($concepts);

        $form = $this->getForm()->prepareForCreating();

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return array
     */
    public function fetchAll()
    {
        $items = $this->getRepository()->findAll($this->getParams());
        $count = $this->getRepository()->count($this->getParams());

        /*$this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));*/

        return array('items' => $items, 'params' => $this->getParams());
    }

    /**
     * Save a new entity to the dta source
     */
    public function save()
    {
        $values = $this->getForm()->getData();

        date_default_timezone_set('UTC');
        $date = date('Y-m-d');

        $values['date_record'] = $date;

        return $this->getRepository()->insert($values);
    }

    /**
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());

        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();
        $this->getForm()->setZones($zones);

        $concepts = $this->getServiceConcept()->retrieveAllConceptsByExpense();
        $this->getForm()->setConcepts($concepts);

        $form = $this->getForm()->prepareForEditing();
        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return void
     */
    public function update()
    {
        $values = $this->getForm()->getData();

        date_default_timezone_set('UTC');
        $date = date('Y-m-d');

        $values['date_record'] = $date;

        $this->getRepository()->update($values);
    }

    /**
     * @return boolean
     */
    public function isPersonalizeValid()
    {
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getFormPersonalize();

        $form->setZones($zones);

        return $form->isValid();
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();
        $concepts = $this->getServiceConcept()->retrieveAllConceptsByExpense();

        $this->getForm()->setZones($zones);
        $this->getForm()->setConcepts($concepts);

        $form = $this->getForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return array
     */
    public function show()
    {
        return array('entity' => $this->retrieveOne(), 'params' => $this->getParams());
    }
}
