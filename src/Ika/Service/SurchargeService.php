<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Mandragora\View\Helper\UrlHelper;

class SurchargeService extends CrudService
{
    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/surcharge.php';
        $this->routes = array(
            'list' => 'surcharge_list',
            'save' => 'surcharge_save',
            'update' => 'surcharge_update',
        );
        $this->scripts = array(
            'create' => 'surcharge/create.html.twig',
            'edit' => 'surcharge/edit.html.twig',
        );
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $this->getForm()->setPayment($this->getParams());

        $form = $this->getForm()->prepareForCreating();
        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * return array
     */
    public function fetchAll()
    {
        $items = $this->getRepository()->findAll($this->getParams());
        $count = $this->getRepository()->count($this->getParams());

        $this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));

        return array('paginator' => $this->getPaginator(), 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function show()
    {
        return array('entity' => $this->retrieveOne(), 'params' => $this->getParams());
    }
}
