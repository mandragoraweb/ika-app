<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Mandragora\View\Helper\UrlHelper;
use \Ika\Service\ZoneService;

class ZoneAccountService extends CrudService
{
    /**
     * @var ZoneService
     */
    protected $zoneService;

    /**
     * @return ZoneService
     */
    public function getZoneService()
    {
        return $this->zoneService;
    }

    /**
     * @param ZoneService $zoneService
     */
    public function setZoneService(ZoneService $zoneService)
    {
        $this->zoneService = $zoneService;
    }

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/zone-account.php';
        $this->routes = array(
            'state' => 'zone-account_show-balance',
        );
        $this->scripts = array(
            'detail' => 'zone-account/customize.html.twig',
        );
    }

    /**
     * @return array
     */
    public function getDetailForm(UrlHelper $urlHelper)
    {
        $this->getZoneService()->setParams($this->getParams());
        $zones = $this->getZoneService()->retrieveAllZones();

        $this->getForm()->setZones($zones);

        $this->getForm()->get('zone_id')->setAttribute('required', true);

        $form = $this->getForm()->prepareForCreating();

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('state'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $this->getZoneService()->setParams($this->getParams());
        $zones = $this->getZoneService()->retrieveAllZones();

        $this->getForm()->setZones($zones);

        $form = $this->getForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }
}
