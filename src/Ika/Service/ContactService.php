<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Swift_SmtpTransport as Transport;
use \Swift_Message as Message;
use \Swift_Mailer as Mailer;

class ContactService extends CrudService
{
    /**
     * @var \Zend\Form\FormInterface
     */
    protected $contactForm;

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->scripts = array(
            'contact' => 'index/contact.html.twig',
        );
    }

    /**
     * @return \Zend\Form\FormInterface
     */
    public function getContactForm()
    {
        if (!$this->contactForm) {
            $this->contactForm = $this->buildForm(require 'config/forms/contact.php');
        }

        return $this->contactForm;
    }

    /**
     * @return boolean
     */
    public function isContactValid($values)
    {
        $form = $this->getContactForm();
        $form->setData($values);

        return $form->isValid();
    }

    /**
     * Send to message
     */
    public function sendMessage()
    {
        $values = $this->getContactForm()->getData();

        $transport = Transport::newInstance('smtp.gmail.com', 465, 'ssl');
        $transport->setUsername('mailer@mandragora-web-systems.com')
                  ->setPassword('@_T0w4t1c&%');
        $message = Message::newInstance();
        $message->setTo('jmhtello@gmail.com', 'Contacto');
        $message->setSubject('Contacto');
        $message->setBody($values['comment']);
        $message->setFrom($values['email'], $values['name']);
        $message->setReplyTo($values['email'], $values['name']);

        $mail = Mailer::newInstance($transport);
        $mail->send($message, $failures);

        return array('values' => $values);
    }
}
