<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Ika\Service\UserService;
use \Mandragora\View\Helper\UrlHelper;
use \Mandragora\Password\SecurePasswordGenerator;
use \Zend\Form\Factory;
use \Swift_Message as Message;
use \Swift_Mailer as Mailer;
use PasswordLib\PasswordLib;

class ProfileService extends CrudService
{

    /**
     * @var UserService
     */
    protected $serviceUser;

    /**
     * @var \Mandragora\Password\SecurePasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @var \Zend\Form\FormInterface
     */
    protected $changePasswordForm;

    /**
     * @var \Swift_Message
     */
    protected $message;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var view
     */
    protected $view;

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/profile.php';
        $this->routes = array(
            'list' => 'profile_list',
            'save' => 'profile_save',
            'update' => 'profile_update',
            'new_password' => 'profile_new_password',
        );
        $this->scripts = array(
            'create' => 'profile/create.html.twig',
            'edit' => 'profile/edit.html.twig',
            'change_password' => 'profile/change-password.html.twig',
            'new-user' => 'mail/new-user.html.twig',
        );
    }

    /**
     * @return UserService
     */
    public function getServiceUser()
    {
        return $this->serviceUser;
    }

    /**
     * @param UserService $serviceUser
     */
    public function setServiceUser(UserService $serviceUser)
    {
        $this->serviceUser = $serviceUser;
    }

    /**
     * @return \Mandragora\Password\SecurePasswordGenerator
     */
    public function getPasswordGenerator()
    {
        return $this->passwordGenerator;
    }

    /**
     * @param \Mandragora\Password\SecurePasswordGenerator $passwordGenerator
     */
    public function setPasswordGenerator(SecurePasswordGenerator $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
    }

    /**
     * @return
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param $mailMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @param $mailer
     */
    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param $view
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * @return \Zend\Form\FormInterface
     */
    public function getChangePasswordForm(array $messages = null)
    {
        if (!$this->changePasswordForm) {
            $this->changePasswordForm = $this->buildForm(require 'config/forms/change-password.php');
        }

        if (is_array($messages)) {
            $this->changePasswordForm->setMessages($messages);
        }

        return $this->changePasswordForm;
    }

    /**
     * @return array
     */
    public function getFormChangePassword(UrlHelper $urlHelper)
    {

        $form = $this->getChangePasswordForm();
        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('new_password'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isValidChangeForm()
    {
        $form = $this->getChangePasswordForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * Save a new entity to the dta source
     */
    public function save()
    {
        $profileValues = $this->getForm()->getProfileData();
        $profileId = $this->getRepository()->insert($profileValues);

        $userValues = $this->getForm()->getUserData();
        $userValues['profile_id'] = $profileId;
        $this->getServiceUser()->getRepository()->insert($userValues);

        return $profileId;
    }

    /**
     * @return \Mandragora\Form\CrudForm
     */
    protected function getFormEditing()
    {
        if (!$this->form) {
            $this->form = $this->buildFormEditing(require $this->formOptionsPath);
        }

        return $this->form;
    }

    /**
     * @param  array                    $spec
     * @return \Zend\Form\FormInterface
     */
    protected function buildFormEditing(array $spec)
    {
        if (!$this->formFactory) {
            $this->formFactory = new Factory();
        }

        $spec['input_filter']['username']['required'] = false;
        $spec['input_filter']['password']['required'] = false;

        return $this->formFactory->create($spec);
    }

    /**
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());
        $values['user'] = $values['username'];
        $values['user_role'] = $values['role'];

        $form = $this->getFormEditing()->prepareForEditing();

        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isValid()
    {

        $form = $this->getFormEditing();
        $values = $this->getParams();

        $values['role'] = $values['user_role'];
        unset($values['username']);

        $form->setData($values);

        return $form->isValid();
    }

    /**
     * @return void
     */
    public function update()
    {
        $profileValues = $this->getForm()->getProfileData();
        $profileId = $this->getRepository()->update($profileValues);

        $values= $this->getForm()->getData();
        $userValues = $this->getForm()->getUserData();
        $userValues['role'] = $values['user_role'];
        $this->getServiceUser()->getRepository()->update($userValues);
    }

    /**
     * @return int The number of affected rows
     */
    public function delete()
    {
        $values = $this->getRepository()->find($this->getParams());
        $username['username'] = $values['username'];
        $this->getServiceUser()->getRepository()->delete($username);
        $idProfile['profileId'] = $values['profile_id'];
        $this->getRepository()->delete($idProfile);

        return ;

    }

    /**
     * Change an administrator password
     */
    public function changePassword()
    {
        $profile = $this->getRepository()->find($this->getParams());
        $user = $this->getServiceUser()->getRepository()->find($profile);
        $values = $this->getChangePasswordForm()->getData();

        $newPassword['password'] = $this->getPasswordGenerator()->create($values['new-password']);
        $newPassword['username'] = $user['username'];

        $this->getServiceUser()->getRepository()->update($newPassword);
    }

    /**
     * Send to message
     */
    public function sendMessage($values)
    {
        $administrador = $this->getRepository()->findAllWithUserAndProfileInfo($values);

        $passwordLib = new PasswordLib();
        $password = $passwordLib->getRandomToken(10);

        $newpassword = $this->getPasswordGenerator()
                            ->create($password);

        $userValues['username'] = $administrador['username'];
        $userValues['password'] = $newpassword;

        $this->getServiceUser()->updatePassword($userValues);

        $userValues['password'] = $password;

        $message = $this->getMessage();
        //email and username
        $message->setTo($administrador['email'], $administrador['username']);
        $message->setSubject('Usuario nuevo');
        $message->setBody(
            $this->view->render($this->getScript('new-user'), array('params' => $userValues)),
            'text/html'
        );

        $mail = $this->getMailer();
        $mail->send($message, $failures);
    }

}
