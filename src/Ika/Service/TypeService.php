<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;

class TypeService extends CrudService
{
    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/type.php';
        $this->routes = array(
            'list' => 'type_list',
            'save' => 'type_save',
            'update' => 'type_update',
        );
        $this->scripts = array(
            'create' => 'type/create.html.twig',
            'edit' => 'type/edit.html.twig',
        );
    }

    /**
     * return array
     */
    public function retrieveAllTypes()
    {
        return $this->getRepository()->findAll();
    }
}
