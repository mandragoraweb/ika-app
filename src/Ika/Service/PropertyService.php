<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Mandragora\View\Helper\UrlHelper;
use \Mandragora\Password\SecurePasswordGenerator;
use \Ika\Service\ZoneService;
use \Ika\Service\TypeService;
use \Ika\Service\ClientService;
use \Ika\Service\UserService;
use \Swift_Message as Message;
use \Swift_Mailer as Mailer;
use PasswordLib\PasswordLib;

class PropertyService extends CrudService
{
    /**
     * @var ZoneService
     */
    protected $serviceZone;

    /**
     * @var TypeService
     */
    protected $serviceType;

    /**
     * @var ClientService
     */
    protected $serviceClient;

    /**
     * @var UserService
     */
    protected $serviceUser;

    /**
     * @var \Mandragora\Password\SecurePasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @var \Swift_Message
     */
    protected $message;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var view
     */
    protected $view;

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/property.php';
        $this->routes = array(
            'list' => 'property_list',
            'save' => 'property_save',
            'save_property' => 'client_property_save',
            'update' => 'property_update',
            'client_list' => 'client_list',
        );
        $this->scripts = array(
            'create' => 'property/create.html.twig',
            'edit' => 'property/edit.html.twig',
            'create_property' => 'client/create-property.html.twig',
            'new-user' => 'mail/new-user.html.twig',
        );
    }

    /**
     * @param ZoneService $service
     */
    public function setServiceZone(ZoneService $service)
    {
        $this->serviceZone = $service;
    }

    /**
     * @return ZoneService
     */
    public function getServiceZone()
    {
        return $this->serviceZone;
    }

    /**
     * @return AbstractService
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }

    /**
     * @param AbstractService $serviceType
     */
    public function setServiceType(TypeService $serviceType)
    {
        $this->serviceType = $serviceType;
    }

    /**
     * @return ClientService
     */
    public function getServiceClient()
    {
        return $this->serviceClient;
    }

    /**
     * @param ClientService $serviceUser
     */
    public function setServiceClient(ClientService $serviceClient)
    {
        $this->serviceClient = $serviceClient;
    }

    /**
     * @return UserService
     */
    public function getServiceUser()
    {
        return $this->serviceUser;
    }

    /**
     *
     * @param UserService $serviceUser
     */
    public function setServiceUser(UserService $serviceUser)
    {
        $this->serviceUser = $serviceUser;
    }

    /**
     * @return \Mandragora\Password\SecurePasswordGenerator
     */
    public function getPasswordGenerator()
    {
        return $this->passwordGenerator;
    }

    /**
     * @param \Mandragora\Password\SecurePasswordGenerator $passwordGenerator
     */
    public function setPasswordGenerator(SecurePasswordGenerator $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
    }

    /**
     * @return
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param $mailMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @param $mailer
     */
    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param $view
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $types = $this->getServiceType()->retrieveAllTypes();
        $this->getServiceClient()->setParams($this->getParams());
        $clients = $this->getServiceClient()->retrieveAllClients();

        $this->getForm()->setZone($this->getParam('zoneId'));
        $this->getForm()->setTypes($types);
        $this->getForm()->setClients($clients);

        $form = $this->getForm()->prepareForCreating();

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function getFormForClientProperty(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $types = $this->getServiceType()->retrieveAllTypes();
        $zones = $this->getServiceZone()->retrieveAllZones();

        $this->formOptionsPath = 'config/forms/client-property.php';

        $this->getForm()->setClient($this->getParam('clientId'));
        $this->getForm()->setZones($zones);
        $this->getForm()->setTypes($types);

        $form = $this->getForm()->prepareForCreating();

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save_property'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isClientPropertyValid()
    {
        $this->getServiceZone()->setParams($this->getParams());
        $types = $this->getServiceType()->retrieveAllTypes();
        $zones = $this->getServiceZone()->retrieveAllZones();

        $this->formOptionsPath = 'config/forms/client-property.php';

        $this->getForm()->setZones($zones);
        $this->getForm()->setTypes($types);

        $form = $this->getForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return array
     */
    public function fetchAll()
    {
        $items = $this->getRepository()->findAll($this->getParams());
        $count = $this->getRepository()->count($this->getParams());

        $zone = $this->getServiceZone()->getRepository()->find($this->getParams());

        $this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));

        return array('paginator' => $this->getPaginator(), 'zone' => $zone, 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function fetchAllByUsername()
    {
        $username = $this->getServiceUser()->getAuthService()->getIdentity();
        $items = $this->getRepository()->findAllByUsername($username);
        $count = $this->getRepository()->countByUsername($username['username']);

        $this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));

        return array('paginator' => $this->getPaginator(), 'params' => $this->getParams());
    }

    /**
     * @todo This is, at least, poorly named since the method uses the current identity to filter
     *       properties instead of the zone...
     * @return array
     */
    public function fetchAllByZone()
    {
        $identity = $this->getServiceUser()->getAuthService()->getIdentity();

        $items = $this->getRepository()->findAllByIdentity($identity);
        $count = $this->getRepository()->countByZone($identity['username']);

        $this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));

        return array('paginator' => $this->getPaginator(), 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());

        $types = $this->getServiceType()->retrieveAllTypes();
        $this->getServiceClient()->setParams($this->getParams());
        $clients = $this->getServiceClient()->retrieveAllClients();

        $this->getForm()->setTypes($types);
        $this->getForm()->setClients($clients);

        $form = $this->getForm()->prepareForEditing();
        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $types = $this->getServiceType()->retrieveAllTypes();
        $this->getServiceClient()->setParams($this->getParams());
        $clients = $this->getServiceClient()->retrieveAllClients();

        $this->getForm()->setTypes($types);
        $this->getForm()->setClients($clients);

        $form = $this->getForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return array
     */
    public function show()
    {
        return array('entity' => $this->retrieveOne(), 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function retrieveAllProperties()
    {
        return $this->getRepository()->findAllWithoutFilter();
    }

    /**
     * @param  int   $zoneId
     * @return array
     */
    public function retrieveAllByZone($zoneId)
    {
        return $this->getRepository()->findAllByZone($zoneId);
    }

    /**
     * @param $propertyId
     * @return array
     */
    public function retrieveClientId($propertyId)
    {
        return $this->getRepository()->retrieveClientOfProperty($propertyId);
    }

    /**
     * @param  array $values
     * @return array
     */
    public function retrieveOpeningBalance(array $values)
    {
        return $this->getRepository()->findOpeningBalance($values);
    }

    /**
     * Send to message
     */
    public function sendMessage($values)
    {
        $client = $this->getServiceClient()->retrieveClientInformation($values);

        if ( empty($client['email']) ) {
            $admin = $this->getServiceClient()->retrieveZoneAdministrator($values);
            $email = $admin['email'];
        } else {
            $email = $client['email'];
        }

        $passwordLib = new PasswordLib();
        $password = $passwordLib->getRandomToken(10);

        $newpassword = $this->getPasswordGenerator()
                            ->create($password);

        $userValues['username'] = $client['username'];
        $userValues['password'] = $newpassword;

        $this->getServiceUser()->updatePassword($userValues);

        $userValues['password'] = $password;

        $message = $this->getMessage();
        //email and username
        $message->setTo($email, $client['username']);
        $message->setSubject('Usuario nuevo');
        $message->setBody(
            $this->view->render($this->getScript('new-user'), array('params' => $userValues)),
            'text/html'
        );

        $mail = $this->getMailer();
        $mail->send($message, $failures);
    }
}
