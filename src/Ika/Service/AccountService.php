<?php
namespace Ika\Service;

use Mandragora\Service\CrudService;
use Ika\CondominiumManagement\AccountBalance;

class AccountService extends CrudService
{
    /**
     * Dependending on the role, the account state is queried by username (when the role is client)
     * otherwise it is queried by client ID
     *
     * @return array
     */
    public function retrieveAccountBalance(array $user, array $criteria)
    {
        $criteria['username'] = $user['username'];

        if (in_array($user['role'], ['webmaster', 'committee', 'admin'])) {
            $clientPayments = $this->getRepository()->findAccountStateByClient($criteria);
        } elseif ($user['role'] == 'client') {
            $clientPayments = $this->getRepository()->findAccountStateByUsername($criteria);
        }

        return array('account' => new AccountBalance($clientPayments));
    }
}
