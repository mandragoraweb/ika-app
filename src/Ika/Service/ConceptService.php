<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Mandragora\View\Helper\UrlHelper;

class ConceptService extends CrudService
{

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/concept.php';
        $this->routes = array(
            'list' => 'concept_list',
            'save' => 'concept_save',
            'update' => 'concept_update',
        );
        $this->scripts = array(
            'create' => 'concept/create.html.twig',
            'edit' => 'concept/edit.html.twig',
        );
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $form = $this->getForm()->prepareForCreating();
        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * return array
     */
    public function fetchAll()
    {
        $items = $this->getRepository()->findAll($this->getParams());
        $count = $this->getRepository()->count($this->getParams());

        $this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));

        return array('paginator' => $this->getPaginator(), 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());

        $form = $this->getForm()->prepareForEditing();
        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $form = $this->getForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return array
     */
    public function show()
    {
        return array('entity' => $this->retrieveOne(), 'params' => $this->getParams());
    }

    /**
     * return array
     */
    public function retrieveAllConcepts()
    {
        return $this->getRepository()->findAllWithoutFilter();
    }

    /**
     * return array
     */
    public function retrieveAllConceptsByExpense()
    {
        return $this->getRepository()->findAllWithExpense();
    }
}
