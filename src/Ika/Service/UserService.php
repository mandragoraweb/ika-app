<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Mandragora\Authentication\ZendAuthenticationAdapter;
use \Mandragora\Password\SecurePasswordGenerator;
use \Zend\Authentication\AuthenticationService;
use \Mandragora\View\Helper\UrlHelper;

class UserService extends CrudService
{
    /**
     * @var \Zend\Form\FormInterface
     */
    protected $loginForm;

    /**
     * @var \Zend\Form\FormInterface
     */
    protected $changePasswordForm;

    /**
     * @var \Mandragora\Password\SecurePasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @var ZendAuthenticationAdapter
     */
    protected $authAdapter;

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/user.php';
        $this->routes = array(
            'list' => 'user_list_profile',
            'save' => 'user_save',
            'update' => 'user_update',
            'webmaster' => 'zone_list',
            'client' => 'property_list-client',
            'admin' => 'zone_list',
            'committee' => 'account_balance_form',
            'home' => 'index_index',
            'new_password' => 'user_new_password',
        );
        $this->scripts = array(
            'create' => 'user/create.html.twig',
            'edit' => 'user/edit.html.twig',
            'home' => 'index/index.html.twig',
            'change_password' => 'user/formpassword.html.twig',
        );
    }

    /**
     * @return \Mandragora\Password\SecurePasswordGenerator
     */
    public function getPasswordGenerator()
    {
        return $this->passwordGenerator;
    }

    /**
     * @param \Mandragora\Password\SecurePasswordGenerator $passwordGenerator
     */
    public function setPasswordGenerator(SecurePasswordGenerator $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
    }

    /**
     * @return AuthenticationService
     */
    public function getAuthService()
    {
        return $this->authService;
    }

    /**
     *
     * @param AuthenticationService $authService
     */
    public function setAuthService(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @return ZendAuthenticationAdapter
     */
    protected function getAuthAdapter()
    {
        $this->authAdapter->setRepository($this->getRepository());

        $username = $this->getLoginForm()->get('username')->getValue();
        $password = $this->getLoginForm()->get('password')->getValue();
        $this->authAdapter->setCredentials($username, $password);

        return $this->authAdapter;
    }

    /**
     * @param ZendAuthenticationAdapter $authAdapter
     */
    public function setAuthAdapter(ZendAuthenticationAdapter $authAdapter)
    {
        $this->authAdapter = $authAdapter;
    }

    /**
     * @return \Zend\Form\FormInterface
     */
    public function getLoginForm(array $messages = null)
    {
        if (!$this->loginForm) {
            $this->loginForm = $this->buildForm(require 'config/forms/login.php');
        }

        if (is_array($messages)) {
            $this->loginForm->setMessages($messages);
        }

        return $this->loginForm;
    }

    /**
     * @return \Zend\Form\FormInterface
     */
    public function getChangePasswordForm(array $messages = null)
    {
        if (!$this->changePasswordForm) {
            $this->changePasswordForm = $this->buildForm(require 'config/forms/change-password.php');
        }

        if (is_array($messages)) {
            $this->changePasswordForm->setMessages($messages);
        }

        return $this->changePasswordForm;
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $this->getForm()->setProfile($this->getParams());

        $this->getForm()->get('password')->setAttribute('required', true);

        $form = $this->getForm()->prepareForCreating();
        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function getFormChangePassword(UrlHelper $urlHelper)
    {

        $form = $this->getChangePasswordForm();
        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('new_password'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isLoginValid()
    {
        $form = $this->getLoginform();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return boolean
     */
    public function isValidChangeForm()
    {
        $form = $this->getChangePasswordForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return array(
            'messages' => array('messages' => $this->getLoginform()->getMessages())
        );
    }

    /**
     * @return \Zend\Authentication\Result
     */
    public function authenticate()
    {
        return $this->getAuthService()->authenticate($this->getAuthAdapter());
    }

    /**
     * Ends the current user session
     */
    public function logout()
    {
        $this->getAuthService()->clearIdentity();
    }

    /**
     * @return void
     */
    public function save()
    {
        $values = $this->getForm()->getData();
        $values['password'] = $this->getPasswordGenerator()->create($values['password']);

        $this->getRepository()->insert($values);
    }

    /**
     * @return void
     */
    public function update()
    {
        $values = $this->getForm()->getData();

        $this->getRepository()->update($values);
    }

    /**
     * return array
     */
    public function retrieveAllUsers()
    {
        return $this->getRepository()->findAllWithoutFilter();
    }

    /**
     * @return PaginatorInterface
     */
    public function retrieveAllByProfile()
    {
        $items = $this->getRepository()->findAllByProfile($this->getParams());
        $count = $this->getRepository()->countByProfile($this->getParams());

        $this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));

        return array('paginator' => $this->getPaginator(), 'param' => $this->getParams());
    }

    /**
     * return array
     */
    public function retriveAllByAdminRole()
    {
        return $this->getRepository()->findAllByAdminRole();
    }

    public function changePassword()
    {
        $user = $this->getRepository()->find($this->getParams());
        $values = $this->getChangePasswordForm()->getData();
        $password = $this->getPasswordGenerator()->create($values['password']);
        if (!$user) {
           return array('form' => $this->getChangePasswordForm(), 'messages' => 'El usuario no existe');
        } elseif ($this->getPasswordGenerator()->verify($values['password'], $user['password']) ) {
            $newPassword['password'] = $this->getPasswordGenerator()->create($values['new-password']);
            $newPassword['username'] = $this->getParam('username');

            $this->getRepository()->update($newPassword);

            return;
        } else {
           return array('form' => $this->getChangePasswordForm(), 'messages' => 'Password incorrecto');
        }
    }

    public function updatePassword($values)
    {
        $this->getRepository()->update($values);
    }

}
