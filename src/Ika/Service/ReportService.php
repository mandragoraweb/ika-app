<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Mandragora\View\Helper\UrlHelper;
use \Xls\Writer;

class ReportService extends CrudService
{

    /**
     * @var string
     */
    public $filepath;

    /**
     * @var ZoneService
     */
    protected $serviceZone;

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/report.php';
        $this->routes = array(
            'clientBalance' => 'report_client-balance',
            'zoneBalance' => 'report_zone-balance',
            'clientBalanceMonth' => 'report_client-monthly-balance',
            'zoneBalanceMonth' => 'report_zone-monthly-balance',
        );
        $this->scripts = array(
            'clientCostumize' => 'report/customize-client.html.twig',
            'zoneCostumize' => 'report/customize-zone.html.twig',
            'clientCostumizeMonth' => 'report/customize-client-general.html.twig',
            'zoneCostumizeMonth' => 'report/customize-zone-general.html.twig',
        );
    }

    /**
     * @return
     */
    public function getFilepath()
    {
        return $this->filepath;
    }

    /**
     * @param $filepath
     */
    public function setFilepath($filepath)
    {
        $this->filepath = $filepath;
    }

    /**
     * @param AbstractService $user
     */
    public function setServiceZone(ZoneService $service)
    {
        $this->serviceZone = $service;
    }

    /**
     * @return AbstractService
     */
    public function getServiceZone()
    {
        return $this->serviceZone;
    }

    /**
     * @return array
     */
    public function getCustomizeClientForm(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getForm()->prepareForCreating();
        $form->setZones($zones);

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('clientBalance'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return array
     */
    public function getCustomizeZoneForm(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getForm()->prepareForCreating();
        $form->setZones($zones);

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('zoneBalance'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return array
     */
    public function getCustomizeClientGeneralForm(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getForm()->prepareForCreating();
        $form->setZones($zones);

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('clientBalanceMonth'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return array
     */
    public function getCustomizeZoneGeneralForm(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getForm()->prepareForCreating();
        $form->setZones($zones);

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('zoneBalanceMonth'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getForm();
        $form->setZones($zones);

        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * return array
     */
    public function retrieveClientsBalance()
    {
        $values = $this->getForm()->getData();
        $monthlyBalance = $this->getRepository()->findStateByClients($values);
        $zoneName = count($monthlyBalance) > 0 ? $monthlyBalance[0]['name'] : null;

        $dateRange = [];
        $dateRange['start_date'] = $values['start_date'];
        $dateRange['stop_date'] = $values['stop_date'];

        return ['items' => $monthlyBalance, 'date' => $dateRange, 'name' => $zoneName];
    }

    /**
     * return array
     */
    public function retrieveZonesBalance()
    {
        $values = $this->getForm()->getData();
        $result = $this->getRepository()->findStateByZones($values);
        $date['start_date'] = $values['start_date'];
        $date['stop_date'] = $values['stop_date'];

        return array('items' => $result, 'date' => $date, 'name' => $result[0]['name']);
    }

    /**
     * return array
     */
    public function retrieveClientsBalanceMonth()
    {
        $values = $this->getForm()->getData();
        $result = $this->getRepository()->findStateByClientsMonth($values);
        $date['start_date'] = $values['start_date'];
        $date['stop_date'] = $values['stop_date'];

        return array('items' => $result, 'date' => $date, 'name' => $result[0]['name']);
    }

    /**
     * return array
     */
    public function retrieveZonesBalanceMonth()
    {
        $values = $this->getForm()->getData();
        $monthlyBalance = $this->getRepository()->findStateByZonesMonth($values);
        $zoneName = count($monthlyBalance) > 0 ? $monthlyBalance[0]['name'] : null;

        $dateRange = [];
        $dateRange['start_date'] = $values['start_date'];
        $dateRange['stop_date'] = $values['stop_date'];

        return ['items' => $monthlyBalance, 'date' => $dateRange, 'name' => $zoneName];
    }

    /**
     * Generate excel fiel for client and zone
     *
     * @return string
     */
    public function generateFileExcel()
    {
        $filename = $this->getParam('report').'-'.$this->getParam('startDate').'-'.
                    $this->getParam('stopDate') . '.xls';

        if ($this->getParam('report') == 'condominos') {
            $this->clientFile($this->getFilepath(), $filename);
        } elseif ($this->getParam('report') == 'fraccionamientos') {
            $this->zoneFile($this->getFilepath(), $filename);
        }

        return $filename;
    }

    /**
     * generate excel file for client and zone
     *
     * @return string
     */
    public function generateFileExcelMonth()
    {
        $filename = $this->getParam('report').'-mensual'.'-'.$this->getParam('startDate').'-'.
                    $this->getParam('stopDate') . '.xls';
        $values['start_date'] = $this->getParam('startDate');
        $values['stop_date'] = $this->getParam('stopDate');
        if ($this->getParam('report') == 'condominos') {
            $result = $this->getRepository()->findStateByClientsMonth($values);
        } elseif ($this->getParam('report') == 'fraccionamientos') {
            $result = $this->getRepository()->findStateByZonesMonth($values);
        }
        $this->reportFileMonth($this->getFilepath(), $filename, $result);

        return $filename;
    }

    /**
     * @param string $dir
     * @param string $filename
     */
    public function clientFile($dir, $filename)
    {
        $result = 0;
        $resultAmount = 0;
        $resultAmountPaid = 0;
        $workbook = new Writer($dir.$filename);
        $workbook->setVersion(8); // Add UTF-8 support
        $formatHeader = $workbook->addFormat();
        $formatHeader->setBold();
        $formatHeader->setAlign('center');
        $formatHeader->setFontFamily('Arial');
        $formatNumber = $workbook->addFormat();
        $formatNumber->setNumFormat('$0.00');
        $formatNumber->setAlign('center');
        $workSheet = $workbook->addWorksheet('Balance Condóminos');
        $workSheet->setInputEncoding("UTF-8");
        $workSheet->setColumn(0, 0, 20); // Set condomino column width
        $workSheet->setColumn(1, 1, 20); // Set amount column width
        $workSheet->setColumn(2, 2, 20); // Set amoun_paid use column width
        $workSheet->setColumn(3, 3, 25); // Set result column width
        $workSheet->write(
            0, 0, 'Ika Bienes Raíces',
            $formatHeader
        );
        $workSheet->setMerge(0, 0, 0, 3);
        $workSheet->write(1, 0, 'Condomino', $formatHeader);
        $workSheet->write(1, 1, 'Cuotas generadas', $formatHeader);
        $workSheet->write(1, 2, 'Cobros generados', $formatHeader);
        $workSheet->write(1, 3, 'Saldo final', $formatHeader);
        $i = 2;
        $values['start_date'] = $this->getParam('startDate');
        $values['stop_date'] = $this->getParam('stopDate');
        $clients = $this->getRepository()->findStateByClients($values);
        foreach ($clients as $client) {
            $workSheet->write($i, 0, $client['first_name']. ' '.$client['paternal_surname']. ' '. $client['maternal_surname']);

            $amount = 0;
            $amounPaid = 0;

            foreach ($client['properties'] as $property) {
                $amount+= $property['amount'];
                $amounPaid+= $property['amount_paid'];
            }
            $result += ($amount - $amounPaid);
            $resultAmount += $amount;
            $resultAmountPaid += $amounPaid;
            $workSheet->write($i, 1, $amount, $formatNumber);
            $workSheet->write($i, 2, $amounPaid, $formatNumber);
            $workSheet->write($i, 3, ($amount - $amounPaid), $formatNumber);
            $i++;
        }
        $workSheet->write($i, 0, 'Total', $formatHeader);
        $workSheet->write($i, 1, $resultAmount, $formatNumber);
        $workSheet->write($i, 2, $resultAmountPaid, $formatNumber);
        $workSheet->write($i, 3, $result, $formatNumber);
        $workbook->close(); // Write the file
    }

    /**
     * @param string $dir
     * @param string $filename
     */
    public function zoneFile($dir, $filename)
    {
        $result = 0;
        $resultAmount = 0;
        $resultAmountPaid = 0;
        $workbook = new Writer($dir.$filename);
        $workbook->setVersion(8); // Add UTF-8 support
        $formatHeader = $workbook->addFormat();
        $formatHeader->setBold();
        $formatHeader->setAlign('center');
        $formatHeader->setFontFamily('Arial');
        $formatNumber = $workbook->addFormat();
        $formatNumber->setNumFormat('$0.00');
        $formatNumber->setAlign('center');
        $workSheet = $workbook->addWorksheet('Balance Fraccionamientos');
        $workSheet->setInputEncoding("UTF-8");
        $workSheet->setColumn(0, 0, 30); // Set condomino column width
        $workSheet->setColumn(1, 1, 20); // Set amount column width
        $workSheet->setColumn(2, 2, 20); // Set amoun_paid use column width
        $workSheet->setColumn(3, 3, 25); // Set result column width
        $workSheet->write(
            0, 0, 'Ika Bienes Raíces',
            $formatHeader
        );
        $workSheet->setMerge(0, 0, 0, 3);
        $workSheet->write(1, 0, 'Fraccionamiento', $formatHeader);
        $workSheet->write(1, 1, 'Cuotas generadas', $formatHeader);
        $workSheet->write(1, 2, 'Cobros generados', $formatHeader);
        $workSheet->write(1, 3, 'Saldo final', $formatHeader);
        $i = 2;
        $values['start_date'] = $this->getParam('startDate');
        $values['stop_date'] = $this->getParam('stopDate');
        $zones = $this->getRepository()->findStateByZones($values);
        foreach ($zones as $zone) {
            $workSheet->write($i, 0, $zone['name']);

            $amount = 0;
            $amounPaid = 0;

            foreach ($zone['payments'] as $payment) {
                $amount+= $payment['amount'];
                $amounPaid+= $payment['amount_paid'];
            }
            $result += ($amount - $amounPaid);
            $resultAmount += $amount;
            $resultAmountPaid += $amounPaid;
            $workSheet->write($i, 1, $amount, $formatNumber);
            $workSheet->write($i, 2, $amounPaid, $formatNumber);
            $workSheet->write($i, 3, ($amount - $amounPaid), $formatNumber);
            $i++;
        }
        $workSheet->write($i, 0, 'Total', $formatHeader);
        $workSheet->write($i, 1, $resultAmount, $formatNumber);
        $workSheet->write($i, 2, $resultAmountPaid, $formatNumber);
        $workSheet->write($i, 3, $result, $formatNumber);
        $workbook->close(); // Write the file
    }

    /**
     * @param string $dir
     * @param string $filename
     * @param array  $query
     */
    public function reportFileMonth($dir, $filename, $query)
    {
        $result = 0;
        $resultAmount = 0;
        $resultAmountPaid = 0;
        $values = $this->selecMonth($query);
        $workbook = new Writer($dir.$filename);
        $workbook->setVersion(8); // Add UTF-8 support
        $formatHeader = $workbook->addFormat();
        $formatHeader->setBold();
        $formatHeader->setAlign('center');
        $formatHeader->setFontFamily('Arial');
        $formatNumber = $workbook->addFormat();
        $formatNumber->setNumFormat('$0.00');
        $formatNumber->setAlign('center');
        $workSheet = $workbook->addWorksheet($this->getParam('report') . ' Mensual');
        $workSheet->setInputEncoding("UTF-8");
        $workSheet->setColumn(0, 0, 20); // Set condomino column width
        $workSheet->setColumn(1, 1, 20); // Set amount column width
        $workSheet->setColumn(2, 2, 20); // Set amoun_paid use column width
        $workSheet->setColumn(3, 3, 25); // Set result column width
        $workSheet->write(
            0, 0, 'Ika Bienes Raíces',
            $formatHeader
        );
        $workSheet->setMerge(0, 0, 0, 3);
        $workSheet->write(1, 0, 'Mes', $formatHeader);
        $workSheet->write(1, 1, 'Cuotas generadas', $formatHeader);
        $workSheet->write(1, 2, 'Cobros generados', $formatHeader);
        $workSheet->write(1, 3, 'Saldo final', $formatHeader);
        $i = 2;
        foreach ($values as $value) {
            $workSheet->write($i, 0, $value['mount']);
            $result += ($value['resultAmount'] - $value['resultAmountPaid']);
            $resultAmount += $value['resultAmount'];
            $resultAmountPaid += $value['resultAmountPaid'];
            $workSheet->write($i, 1, $value['resultAmount'], $formatNumber);
            $workSheet->write($i, 2, $value['resultAmountPaid'], $formatNumber);
            $workSheet->write($i, 3, ($value['resultAmount'] - $value['resultAmountPaid']), $formatNumber);
            $i++;
        }
        $workSheet->write($i, 0, 'Total', $formatHeader);
        $workSheet->write($i, 1, $resultAmount, $formatNumber);
        $workSheet->write($i, 2, $resultAmountPaid, $formatNumber);
        $workSheet->write($i, 3, $result, $formatNumber);
        $workbook->close(); // Write the file
    }

    /**
     * @param  array $values
     * @return array
     */
    public function selecMonth($values)
    {
        $i =0;
        foreach ($values as $value) {
            $mount = explode('-', $value['date']);
            switch ($mount[1]) {
                case '01':

                    $value['mount'] = 'Enero';
                    break;

                case '02':

                    $value['mount'] = 'Febrero';
                    break;

                case '03':

                    $value['mount'] = 'Marzo';
                    break;

                case '04':

                    $value['mount'] = 'Abril';
                    break;

                case '05':

                    $value['mount'] = 'Mayo';
                    break;

                case '06':

                    $value['mount'] = 'Junio';
                    break;

                case '07':

                    $value['mount'] = 'Julio';
                    break;

                case '08':

                    $value['mount'] = 'Agosto';
                    break;

                case '09':

                    $value['mount'] = 'Septiembre';
                    break;

                case '10':

                    $value['mount'] = 'Octubre';
                    break;

                case '11':

                    $value['mount'] = 'Noviembre';
                    break;

                case '12':

                    $value['mount'] = 'Diciembre';
                    break;
            }
            $balance[$i] = $value;
            $i ++;
        }

        return $balance;
    }
}
