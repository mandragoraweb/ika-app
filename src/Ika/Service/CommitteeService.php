<?php
namespace Ika\Service;

use Mandragora\Service\CrudService;
use Mandragora\View\Helper\UrlHelper;

class CommitteeService extends CrudService
{
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/committee.php';
        $this->routes = [
            'list' => 'committee_list',
            'save' => 'committee_save',
        ];
        $this->scripts = [
           'create' => 'committee/create.html.twig',
        ];
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $form = $this->getForm()->prepareForCreating();
        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        $form->setUsers($this->repository->findAll($this->getParams()));

        return array('form' => $form);
    }

    /**
     * @return boolean
     */
    public function isValid()
    {

        $this->getForm()->setUsers(
            $this->repository->findAll($this->getParams()
            )
        );

        $form = $this->getForm();

        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * Save a new entity to the dta source
     */
    public function save()
    {
        $values = $this->getForm()->getData();
        $values['username'] = $values['user'];
        unset($values['user']);
        $values['zone_id'] = $this->getParam('zoneId');

        $this->getRepository()->insertUserZone($values);
    }

    /**
     * return User[]
     */
    public function fetchAll()
    {
        $items = $this->repository->findAll($this->getParams());
        $count = $this->repository->count($this->getParams());
        $this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));

        return ['paginator' => $this->getPaginator(), 'params' => $this->getParams()];
    }

    /**
     * @return array
     */
    public function show()
    {
        return [
            'entity' => $this->retrieveOneByUserZone(),
            'params' => $this->getParams()
        ];
    }

    /**
     * @return array
     */
    public function retrieveOneByUserZone()
    {
        return $this->getRepository()->findByUserZone($this->getParams());
    }

    /**
     * @return int The number of affected rows
     */
    public function delete()
    {
        return $this->getRepository()->deleteUserZone($this->getParams());
    }
}
