<?php
namespace Ika\Service;

use \Mandragora\View\Helper\UrlHelper;
use \Mandragora\Service\CrudService;
use \Zend\Authentication\AuthenticationService;
use \DateTime;

class ZonePaymentService extends CrudService
{
    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @var FeeService
     */
    protected $serviceFee;

    /**
     * @var ZoneService
     */
    protected $serviceZone;

    /**
     * @var \Zend\Form\FormInterface
     */
    protected $personalizeForm;

    /**
     * @return FeeService
     */
    public function getServiceFee()
    {
        return $this->serviceFee;
    }

    /**
     * @param FeeService $serviceFee
     */
    public function setServiceFee(FeeService $serviceFee)
    {
        $this->serviceFee = $serviceFee;
    }

    /**
     * @param AbstractService $user
     */
    public function setServiceZone(ZoneService $service)
    {
        $this->serviceZone = $service;
    }

    /**
     * @return AbstractService
     */
    public function getServiceZone()
    {
        return $this->serviceZone;
    }

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/zone-payment.php';
        $this->routes = array(
            'list' => 'zone-payment_list',
            'save' => 'zone-payment_save',
            'update' => 'zone-payment_update',
            'edit' => 'zone-payment_edit',
        );
        $this->scripts = array(
            'create' => 'zone-payment/create.html.twig',
            'edit' => 'zone-payment/edit.html.twig',
        );
    }

    /**
     * @return \Zend\Form\FormInterface
     */
    public function getFormPersonalize(array $messages = null)
    {
        if (!$this->personalizeForm) {
            $this->personalizeForm = $this->buildForm(require 'config/forms/zone-payment-personalize.php');
        }

        if (is_array($messages)) {
            $this->personalizeForm->setMessages($messages);
        }

        return $this->personalizeForm;
    }

    /**
     * @return array
     */
    public function getPersonalizeForm(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getFormPersonalize();
        $form->setZones($zones);

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('list'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $this->getForm()->setZones($zones);

        $form = $this->getForm()->prepareForCreating();

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return array
     */
    public function fetchAll()
    {
        $items = $this->getRepository()->findAll($this->getParams());
        $count = $this->getRepository()->count($this->getParams());

        return array('items' => $items, 'count' => $count, 'params' => $this->getParams());
    }

    /**
     * Save a new entity to the dta source
     */
    public function save()
    {
        $values = $this->getForm()->getData();
        $values['date_record'] = (new DateTime('now'))->format('Y-m-d');

        return $this->getRepository()->insert($values);
    }

    /**
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());

        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();
        $this->getForm()->setZones($zones);

        $fees = $this->getServiceFee()->retrieveAllFeesByZone($values['zone_id']);
        $this->getForm()->setFees($fees);

        $form = $this->getForm()->prepareForEditing();
        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return void
     */
    public function update()
    {
        $values = $this->getForm()->getData();

        date_default_timezone_set('UTC');
        $date = date('Y-m-d');

        $values['date_record'] = $date;

        $this->getRepository()->update($values);
    }

    /**
     * @return boolean
     */
    public function isPersonalizeValid()
    {
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getFormPersonalize();

        $form->setZones($zones);

        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $fees = $this->getServiceFee()->retrieveAllFees();
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();
        $options = [];
        foreach ($zones as $zone) {
            $options[$zone['zone_id']] = $zone['name'];
        }

        $this->getForm()->setFees($fees);
        $this->getForm()->setZoneOptions($options);

        $form = $this->getForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return array
     */
    public function show()
    {
        return array('entity' => $this->retrieveOne(), 'params' => $this->getParams());
    }

    /**
     * retrieve all properties by zone
     */
    public function retrievePropertyByZone()
    {
        return array(
            'fees' => $this->getServiceFee()->getRepository()->findAllByZone($this->getParams())
        );
    }
}
