<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Ika\Service\ProfileService;
use \Ika\Service\UserService;
use \Mandragora\View\Helper\UrlHelper;
use \Mandragora\Password\SecurePasswordGenerator;
use \Zend\Form\Factory;

class ClientService extends CrudService
{
    /**
     * @var ProfileService
     */
    protected $serviceProfile;

    /**
     * @var UserService
     */
    protected $serviceUser;

    /**
     * @var PropertyService
     */
    protected $propertyService;

    /**
     * @var ZoneService
     */
    protected $zoneService;

    /**
     * @var \Mandragora\Password\SecurePasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @var \Ika\Form\ChangeClientPasswordForm
     */
    protected $changePasswordForm;

    /**
     * @var array
     */
    protected $client;

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/client.php';
        $this->routes = array(
            'list' => 'client_list',
            'save' => 'client_save',
            'update' => 'client_update',
            'new_password' => 'client_new_password',
        );
        $this->scripts = array(
            'create' => 'client/create.html.twig',
            'create_property' => 'client/create-property.html.twig',
            'edit' => 'client/edit.html.twig',
            'change_password' => 'client/change-password.html.twig',
        );
    }

    /**
     * @return
     */
    public function getServiceProfile()
    {
        return $this->serviceProfile;
    }

    /**
     * @param $serviceProfile
     */
    public function setServiceProfile($serviceProfile)
    {
        $this->serviceProfile = $serviceProfile;
    }

    /**
     * @return UserService
     */
    public function getServiceUser()
    {
        return $this->serviceUser;
    }

    /**
     * @param UserService $serviceUser
     */
    public function setServiceUser(UserService $serviceUser)
    {
        $this->serviceUser = $serviceUser;
    }

    /**
     * @return PropertyService
     */
    public function getPropertyService()
    {
        $clientId = $this->propertyService->getParam('clientId');
        $this->propertyService->setParams($this->getParams());
        $this->propertyService->setParam('clientId', $clientId);

        return $this->propertyService;
    }

    /**
     *
     * @param $propertyService
     */
    public function setPropertyService(PropertyService $propertyService)
    {
        $this->propertyService = $propertyService;
    }

    /**
     * @return ZoneService
     */
    public function getZoneService()
    {
        return $this->zoneService;
    }

    /**
     * @param ZoneService $zoneService
     */
    public function setZoneService(ZoneService $zoneService)
    {
        $this->zoneService = $zoneService;
    }

    /**
     * @return \Mandragora\Password\SecurePasswordGenerator
     */
    public function getPasswordGenerator()
    {
        return $this->passwordGenerator;
    }

    /**
     * @param \Mandragora\Password\SecurePasswordGenerator $passwordGenerator
     */
    public function setPasswordGenerator(SecurePasswordGenerator $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
    }

    /**
     * @return array
     */
    public function initChangePasswordForm(UrlHelper $urlHelper = null)
    {
        $form = $this->getChangePasswordForm();

        $form->setClientId($this->getParam('clientId'));
        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('new_password'), $this->getParams())
        );

        return array('form' => $this->changePasswordForm, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isChangePasswordFormValid()
    {
        $form = $this->getChangePasswordForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * Verify that both the user and client entries exist in the datasource
     *
     * @return boolean
     */
    public function clientExist()
    {
        $this->client = $this->getRepository()->find($this->getParams());
        if (!$this->client) {
            $this->getChangePasswordForm()->setClientNotFoundErrorMessage('El cliente no existe');

            return false;
        }

        return true;
    }

    /**
     * @return @void
     */
    public function changePassword()
    {
        $values = $this->getChangePasswordForm()->getData();

        $credentials = [
            'password' => $this->getPasswordGenerator()->create($values['new-password']),
            'username' => $this->client['username'],
        ];

        $this->getServiceUser()->getRepository()->update($credentials);
    }

    /**
     * @return \Ika\Form\ClientForm
     */
    protected function getChangePasswordForm()
    {
        if (!$this->changePasswordForm) {
            $this->changePasswordForm = $this->buildForm(require 'config/forms/change-password.php');
        }

        return $this->changePasswordForm;
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $this->getZoneService()->setParams($this->getParams());
        $zones = $this->getZoneService()->retrieveAllZones();

        $this->getForm()->setZones($zones);

        $form = $this->getForm()->prepareForCreating();

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * Save a new entity to the dta source
     */
    public function save()
    {
        $profileValues = $this->getForm()->getProfileData();
        $profileId = $this->getServiceProfile()->getRepository()->insert($profileValues);

        $clientValues = $this->getForm()->getClientData();
        $clientValues['profile_id'] = $profileId;
        $clientId = $this->getRepository()->insert($clientValues);

        $userValues = $this->getForm()->getUserData();

        $userValues['username'] = $userValues['user'];
        unset($userValues['user']);
        $userValues['role'] = 'client';
        $userValues['profile_id'] = $profileId;

        $this->getServiceUser()->getRepository()->insert($userValues);

        $propertyValues = $this->getForm()->getPropertyData();

        if ((integer) $propertyValues['existing_property'] == 1) {
            unset($propertyValues['existing_property']);
            $propertyValues['client_id'] = $clientId;

            $this->getPropertyService()->getRepository()->update($propertyValues);
        }

        return $clientId;
    }

    /**
     * @return \Mandragora\Form\CrudForm
     */
    protected function getFormEditing()
    {
        if (!$this->form) {
            $this->form = $this->buildFormEditing(require $this->formOptionsPath);
        }

        return $this->form;
    }

    /**
     * @param  array                    $spec
     * @return \Zend\Form\FormInterface
     */
    protected function buildFormEditing(array $spec)
    {
        if (!$this->formFactory) {
            $this->formFactory = new Factory();
        }

        $spec['input_filter']['user']['required'] = false;
        $spec['input_filter']['password']['required'] = false;

        return $this->formFactory->create($spec);
    }

    /**
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());
        $values['user'] = $values['username'];

        $form = $this->getFormEditing()->prepareForEditing();

        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $form = $this->getFormEditing();
        $form->prepareForEditing();
        $form->setData($this->getParams());
        if ($form->get('user')->getAttribute('type') === 'hidden') {
            $form->setValidationGroup(
                'first_name', 'paternal_surname', 'maternal_surname', 'birth', 'social_reason',
                'car', 'email', 'phone', 'mobile', 'rfc', 'profile_id', 'client_id',
                'opening_balance', 'balance_date'
            );
        }

        return $form->isValid();
    }

    /**
     * @return bool
     */
    public function isCreationValid()
    {
        $values = $this->getParams();
        $form = $this->getForm();

        if ( (integer) $values['existing_property'] == 1) {
            $this->getZoneService()->setParams($this->getParams());

            $zones = $this->getZoneService()->retrieveAllZones();
            $properties = $this->getPropertyService()->retrieveAllProperties();

            $form->setZones($zones);
            $form->setProperties($properties);
        }

        $form->setData($values);

        return $form->isValid();
    }

    /**
     * @return void
     */
    public function update()
    {
        $profileValues = $this->getForm()->getProfileData();
        $this->getServiceProfile()->getRepository()->update($profileValues);

        $clientValues = $this->getForm()->getClientData();
        $clientValues['profile_id'] = $profileValues['profile_id'];
        $this->getRepository()->update($clientValues);
    }

    /**
     * @return int The number of affected rows
     */
    public function delete()
    {
        $values = $this->getRepository()->find($this->getParams());
        $idClient['clientId'] = $values['client_id'];
        $this->getRepository()->delete($idClient);
        $username['username'] = $values['username'];
        $this->getServiceUser()->getRepository()->delete($username);
        $idProfile['profileId'] = $values['profile_id'];
        $this->getServiceProfile()->getRepository()->delete($idProfile);

        return ;

    }

    /**
     * return array
     */
    public function retrieveAllClients()
    {
        return $this->getRepository()->findAllWithoutFilter($this->getParams());
    }

    /**
     * @param array $values
     */
    public function retrieveClientInformation($values)
    {
        return $this->getRepository()->findAllWithUserAndProfileInfo($values);
    }

    /**
     * @param array $values
     */
    public function retrieveZoneAdministrator($values)
    {
        return $this->getRepository()->findZoneAdministrator($values);
    }
}
