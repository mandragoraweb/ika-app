<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Mandragora\View\Helper\UrlHelper;
use \Ika\Service\UserService;

class ZoneService extends CrudService
{
    /**
     * @var UserService
     */
    protected $serviceUser;

    /**
     * @return UserService
     */
    public function getServiceUser()
    {
        return $this->serviceUser;
    }

    /**
     * @param UserService $serviceUser
     */
    public function setServiceUser(UserService $serviceUser)
    {
        $this->serviceUser = $serviceUser;
    }

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/zone.php';
        $this->routes = array(
            'list' => 'zone_list',
            'save' => 'zone_save',
            'update' => 'zone_update',
        );
        $this->scripts = array(
            'create' => 'zone/create.html.twig',
            'edit' => 'zone/edit.html.twig',
        );
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $users = $this->getServiceUser()->retriveAllByAdminRole();

        $this->getForm()->setUsers($users);

        $form = $this->getForm()->prepareForCreating();

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());
        $values['user'] = $values['username'];
        $users = $this->getServiceUser()->retriveAllByAdminRole();

        $this->getForm()->setUsers($users);
        $this->getForm()->setAdminUsername($values['username']);

        $form = $this->getForm()->prepareForEditing();
        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $users = $this->getServiceUser()->retriveAllByAdminRole();

        $this->getForm()->setUsers($users);

        $form = $this->getForm();

        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * return array
     */
    public function retrieveAllZones()
    {
        return $this->getRepository()->findAllWithoutFilter($this->getParams());
    }

    /**
     * Save a new entity to the dta source
     */
    public function save()
    {
        $values = $this->getForm()->getData();
        $username = $values['user'];
        unset($values['user']);
        unset($values['admin_username']);

        $zoneId = $this->getRepository()->insert($values);

        $this->getRepository()->insertUserZone(['username' => $username, 'zone_id' => $zoneId]);
    }

    /**
     * @return void
     */
    public function update()
    {
        $values = $this->getForm()->getData();
        $username = $values['user'];
        $admin = $values['admin_username'];
        unset($values['user']);
        unset($values['admin_username']);

        $this->getRepository()->update($values);
        $this->getRepository()->updateUserZone(
            ['username' => $username, 'zone_id' => $values['zone_id']],
            ['username' => $admin, 'zone_id' => $values['zone_id']]
        );
    }
}
