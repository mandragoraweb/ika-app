<?php
namespace Ika\Service;

use \Mandragora\Service\CrudService;
use \Mandragora\View\Helper\UrlHelper;
use \Ika\Service\TypeService;
use \Ika\Service\ConceptService;
use \Ika\Service\ZoneService;

class FeeService extends CrudService
{

    /**
     * @var TypeService
     */
    protected $serviceType;

    /**
     * @var ConceptService
     */
    protected $serviceConcept;

    /**
     * @var ZoneService
     */
    protected $serviceZone;

    /**
     * @return TypeService
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }

    /**
     * @param TypeService $serviceType
     */
    public function setServiceType(TypeService $serviceType)
    {
        $this->serviceType = $serviceType;
    }

    /**
     * @return ConceptService
     */
    public function getServiceConcept()
    {
        return $this->serviceConcept;
    }

    /**
     * @param ConceptService $serviceConcept
     */
    public function setServiceConcept(ConceptService $serviceConcept)
    {
        $this->serviceConcept = $serviceConcept;
    }

    /**
     * @param ZoneService $service
     */
    public function setServiceZone(ZoneService $service)
    {
        $this->serviceZone = $service;
    }

    /**
     * @return ZoneService
     */
    public function getServiceZone()
    {
        return $this->serviceZone;
    }

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/fee.php';
        $this->routes = array(
            'list' => 'fee_list',
            'save' => 'fee_save',
            'update' => 'fee_update',
        );
        $this->scripts = array(
            'create' => 'fee/create.html.twig',
            'edit' => 'fee/edit.html.twig',
        );
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $types = $this->getServiceType()->retrieveAllTypes();
        $concepts = $this->getServiceConcept()
                         ->retrieveAllConcepts();

        $this->getForm()->setConcepts($concepts);
        $this->getForm()->setTypes($types);
        $this->getForm()->setZone($this->getParams());

        $form = $this->getForm()->prepareForCreating();
        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * return array
     */
    public function fetchAll()
    {
        $items = $this->getRepository()->findAll($this->getParams());
        $count = $this->getRepository()->count($this->getParams());

        $zone = $this->getServiceZone()->getRepository()->find($this->getParams());

        $this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));

        return array('paginator' => $this->getPaginator(), 'zone' => $zone, 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());

        $types = $this->getServiceType()->retrieveAllTypes();
        $concepts = $this->getServiceConcept()
                         ->retrieveAllConcepts($this->getParam('zoneId'));

        $this->getForm()->setConcepts($concepts);
        $this->getForm()->setTypes($types);

        $form = $this->getForm()->prepareForEditing();
        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        $types = $this->getServiceType()->retrieveAllTypes();
        $concepts = $this->getServiceConcept()
                         ->retrieveAllConcepts();

        $this->getForm()->setConcepts($concepts);
        $this->getForm()->setTypes($types);

        $form = $this->getForm();
        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return array
     */
    public function show()
    {
        return array('entity' => $this->retrieveOne(), 'params' => $this->getParams());
    }

    /**
     * return array
     */
    public function retrieveAllFees()
    {
        return $this->getRepository()->findAllWithoutFilter();
    }

    /**
     * @param  int   $zoneId
     * @return array
     */
    public function retrieveAllFeesByZone($zoneId)
    {
        return $this->getRepository()->findAllByZone(array('zoneId' => $zoneId));
    }
}
