<?php
namespace Ika\Service;

use \Mandragora\View\Helper\UrlHelper;
use \Mandragora\Service\CrudService;
use \Zend\Authentication\AuthenticationService;
use \Ika\Service\FeeService;
use \Ika\Service\ZoneService;
use \Ika\Service\PropertyService;

class PaymentService extends CrudService
{

    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @var FeeService
     */
    protected $serviceFee;

    /**
     * @var ZoneService
     */
    protected $serviceZone;

    /**
     * @var PropertyService
     */
    protected $serviceProperty;

    /**
     * @var \Zend\Form\FormInterface
     */
    protected $personalizeForm;

    /**
     * @return FeeService
     */
    public function getServiceFee()
    {
        return $this->serviceFee;
    }

    /**
     * @param FeeService $serviceFee
     */
    public function setServiceFee(FeeService $serviceFee)
    {
        $this->serviceFee = $serviceFee;
    }

    /**
     * @param AbstractService $user
     */
    public function setServiceZone(ZoneService $service)
    {
        $this->serviceZone = $service;
    }

    /**
     * @return AbstractService
     */
    public function getServiceZone()
    {
        return $this->serviceZone;
    }

    /**
     * @return PropertyService
     */
    public function getServiceProperty()
    {
        return $this->serviceProperty;
    }

    /**
     * @param PropertyService $serviceProperty
     */
    public function setServiceProperty(PropertyService $serviceProperty)
    {
        $this->serviceProperty = $serviceProperty;
    }

    /**
     * @return AuthenticationService
     */
    protected function getAuthService()
    {
        return $this->authService;
    }

    /**
     * @param AuthenticationService $authService
     */
    public function setAuthService(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Initialize form options and routes names
     */
    public function __construct()
    {
        $this->formOptionsPath = 'config/forms/payment.php';
        $this->routes = array(
            'list' => 'payment_list',
            'create' => 'payment_create',
            'save' => 'payment_save',
            'update' => 'payment_update',
            'edit' => 'payment_edit',
            'personalize' => 'payment_personalize'
        );
        $this->scripts = array(
            'create' => 'payment/create.html.twig',
            'edit' => 'payment/edit.html.twig',
            'detail' => 'payment/customize.html.twig',
        );
    }

    /**
     * @return \Zend\Form\FormInterface
     */
    public function getFormPersonalize(array $messages = null)
    {
        if (!$this->personalizeForm) {
            $this->personalizeForm = $this->buildForm(require 'config/forms/payment-personalize.php');
        }

        if (is_array($messages)) {
            $this->personalizeForm->setMessages($messages);
        }

        return $this->personalizeForm;
    }

    /**
     * @return array
     */
    public function getPersonalizeForm(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $form = $this->getFormPersonalize();
        $form->setZones($zones);

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('list'), $this->getParams())
        );

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return array
     */
    public function getFormForCreating(UrlHelper $urlHelper)
    {
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();

        $this->getForm()->setZones($zones);

        $form = $this->getForm()->prepareForCreating();

        $form->setAttribute(
            'action', $urlHelper->url($this->getRoute('save'), $this->getParams())
        );

        return array('form' => $form);
    }

    /**
     * @return array
     */
    public function fetchAll()
    {
        $items = $this->getRepository()->findAll($this->getParams());
        $count = $this->getRepository()->count($this->getParams());

        /*$this->getPaginator()
             ->setCurrentItems($count, $items)
             ->setCurrentPage($this->getParam('page', 1));*/

        return array('items' => $items, 'params' => $this->getParams());
    }

    /**
     * Save a new entity to the dta source
     */
    public function save()
    {
        $user = $this->getAuthService()->getIdentity();

        $values = $this->getForm()->getData();

        unset($values['zone_id']);
        unset($values['opening_balance']);
        unset($values['empty_payment']);

        $values['username'] = $user['username'];
        $values['date_record'] = date('Y-m-d');
        $clientId = $this->getServiceProperty()->retrieveClientId($values['property_id']);

        $values['client_id'] = $clientId;

        return $this->getRepository()->insert($values);
    }

    /**
     * @param  UrlHelper $urlHelper
     * @return array
     */
    public function edit(UrlHelper $urlHelper)
    {
        $values = $this->retrieveOne($this->getParams());

        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();
        $this->getForm()->setZones($zones);

        $properties = $this->getServiceProperty()->retrieveAllByZone($values['zone_id']);
        $this->getForm()->setProperties($properties);

        $fees = $this->getServiceFee()->retrieveAllFeesByZone($values['zone_id']);
        $this->getForm()->setFees($fees);

        $form = $this->getForm()->prepareForEditing();

        if (is_null($values['folio_receipt'])) {
            $form->get('empty_payment')->setAttributes(['checked' => 'checked', 'disabled' => 'disabled']);
            $form->get('folio_receipt')->setAttributes(['disabled' => 'disabled']);
            $form->get('amount_paid')->setAttributes(['disabled' => 'disabled']);
        }

        $action = $urlHelper->url($this->getRoute('update'), $this->getParams());

        $form->setAttribute('action', $action);
        $form->setData($values);

        return array('form' => $form, 'params' => $this->getParams());
    }

    /**
     * @return void
     */
    public function update()
    {
        $user = $this->getAuthService()->getIdentity();

        $values = $this->getForm()->getData();

        unset($values['zone_id']);
        unset($values['opening_balance']);
        unset($values['empty_payment']);

        date_default_timezone_set('UTC');
        $date = date('Y-m-d');

        $values['username'] = $user['username'];
        $values['date_record'] = $date;

        $this->getRepository()->update($values);
    }

    /**
     * @return boolean
     */
    public function isPersonalizeValid()
    {
        $zones = $this->getServiceZone()->retrieveAllZones();
        $properties = $this->getServiceProperty()->retrieveAllProperties();

        $form = $this->getFormPersonalize();

        $form->setZones($zones);
        $form->setProperties($properties);

        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @return boolean
     */
    public function isValid()
    {

        $values = $this->getParams();

        $form = $this->prepareForm($this->getForm());

        if ((integer) $values['empty_payment'] == 1) {
            $form->getInputFilter()->remove('folio_receipt');
            $form->getInputFilter()->remove('amount_paid');
        }

        $form->setData($values);

        return $form->isValid();
    }

    /**
     * @return boolean
     */
    public function isEditingValid()
    {
        $form = $this->prepareForm($this->getForm());
        $form->prepareForEditing();

        $form->setData($this->getParams());

        return $form->isValid();
    }

    /**
     * @param  \Mandragora\Form\CrudForm $form
     * @return \Mandragora\Form\CrudForm
     */
    public function prepareForm($form)
    {
        $fees = $this->getServiceFee()->retrieveAllFees();
        $this->getServiceZone()->setParams($this->getParams());
        $zones = $this->getServiceZone()->retrieveAllZones();
        $properties = $this->getServiceProperty()->retrieveAllProperties();

        $form->setFees($fees);
        $form->setZones($zones);
        $form->setProperties($properties);

        return $form;
    }

    /**
     * @return array
     */
    public function show()
    {
        return array('entity' => $this->retrieveOne(), 'params' => $this->getParams());
    }

    /**
     * retrieve all properties by zone
     */
    public function retrievePropertyByZone()
    {
        return array(
            'properties' => $this->getServiceProperty()->getRepository()->findAllPropertiesByZone($this->getParams()),
            'fees' => $this->getServiceFee()->getRepository()->findAllByZone($this->getParams())
        );
    }

    /**
     * retrieve opening balance
     */
    public function retrieveOpeningBalanceByProperty()
    {
        return array(
            'openBalance' => $this->getServiceProperty()->retrieveOpeningBalance($this->getParams())
        );
    }
}
