<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class FeeForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove zone_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('fee_id');

        return $this;
    }

    /**
     * @param array $values
     */
    public function setZone($values)
    {
        $this->get('zone_id')->setValue($values['zoneId']);
    }

    /**
     * @param array $types
     */
    public function setTypes($types)
    {
        $valuesType = array();

        foreach ($types as $type) {
            $valuesType[$type['type_id']] = $type['description'];
        }

        return $this->get('type_id')->setValueOptions($valuesType);;
    }

    /**
     * @param array $users
     */
    public function setConcepts($concepts)
    {
        $valuesConcept = array();

        foreach ($concepts as $concept) {
            $valuesConcept[$concept['concept_id']] = $concept['concept'];
        }

        return $this->get('concept_id')->setValueOptions($valuesConcept);;
    }
}
