<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class PaymentForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove zone_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('payment_id');

        return $this;
    }

    public function prepareForEditing()
    {
        $this->getInputFilter()->remove('folio_receipt');

        return $this;
    }

    /**
     * @param aray $zones
     */
    public function setZones($zones)
    {
        $valuesZones = array();

        foreach ($zones as $zone) {
            $valuesZones[$zone['zone_id']] = $zone['name'];
        }

        return $this->get('zone_id')->setValueOptions($valuesZones);;
    }

    /**
     * @param array $properties
     */
    public function setProperties($properties)
    {
        $valuesProperty = array();

        foreach ($properties as $property) {
            $valuesProperty[$property['property_id']] = $property['street'].' '.$property['number'];
        }

        return $this->get('property_id')->setValueOptions($valuesProperty);;
    }

    /**
     * @param array $fees
     */
    public function setFees($fees)
    {
        $valuesFee = array();

        foreach ($fees as $fee) {
            $valuesFee[$fee['fee_id']] = $fee['concept'] .'('. $fee['amount'] .')';
        }

        return $this->get('fee_id')->setValueOptions($valuesFee);;
    }
}
