<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class PaymentPersonalizeForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        return $this;

    }

    /**
     * @param aray $zones
     */
    public function setZones($zones)
    {
        $valuesZones = array();

        foreach ($zones as $zone) {
            $valuesZones[$zone['zone_id']] = $zone['name'];
        }

        return $this->get('zone_id')->setValueOptions($valuesZones);;
    }

    /**
     * @param aray $properties
     */
    public function setProperties($properties)
    {
        $valuesProperty = array();

        foreach ($properties as $property) {
            $valuesProperty[$property['zone_id']] = $property['street'].' '.$property['number'];
        }

        return $this->get('property_id')->setValueOptions($valuesProperty);;
    }
}
