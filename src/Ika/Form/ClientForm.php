<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class ClientForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove client_id and profile_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('client_id');
        $this->remove('profile_id');

        $this->prepare();

        return $this;
    }

    /**
     * Remove client_id and profile_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForEditing()
    {
        $this->getInputFilter()->remove('user');
        $this->get('user')->setAttribute('type', 'hidden');
        $this->remove('password');

        $this->prepare();

        return $this;
    }

    /**
     * @param aray $zones
     */
    public function setZones($zones)
    {
        $valuesZones = array();

        foreach ($zones as $zone) {
            $valuesZones[$zone['zone_id']] = $zone['name'];
        }

        return $this->get('zone_id')->setValueOptions($valuesZones);;
    }

    /**
     * @param array $properties
     */
    public function setProperties($properties)
    {
        $valuesProperty = array();

        foreach ($properties as $property) {
            $valuesProperty[$property['property_id']] = $property['street'].' '.$property['number'];
        }

        return $this->get('property_id')->setValueOptions($valuesProperty);;
    }

    /**
     * @return array
     */
    public function getProfileData()
    {
        $profileValues = $this->getData();

        unset($profileValues['social_reason']);
        unset($profileValues['rfc']);
        unset($profileValues['car']);
        unset($profileValues['phone']);
        unset($profileValues['mobile']);
        unset($profileValues['client_id']);
        unset($profileValues['user']);
        unset($profileValues['username']);
        unset($profileValues['password']);
        unset($profileValues['opening_balance']);
        unset($profileValues['balance_date']);
        unset($profileValues['zone_id']);
        unset($profileValues['property_id']);
        unset($profileValues['existing_property']);

        if (empty($profileValues['birth'])) {
            $profileValues['birth'] = null;
        }

        return $profileValues;
    }

    /**
     * @return array
     */
    public function getClientData()
    {
        $clientValues = $this->getData();

        unset($clientValues['paternal_surname']);
        unset($clientValues['maternal_surname']);
        unset($clientValues['first_name']);
        unset($clientValues['birth']);
        unset($clientValues['user']);
        unset($clientValues['username']);
        unset($clientValues['password']);
        unset($clientValues['email']);
        unset($clientValues['zone_id']);
        unset($clientValues['property_id']);
        unset($clientValues['existing_property']);

        return $clientValues;
    }

    /**
     * @return array
     */
    public function getUserData()
    {
        $userValues = $this->getData();

        unset($userValues['social_reason']);
        unset($userValues['email']);
        unset($userValues['rfc']);
        unset($userValues['car']);
        unset($userValues['phone']);
        unset($userValues['mobile']);
        unset($userValues['client_id']);;
        unset($userValues['paternal_surname']);
        unset($userValues['maternal_surname']);
        unset($userValues['first_name']);
        unset($userValues['birth']);
        unset($userValues['opening_balance']);
        unset($userValues['balance_date']);
        unset($userValues['zone_id']);
        unset($userValues['property_id']);
        unset($userValues['existing_property']);

        return $userValues;
    }

    /**
     * @return array
     */
    public function getPropertyData()
    {
        $propertyValues = $this->getData();

        unset($propertyValues['social_reason']);
        unset($propertyValues['email']);
        unset($propertyValues['rfc']);
        unset($propertyValues['car']);
        unset($propertyValues['phone']);
        unset($propertyValues['mobile']);
        unset($propertyValues['client_id']);;
        unset($propertyValues['paternal_surname']);
        unset($propertyValues['maternal_surname']);
        unset($propertyValues['first_name']);
        unset($propertyValues['birth']);
        unset($propertyValues['opening_balance']);
        unset($propertyValues['balance_date']);
        unset($propertyValues['zone_id']);
        unset($propertyValues['user']);
        unset($propertyValues['profile_id']);

        return $propertyValues;
    }
}
