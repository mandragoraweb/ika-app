<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class TypeForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove type_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('type_id');

        return $this;
    }
}
