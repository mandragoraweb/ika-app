<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class ZonePaymentForm extends CrudForm implements HasZoneElement
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove payment_zone_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('zone_payment_id');

        return $this;
    }

    /**
     * @param aray $zones
     */
    public function setZoneOptions(array $zones)
    {
        $this->get('zone_id')->setValueOptions($zones);
    }

    /**
     * @param ZoneElementHydrator $hydrator
     */
    public function hydrate(ZoneElementHydrator $hydrator)
    {
        $hydrator->configureZoneElement($this);
    }

    /**
     * @param array $fees
     */
    public function setFees($fees)
    {
        $valuesFee = array();

        foreach ($fees as $fee) {
            $valuesFee[$fee['fee_id']] = $fee['concept'] .'('. $fee['amount'] .')';
        }

        return $this->get('fee_id')->setValueOptions($valuesFee);
    }
}
