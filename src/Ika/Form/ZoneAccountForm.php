<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Form;

use Zend\Form\Form;

class ZoneAccountForm extends Form implements HasZoneElement
{
    /** @var string */
    public static $CLASS = __CLASS__;

    /**
     * @param array $options
     */
    public function setZoneOptions(array $options)
    {
        $this->get('zone_id')->setValueOptions($options);
    }

    /**
     * @param ZoneElementHydrator $hydrator
     */
    public function hydrate(ZoneElementHydrator $hydrator)
    {
        $hydrator->configureZoneElement($this);
    }
}
