<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class ZoneForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove zone_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('zone_id');

        return $this;
    }

    /**
     * @param array $users
     */
    public function setUsers($users)
    {
        $valuesUser = array();

        foreach ($users as $user) {
            $valuesUser[$user['username']] = $user['first_name'].' '.$user['paternal_surname'].' '.$user['maternal_surname'];
        }

        return $this->get('user')->setValueOptions($valuesUser);;
    }

    /**
     * @param string $username
     */
    public function setAdminUsername($username)
    {
        return $this->get('admin_username')->setValue($username);;
    }
}
