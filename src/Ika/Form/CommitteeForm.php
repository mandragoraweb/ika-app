<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class CommitteeForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @param aray $users
     */
    public function setUsers($users)
    {
        $options = [];
        foreach ($users as $user) {
            $options[$user['username']] = "{$user['first_name']} {$user['paternal_surname']} {$user['maternal_surname']}";
        }

        return $this->get('user')->setValueOptions($options);
    }
}
