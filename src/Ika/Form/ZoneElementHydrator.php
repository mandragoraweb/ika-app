<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Form;

use Ika\Repository\ZoneRepository;
use Zend\Authentication\AuthenticationService;

class ZoneElementHydrator
{
    /** @type ZoneRepository */
    protected $allZones;

    /** @type AuthenticationService */
    protected $authentication;

    /**
     * @param ZoneRepository $allZones
     */
    public function __construct(ZoneRepository $allZones, AuthenticationService $authentication)
    {
        $this->allZones = $allZones;
        $this->authentication = $authentication;
    }

    /**
     * Add all the zone options values
     */
    public function configureZoneElement(HasZoneElement $form)
    {
        $zones = $this->allZones->supervisedBy($this->authentication->getIdentity());
        $options = [];
        foreach ($zones as $zone) {
            $options[$zone['zone_id']] = $zone['name'];
        }

        $form->setZoneOptions($options);
    }
}
