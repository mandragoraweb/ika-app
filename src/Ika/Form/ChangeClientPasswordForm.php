<?php
namespace Ika\Form;

use Zend\Form\Form;

class ChangeClientPasswordForm extends Form
{
    /**
     * @param string $message
     */
    public function setClientNotFoundErrorMessage($message)
    {
        $this->get('client_id')->setMessages([$message]);
    }

    /**
     * @param int $clientId
     */
    public function setClientId($clientId)
    {
        $this->get('client_id')->setValue($clientId);
    }
}
