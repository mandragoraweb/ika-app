<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class UserForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        return $this;
    }

    /**
     * remove username and password
     *
     * @see \Mandragora\Form\CrudForm::prepareForEditing()
     */
    public function prepareForEditing()
    {
        $this->remove('username');
        $this->remove('password');

        return $this;
    }

    /**
     * @param aray $zone
     */
    public function setProfile($profile)
    {
        return $this->get('profile_id')->setValue((int) $profile['profileId']);
    }
}
