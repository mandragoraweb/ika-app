<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class ZoneExpensesForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove payment_zone_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('zone_expenses_id');

        return $this;
    }

    /**
     * @param aray $zones
     */
    public function setZones($zones)
    {
        $valuesZones = array();

        foreach ($zones as $zone) {
            $valuesZones[$zone['zone_id']] = $zone['name'];
        }

        return $this->get('zone_id')->setValueOptions($valuesZones);;
    }

    /**
     * @param array $fees
     */
    public function setConcepts($concepts)
    {
        $valuesConcept = array();

        foreach ($concepts as $concept) {
            $valuesConcept[$concept['concept_id']] = $concept['concept'] .'('. $concept['code'] .')';
        }

        return $this->get('concept_id')->setValueOptions($valuesConcept);;
    }
}
