<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class SurchargeForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove surcharge_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('surcharge_id');

        return $this;
    }

    /**
     * @param aray $payment
     */
    public function setPayment($payment)
    {
        return $this->get('payment_id')->setValue((int) $payment['paymentId']);
    }
}
