<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class PropertyForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove property_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('property_id');

        return $this;
    }

    /**
     * @param int $zoneId
     */
    public function setZone($zoneId)
    {
        return $this->get('zone_id')->setValue($zoneId);;
    }

    public function setZones(array $zones)
    {
        $options = array();
        foreach ($zones as $zone) {
            $options[$zone['zone_id']] = $zone['name'];
        }

        return $this->get('zone_id')->setValueOptions($options);
    }

    /**
     * @param array $types
     */
    public function setTypes(array $types)
    {
        $valuesType = array();

        foreach ($types as $type) {
            $valuesType[$type['type_id']] = $type['description'];
        }

        return $this->get('type_id')->setValueOptions($valuesType);;
    }

    /**
     * @param int $clientId
     */
    public function setClient($clientId)
    {
        return $this->get('client_id')->setValue($clientId);;
    }

    /**
     * @param array $clients
     */
    public function setClients(array $clients)
    {
        $valuesClient = array();

        foreach ($clients as $client) {
            $valuesClient[$client['client_id']] = $client['first_name'].' '.$client['paternal_surname'].' '.$client['maternal_surname'];
        }

        return $this->get('client_id')->setValueOptions($valuesClient);;
    }
}
