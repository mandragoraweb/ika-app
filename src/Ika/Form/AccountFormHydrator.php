<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Form;

use Ika\Repository\ClientRepository;
use Ika\Repository\PropertyRepository;
use Ika\Repository\ZoneRepository;

class AccountFormHydrator
{
    /** @type ZoneRepository */
    protected $allZones;

    /** @type ClientRepository */
    protected $allHomeowners;

    /** @type PropertyRepository  */
    protected $allProperties;

    /** @type array */
    protected $identity;

    /**
     * @param ZoneRepository     $allZones
     * @param ClientRepository   $allHomeowners
     * @param PropertyRepository $allPorperties
     */
    public function __construct(
        ZoneRepository $allZones,
        ClientRepository $allHomeowners,
        PropertyRepository $allPorperties)
    {
        $this->allZones = $allZones;
        $this->allHomeowners = $allHomeowners;
        $this->allProperties = $allPorperties;
    }

    /**
     * @param array $identity
     */
    public function setCurrentUserIdentity(array $identity)
    {
        $this->identity = $identity;
    }

    /**
     * The client_id form element is required for all roles except for the client role,
     * because it can only see its own account balance
     *
     * @param AccountForm $form
     */
    public function configureClientElement(AccountForm $form)
    {
        if (in_array($this->identity['role'], ['webmaster', 'committee', 'admin'])) {

            $form->makeClientRequired();
            $form->makePropertyRequired();

            return;
        }
        $form->remove('client_id');
        $form->remove('zone_id');
    }

    /**
     * Add all the zone options values
     */
    public function configureZoneElement(AccountForm $form)
    {
        if (!$form->hasZoneElement()) {
            return;
        }

        $zones = $this->allZones->supervisedBy($this->identity);
        $options = [];
        foreach ($zones as $zone) {
            $options[$zone['zone_id']] = $zone['name'];
        }

        $zone = $form->getZoneElement();
        $zone->setValueOptions($options);
    }

    public function configureHomeownerElement(AccountForm $form)
    {
        if (!$form->hasZoneElement()) {
            return;
        }

        $zone = $form->getZoneElement();
        if ($zone->getValue()) {
            $homeowners = $this->allHomeowners->inSubdivision($zone->getValue());
            $options = [];
            foreach ($homeowners as $homeowner) {
                $options[$homeowner['client_id']] = sprintf(
                    '%s %s %s',
                    $homeowner['first_name'],
                    $homeowner['paternal_surname'],
                    $homeowner['maternal_surname']
                );
            }
            $form->getHomeownerElement()->setValueOptions($options);
        }
    }

    public function configurePropertiesElement(AccountForm $form)
    {
        if (in_array($this->identity['role'], ['webmaster', 'committee', 'admin'])) {

            if (!$form->hasClientElement()) {
                return;
            }

            $client = $form->getHomeownerElement();
            if ($client->getValue()) {
                $properties = $this->allProperties->toClient($client->getValue());
                $options = [];
                foreach ($properties as $property) {
                    $options[$property['property_id']] = sprintf(
                        '%s %s',
                        $property['street'],
                        $property['number']
                    );
                }

                $form->getPropertiesElement()->setValueOptions($options);
            }
        }
        if ($this->identity['role'] == 'client') {
            $properties = $this->allProperties->myProperties($this->identity['profile_id']);
            $options = [];
            foreach ($properties as $property) {
                $options[$property['property_id']] = sprintf(
                    '%s %s',
                    $property['street'],
                    $property['number']
                );
            }

            $form->getPropertiesElement()->setValueOptions($options);
        }

    }
}
