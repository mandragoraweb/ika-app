<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class ZoneExpensesPersonalizeForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        return $this;

    }

    /**
     * @param aray $zones
     */
    public function setZones($zones)
    {
        $valuesZones = array();

        foreach ($zones as $zone) {
            $valuesZones[$zone['zone_id']] = $zone['name'];
        }

        return $this->get('zone_id')->setValueOptions($valuesZones);;
    }
}
