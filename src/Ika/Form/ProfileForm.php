<?php
namespace Ika\Form;

use \Mandragora\Form\CrudForm;

class ProfileForm extends CrudForm
{
    /**
     * @var string
     */
    public static $CLASS = __CLASS__;

    /**
     * Remove zone_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        $this->remove('profile_id');

        return $this;
    }

    /**
     * Remove client_id and profile_id element
     *
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForEditing()
    {
        $this->get('user')->setAttribute('type', 'hidden');
        $this->remove('password');

        $this->prepare();

        return $this;
    }

    /**
     * @return array
     */
    public function getProfileData()
    {
        $profileValues = $this->getData();

        unset($profileValues['username']);
        unset($profileValues['user']);
        unset($profileValues['password']);
        unset($profileValues['user_role']);

        if ( empty($profileValues['birth'])) {
            $profileValues['birth'] = null;
        }

        return $profileValues;
    }

    public function getUserData()
    {
        $userValues = $this->getData();
        $userValues['role'] = $userValues['user_role'];
        unset($userValues['user_role']);

        if (!empty($userValues['user'])) {
            $userValues['username'] = $userValues['user'];
            unset($userValues['user']);
        }

        unset($userValues['paternal_surname']);
        unset($userValues['maternal_surname']);
        unset($userValues['first_name']);
        unset($userValues['birth']);
        unset($userValues['email']);

        return $userValues;
    }
}
