<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2013-2014 (http://www.mandragora-web-systems.com)
 */
namespace Ika\Form;

use Mandragora\Form\CrudForm;

class AccountForm extends CrudForm
{
    /** @var string */
    public static $CLASS = __CLASS__;

    /**
     * @see \Mandragora\Form\CrudForm::prepareForCreating()
     */
    public function prepareForCreating()
    {
        return $this;
    }

    /**
     * Populate form with default values
     *
     * @param AccountFormHydrator $hydrator
     */
    public function hydrate(AccountFormHydrator $hydrator)
    {
        $hydrator->configureClientElement($this);
        $hydrator->configureZoneElement($this);
        $hydrator->configureHomeownerElement($this);
        $hydrator->configurePropertiesElement($this);
    }

    /**
     * @return boolean
     */
    public function hasZoneElement()
    {
        return $this->has('zone_id');
    }

    /**
     * @return \Zend\Form\Element\Select
     */
    public function getZoneElement()
    {
        return $this->get('zone_id');
    }

    /**
     * @return bool
     */
    public function hasClientElement()
    {
        return $this->has('client_id');
    }

    /**
     * @return \Zend\Form\Element\Select
     */
    public function getHomeownerElement()
    {
        return $this->get('client_id');
    }

    /**
     * @return \Zend\Form\Element\Select
     */
    public function getPropertiesElement()
    {
        return $this->get('property_id');
    }

    /**
     * The 'client' role needs the client ID as optional, because that role is only allowed to see
     * its own account balance.
     */
    public function makeClientRequired()
    {
        $this->get('client_id')->setAttribute('required', true);
    }

    /**
     *
     */
    public function makePropertyRequired()
    {
        $this->get('property_id')->setAttribute('required', true);
    }
}
