<?php
namespace Ika\Event;

use \Zend\EventManager\EventInterface;

class LoginFormEvent
{
    /**
     * @param EventInterface $e
     */
    public function __invoke(EventInterface $e)
    {
        $this->assignLoginForm($e);
    }

    /**
     * @param EventInterface $e
     */
    public function assignLoginForm(EventInterface $e)
    {
        $controller = $e->getTarget();
        $controller->assign(array('form' => $controller->getUser()->getLoginForm()));
    }
}
