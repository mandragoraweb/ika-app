<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2014 (http://www.mandragora-web-systems.com)
 */
namespace spec\Mandragora\Repository\Specification;

use PhpSpec\ObjectBehavior;
use Doctrine\DBAL\Query\QueryBuilder;

class ByPageSpecificationSpec extends ObjectBehavior
{
    function it_modifies_query_to_show_the_first_10_results(QueryBuilder $qb)
    {
        $this->setLimit(10);
        $this->setCriteria(['page' => 1]);

        $qb->getQueryPart('select')->willReturn([]);
        $qb->setFirstResult(0)->shouldBeCalled();
        $qb->setMaxResults(10)->shouldBeCalled();

        $this->match($qb);
    }

    function it_modifies_query_to_show_the_second_page_of_results(QueryBuilder $qb)
    {
        $this->setLimit(5);
        $this->setCriteria(['page' => 2]);

        $qb->getQueryPart('select')->willReturn([]);
        $qb->setFirstResult(5)->shouldBeCalled();
        $qb->setMaxResults(5)->shouldBeCalled();

        $this->match($qb);
    }
}
