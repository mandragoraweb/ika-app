<?php

namespace spec\Mandragora\EventListener;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Zend\EventManager\EventInterface;
use Mandragora\Controller\CrudController;
use Mandragora\Service\CrudService;
use Mandragora\Controller\HttpController;

class ServiceParamsEventListenerSpec extends ObjectBehavior
{
    function it_passes_request_parameters_to_service_when_the_controller_has_a_service(
        EventInterface $event,
        CrudController $controller,
        CrudService $service
    )
    {
        $params = ['typeId' => '1'];

        $service->setParams($params)->shouldBeCalled();

        $controller->initParams()->shouldBeCalled();
        $controller->getService()->willReturn($service);
        $controller->getParams()->willReturn($params);

        $event->getTarget()->willReturn($controller);

        $this->initServiceParams($event);
    }

    function it_skips_controllers_without_a_service(
        EventInterface $event,
        HttpController $controller
    )
    {
        $controller->initParams()->shouldBeCalled();

        $event->getTarget()->willReturn($controller);

        $this->initServiceParams($event);
    }
}
