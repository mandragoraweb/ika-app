<?php

namespace spec\Mandragora\EventListener;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Zend\EventManager\EventInterface;
use Mandragora\Controller\HttpController;
use Symfony\Component\HttpFoundation\Response;
use Mandragora\View\Renderer;

class ViewRenderingEventListenerSpec extends ObjectBehavior
{
    function let(Renderer $renderer)
    {
        $this->beConstructedWith($renderer);
    }

    function it_skips_rendering_on_redirect(
        EventInterface $event,
        HttpController $controller,
        Response $response
    )
    {
        $event->getTarget()->willReturn($controller);
        $controller->getResponse()->willReturn($response);
        $response->isRedirect()->willReturn(true);

        $this->render($event);
    }

    function it_skips_rendering_if_content_has_been_set(
        EventInterface $event,
        HttpController $controller,
        Response $response
    )
    {
        $event->getTarget()->willReturn($controller);
        $controller->getResponse()->willReturn($response);
        $response->isRedirect()->willReturn(false);
        $response->getContent()->willReturn('Some existing content');

        $this->render($event);
    }

    function it_pass_data_and_selects_the_view_script_to_show_passing_its_output_to_the_response(
        EventInterface $event,
        HttpController $controller,
        Response $response,
        Renderer $renderer
    )
    {
        $data = ['username' => 'montealegreluis'];
        $viewScriptPath = 'user/show.html.twig';
        $content = '<h1>Hello montealegreluis!</h1>';

        $event->getTarget()->willReturn($controller);

        $controller->getResponse()->willReturn($response);
        $controller->getData()->willReturn($data);
        $controller->getViewScript()->willReturn($viewScriptPath);

        $response->isRedirect()->willReturn(false);
        $response->getContent()->willReturn(null);
        $response->setContent($content)->shouldBeCalled();

        $renderer->render()->willReturn($content);
        $renderer->setData($data)->shouldBeCalled();
        $renderer->setTemplatePath($viewScriptPath)->shouldBeCalled();

        $this->render($event);
    }
}
