<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2014 (http://www.mandragora-web-systems.com)
 */
namespace spec\Mandragora\EventListener;

use PhpSpec\ObjectBehavior;
use Zend\Authentication\AuthenticationService;
use Zend\EventManager\EventInterface;
use Mandragora\Controller\CrudController;

class AuthenticationEventListenerSpec extends ObjectBehavior
{
    function let(AuthenticationService $authenticationService)
    {
        $this->beConstructedWith($authenticationService);
    }

    function it_redirects_to_initial_page_when_the_user_is_not_authenticated(
        AuthenticationService $authenticationService,
        EventInterface $event,
        CrudController $controller
    )
    {
        $controller->getAction()->willReturn('createAction');
        $controller->redirect('index_index')->shouldBeCalled();
        $authenticationService->hasIdentity()->willReturn(false);
        $event->getTarget()->willReturn($controller);

        $this->isAuthenticated($event);
    }

    function it_skips_validation_when_the_user_is_in_the_authentication_action(
        AuthenticationService $authenticationService,
        EventInterface $event,
        CrudController $controller
    )
    {
        $controller->getAction()->willReturn('authenticateAction');
        $event->getTarget()->willReturn($controller);

        $this->isAuthenticated($event);
    }

    function it_assigns_the_authenticated_user_role_to_the_controller(
        AuthenticationService $authenticationService,
        EventInterface $event,
        CrudController $controller
    )
    {
        $role = ['role' => 'webmaster'];
        $user = ['username' => 'montealegreluis', 'role' => 'webmaster'];

        $controller->getAction()->willReturn('createAction');
        $controller->assign($role)->shouldBeCalled();
        $controller->setParam('role', $user['role'])->shouldBeCalled();
        $controller->setParam('username', $user['username'])->shouldBeCalled();
        $authenticationService->hasIdentity()->willReturn(true);
        $authenticationService->getIdentity()->willReturn($user);
        $event->getTarget()->willReturn($controller);

        $this->isAuthenticated($event);
    }
}
