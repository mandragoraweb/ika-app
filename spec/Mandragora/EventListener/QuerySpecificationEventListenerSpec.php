<?php

namespace spec\Mandragora\EventListener;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Zend\EventManager\EventInterface;
use Doctrine\DBAL\Query\QueryBuilder;
use Mandragora\Repository\Specification\AbstractSpecification;

class QuerySpecificationEventListenerSpec extends ObjectBehavior
{
    function it_sets_the_criteria_and_matches_the_query_with_the_specification(
        EventInterface $event,
        QueryBuilder $qb,
        AbstractSpecification $specification
    )
    {
        $criteria = ['page' => 1];

        $this->beConstructedWith($specification);

        $event->getParam('criteria')->willReturn($criteria);
        $event->getParam('qb')->willReturn($qb);

        $specification->setCriteria($criteria)->shouldBeCalled();
        $specification->match($qb)->shouldBeCalled();

        $this->matchQuery($event);
    }
}
