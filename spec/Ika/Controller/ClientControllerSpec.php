<?php

namespace spec\Ika\Controller;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ika\Service\ClientService;
use Mandragora\View\Helper\UrlHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ika\Form\ClientForm;

class ClientControllerSpec extends ObjectBehavior
{
    function let(Request $request, Response $response)
    {
        $this->beConstructedWith($request, $response);
    }

    function it_renders_change_password_form_again_if_form_validation_fails(
        ClientService $clientService,
        UrlHelper $urlHelper,
        ClientForm $form
    )
    {
        $viewData = ['form' => $form, 'params' => ['clientId' => 1]];

        $clientService->isChangePasswordFormValid()->willReturn(false);
        $clientService->initChangePasswordForm($urlHelper)->willReturn($viewData);
        $clientService->getScript('change_password')->shouldBeCalled();

        $this->setService($clientService);
        $this->setUrlHelper($urlHelper);
        $this->changePasswordAction();
    }

    function it_renders_change_password_form_again_if_client_does_not_exist(
        ClientService $clientService,
        UrlHelper $urlHelper,
        ClientForm $form
    )
    {
        $viewData = ['form' => $form, 'params' => ['clientId' => 1]];

        $clientService->isChangePasswordFormValid()->willReturn(true);
        $clientService->clientExist()->willReturn(false);
        $clientService->initChangePasswordForm($urlHelper)->willReturn($viewData);
        $clientService->getScript('change_password')->shouldBeCalled();

        $this->setService($clientService);
        $this->setUrlHelper($urlHelper);
        $this->changePasswordAction();
    }

    function it_changes_password_and_redirects_to_the_clients_list_when_values_are_valid(
        ClientService $clientService,
        UrlHelper $urlHelper
    )
    {
        $listRoute = 'client_list';

        $clientService->isChangePasswordFormValid()->willReturn(true);
        $clientService->clientExist()->willReturn(true);
        $clientService->changePassword()->shouldBeCalled();
        $clientService->getRoute('list')->willReturn($listRoute);

        $urlHelper->url($listRoute, [])->willReturn('/condominos/listar');

        $this->setUrlHelper($urlHelper);
        $this->setService($clientService);
        $this->changePasswordAction();
    }
}
