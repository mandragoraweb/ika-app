<?php
namespace spec\Ika\Repository;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Mandragora\Database\DbalConnection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Driver\Statement;
use Zend\EventManager\EventManagerInterface;

class TypeRepositorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Ika\Repository\TypeRepository');
    }

    function it_finds_property_type_by_id(DbalConnection $connection, QueryBuilder $qb, Statement $st)
    {
        $parameters = array('typeId' => 1);
        $propertyType = array('1' => 'Casa');
        $sql = 'SELECT * FROM type t WHERE t.type_id = :typeId';

        $st->fetch()->willReturn($propertyType);

        $qb->select('*')->willReturn($qb);
        $qb->from('type', 't')->willReturn($qb);
        $qb->where('type_id = :typeId')->willReturn($qb);
        $qb->setParameters($parameters)->willReturn($qb);
        $qb->getSQL()->willReturn($sql);
        $qb->getParameters()->willReturn($parameters);

        $connection->createQueryBuilder()->willReturn($qb);
        $connection->executeQuery($sql, $parameters)->willReturn($st);;
        $this->setConnection($connection);

        $this->find($parameters)->shouldReturn($propertyType);
    }

    function it_finds_all_property_types(
        DbalConnection $connection,
        QueryBuilder $qb,
        EventManagerInterface $eventManager
    )
    {
        $propertyTypes = array('1' => 'Casa', '2' => 'Terreno');
        $sql = 'SELECT * FROM type t';
        $criteria = [];
        $eventParams = ['criteria' => $criteria, 'qb' => $qb];
        $identifiers = [
            'Mandragora\Repository\AbstractRepository',
            'Ika\Repository\TypeRepository'
        ];

        $qb->select('*')->willReturn($qb);
        $qb->from('type', 't')->willReturn($qb);
        $qb->getSQL()->willReturn($sql);

        $eventManager->trigger('onFindAll', $this, $eventParams)->shouldBeCalled();
        $eventManager->setIdentifiers($identifiers)->shouldBeCalled();

        $connection->createQueryBuilder()->willReturn($qb);
        $connection->fetchAll($sql)->willReturn($propertyTypes);

        $this->setEventManager($eventManager);
        $this->setConnection($connection);

        $this->findAll($criteria)->shouldReturn($propertyTypes);
    }
}
