<?php
namespace spec\Ika\Service;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Zend\Form\Factory;
use Ika\Repository\ClientRepository;
use Ika\Service\UserService;
use Ika\Repository\UserRepository;
use Ika\Form\ChangeClientPasswordForm;
use PhpSpec\Exception\Example\PendingException;
use Mandragora\Password\SecurePasswordGenerator;

class ClientServiceSpec extends ObjectBehavior
{
    function it_validates_change_password_form(
        Factory $factory,
        ChangeClientPasswordForm $changePasswordForm
    )
    {
        $spec = require 'config/forms/change-password.php';
        $newPassword = [
            'clientId' => 1, 'new-password' => 'changeme', 'repeat-password' => 'changeme'
        ];

        $changePasswordForm->setData($newPassword)->shouldBeCalled();
        $changePasswordForm->isValid()->willReturn(true);

        $factory->create($spec)->willReturn($changePasswordForm);

        $this->setParams($newPassword);
        $this->setFormFactory($factory);
        $this->isChangePasswordFormValid()->shouldReturn(true);
    }

    function it_fails_validation_if_password_confirmation_does_not_match(
        Factory $factory,
        ChangeClientPasswordForm $changePasswordForm
    )
    {
        $spec = require 'config/forms/change-password.php';
        $newPassword = [
            'clientId' => 1, 'new-password' => 'changeme', 'repeat-password' => 'changemE'
        ];

        $changePasswordForm->setData($newPassword)->shouldBeCalled();
        $changePasswordForm->isValid()->willReturn(false);

        $factory->create($spec)->willReturn($changePasswordForm);

        $this->setParams($newPassword);
        $this->setFormFactory($factory);
        $this->isChangePasswordFormValid()->shouldReturn(false);
    }

    function it_validates_client_exists(ClientRepository $clientRepository)
    {
        $newPassword = [
            'clientId' => 1, 'new-password' => 'changeme', 'repeat-password' => 'changeme'
        ];
        $client = [
            'social_reason' => 'Mandrágora Web-Systems',
            'first_name' => 'Luis',
            'username' => 'montealegreluis',
            'role' => 'client',
        ];

        $clientRepository->find($newPassword)->willReturn($client);

        $this->setRepository($clientRepository);
        $this->setParams($newPassword);
        $this->clientExist()->shouldReturn(true);
    }

    function it_fails_validation_if_client_does_not_exist(
        Factory $factory,
        ChangeClientPasswordForm $changePasswordForm,
        ClientRepository $clientRepository
    )
    {
        $spec = require 'config/forms/change-password.php';
        $newPassword = [
            'clientId' => 100, 'new-password' => 'changeme', 'repeat-password' => 'changeme'
        ];
        $message = 'El cliente no existe';

        $changePasswordForm->setClientNotFoundErrorMessage($message)->shouldBeCalled();

        $factory->create($spec)->willReturn($changePasswordForm);

        $clientRepository->find($newPassword)->willReturn(null);

        $this->setRepository($clientRepository);
        $this->setParams($newPassword);
        $this->setFormFactory($factory);
        $this->clientExist()->shouldReturn(false);
    }

    function it_changes_password_if_values_are_correct(
        Factory $factory,
        ChangeClientPasswordForm $changePasswordForm,
        ClientRepository $clientRepository,
        SecurePasswordGenerator $password,
        UserService $userService,
        UserRepository $userRepository
    )
    {
        $spec = require 'config/forms/change-password.php';
        $newPassword = [
            'clientId' => 1, 'new-password' => 'changeme', 'repeat-password' => 'changeme'
        ];
        $client = [
            'social_reason' => 'Mandrágora Web-Systems',
            'first_name' => 'Luis',
            'username' => 'montealegreluis',
            'role' => 'client',
        ];
        $hashedPassword = 'hashed-password';
        $userValues = [
            'password' => $hashedPassword,
            'username' => $client['username'],
        ];

        $changePasswordForm->getData()->willReturn($newPassword);

        $factory->create($spec)->willReturn($changePasswordForm);

        $clientRepository->find($newPassword)->willReturn($client);

        $password->create($newPassword['new-password'])->willReturn($hashedPassword);

        $userRepository->update($userValues)->shouldBeCalled();

        $userService->getRepository()->willReturn($userRepository);

        $this->setServiceUser($userService);
        $this->setRepository($clientRepository);
        $this->setParams($newPassword);
        $this->setPasswordGenerator($password);
        $this->setFormFactory($factory);
        $this->clientExist();

        $this->changePassword();
    }
}
