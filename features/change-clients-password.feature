Feature: Change clients password
    In order to change clients passwords
    As a webmaster
    I need to be able to set a new password for a given client

    Scenario: Change a client password
        Given I am logged as a webmaster
        When I follow "Condóminos"
        And I follow "Cambiar contraseña"
        And I fill in the following:
            | new-password    | changeme |
            | repeat-password | changeme |
        And I press "send"
        Then I should be on "/condominos/listar"
