<?php
/**
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @copyright  Mandrágora Web-Based Systems 2014 (http://www.mandragora-web-systems.com)
 */
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\Step\Given;

/**
 * Features context.
 */
class FeatureContext extends MinkContext
{
    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
    }

    /**
     * @When /^I fill the login form with webmaster credentials$/
     */
    public function iFillTheLoginFormWithWebmasterCredentials()
    {
        return [
            new Given('I fill in "username" with "webmaster"'),
            new Given('I fill in "password" with "webmaster"'),
        ];
    }

    /**
     * @Given /^I send the login form$/
     */
    public function iSendTheLoginForm()
    {
        return [
            new Given('I press "send"'),
        ];
    }

    /**
     * @Given /^I am logged as a webmaster$/
     */
    public function iAmLoggedAsAWebmaster()
    {
        return [
            new Given('I am on homepage'),
            new Given('I fill in "username" with "webmaster"'),
            new Given('I fill in "password" with "webmaster"'),
            new Given('I press "send"'),
        ];
    }
}
