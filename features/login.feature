Feature: Login
    In order to access and manage account information
    As a registered user
    I need to be able to log into the system

    Scenario: Login as a webmaster
        Given I am on homepage
        When I fill the login form with webmaster credentials
        And I send the login form
        Then I should be on "/fraccionamientos/listar"
