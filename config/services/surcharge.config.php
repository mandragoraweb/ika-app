<?php
use \Ika\Repository\SurchargeRepository;
use \Ika\Service\SurchargeService;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\Controller\CrudController;
use \Mandragora\EventListener\QuerySpecificationEventListener;

return array(
    'invokables' => array(
        'surcharge_repository' => SurchargeRepository::$CLASS,
    ),
    'factories' => array(
        'surcharge_service' => function(ServiceManager $sm) {
            $surcharge = new SurchargeService();
            $surcharge->setRepository($sm->get('surcharge_repository'));

            return $surcharge;
        },
        'surcharge_controller' => function(ServiceManager $sm) {
            $controller = new CrudController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('surcharge_service'));

            return $controller;
        },
        'surcharge_list' => function(ServiceManager $sm) {
            $controller = $sm->get('surcharge_controller');
            $surcharge = $controller->getService();

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $surcharge->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $surcharge->setPaginator($sm->get('paginator'));

            return $controller;
        },
    ),
);
