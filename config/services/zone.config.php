<?php
use \Ika\Repository\ZoneRepository;
use \Ika\Repository\UserRepository;
use \Ika\Service\ZoneService;
use \Ika\Service\UserService;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\Controller\CrudController;
use \Mandragora\EventListener\QuerySpecificationEventListener;
use Mandragora\Repository\Specification\ChainedSpecification;
use Ika\Repository\Specification\ZonesByRoleSpecification;

return array(
    'invokables' => array(
        'zone_repository' => ZoneRepository::$CLASS,
        'user_repository' => UserRepository::$CLASS,
    ),
    'factories' => array(
        'user_service' => function(ServiceManager $sm) {
            $user = new UserService();
            $user->setRepository($sm->get('user_repository'));

            return $user;
        },
        'zone_service' => function(ServiceManager $sm) {
            $zone = new ZoneService();
            $zone->setRepository($sm->get('zone_repository'));
            $zone->setServiceUser($sm->get('user_service'));

            return $zone;
        },
        'zone_controller' => function(ServiceManager $sm) {
            $controller = new CrudController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('zone_service'));

            return $controller;
        },
        'zone_list' => function(ServiceManager $sm) {
            $controller = $sm->get('zone_controller');
            $zone = $controller->getService();
            $chainedSpecification = new ChainedSpecification([
                $sm->get('by_page_spec'), new ZonesByRoleSpecification(),
            ]);

            $queryEventListener = new QuerySpecificationEventListener($chainedSpecification);
            $zone->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $zone->setPaginator($sm->get('paginator'));

            return $controller;
        },
    ),
);
