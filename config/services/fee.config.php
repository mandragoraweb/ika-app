<?php
use \Ika\Repository\FeeRepository;
use \Ika\Repository\TypeRepository;
use \Ika\Repository\ConceptRepository;
use \Ika\Repository\ZoneRepository;
use \Ika\Service\FeeService;
use \Ika\Service\TypeService;
use \Ika\Service\ConceptService;
use \Ika\Service\ZoneService;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\Controller\CrudController;
use \Mandragora\EventListener\QuerySpecificationEventListener;

return array(
    'invokables' => array(
        'fee_repository' => FeeRepository::$CLASS,
        'type_repository' => TypeRepository::$CLASS,
        'concept_repository' => ConceptRepository::$CLASS,
        'zone_repository' => ZoneRepository::$CLASS,
    ),
    'factories' => array(
        'type_service' => function(ServiceManager $sm) {
            $type = new TypeService();
            $type->setRepository($sm->get('type_repository'));

            return $type;
        },
        'concept_service' => function(ServiceManager $sm) {
            $concept = new ConceptService();
            $concept->setRepository($sm->get('concept_repository'));

            return $concept;
        },
        'zone_service' => function(ServiceManager $sm) {
            $zone = new ZoneService();
            $zone->setRepository($sm->get('zone_repository'));

            return $zone;
        },
        'fee_service' => function(ServiceManager $sm) {
            $fee = new FeeService();
            $fee->setRepository($sm->get('fee_repository'));
            $fee->setServiceType($sm->get('type_service'));
            $fee->setServiceConcept($sm->get('concept_service'));

            return $fee;
        },
        'fee_controller' => function(ServiceManager $sm) {
            $controller = new CrudController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('fee_service'));

            return $controller;
        },
        'fee_list' => function(ServiceManager $sm) {
            $controller = $sm->get('fee_controller');
            $fee = $controller->getService();

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $fee->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $fee->setPaginator($sm->get('paginator'));
            $fee->setServiceZone($sm->get('zone_service'));

            return $controller;
        },
    ),
);
