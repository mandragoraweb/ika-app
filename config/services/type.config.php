<?php
use \Ika\Repository\TypeRepository;
use \Ika\Service\TypeService;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\Controller\CrudController;
use \Mandragora\EventListener\QuerySpecificationEventListener;

return array(
    'invokables' => array(
        'type_repository' => TypeRepository::$CLASS
    ),
    'factories' => array(
        'type_service' => function(ServiceManager $sm) {
            $type = new TypeService();
            $type->setRepository($sm->get('type_repository'));

            return $type;
        },
        'type_controller' => function(ServiceManager $sm) {
            $controller = new CrudController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('type_service'));

            return $controller;
        },
        'type_list' => function(ServiceManager $sm) {
            $controller = $sm->get('type_controller');
            $type = $controller->getService();

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $type->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $type->setPaginator($sm->get('paginator'));

            return $controller;
        },
    ),
);
