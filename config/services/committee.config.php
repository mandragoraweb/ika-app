<?php
use \Ika\Repository\UserRepository;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\EventListener\QuerySpecificationEventListener;
use \Mandragora\Controller\CrudController;
use \Ika\Service\CommitteeService;
use \Mandragora\Repository\Specification\ChainedSpecification;
use \Ika\Repository\Specification\CommitteeInZoneSpecification;
use \Ika\Repository\Specification\ExcludeCommitteeByZoneRegisterSpecification;

return array(
    'invokables' => array(
        'user_repository' => UserRepository::$CLASS,
    ),
    'factories' => array(
        'committee_service' => function(ServiceManager $sm) {
            $service = new CommitteeService();
            $service->setRepository($sm->get('user_repository'));

            return $service;
        },
        'committee_controller' => function(ServiceManager $sm) {
            $controller = new CrudController($sm->get('request'), $sm->get('response'));
            $service = $sm->get('committee_service');
            $controller->setService($service);

            /*$chainedSpecification = new ChainedSpecification([
                new CommitteeInZoneSpecification, new ExcludeCommitteeByZoneRegisterSpecification(),
            ]);*/
            $queryEventListener = new QuerySpecificationEventListener(new ExcludeCommitteeByZoneRegisterSpecification);
            $service->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);

            return $controller;
        },
        'committee_list' => function(ServiceManager $sm) {
            $controller = new CrudController($sm->get('request'), $sm->get('response'));
            $service = $sm->get('committee_service');
            $controller->setService($service);

            $specification = new ChainedSpecification([
                $sm->get('by_page_spec'), new CommitteeInZoneSpecification(),
            ]);

            $queryEventListener = new QuerySpecificationEventListener($specification);
            $service->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $service->setPaginator($sm->get('paginator'));

            return $controller;
        }
    ),
);
