<?php
use \Ika\CondominiumManagement\ViewHomeowners;
use \Ika\Service\PropertyService;
use \Ika\Repository\PropertyRepository;
use \Ika\Repository\ClientRepository;
use \Ika\Repository\ProfileRepository;
use \Ika\Repository\UserRepository;
use \Ika\Repository\ZoneRepository;
use \Ika\Repository\TypeRepository;
use \Ika\Service\ClientService;
use \Ika\Service\UserService;
use \Ika\Service\ZoneService;
use \Ika\Service\TypeService;
use \Ika\Controller\ClientController;
use \Ika\Controller\ViewHomeownersController;
use \Ika\Repository\Specification\ClientsByZoneSpecification;
use \Ika\Repository\Specification\ZonesByRoleSpecification;
use \Mandragora\EventListener\QuerySpecificationEventListener;
use \Mandragora\Repository\Specification\ChainedSpecification;
use \Zend\ServiceManager\ServiceManager;


return array(
    'invokables' => array(
        'client_repository' => ClientRepository::$CLASS,
        'profile_repository' => ProfileRepository::$CLASS,
        'user_repository' => UserRepository::$CLASS,
        'property_repository' => PropertyRepository::$CLASS,
        'zone_repository' => ZoneRepository::$CLASS,
        'type_repository' => TypeRepository::$CLASS,
    ),
    'factories' => array(
        'zone_service' => function(ServiceManager $sm) {
            $zone = new ZoneService();
            $zone->setRepository($sm->get('zone_repository'));

            $queryEventListener = new QuerySpecificationEventListener(new ZonesByRoleSpecification());
            $zone->getRepository()->getEventManager()->attach('filterByRole', $queryEventListener);

            return $zone;
        },
        'type_service' => function(ServiceManager $sm) {
            $type = new TypeService();
            $type->setRepository($sm->get('type_repository'));

            return $type;
        },
        'property_service' => function(ServiceManager $sm) {
            $property = new PropertyService();
            $property->setRepository($sm->get('property_repository'));
            $property->setServiceZone($sm->get('zone_service'));
            $property->setServiceType($sm->get('type_service'));

            return $property;
        },
        'profile_service' => function(ServiceManager $sm) {
            $profile = new ClientService();
            $profile->setRepository($sm->get('profile_repository'));

            return $profile;
        },
        'user_service' => function(ServiceManager $sm) {
            $user = new UserService();
            $user->setRepository($sm->get('user_repository'));

            return $user;
        },
        'client_service' => function(ServiceManager $sm) {
            $client = new ClientService();
            $client->setRepository($sm->get('client_repository'));
            $client->setServiceProfile($sm->get('profile_service'));
            $client->setServiceUser($sm->get('user_service'));
            $client->setPropertyService($sm->get('property_service'));
            $client->setZoneService($sm->get('zone_service'));
            $client->setPasswordGenerator($sm->get('password_generator'));

            return $client;
        },
        'client_controller' => function(ServiceManager $sm) {
            $controller = new ClientController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('client_service'));

            return $controller;
        },
        'client_list' => function(ServiceManager $sm) {
            $controller = new ViewHomeownersController($sm->get('request'), $sm->get('response'));
            $controller->setUseCase($sm->get('use_case.view_homeowners'));

            $eventManager =$controller->getEventManager();
            $eventManager->attach('preDispatch', $sm->get('authentication_event_listener'));

            return $controller;
        },
        'use_case.view_homeowners' => function(ServiceManager $sm) {
            $repository = $sm->get('client_repository');
            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $repository->getEventManager()->attach('onFindAll', $queryEventListener);

            return new ViewHomeowners(
                $sm->get('auth_service'), $repository, $sm->get('paginator')
            );
        },
        'client_property' => function(ServiceManager $sm) {
            $controller = $sm->get('client_controller');
            $controller->getService()->setPropertyService($sm->get('property_service'));

            return $controller;
        }
    ),
);
