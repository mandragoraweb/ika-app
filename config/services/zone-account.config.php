<?php
use \Ika\Repository\ZoneAccountRepository;
use \Ika\Repository\ZoneRepository;
use \Ika\Service\ZoneAccountService;
use \Ika\Service\ZoneService;
use \Ika\Controller\ZoneAccountController;
use \Ika\Form\ZoneElementHydrator;
use \Ika\CondominiumManagement\CalculateSubdivisionAccountBalance;
use \Zend\ServiceManager\ServiceManager;

return array(
    'invokables' => array(
        'zone_account_repository' => ZoneAccountRepository::$CLASS,
        'zone_repository' => ZoneRepository::$CLASS,
    ),
    'factories' => array(
        'zone_service' => function(ServiceManager $sm) {
            $zone = new ZoneService();
            $zone->setRepository($sm->get('zone_repository'));

            return $zone;
        },
        'zone_account_service' => function(ServiceManager $sm) {
            $acount = new ZoneAccountService();
            $acount->setRepository($sm->get('zone_account_repository'));
            $acount->setZoneService($sm->get('zone_service'));

            return $acount;
        },
        'zone_account_controller' => function(ServiceManager $sm) {
            $controller = new ZoneAccountController($sm->get('request'), $sm->get('response'));
            $controller->setZoneAccountForm($sm->get('form.account_balance'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sm->get('authentication_event_listener'));

            return $controller;
        },
        'zone_account_balance_controller' => function(ServiceManager $sm) {
            $controller = $sm->get('zone_account_controller');
            $controller->setUseCase($sm->get('use_case.calculate_account_balance'));

            return $controller;
        },
        'use_case.calculate_account_balance' => function(ServiceManager $sm) {
            return new CalculateSubdivisionAccountBalance($sm->get('zone_account_repository'));
        },
        'form.account_balance' => function(ServiceManager $sm) {
            /** @var Ika\Form\ZonePaymentForm  $form */
            $form = $sm->get('form_factory')->create(require 'config/forms/zone-account.php');
            $form->hydrate($sm->get('form.account_balance_hydrator'));

            return $form;
        },
        'form.account_balance_hydrator' => function(ServiceManager $sm) {
            return new ZoneElementHydrator($sm->get('zone_repository'), $sm->get('auth_service'));
        },
    ),
);
