<?php
use \Ika\Repository\ReportRepository;
use \Ika\Repository\ZoneRepository;
use \Ika\Service\ReportService;
use \Ika\Service\ZoneService;
use \Ika\Controller\ReportController;
use \Ika\Repository\Specification\ZonesByRoleSpecification;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\EventListener\QuerySpecificationEventListener;



return array(
    'invokables' => array(
        'report_repository' => ReportRepository::$CLASS,
        'zone_repository' => ZoneRepository::$CLASS,
    ),
    'factories' => array(
        'zone_service' => function(ServiceManager $sm) {
            $zone = new ZoneService();
            $repository = $sm->get('zone_repository');
            $queryListener = new QuerySpecificationEventListener(new ZonesByRoleSpecification());
            $repository->getEventManager()->attach('filterByRole', $queryListener);
            $zone->setRepository($repository);

            return $zone;
        },
        'report_service' => function(ServiceManager $sm) {
            $report = new ReportService();
            $report->setRepository($sm->get('report_repository'));
            $report->setServiceZone($sm->get('zone_service'));

            return $report;
        },
        'report_controller' => function(ServiceManager $sm) {
            $controller = new ReportController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('report_service'));

            return $controller;
        },
    ),
);
