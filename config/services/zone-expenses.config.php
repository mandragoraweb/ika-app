<?php
use \Ika\Repository\ZoneExpensesRepository;
use \Ika\Repository\ConceptRepository;
use \Ika\Repository\ZoneRepository;
use \Ika\Service\ZoneExpensesService;
use \Ika\Service\ConceptService;
use \Ika\Service\ZoneService;
use \Ika\Controller\ZoneExpensesController;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\EventListener\QuerySpecificationEventListener;
use Ika\Repository\Specification\ZonesByRoleSpecification;

return array(
    'invokables' => array(
        'zone_expenses_repository' => ZoneExpensesRepository::$CLASS,
        'concept_repository' => ConceptRepository::$CLASS,
        'zone_repository' => ZoneRepository::$CLASS,
    ),
    'factories' => array(
        'concept_service' => function(ServiceManager $sm) {
            $concept = new ConceptService();
            $concept->setRepository($sm->get('concept_repository'));

            return $concept;
        },
        'zone_service' => function(ServiceManager $sm) {
            $zone = new ZoneService();
            $repository = $sm->get('zone_repository');
            $queryListener = new QuerySpecificationEventListener(new ZonesByRoleSpecification());
            $repository->getEventManager()->attach('filterByRole', $queryListener);
            $zone->setRepository($repository);

            return $zone;
        },
        'zone_expenses_service' => function(ServiceManager $sm) {
            $expenses = new ZoneExpensesService();
            $expenses->setRepository($sm->get('zone_expenses_repository'));
            $expenses->setServiceConcept($sm->get('concept_service'));
            $expenses->setServiceZone($sm->get('zone_service'));

            return $expenses;
        },
        'zone_expenses_controller' => function(ServiceManager $sm) {
            $controller = new ZoneExpensesController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('zone_expenses_service'));

            return $controller;
        },
        'zone_expenses_list' => function(ServiceManager $sm) {
            $controller = $sm->get('zone_expenses_controller');
            $expenses = $controller->getService();

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $expenses->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $expenses->setPaginator($sm->get('paginator'));

            return $controller;
        },
    ),
);
