<?php
use \Ika\Repository\PaymentRepository;
use \Ika\Repository\FeeRepository;
use \Ika\Repository\ZoneRepository;
use \Ika\Repository\PropertyRepository;
use \Ika\Service\PaymentService;
use \Ika\Service\FeeService;
use \Ika\Service\ZoneService;
use \Ika\Service\PropertyService;
use \Ika\Controller\PaymentController;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\EventListener\QuerySpecificationEventListener;
use Ika\Repository\Specification\ZonesByRoleSpecification;

return array(
    'invokables' => array(
        'payment_repository' => PaymentRepository::$CLASS,
        'fee_repository' => FeeRepository::$CLASS,
        'zone_repository' => ZoneRepository::$CLASS,
        'property_repository' => PropertyRepository::$CLASS,
    ),
    'factories' => array(
        'fee_service' => function(ServiceManager $sm) {
            $fee = new FeeService();
            $fee->setRepository($sm->get('fee_repository'));

            return $fee;
        },
        'zone_service' => function(ServiceManager $sm) {
            $zone = new ZoneService();
            $repository = $sm->get('zone_repository');
            $queryListener = new QuerySpecificationEventListener(new ZonesByRoleSpecification());
            $repository->getEventManager()->attach('filterByRole', $queryListener);
            $zone->setRepository($repository);

            return $zone;
        },
        'property_service' => function(ServiceManager $sm) {
            $property = new PropertyService();
            $property->setRepository($sm->get('property_repository'));

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $property->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);

            return $property;
        },
        'payment_service' => function(ServiceManager $sm) {
            $payment = new PaymentService();
            $payment->setRepository($sm->get('payment_repository'));
            $payment->setServiceFee($sm->get('fee_service'));
            $payment->setAuthService($sm->get('auth_service'));
            $payment->setServiceZone($sm->get('zone_service'));
            $payment->setServiceProperty($sm->get('property_service'));

            return $payment;
        },
        'payment_controller' => function(ServiceManager $sm) {
            $controller = new PaymentController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('payment_service'));

            return $controller;
        },
        'payment_list' => function(ServiceManager $sm) {
            $controller = $sm->get('payment_controller');
            $payment = $controller->getService();

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $payment->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $payment->setPaginator($sm->get('paginator'));

            return $controller;
        },
    ),
);
