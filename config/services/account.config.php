<?php
use Zend\ServiceManager\ServiceManager;
use Mandragora\EventListener\QuerySpecificationEventListener;
use Ika\Repository\AccountRepository;
use Ika\Repository\ClientRepository;
use Ika\Service\AccountService;
use Ika\Service\ClientService;
use Ika\Repository\Specification\ClientsByZoneSpecification;
use Ika\Controller\AccountBalanceController;
use Ika\Form\AccountFormHydrator;
use Ika\Repository\ZoneRepository;
use Ika\Controller\HomeownersInSubdivisionController;
use \Ika\Controller\CustomerPropertiesController;
use \Ika\Repository\PropertyRepository;

return array(
    'invokables' => array(
        'account_repository' => AccountRepository::$CLASS,
        'client_repository' => ClientRepository::$CLASS,
        'zone_repository' => ZoneRepository::$CLASS,
        'property_repository' => PropertyRepository::$CLASS,
    ),
    'factories' => array(
        'client_service' => function(ServiceManager $sm) {
            $client = new ClientService();
            $client->setRepository($sm->get('client_repository'));

            $queryEventListener = new QuerySpecificationEventListener(
                new ClientsByZoneSpecification($sm->get('client_repository'))
            );
            $client->getRepository()->getEventManager()->attach('filterByRole', $queryEventListener);

            return $client;
        },
        'account_service' => function(ServiceManager $sm) {
            $acount = new AccountService();
            $acount->setRepository($sm->get('account_repository'));

            return $acount;
        },
        'account_controller' => function(ServiceManager $sm) {
            $controller = $sm->get('account_form_controller');
            $controller->setAccountBalanceService($sm->get('account_service'));

            return $controller;
        },
        'account_form_controller' => function(ServiceManager $sm) {
            $controller = new AccountBalanceController($sm->get('request'), $sm->get('response'));
            $controller->setAccountBalanceForm($sm->get('account_form'));
            $controller->setFormHydrator($sm->get('account_form_hydrator'));
            $controller->setAuthenticationService($sm->get('auth_service'));
            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sm->get('authentication_event_listener'));

            return $controller;
        },
        'account_form' => function(ServiceManager $sm) {
            return $sm->get('form_factory')->create(require 'config/forms/account.php');
        },
        'account_form_hydrator' => function(ServiceManager $sm) {
            return new AccountFormHydrator($sm->get('zone_repository'), $sm->get('client_repository'), $sm->get('property_repository'));
        },
        'client_controller' => function(ServiceManager $sm) {
            $controller = new HomeownersInSubdivisionController($sm->get('request'), $sm->get('response'));
            $controller->setClientRepository($sm->get('client_repository'));

            return $controller;
        },
        'property_controller' => function(ServiceManager $sm) {
            $controller = new CustomerPropertiesController($sm->get('request'), $sm->get('response'));
            $controller->setPropertiesRepository($sm->get('property_repository'));

            return $controller;
        },
    ),
);
