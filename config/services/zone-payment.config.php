<?php
use \Ika\Repository\ZonePaymentRepository;
use \Ika\Repository\FeeRepository;
use \Ika\Repository\ZoneRepository;
use \Ika\Service\ZonePaymentService;
use \Ika\Service\FeeService;
use \Ika\Service\ZoneService;
use \Ika\Controller\ZonePaymentController;
use \Ika\Controller\NewSubdivisionPaymentController;
use \Ika\Controller\ViewPaymentsFormController;
use \Ika\Form\ZoneElementHydrator;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\EventListener\QuerySpecificationEventListener;

return array(
    'invokables' => array(
        'zone_payment_repository' => ZonePaymentRepository::$CLASS,
        'fee_repository' => FeeRepository::$CLASS,
        'zone_repository' => ZoneRepository::$CLASS,
    ),
    'factories' => array(
        'fee_service' => function(ServiceManager $sm) {
            $fee = new FeeService();
            $fee->setRepository($sm->get('fee_repository'));

            return $fee;
        },
        'zone_service' => function(ServiceManager $sm) {
            $zone = new ZoneService();
            $zone->setRepository($sm->get('zone_repository'));

            return $zone;
        },
        'zone_payment_service' => function(ServiceManager $sm) {
            $payment = new ZonePaymentService();
            $payment->setRepository($sm->get('zone_payment_repository'));
            $payment->setServiceFee($sm->get('fee_service'));
            $payment->setServiceZone($sm->get('zone_service'));

            return $payment;
        },
        'zone_payment_controller' => function(ServiceManager $sm) {
            $controller = new ZonePaymentController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('zone_payment_service'));

            return $controller;
        },
        'controller.zone_payment_customize' => function(ServiceManager $sm) {
            $controller = new ViewPaymentsFormController($sm->get('request'), $sm->get('response'));
            $controller->setZonePaymentForm($sm->get('form.zone_payment_personalize'));

            $eventManager =$controller->getEventManager();
            $eventManager->attach('preDispatch', $sm->get('authentication_event_listener'));

            return $controller;
        },
        'form.zone_payment_personalize' => function(ServiceManager $sm) {
            /** @var Ika\Form\ZonePaymentPersonalizeForm  $form */
            $form = $sm->get('form_factory')->create(require 'config/forms/zone-payment-personalize.php');
            $form->hydrate($sm->get('form.zone_payment_hydrator'));

            return $form;
        },
        'controller.zone_payment_form' => function(ServiceManager $sm) {
            $controller = new NewSubdivisionPaymentController($sm->get('request'), $sm->get('response'));
            $controller->setZonePaymentForm($sm->get('form.zone_payment'));

            $eventManager =$controller->getEventManager();
            $eventManager->attach('preDispatch', $sm->get('authentication_event_listener'));

            return $controller;
        },
        'form.zone_payment' => function(ServiceManager $sm) {
            /** @var Ika\Form\ZonePaymentForm  $form */
            $form = $sm->get('form_factory')->create(require 'config/forms/zone-payment.php');
            $form->hydrate($sm->get('form.zone_payment_hydrator'));

            return $form;
        },
        'form.zone_payment_hydrator' => function(ServiceManager $sm) {
            return new ZoneElementHydrator($sm->get('zone_repository'), $sm->get('auth_service'));
        },
        'zone_payment_list' => function(ServiceManager $sm) {
            $controller = $sm->get('zone_payment_controller');
            $payment = $controller->getService();

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $payment->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $payment->setPaginator($sm->get('paginator'));

            return $controller;
        },
    ),
);
