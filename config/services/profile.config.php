<?php
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\EventListener\QuerySpecificationEventListener;
use \Mandragora\Repository\Specification\ChainedSpecification;
use \Ika\Repository\ProfileRepository;
use \Ika\Repository\UserRepository;
use \Ika\Service\ProfileService;
use \Ika\Service\UserService;
use \Ika\Controller\ProfileController;
use \Ika\Repository\Specification\ExcludeClientsSpecification;

return array(
    'invokables' => array(
        'profile_repository' => ProfileRepository::$CLASS,
        'user_repository' => UserRepository::$CLASS,
    ),
    'factories' => array(
        'user_service' => function(ServiceManager $sm) {
            $user = new UserService();
            $user->setRepository($sm->get('user_repository'));

            return $user;
        },
        'profile_service' => function(ServiceManager $sm) {
            $profile = new ProfileService();
            $profile->setRepository($sm->get('profile_repository'));
            $profile->setServiceUser($sm->get('user_service'));
            $profile->setPasswordGenerator($sm->get('password_generator'));

            return $profile;
        },
        'profile_controller' => function(ServiceManager $sm) {
            $controller = new ProfileController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('profile_service'));

            return $controller;
        },
        'profile_list' => function(ServiceManager $sm) {
            $controller = $sm->get('profile_controller');
            $profile = $controller->getService();

            $specification = new ChainedSpecification([
                $sm->get('by_page_spec'), new ExcludeClientsSpecification(),
            ]);
            $queryEventListener = new QuerySpecificationEventListener($specification);
            $profile->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $profile->setPaginator($sm->get('paginator'));

            return $controller;
        },
        'profile_save' => function(ServiceManager $sm) {
            $controller = $sm->get('profile_controller');
            $profile = $controller->getService();
            $profile->setPasswordGenerator($sm->get('password_generator'));
            $profile->setMessage($sm->get('mailer_message'));
            $profile->setMailer($sm->get('mailer'));
            $profile->setView($sm->get('view'));

            return $controller;
        },
    ),
);
