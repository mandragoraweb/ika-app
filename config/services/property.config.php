<?php
use \Ika\Repository\PropertyRepository;
use \Ika\Repository\ZoneRepository;
use \Ika\Repository\TypeRepository;
use \Ika\Repository\ClientRepository;
use \Ika\Repository\UserRepository;
use \Ika\Service\PropertyService;
use \Ika\Service\ZoneService;
use \Ika\Service\TypeService;
use \Ika\Service\ClientService;
use \Ika\Service\UserService;
use \Ika\Controller\PropertyController;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\EventListener\QuerySpecificationEventListener;

return array(
    'invokables' => array(
        'property_repository' => PropertyRepository::$CLASS,
        'zone_repository' => ZoneRepository::$CLASS,
        'type_repository' => TypeRepository::$CLASS,
        'client_repository' => ClientRepository::$CLASS,
        'user_repository' => UserRepository::$CLASS,
    ),
    'factories' => array(
        'user_service' => function(ServiceManager $sm) {
            $user = new UserService();
            $user->setAuthService($sm->get('auth_service'));
            $user->setRepository($sm->get('user_repository'));

            return $user;
        },
        'zone_service' => function(ServiceManager $sm) {
            $zone = new ZoneService();
            $zone->setRepository($sm->get('zone_repository'));

            return $zone;
        },
        'type_service' => function(ServiceManager $sm) {
            $type = new TypeService();
            $type->setRepository($sm->get('type_repository'));

            return $type;
        },
        'client_service' => function(ServiceManager $sm) {
            $client = new ClientService();
            $client->setRepository($sm->get('client_repository'));

            return $client;
        },
        'property_service' => function(ServiceManager $sm) {
            $property = new PropertyService();
            $property->setRepository($sm->get('property_repository'));
            $property->setServiceZone($sm->get('zone_service'));
            $property->setServiceType($sm->get('type_service'));
            $property->setServiceClient($sm->get('client_service'));
            $property->setServiceUser($sm->get('user_service'));

            return $property;
        },
        'property_controller' => function(ServiceManager $sm) {
            $controller = new PropertyController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('property_service'));

            return $controller;
        },
        'property_list' => function(ServiceManager $sm) {
            $controller = $sm->get('property_controller');
            $property = $controller->getService();

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $property->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $property->setPaginator($sm->get('paginator'));

            return $controller;
        },
        'client_property_save' => function(ServiceManager $sm) {
            $controller = $sm->get('property_controller');
            $property = $controller->getService();
            $property->setPasswordGenerator($sm->get('password_generator'));
            $property->setMessage($sm->get('mailer_message'));
            $property->setMailer($sm->get('mailer'));
            $property->setView($sm->get('view'));

            return $controller;
        },
    ),
);
