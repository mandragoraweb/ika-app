<?php
use \Ika\Event\LoginFormEvent;
use \Ika\Service\UserService;
use \Ika\Service\ContactService;
use \Ika\Controller\IndexController;
use \Zend\ServiceManager\ServiceManager;

return array(
    'factories' => array(
        'index_controller' => function(ServiceManager $sm) {
            $controller = new IndexController($sm->get('request'), $sm->get('response'));
            $controller->setUser(new UserService());
            $controller->setContact(new ContactService());

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', new LoginFormEvent());

            return $controller;
        },
    ),
);
