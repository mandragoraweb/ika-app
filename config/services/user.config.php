<?php
use \Ika\Repository\UserRepository;
use \Ika\Service\UserService;
use \Ika\Controller\UserController;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\EventListener\QuerySpecificationEventListener;

return array(
    'invokables' => array(
        'user_repository' => UserRepository::$CLASS,
    ),
    'factories' => array(
        'user_service' => function(ServiceManager $sm) {
            $user = new UserService();
            $user->setRepository($sm->get('user_repository'));
            $user->setPasswordGenerator($sm->get('password_generator'));

            return $user;
        },
        'user_controller' => function(ServiceManager $sm) {
            $controller = new UserController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('user_service'));

            return $controller;
        },
        'user_authenticate' => function(ServiceManager $sm) {
            $controller = $sm->get('user_logout');
            $user = $controller->getService();
            $user->setAuthAdapter($sm->get('auth_adapter'));

            return $controller;
        },
        'user_logout' => function(ServiceManager $sm) {
            $controller = $sm->get('user_controller');
            $user = $controller->getService();
            $user->setAuthService($sm->get('auth_service'));

            return $controller;
        },
        'user_list' => function(ServiceManager $sm) {
            $controller = $sm->get('user_controller');
            $user = $controller->getService();

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $user->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $user->setPaginator($sm->get('paginator'));

            return $controller;
        },
    ),
);
