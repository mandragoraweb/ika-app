<?php
use Zend\ServiceManager\ServiceManager;
use Ika\Controller\AnnualIncomeStatementController;
use Ika\Controller\AnnualExpensesStatementController;
use Ika\Form\ZoneElementHydrator;
use Ika\Repository\ZoneRepository;
use Ika\Repository\AnnualStatementRepository;

return [
    'invokables' => [
        'repository.zones' => ZoneRepository::$CLASS,
        'repository.annual_statement' => AnnualStatementRepository::$CLASS,
    ],
    'factories' => [
        'controller.annual_income_form' => function(ServiceManager $sm) {
            $controller = new AnnualIncomeStatementController($sm->get('request'), $sm->get('response'));
            $controller->setAnnualStatementForm($sm->get('form.annual_balance'));

            $eventManager =$controller->getEventManager();
            $eventManager->attach('preDispatch', $sm->get('authentication_event_listener'));

            return $controller;
        },
        'controller.annual_income_statement'  => function(ServiceManager $sm) {
            $controller = $sm->get('controller.annual_income_form');
            $controller->setAnnualStatementRepository($sm->get('repository.annual_statement'));

            return $controller;
        },
        'controller.annual_expenses_form' => function(ServiceManager $sm) {
            $controller = new AnnualExpensesStatementController($sm->get('request'), $sm->get('response'));
            $controller->setAnnualStatementForm($sm->get('form.annual_balance'));

            $eventManager =$controller->getEventManager();
            $eventManager->attach('preDispatch', $sm->get('authentication_event_listener'));

            return $controller;
        },
        'controller.annual_expenses_statement' => function(ServiceManager $sm) {
            $controller = $sm->get('controller.annual_expenses_form');
            $controller->setAnnualStatementRepository($sm->get('repository.annual_statement'));

            return $controller;
        },
        'form.annual_balance' => function(ServiceManager $sm) {
            /** @var Ika\Form\AnnualStatementForm  $form */
            $form = $sm->get('form_factory')->create(require 'config/forms/annual-statement.php');
            $form->hydrate($sm->get('form.annual_balance_hydrator'));

            return $form;
        },
        'form.annual_balance_hydrator' => function(ServiceManager $sm) {
            return new ZoneElementHydrator($sm->get('repository.zones'), $sm->get('auth_service'));
        },
    ],
];
