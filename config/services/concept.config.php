<?php
use \Ika\Repository\ConceptRepository;
use \Ika\Service\ConceptService;
use \Zend\ServiceManager\ServiceManager;
use \Mandragora\Controller\CrudController;
use \Mandragora\EventListener\QuerySpecificationEventListener;

return array(
    'invokables' => array(
        'concept_repository' => ConceptRepository::$CLASS,
    ),
    'factories' => array(
        'concept_service' => function(ServiceManager $sm) {
            $concept = new ConceptService();
            $concept->setRepository($sm->get('concept_repository'));

            return $concept;
        },
        'concept_controller' => function(ServiceManager $sm) {
            $controller = new CrudController($sm->get('request'), $sm->get('response'));
            $controller->setService($sm->get('concept_service'));

            return $controller;
        },
        'concept_list' => function(ServiceManager $sm) {
            $controller = $sm->get('concept_controller');
            $concept = $controller->getService();

            $queryEventListener = new QuerySpecificationEventListener($sm->get('by_page_spec'));
            $concept->getRepository()->getEventManager()->attach('onFindAll', $queryEventListener);
            $concept->setPaginator($sm->get('paginator'));

            return $controller;
        },
    ),
);
