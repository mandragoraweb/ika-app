<?php
return [
    'type' => 'Ika\Form\AnnualStatementForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => [
        'method' => 'post',
        'id' => 'annual-statement-form',
        'class' => 'form-horizontal',
    ],
    'elements' => [
        [
            'spec' => [
                'name' => 'zone_id',
                'options' => [
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Selecciona un fraccionamiento',
                ],
                'attributes' => [
                    'required' => true,
                    'id' => 'zone-id',
                ],
                'type'  => 'Select',
            ]
        ],
        [
            'spec' => [
                'name' => 'year',
                'options' => [
                    'label' => 'Año',
                ],
                'attributes' => [
                    'id' => 'year',
                    'required' => true,
                ],
                'type'  => 'Text',
            ]
        ],
        [
            'spec' => [
                'name' => 'csrf',
                'type' => 'Csrf',
            ],
        ],
        [
            'spec' => [
                'name' => 'send',
                'options' => [
                    'label' => 'Buscar',
                ],
                'attributes' => [
                    'type' => 'submit',
                    'value' => 'Buscar',
                ],
                'type'  => 'Button',
            ],
        ],
    ],
    'input_filter' => [
        'zone_id' => [
            'required' => true,
        ],
        'year' => [
            'required' => true,
        ],
    ],
];
