<?php
// TODO: Confirm the values that need to be required (form and database)
return array(
    'type' => 'Ika\Form\ProfileForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'profile-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'profile_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'user',
                'options' => array(
                    'label' => 'Nombre de usuario',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 20,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'user_role',
                'options' => array(
                    'label' => 'Permisos',
                    'value_options' => array(
                        'webmaster' => 'Webmaster',
                        'admin' => 'Administrador',
                        'committee' => 'Comité de vigilancia',
                    ),
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Radio',
            )
        ),
        array(
            'spec' => array(
                'name' => 'first_name',
                'options' => array(
                    'label' => 'Nombre',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 45,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'paternal_surname',
                'options' => array(
                    'label' => 'Apellido paterno',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 45,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'maternal_surname',
                'options' => array(
                    'label' => 'Apellido materno',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 45,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'email',
                'options' => array(
                    'label' => 'Correo eléctronico',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 90,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'birth',
                'options' => array(
                    'label' => 'Fecha de nacimiento',
                    'format' => 'Y-m-d',
                ),
                'attributes' => array(
                    'class' => 'ui-date',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'user' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'password' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'user_role' => array(
            'required' => true,
        ),
        'paternal_surname' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'maternal_surname' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'first_name' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'email' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'birth' => array(
            'required' => false
        ),
    ),
);
