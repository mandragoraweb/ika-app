<?php
use Zend\Validator\Date;
return array(
    'type' => 'Ika\Form\PropertyForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'property-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'property_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'client_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'zone_id',
                'options' => array(
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Selecciona un fraccionamiento',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'type_id',
                'options' => array(
                    'label' => 'Tipo de propiedad',
                    'empty_option' => 'Selecciona un tipo de propiedad',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'street',
                'options' => array(
                    'label' => 'Calle',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'number',
                'options' => array(
                    'label' => 'Número',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'internal_number',
                'options' => array(
                    'label' => 'Número Interior',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'block',
                'options' => array(
                    'label' => 'Manzana',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'lot',
                'options' => array(
                    'label' => 'Lote',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'opening_balance',
                'options' => array(
                    'label' => 'Saldo Inicial',
                ),
                'attributes' => array(
                    'required' => true,
                    'value' => 0
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'balance_date',
                'attributes' => array(
                    'required' => true,
                    'class' => 'ui-date',
                ),
                'options' => array(
                    'label' => 'Fecha de Saldo Inicial',
                    'format' => 'Y-m-d',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'street' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'number' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'internal_number' => array(
            'required' => false,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'block' => array(
            'required' => false,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'lot' => array(
            'required' => false,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'opening_balance' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'balance_date' => array(
            'required' => true,
            'allowEmpty' => false,
            'validators' => array(
                array(
                    'name' => 'Zend\Validator\Date',
                    'options' => array(
                        'messages' => array(
                            Date::INVALID_DATE => 'La fecha del saldo inicial no parece ser una fecha válida',
                            Date::FALSEFORMAT => 'La fecha del saldo inicial no se ajusta al formato de fecha "%format%"',
                        ),
                    ),
                ),
            ),
        ),
    ),
);
