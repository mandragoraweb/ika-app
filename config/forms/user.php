<?php
return array(
    'type' => 'Ika\Form\UserForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'user-form',
        'class' => 'gestor',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'profile_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'username',
                'options' => array(
                    'label' => 'Login',
                ),
                'attributes' => array(
                    'required' => true,
                    'size' => 45,
                    'maxlength' => 20,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'password',
                'options' => array(
                    'label' => 'Password',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 100,
                ),
                'type'  => 'Password',
            )
        ),
        array(
            'spec' => array(
                'name' => 'role',
                'options' => array(
                    'label' => 'Permisos',
                     'value_options' => array(
                             'webmaster' => 'Webmaster',
                             'admin' => 'Administrador',
                             'client' => 'Cliente',
                             'commitee' => 'Comité de vigilancia',
                     ),
                ),
                'attributes' => array(
                    'required' => true,
                 ),
                'type'  => 'Radio',
            )
        ),
        array(
            'spec' => array(
                'name' => 'comment',
                'options' => array(
                    'label' => 'Comentario',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 100,
                    'rows' => 10,
                    'cols' => 50,

                ),
                'type'  => 'Textarea',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'attributes' => array(
                    'value' => 'Enviar',
                ),
                'type'  => 'Submit',
            ),
        ),
    ),
    'input_filter' => array(
        'username' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
            'validators' => array(
                'name'    => 'Zend\Validator\Db\NoRecordExists',
                'options' => array(
                    'table' => 'user',
                    'field' => 'username',
                    'messages' => array(
                        \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'The specified name already exists in database'
                    ),
                ),
            ),
        ),
        'password' => array(
            'required' => false,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'role' => array(
            'required' => true,
        )
    ),
);
