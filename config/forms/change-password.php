<?php
return array(
    'type' => 'Ika\Form\ChangeClientPasswordForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'change-password-form',
        'class' => 'form-horizontal'
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'client_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'new-password',
                'options' => array(
                    'label' => 'Nueva Contraseña',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type' => 'Password',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'repeat-password',
                'options' => array(
                    'label' => 'Repetir Contraseña',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type' => 'Password',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'new-password' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'repeat-password' => array(
            'required' => true,
            'validators' => array(
                array(
                    'name'    => 'Zend\Validator\Identical',
                    'options' => array(
                        'token' => 'new-password',
                    ),
                ),
            ),
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
    ),
);
