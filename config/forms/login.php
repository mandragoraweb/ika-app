<?php
return array(
    'type' => 'Zend\Form\Form',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'action' => '/cuentas/verificar',
        'id' => 'login-form',
        'class' => 'ajax'
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'username',
                'options' => array(
                    'label' => 'Usuario',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'password',
                'options' => array(
                    'label' => 'Contraseña',
                ),
                'type' => 'Password',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'attributes' => array(
                    'value' => 'Iniciar sesión',
                ),
                'type'  => 'Submit',
            ),
        ),
    ),
    'input_filter' => array(
        'username' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'password' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
    ),
);
