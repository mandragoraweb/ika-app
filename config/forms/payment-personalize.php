<?php
return array(
    'type' => 'Ika\Form\PaymentPersonalizeForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'payment-personalize-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'zone_id',
                'attributes' => array(
                    'id' => 'zone',
                ),
                'options' => array(
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Selecciona un fraccionamiento',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'property_id',
                'attributes' => array(
                    'id' => 'property',
                ),
                'options' => array(
                    'label' => 'Propiedad',
                    'empty_option' => 'Selecciona una propiedad',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
    ),
    'input_filter' => array(
        'zone_id' => array(
            'required' => false,
        ),
        'property_id' => array(
            'required' => false,
        ),
    ),
);
