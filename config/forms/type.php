<?php
return array(
    'type' => 'Ika\Form\TypeForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'type-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'type_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'description',
                'options' => array(
                    'label' => 'Descripción',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 90,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'description' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            )
        )
    ),
);
