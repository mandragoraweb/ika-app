<?php
return array(
    'type' => 'Ika\Form\ReportForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'report-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'zone_id',
                'attributes' => array(
                    'id' => 'zone',
                    'required' => true,
                ),
                'options' => array(
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Seleccione un fraccionamiento',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'start_date',
                'options' => array(
                    'label' => 'Fecha inicial',
                    'format' => 'Y-m-d',
                ),
                'attributes' => array(
                    'required' => true,
                    'class' => 'ui-date',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'stop_date',
                'options' => array(
                    'label' => 'Fecha final',
                    'format' => 'Y-m-d',
                ),
                'attributes' => array(
                    'required' => true,
                    'class' => 'ui-date',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Reporte',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Reporte',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'start_date' => array(
            'required' => true,
        ),
        'stop_date' => array(
            'required' => true,
        ),
    ),
);
