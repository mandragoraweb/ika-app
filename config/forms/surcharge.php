<?php
return array(
    'type' => 'Ika\Form\SurchargeForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'surcharge-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'surcharge_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'payment_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'surcharge_type',
                'options' => array(
                    'label' => 'Tipo de cargo',
                    'value_options' => array(
                        'M' => 'Monto fijo',
                        'P' => 'Porcentaje',
                    ),
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Radio',
            )
        ),
        array(
            'spec' => array(
                'name' => 'fixed',
                'options' => array(
                    'label' => 'Cantidad',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'fixed' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'surcharge_type' => array(
            'required' => true,
        ),
    ),
);
