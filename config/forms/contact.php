<?php
return array(
    'type' => 'Zend\Form\Form',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'action' => '/correo/enviar',
        'id' => 'contact-form',
        'class' => 'ajax'
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'name',
                'options' => array(
                    'label' => 'Nombre',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'email',
                'options' => array(
                    'label' => ' e-mail',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'comment',
                'options' => array(
                    'label' => 'Comentario',
                ),
                'attributes' => array(
                    'rows' => 15,
                    'cols' => 70,
                ),
                'type'  => 'Textarea',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'attributes' => array(
                    'value' => 'Enviar',
                ),
                'type'  => 'Submit',
            ),
        ),
    ),
    'input_filter' => array(
        'name' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'email' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'comment' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
    ),
);
