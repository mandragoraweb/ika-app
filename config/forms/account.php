<?php
return [
    'type' => 'Ika\Form\AccountForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => [
        'method' => 'post',
        'id' => 'account-form',
        'class' => 'form-horizontal',
    ],
    'elements' => [
        [
            'spec' => [
                'name' => 'zone_id',
                'options' => [
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Selecciona un fraccionamiento',
                ],
                'attributes' => [
                    'required' => true,
                    'id' => 'zone-id',
                ],
                'type'  => 'Select',
            ]
        ],
        [
            'spec' => [
                'name' => 'client_id',
                'options' => [
                    'label' => 'Condómino',
                    'empty_option' => 'Selecciona un condómino',
                ],
                'attributes' => [
                    'id' => 'client-id',
                ],
                'type'  => 'Select',
            ]
        ],
        [
            'spec' => [
                'name' => 'property_id',
                'options' => [
                    'label' => 'Propiedad',
                    'empty_option' => 'Selecciona una propiedad',
                ],
                'attributes' => [
                    'id' => 'property-id',
                ],
                'type'  => 'Select',
            ]
        ],
        [
            'spec' => [
                'name' => 'start_date',
                'options' => [
                    'label' => 'Fecha inicial',
                    'format' => 'Y-m-d',
                ],
                'attributes' => [
                    'required' => true,
                    'class' => 'ui-date',
                ],
                'type'  => 'Text',
            ]
        ],
        [
            'spec' => [
                'name' => 'stop_date',
                'options' => [
                    'label' => 'Fecha final',
                    'format' => 'Y-m-d',
                ],
                'attributes' => [
                    'required' => true,
                    'class' => 'ui-date',
                ],
                'type'  => 'Text',
            ]
        ],
        [
            'spec' => [
                'name' => 'csrf',
                'type' => 'Csrf',
            ],
        ],
        [
            'spec' => [
                'name' => 'send',
                'options' => [
                    'label' => 'Buscar',
                ],
                'attributes' => [
                    'type' => 'submit',
                    'value' => 'Buscar',
                ],
                'type'  => 'Button',
            ],
        ],
    ],
    'input_filter' => [
        'client_id' => [
            'required' => false,
        ],
        'property_id' => [
            'required' => false,
        ],
        'start_date' => [
            'required' => true,
        ],
        'stop_date' => [
            'required' => true,
        ],
    ],
];
