<?php
return array(
    'type' => 'Ika\Form\ZonePaymentPersonalizeForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'zone-payment-personalize-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'zone_id',
                'attributes' => array(
                    'id' => 'zone',
                ),
                'options' => array(
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Seleccione un fraccionamiento',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
    ),
    'input_filter' => array(
        'zone_id' => array(
            'required' => false,
        ),
    ),
);
