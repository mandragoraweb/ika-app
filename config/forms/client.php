<?php
use Zend\Validator\Date;
use Ika\Validator\UniqueUsernameValidator;
use Ika\Repository\UserRepository;
use Mandragora\Database\DbalConnection;
use Zend\Validator\Callback;

$repository = new UserRepository();
$repository->setConnection(new DbalConnection(require 'config/db.config.php'));

return array(
    'type' => 'Ika\Form\ClientForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'client-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'client_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'profile_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'user',
                'options' => array(
                    'label' => 'Nombre de usuario',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 20,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'first_name',
                'options' => array(
                    'label' => 'Nombre',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 45,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'paternal_surname',
                'options' => array(
                    'label' => 'Apellido paterno',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 45,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'maternal_surname',
                'options' => array(
                    'label' => 'Apellido materno',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 45,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'birth',
                'attributes' => array(
                    'required' => false,
                    'class' => 'ui-date',
                ),
                'options' => array(
                    'label' => 'Fecha de nacimiento',
                    'format' => 'Y-m-d',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'social_reason',
                'options' => array(
                    'label' => 'Razon social',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 120,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'car',
                'options' => array(
                    'label' => 'Automovil',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 90,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'email',
                'options' => array(
                    'label' => 'Correo eléctronico',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 90,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'phone',
                'options' => array(
                    'label' => 'Telefóno',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 15,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'mobile',
                'options' => array(
                    'label' => 'Celular',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 20,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'rfc',
                'options' => array(
                    'label' => 'RFC/CURP',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 15,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'existing_property',
                'options' => array(
                    'label' => 'Propiedad existente',
                    'uncheked_value' => false,
                    'checked_value' => true,
                ),
                'attributes' => array(
                    'required' => false,
                    'id' => 'existing_property',
                ),
                'type'  => 'Checkbox',
            )
        ),
        array(
            'spec' => array(
                'name' => 'zone_id',
                'attributes' => array(
                    'id' => 'zone',
                    'class' => 'ui-helper-hidden-accessible',
                ),
                'options' => array(
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Selecciona un fraccionamiento',
                    'label_attributes' => array(
                        'class' => 'ui-helper-hidden-accessible',
                    ),
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'property_id',
                'attributes' => array(
                    'id' => 'property',
                    'class' => 'ui-helper-hidden-accessible',
                ),
                'options' => array(
                    'label' => 'Propiedad',
                    'empty_option' => 'Selecciona una propiedad',
                    'label_attributes' => array(
                        'class' => 'ui-helper-hidden-accessible',
                    ),
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'user' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
            'validators' => array(
                array(
                    'name' => 'Zend\Validator\Callback',
                    'options' => array(
                        'callback' => new UniqueUsernameValidator($repository),
                        'messages' => array(
                            Callback::INVALID_VALUE => 'El nombre usuario ya existe, ingrese otro.',
                        ),
                    ),
                ),
            ),
        ),
        'paternal_surname' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'maternal_surname' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'first_name' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'birth' => array(
            'required' => false,
            'allowEmpty' => true,
            'validators' => array(
                array(
                    'name' => 'Zend\Validator\Date',
                    'options' => array(
                        'messages' => array(
                            Date::INVALID_DATE => 'La fecha de nacimiento no parece ser una fecha válida',
                            Date::FALSEFORMAT => 'La fecha de nacimiento no se ajusta al formato de fecha "%format%"',
                        ),
                    ),
                ),
            ),
        ),
        'rfc' => array(
            'required' => false,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'email' => array(
            'required' => false,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'phone' => array(
            'required' => false,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'mobile' => array(
            'required' => false,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'zone_id' => array(
            'required' => false,
        ),
        'property_id' => array(
            'required' => false,
        ),
    ),
);
