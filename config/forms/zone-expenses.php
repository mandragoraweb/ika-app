<?php
return array(
    'type' => 'Ika\Form\ZoneExpensesForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'zone-expenses-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'zone_expenses_id',
                'type'  => 'Hidden',
            )
        ),
         array(
            'spec' => array(
                'name' => 'zone_id',
                'attributes' => array(
                    'id' => 'zone',
                    'required' => true,
                ),
                'options' => array(
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Seleccione un fraccionamiento',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'concept_id',
                'attributes' => array(
                    'id' => 'concept',
                    'required' => true,
                ),
                'options' => array(
                    'label' => 'Concepto',
                    'empty_option' => 'Selecciona un concepto',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'amount_paid',
                'options' => array(
                    'label' => 'Cantidad',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'date',
                'options' => array(
                    'label' => 'Fecha',
                    'format' => 'Y-m-d',
                ),
                'attributes' => array(
                    'required' => true,
                    'class' => 'ui-date',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'iva',
                'options' => array(
                    'label' => 'I.V.A.',
                ),
                'attributes' => array(
                    'required' => false,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'comment',
                'options' => array(
                    'label' => 'Comentario',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 1000,
                ),
                'type'  => 'Textarea',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'zone_id' => array(
            'required' => false,
        ),
        'concept_id' => array(
            'required' => false,
        ),
        'amount_paid' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'date' => array(
            'required' => true,
        ),
        'iva' => array(
            'required' => false,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
    ),
);
