<?php
return array(
    'type' => 'Ika\Form\ZoneForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'zone-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'zone_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'admin_username',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'name',
                'options' => array(
                    'label' => 'Nombre',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 150,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'user',
                'options' => array(
                    'label' => 'Administrador',
                    'empty_option' => 'Selecciona administrador',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'name' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'user' => array(
            'required' => true,
        )
    ),
);
