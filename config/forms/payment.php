<?php
use Zend\Validator\Date;
use \Ika\Repository\PaymentRepository;
use \Mandragora\Database\DbalConnection;
use \Ika\Validator\UniqueZoneFolioValidator;
use Zend\Validator\Callback;

$repository = new PaymentRepository();
$repository->setConnection(new DbalConnection(require 'config/db.config.php'));

return array(
    'type' => 'Ika\Form\PaymentForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'payment-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'payment_id',
                'type'  => 'Hidden',
            )
        ),
         array(
            'spec' => array(
                'name' => 'zone_id',
                'attributes' => array(
                    'id' => 'zone',
                    'required' => true,
                ),
                'options' => array(
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Seleccione un fraccionamiento',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'property_id',
                'attributes' => array(
                    'id' => 'property-apply',
                    'required' => true,
                ),
                'options' => array(
                    'label' => 'Propiedad',
                    'empty_option' => 'Selecciona una propiedad',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'fee_id',
                'attributes' => array(
                    'id' => 'fee',
                    'required' => true,
                ),
                'options' => array(
                    'label' => 'Cuota',
                    'empty_option' => 'Selecciona una cuota',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'empty_payment',
                'options' => array(
                    'label' => 'Pago en ceros',
                    'uncheked_value' => null,
                    'checked_value' => true,
                ),
                'attributes' => array(
                    'required' => false,
                    'id' => 'empty_payment',
                ),
                'type'  => 'Checkbox',
            )
        ),
        array(
            'spec' => array(
                'name' => 'folio_receipt',
                'options' => array(
                    'label' => 'Folio',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 100,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'amount_paid',
                'options' => array(
                    'label' => 'Cantidad pagada',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'date',
                'options' => array(
                    'label' => 'Fecha',
                    'format' => 'Y-m-d',
                ),
                'attributes' => array(
                    'required' => true,
                    'class' => 'ui-date',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'comment',
                'options' => array(
                    'label' => 'Comentario',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 100,
                ),
                'type'  => 'Textarea',
            )
        ),
        array(
            'spec' => array(
                'name' => 'opening_balance',
                'options' => array(
                    'label' => 'Saldo inicial',
                    'label_attributes' => array(
                        'class' => 'ui-helper-hidden-accessible',
                    ),
                ),
                'attributes' => array(
                    'required' => false,
                    'readonly' => true,
                    'id' => 'opening-balance',
                    'class' => 'ui-helper-hidden-accessible',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'is_opening_balance',
                'options' => array(
                    'label' => 'Tomar del Saldo Inicial',
                    'label_attributes' => array(
                        'class' => 'ui-helper-hidden-accessible',
                    ),
                    'uncheked_value' => null,
                    'checked_value' => true,
                ),
                'attributes' => array(
                    'required' => false,
                    'id' => 'is_opening_balance',
                    'class' => 'ui-helper-hidden-accessible',
                ),
                'type'  => 'Checkbox',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'zone_id' => array(
            'required' => false,
        ),
        'property_id' => array(
            'required' => false,
        ),
        'fee_id' => array(
            'required' => false,
        ),
        'folio_receipt' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
            'validators' => array(
                array(
                    'name' => 'Zend\Validator\Callback',
                    'options' => array(
                        'callback' => new UniqueZoneFolioValidator($repository),
                        'messages' => array(
                            Callback::INVALID_VALUE => 'El folio se encuentra en uso',
                        ),
                    ),
                ),
            ),
        ),
        'amount_paid' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'date' => array(
            'required' => true,
            'allowEmpty' => false,
            'validators' => array(
                array(
                    'name' => 'Zend\Validator\Date',
                    'options' => array(
                        'messages' => array(
                            Date::INVALID_DATE => 'La fecha del pago no parece ser una fecha válida',
                            Date::FALSEFORMAT => 'La fecha del pagol no se ajusta al formato de fecha "%format%"',
                        ),
                    ),
                ),
            ),
        ),
        'is_opening_balance' => array(
            'required' => false,
        ),
        'opening_balance' => array(
            'required' => false,
        ),
    ),
);
