<?php
return array(
    'type' => 'Ika\Form\ZoneAccountForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'zone-account-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'zone_id',
                'options' => array(
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Selecciona un fraccionamiento',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'start_date',
                'options' => array(
                    'label' => 'Fecha inicial',
                    'format' => 'Y-m-d',
                ),
                'attributes' => array(
                    'required' => true,
                    'class' => 'ui-date',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'stop_date',
                'options' => array(
                    'label' => 'Fecha final',
                    'format' => 'Y-m-d',
                ),
                'attributes' => array(
                    'required' => true,
                    'class' => 'ui-date',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Buscar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Buscar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'zone_id' => array(
            'required' => false,
        ),
        'start_date' => array(
            'required' => true,
        ),
        'stop_date' => array(
            'required' => true,
        ),
    ),
);
