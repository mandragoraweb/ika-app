<?php
return array(
    'type' => 'Ika\Form\ConceptForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'concept-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'concept_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'code',
                'options' => array(
                    'label' => 'Clave',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 45,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'concept',
                'options' => array(
                    'label' => 'Concepto',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 100,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'type',
                'options' => array(
                    'label' => 'Tipo de concepto',
                    'value_options' => array(
                         'I' => 'Ingreso',
                         'O' => 'Gasto',
                     ),
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Radio',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'code' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'concept' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'type' => array(
            'required' => true,
        )
    ),
);
