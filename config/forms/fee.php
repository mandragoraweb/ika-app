<?php
return array(
    'type' => 'Ika\Form\FeeForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'fee-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'fee_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'zone_id',
                'type'  => 'Hidden',
            )
        ),
        array(
            'spec' => array(
                'name' => 'type_id',
                'options' => array(
                    'label' => 'Tipo de propiedad',
                    'empty_option' => 'Selecciona un tipo de propiedad',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'concept_id',
                'options' => array(
                    'label' => 'Concepto',
                    'empty_option' => 'Selecciona un concepto',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'amount',
                'options' => array(
                    'label' => 'Cuota autorizada',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'amount' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
    ),
);
