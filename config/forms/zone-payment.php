<?php
return array(
    'type' => 'Ika\Form\ZonePaymentForm',
    'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
    'attributes' => array(
        'method' => 'post',
        'id' => 'zone-payment-form',
        'class' => 'form-horizontal',
    ),
    'elements' => array(
        array(
            'spec' => array(
                'name' => 'zone_payment_id',
                'type'  => 'Hidden',
            )
        ),
         array(
            'spec' => array(
                'name' => 'zone_id',
                'attributes' => array(
                    'id' => 'zone',
                    'required' => true,
                ),
                'options' => array(
                    'label' => 'Fraccionamiento',
                    'empty_option' => 'Seleccione un fraccionamiento',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'fee_id',
                'attributes' => array(
                    'id' => 'fee',
                    'required' => true,
                ),
                'options' => array(
                    'label' => 'Cuota',
                    'empty_option' => 'Selecciona una cuota',
                ),
                'type'  => 'Select',
            )
        ),
        array(
            'spec' => array(
                'name' => 'folio_receipt',
                'options' => array(
                    'label' => 'Folio',
                ),
                'attributes' => array(
                    'size' => 45,
                    'maxlength' => 100,
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'amount_paid',
                'options' => array(
                    'label' => 'Cantidad pagada',
                ),
                'attributes' => array(
                    'required' => true,
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'date',
                'options' => array(
                    'label' => 'Fecha',
                    'format' => 'Y-m-d',
                ),
                'attributes' => array(
                    'required' => true,
                    'class' => 'ui-date',
                ),
                'type'  => 'Text',
            )
        ),
        array(
            'spec' => array(
                'name' => 'csrf',
                'type' => 'Csrf',
            ),
        ),
        array(
            'spec' => array(
                'name' => 'send',
                'options' => array(
                    'label' => 'Guardar',
                ),
                'attributes' => array(
                    'type' => 'submit',
                    'value' => 'Guardar',
                ),
                'type'  => 'Button',
            ),
        ),
    ),
    'input_filter' => array(
        'zone_id' => array(
            'required' => false,
        ),
        'fee_id' => array(
            'required' => false,
        ),
        'folio_receipt' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'amount_paid' => array(
            'required' => true,
            'filters'  => array(
                array('name' => 'Zend\Filter\StringTrim'),
                array('name' => 'Zend\Filter\StripTags'),
            ),
        ),
        'date' => array(
            'required' => true,
        ),
    ),
);
