<?php
return [
    'report_customize-client' => [
        'path' => '/reportes/personalizar/condominos',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'customizeClientAction',
        ],
    ],
    'report_customize-client-general' => [
        'path' => '/reportes/personalizar/condominos/mensual',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'customizeClientGeneralAction',
        ],
    ],
    'report_customize-zone' => [
        'path' => '/reportes/personalizar/fraccionamientos',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'customizeZoneAction',
        ],
    ],
    'report_customize-zone-general' => [
        'path' => '/reportes/personalizar/fraccionamientos/mensual',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'customizeZoneGeneralAction',
        ],
    ],
    'report_client-balance' => [
        'path' => '/reportes/condominos/balance',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'showClientBalanceAction',
        ],
    ],
    'report_zone-balance' => [
        'path' => '/reportes/fraccionamientos/balance',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'showZoneBalanceAction',
        ],
    ],
    'report_client-monthly-balance' => [
        'path' => '/reportes/condominos/balance/mensual',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'showClientBalanceMonthAction',
        ],
    ],
    'report_zone-monthly-balance' => [
        'path' => '/reportes/fraccionamientos/balance/mensual',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'showZoneBalanceMonthAction',
        ],
    ],
    'generate_report' => [
        'path' => '/reportes/descargar/{report}/desde/{startDate}/hasta/{stopDate}',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'generateReportAction',
        ],
    ],
    'generate_report_month' => [
        'path' => '/reportes/descargar/{report}/desde/{startDate}/hasta/{stopDate}/mensual',
        'defaults' => [
            'config' => 'config/services/report.config.php',
            'controller' => 'report_controller',
            'action' => 'reportMonthAction',
        ],
    ],
    'reports.view_annual_income_form' => [
        'path' => '/reportes/ingresos/personalizar',
        'defaults' => [
            'config' => 'config/services/annual-statement.config.php',
            'controller' => 'controller.annual_income_form',
            'action' => 'viewIncomeStatementForm',
        ],
    ],
    'reports.annual_income_statement' => [
        'path' => '/reportes/ingresos/anual',
        'defaults' => [
            'config' => 'config/services/annual-statement.config.php',
            'controller' => 'controller.annual_income_statement',
            'action' => 'viewAnnualIncomeStatement',
        ],
    ],
    'reports.view_annual_expenses_form' => [
        'path' => '/reportes/egresos/personalizar',
        'defaults' => [
            'config' => 'config/services/annual-statement.config.php',
            'controller' => 'controller.annual_expenses_form',
            'action' => 'viewExpensesStatementForm',
        ],
    ],
    'reports.annual_expenses_statement' => [
        'path' => '/reportes/egresos/anual',
        'defaults' => [
            'config' => 'config/services/annual-statement.config.php',
            'controller' => 'controller.annual_expenses_statement',
            'action' => 'viewAnnualExpensesStatement',
        ],
    ],
];
