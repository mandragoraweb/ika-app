<?php
return array(
    'profile_list' => array(
        'path' => '/administradores/listar',
        'defaults' => array(
            'config' => 'config/services/profile.config.php',
            'controller' => 'profile_list',
            'action' => 'listAction',
        ),
    ),
    'profile_create' => array(
        'path' => '/administradores/crear',
        'defaults' => array(
            'config' => 'config/services/profile.config.php',
            'controller' => 'profile_controller',
            'action' => 'createAction',
        ),
    ),
    'profile_save' => array(
        'path' => '/administradores/guardar',
        'defaults' => array(
            'config' => 'config/services/profile.config.php',
            'controller' => 'profile_save',
            'action' => 'saveAction',
        ),
    ),
    'profile_edit' => array(
        'path' => '/administradores/editar/{profileId}',
        'defaults' => array(
            'config' => 'config/services/profile.config.php',
            'controller' => 'profile_controller',
            'action' => 'editAction',
        ),
    ),
    'profile_update' => array(
        'path' => '/administradores/actualizar/{profileId}',
        'defaults' => array(
            'config' => 'config/services/profile.config.php',
            'controller' => 'profile_controller',
            'action' => 'updateAction',
        ),
    ),
    'profile_remove' => array(
        'path' => '/administradores/remover/{profileId}',
        'defaults' => array(
            'config' => 'config/services/profile.config.php',
            'controller' => 'profile_controller',
            'action' => 'removeAction',
        ),
    ),
    'profile_delete' => array(
        'path' => '/administradores/eliminar/{profileId}',
        'defaults' => array(
            'config' => 'config/services/profile.config.php',
            'controller' => 'profile_controller',
            'action' => 'deleteAction',
        ),
    ),
    'profile_change-password' => array(
        'path' => '/admisnitradores/cambiar-contraseña/{profileId}',
        'defaults' => array(
            'config' => 'config/services/profile.config.php',
            'controller' => 'profile_controller',
            'action' => 'formPasswordAction',
        ),
    ),
    'profile_new_password' => array(
        'path' => '/admisnitradores/nueva-contraseña/{profileId}',
        'defaults' => array(
            'config' => 'config/services/profile.config.php',
            'controller' => 'profile_controller',
            'action' => 'changePasswordAction',
        ),
    ),
);
