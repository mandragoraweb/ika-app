<?php
return array(
    'fee_list' => array(
        'path' => '/fraccionamientos/{zoneId}/cuotas/listar',
        'defaults' => array(
            'config' => 'config/services/fee.config.php',
            'controller' => 'fee_list',
            'action' => 'listAction',
        ),
    ),
    'fee_create' => array(
        'path' => '/fraccionamientos/{zoneId}/cuotas/crear',
        'defaults' => array(
            'config' => 'config/services/fee.config.php',
            'controller' => 'fee_controller',
            'action' => 'createAction',
        ),
    ),
    'fee_save' => array(
        'path' => '/fraccionamientos/{zoneId}/cuotas/guardar',
        'defaults' => array(
            'config' => 'config/services/fee.config.php',
            'controller' => 'fee_controller',
            'action' => 'saveAction',
        ),
    ),
    'fee_edit' => array(
        'path' => '/fraccionamientos/{zoneId}/cuotas/editar/{feeId}',
        'defaults' => array(
            'config' => 'config/services/fee.config.php',
            'controller' => 'fee_controller',
            'action' => 'editAction',
        ),
    ),
    'fee_update' => array(
        'path' => '/fraccionamientos/{zoneId}/cuotas/actualizar/{feeId}',
        'defaults' => array(
            'config' => 'config/services/fee.config.php',
            'controller' => 'fee_controller',
            'action' => 'updateAction',
        ),
    ),
    'fee_remove' => array(
        'path' => '/fraccionamientos/{zoneId}/cuotas/remover/{feeId}',
        'defaults' => array(
            'config' => 'config/services/fee.config.php',
            'controller' => 'fee_controller',
            'action' => 'removeAction',
        ),
    ),
    'fee_delete' => array(
        'path' => '/fraccionamientos/{zoneId}/cuotas/eliminar/{feeId}',
        'defaults' => array(
            'config' => 'config/services/fee.config.php',
            'controller' => 'fee_controller',
            'action' => 'deleteAction',
        ),
    ),
);
