<?php
return array(
    'concept_list' => array(
        'path' => '/conceptos/listar/',
        'defaults' => array(
            'config' => 'config/services/concept.config.php',
            'controller' => 'concept_list',
            'action' => 'listAction',
        ),
    ),
    'concept_create' => array(
        'path' => '/conceptos/crear/',
        'defaults' => array(
            'config' => 'config/services/concept.config.php',
            'controller' => 'concept_controller',
            'action' => 'createAction',
        ),
    ),
    'concept_save' => array(
        'path' => '/conceptos/guardar/',
        'defaults' => array(
            'config' => 'config/services/concept.config.php',
            'controller' => 'concept_controller',
            'action' => 'saveAction',
        ),
    ),
    'concept_edit' => array(
        'path' => '/conceptos/editar/{conceptId}',
        'defaults' => array(
            'config' => 'config/services/concept.config.php',
            'controller' => 'concept_controller',
            'action' => 'editAction',
        ),
    ),
    'concept_update' => array(
        'path' => '/conceptos/actualizar/{conceptId}',
        'defaults' => array(
            'config' => 'config/services/concept.config.php',
            'controller' => 'concept_controller',
            'action' => 'updateAction',
        ),
    ),
    'concept_remove' => array(
        'path' => '/conceptos/remover/{conceptId}',
        'defaults' => array(
            'config' => 'config/services/concept.config.php',
            'controller' => 'concept_controller',
            'action' => 'removeAction',
        ),
    ),
    'concept_delete' => array(
        'path' => '/conceptos/eliminar/{conceptId}',
        'defaults' => array(
            'config' => 'config/services/concept.config.php',
            'controller' => 'concept_controller',
            'action' => 'deleteAction',
        ),
    ),
);
