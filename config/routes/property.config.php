<?php
return array(
    'property_list' => array(
        'path' => '/fraccionamientos/{zoneId}/propiedades/listar',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'property_list',
            'action' => 'listAction',
        ),
    ),
    'property_create' => array(
        'path' => '/fraccionamientos/{zoneId}/propiedades/crear',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'property_controller',
            'action' => 'createAction',
        ),
    ),
    'property_save' => array(
        'path' => '/fraccionamientos/{zoneId}/propiedades/guardar',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'property_controller',
            'action' => 'saveAction',
        ),
    ),
    'client_property_save' => array(
        'path' => '/condominos/propiedades/guardar',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'client_property_save',
            'action' => 'saveClientPropertyAction',
        ),
    ),
    'property_edit' => array(
        'path' => '/fraccionamientos/{zoneId}/propiedades/editar/{propertyId}',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'property_controller',
            'action' => 'editAction',
        ),
    ),
    'property_update' => array(
        'path' => '/fraccionamientos/{zoneId}/propiedades/actualizar/{propertyId}',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'property_controller',
            'action' => 'updateAction',
        ),
    ),
    'property_remove' => array(
        'path' => '/fraccionamientos/{zoneId}/propiedades/remover/{propertyId}',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'property_controller',
            'action' => 'removeAction',
        ),
    ),
    'property_delete' => array(
        'path' => '/fraccionamientos/{zoneId}/propiedades/eliminar/{propertyId}',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'property_controller',
            'action' => 'deleteAction',
        ),
    ),
    'property_list-client' => array(
        'path' => '/propiedades/lista',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'property_list',
            'action' => 'listClientAction',
        ),
    ),
    'property_list-zone' => array(
        'path' => '/propiedades/zona',
        'defaults' => array(
            'config' => 'config/services/property.config.php',
            'controller' => 'property_list',
            'action' => 'listZoneAction',
        ),
    ),
);
