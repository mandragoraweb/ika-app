<?php
return array(
    'payment_list' => array(
        'path' => '/pagos/listar',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_list',
            'action' => 'listAction',
        ),
    ),
    'payment_create' => array(
        'path' => '/pagos/crear',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_controller',
            'action' => 'createAction',
        ),
    ),
    'payment_save' => array(
        'path' => '/pagos/guardar',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_controller',
            'action' => 'saveAction',
        ),
    ),
    'payment_edit' => array(
        'path' => '/pagos/editar/{paymentId}',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_controller',
            'action' => 'editAction',
        ),
    ),
    'payment_update' => array(
        'path' => '/pagos/actualizar/{paymentId}',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_controller',
            'action' => 'updateAction',
        ),
    ),
    'payment_remove' => array(
        'path' => '/pagos/remover/{paymentId}',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_controller',
            'action' => 'removeAction',
        ),
    ),
    'payment_delete' => array(
        'path' => '/pagos/eliminar/{paymentId}',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_controller',
            'action' => 'deleteAction',
        ),
    ),
    'payment_personalize' => array(
        'path' => '/pagos/personalizar',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_controller',
            'action' => 'customizeAction',
        ),
    ),
    'payment_properties' => array(
        'path' => '/pagos/propiedades/{zoneId}',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_list',
            'action' => 'propertyByZoneAction',
        ),
    ),
    'payment_by-property' => array(
        'path' => '/pagos/propiedad/{propertyId}',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_list',
            'action' => 'paymentsByPropertyAction',
        ),
    ),
    'payment_opening-balance' => array(
        'path' => '/pagos/propiedad/{propertyId}/saldo-inicial',
        'defaults' => array(
            'config' => 'config/services/payment.config.php',
            'controller' => 'payment_list',
            'action' => 'openingBalanceByPropertyAction',
        ),
    ),
);
