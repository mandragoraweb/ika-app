<?php
return [
    'zone-account_customize' => [
        'path' => '/estado-cuenta/fraccionamiento/personalizar',
        'defaults' => [
            'config' => 'config/services/zone-account.config.php',
            'controller' => 'zone_account_controller',
            'action' => 'showAccountBalanceForm',
        ],
    ],
    'zone-account_show-balance' => [
        'path' => '/estado-cuenta/fraccionamiento/mostrar',
        'defaults' => [
            'config' => 'config/services/zone-account.config.php',
            'controller' => 'zone_account_balance_controller',
            'action' => 'showBalanceAction',
        ],
    ],
];
