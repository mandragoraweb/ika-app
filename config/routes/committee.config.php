<?php
return array(
    'committee_list' => array(
        'path' => '/fraccionamientos/{zoneId}/comite/listar',
        'defaults' => array(
            'config' => 'config/services/committee.config.php',
            'controller' => 'committee_list',
            'action' => 'listAction',
        ),
    ),

    'committee_create' => array(
        'path' => '/fraccionamientos/{zoneId}/comite/crear',
        'defaults' => array(
            'config' => 'config/services/committee.config.php',
            'controller' => 'committee_controller',
            'action' => 'createAction',
        ),
    ),
    'committee_save' => array(
        'path' => '/fraccionamientos/{zoneId}/comite/guardar',
        'defaults' => array(
            'config' => 'config/services/committee.config.php',
            'controller' => 'committee_controller',
            'action' => 'saveAction',
        ),
    ),
    'committee_remove' => array(
        'path' => '/fraccionamientos/{zoneId}/comite/remover/{user}',
        'defaults' => array(
            'config' => 'config/services/committee.config.php',
            'controller' => 'committee_controller',
            'action' => 'removeAction',
        ),
    ),
    'committee_delete' => array(
        'path' => '/fraccionamientos/{zoneId}/comite/eliminar/{user}',
        'defaults' => array(
            'config' => 'config/services/committee.config.php',
            'controller' => 'committee_controller',
            'action' => 'deleteAction',
        ),
    ),
);
