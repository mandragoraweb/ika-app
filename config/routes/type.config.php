<?php
return array(
    'type_list' => array(
        'path' => '/tipos/listar',
        'defaults' => array(
            'config' => 'config/services/type.config.php',
            'controller' => 'type_list',
            'action' => 'listAction',
        ),
    ),
    'type_create' => array(
        'path' => '/tipos/crear',
        'defaults' => array(
            'config' => 'config/services/type.config.php',
            'controller' => 'type_controller',
            'action' => 'createAction',
        ),
    ),
    'type_save' => array(
        'path' => '/tipos/guardar',
        'defaults' => array(
            'config' => 'config/services/type.config.php',
            'controller' => 'type_controller',
            'action' => 'saveAction',
        ),
    ),
    'type_edit' => array(
        'path' => '/tipos/editar/{typeId}',
        'defaults' => array(
            'config' => 'config/services/type.config.php',
            'controller' => 'type_controller',
            'action' => 'editAction',
        ),
    ),
    'type_update' => array(
        'path' => '/tipos/actualizar/{typeId}',
        'defaults' => array(
            'config' => 'config/services/type.config.php',
            'controller' => 'type_controller',
            'action' => 'updateAction',
        ),
    ),
    'type_remove' => array(
        'path' => '/tipos/remover/{typeId}',
        'defaults' => array(
            'config' => 'config/services/type.config.php',
            'controller' => 'type_controller',
            'action' => 'removeAction',
        ),
    ),
    'type_delete' => array(
        'path' => '/tipos/eliminar/{typeId}',
        'defaults' => array(
            'config' => 'config/services/type.config.php',
            'controller' => 'type_controller',
            'action' => 'deleteAction',
        ),
    ),
);
