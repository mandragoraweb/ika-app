<?php
return array(
    'zone_list' => array(
        'path' => '/fraccionamientos/listar',
        'defaults' => array(
            'config' => 'config/services/zone.config.php',
            'controller' => 'zone_list',
            'action' => 'listAction',
        ),
    ),
    'zone_create' => array(
        'path' => '/fraccionamientos/crear',
        'defaults' => array(
            'config' => 'config/services/zone.config.php',
            'controller' => 'zone_controller',
            'action' => 'createAction',
        ),
    ),
    'zone_save' => array(
        'path' => '/fraccionamientos/guardar',
        'defaults' => array(
            'config' => 'config/services/zone.config.php',
            'controller' => 'zone_controller',
            'action' => 'saveAction',
        ),
    ),
    'zone_edit' => array(
        'path' => '/fraccionamientos/editar/{zoneId}',
        'defaults' => array(
            'config' => 'config/services/zone.config.php',
            'controller' => 'zone_controller',
            'action' => 'editAction',
        ),
    ),
    'zone_update' => array(
        'path' => '/fraccionamientos/actualizar/{zoneId}',
        'defaults' => array(
            'config' => 'config/services/zone.config.php',
            'controller' => 'zone_controller',
            'action' => 'updateAction',
        ),
    ),
    'zone_remove' => array(
        'path' => '/fraccionamientos/remover/{zoneId}',
        'defaults' => array(
            'config' => 'config/services/zone.config.php',
            'controller' => 'zone_controller',
            'action' => 'removeAction',
        ),
    ),
    'zone_delete' => array(
        'path' => '/fraccionamientos/eliminar/{zoneId}',
        'defaults' => array(
            'config' => 'config/services/zone.config.php',
            'controller' => 'zone_controller',
            'action' => 'deleteAction',
        ),
    ),
);
