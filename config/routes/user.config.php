<?php
return array(
    'user_list' => array(
        'path' => '/usuarios/listar',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_list',
            'action' => 'listAction',
        ),
    ),
    'user_list_profile' => array(
        'path' => '/perfil/{profileId}/usuarios/listar',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_list',
            'action' => 'listProfileAction',
        ),
    ),
    'user_create' => array(
        'path' => '/perfil/{profileId}/usuarios/crear',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_controller',
            'action' => 'createAction',
        ),
    ),
    'user_save' => array(
        'path' => '/perfil/{profileId}/usuarios/guardar',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_controller',
            'action' => 'saveAction',
        ),
    ),
    'user_edit' => array(
        'path' => '/perfil/{profileId}/usuarios/editar/{username}',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_controller',
            'action' => 'editAction',
        ),
    ),
    'user_update' => array(
        'path' => '/perfil/{profileId}/usuarios/actualizar/{username}',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_controller',
            'action' => 'updateAction',
        ),
    ),
    'user_remove' => array(
        'path' => '/perfil/{profileId}/usuarios/remover/{username}',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_controller',
            'action' => 'removeAction',
        ),
    ),
    'user_delete' => array(
        'path' => '/perfil/{profileId}/usuarios/eliminar/{username}',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_controller',
            'action' => 'deleteAction',
        ),
    ),
    'user_change_password' => array(
        'path' => '/perfil/{profileId}/usuarios/cambiar-contraseña/{username}',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_controller',
            'action' => 'formPasswordAction',
        ),
    ),
    'user_new_password' => array(
        'path' => '/perfil/{profileId}/usuarios/nueva-contraseña/{username}',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_controller',
            'action' => 'changePasswordAction',
        ),
    ),
    'user_logout' => array(
        'path' => '/cuentas/salir',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_logout',
            'action' => 'logoutAction',
        ),
    ),
);
