<?php
return array(
    'surcharge_list' => array(
        'path' => 'pagos/{paymentId}/recargo/listar',
        'defaults' => array(
            'config' => 'config/services/surcharge.config.php',
            'controller' => 'surcharge_list',
            'action' => 'listAction',
        ),
    ),
    'surcharge_create' => array(
        'path' => 'pagos/{paymentId}/recargo/crear',
        'defaults' => array(
            'config' => 'config/services/surcharge.config.php',
            'controller' => 'surcharge_controller',
            'action' => 'createAction',
        ),
    ),
    'surcharge_save' => array(
        'path' => 'pagos/{paymentId}/recargo/guardar',
        'defaults' => array(
            'config' => 'config/services/surcharge.config.php',
            'controller' => 'surcharge_controller',
            'action' => 'saveAction',
        ),
    ),
    'surcharge_edit' => array(
        'path' => 'pagos/{paymentId}/recargo/editar/{surchargeId}',
        'defaults' => array(
            'config' => 'config/services/surcharge.config.php',
            'controller' => 'surcharge_controller',
            'action' => 'editAction',
        ),
    ),
    'surcharge_update' => array(
        'path' => 'pagos/{paymentId}/recargo/actualizar/{surchargeId}',
        'defaults' => array(
            'config' => 'config/services/surcharge.config.php',
            'controller' => 'surcharge_controller',
            'action' => 'updateAction',
        ),
    ),
    'surcharge_remove' => array(
        'path' => 'pagos/{paymentId}/recargo/remover/{surchargeId}',
        'defaults' => array(
            'config' => 'config/services/surcharge.config.php',
            'controller' => 'surcharge_controller',
            'action' => 'removeAction',
        ),
    ),
    'surcharge_delete' => array(
        'path' => 'pagos/{paymentId}/recargo/eliminar/{surchargeId}',
        'defaults' => array(
            'config' => 'config/services/surcharge.config.php',
            'controller' => 'surcharge_controller',
            'action' => 'deleteAction',
        ),
    ),
);
