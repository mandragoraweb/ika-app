<?php
return [
    'zone-payment_create' => [
        'path' => '/pagos/fraccionamiento/crear',
        'defaults' => [
            'config' => 'config/services/zone-payment.config.php',
            'controller' => 'controller.zone_payment_form',
            'action' => 'showPaymentZoneForm',
        ],
    ],
    'zone-payment_save' => [
        'path' => '/pagos/fraccionamiento/guardar',
        'defaults' => [
            'config' => 'config/services/zone-payment.config.php',
            'controller' => 'zone_payment_controller',
            'action' => 'saveAction',
        ],
    ],
    'zone-payment_edit' => [
        'path' => '/pagos/fraccionamiento/editar/{zonePaymentId}',
        'defaults' => [
            'config' => 'config/services/zone-payment.config.php',
            'controller' => 'zone_payment_controller',
            'action' => 'editAction',
        ],
    ],
    'zone-payment_update' => [
        'path' => '/pagos/fraccionamiento/actualizar/{zonePaymentId}',
        'defaults' => [
            'config' => 'config/services/zone-payment.config.php',
            'controller' => 'zone_payment_controller',
            'action' => 'updateAction',
        ],
    ],
    'zone-payment_remove' => [
        'path' => '/pagos/fraccionamiento/remover/{zonePaymentId}',
        'defaults' => [
            'config' => 'config/services/zone-payment.config.php',
            'controller' => 'zone_payment_controller',
            'action' => 'removeAction',
        ],
    ],
    'zone-payment_delete' => [
        'path' => '/pagos/fraccionamiento/eliminar/{zonePaymentId}',
        'defaults' => [
            'config' => 'config/services/zone-payment.config.php',
            'controller' => 'zone_payment_controller',
            'action' => 'deleteAction',
        ],
    ],
    'zone-payment_list' => [
        'path' => '/pagos/fraccionamiento/listar',
        'defaults' => [
            'config' => 'config/services/zone-payment.config.php',
            'controller' => 'controller.zone_payment_customize',
            'action' => 'showPaymentZoneForm',
        ],
    ],
    'zone-payment_properties-by-zone' => [
        'path' => '/pagos/fraccionamiento/propiedades/{zoneId}',
        'defaults' => [
            'config' => 'config/services/zone-payment.config.php',
            'controller' => 'zone_payment_list',
            'action' => 'propertyByZoneAction',
        ],
    ],
    'zone-payment_payments-by-zone' => [
        'path' => '/pagos/fraccionamiento/pagos/{zoneId}',
        'defaults' => [
            'config' => 'config/services/zone-payment.config.php',
            'controller' => 'zone_payment_list',
            'action' => 'paymentsByZoneAction',
        ],
    ],
];
