<?php
return [
    'client_list' => [
        'path' => '/condominos/listar',
        'defaults' => [
            'config' => 'config/services/client.config.php',
            'controller' => 'client_list',
            'action' => 'showHomeowners',
        ],
    ],
    'client_create' => [
        'path' => '/condominos/crear',
        'defaults' => [
            'config' => 'config/services/client.config.php',
            'controller' => 'client_controller',
            'action' => 'createAction',
        ],
    ],
    'client_save' => [
        'path' => '/condominos/guardar',
        'defaults' => [
            'config' => 'config/services/client.config.php',
            'controller' => 'client_property',
            'action' => 'saveAction',
        ],
    ],
    'client_edit' => [
        'path' => '/condominos/editar/{clientId}',
        'defaults' => [
            'config' => 'config/services/client.config.php',
            'controller' => 'client_controller',
            'action' => 'editAction',
        ],
    ],
    'client_update' => [
        'path' => '/condominos/actualizar/{clientId}',
        'defaults' => [
            'config' => 'config/services/client.config.php',
            'controller' => 'client_controller',
            'action' => 'updateAction',
        ],
    ],
    'client_remove' => [
        'path' => '/condominos/remover/{clientId}',
        'defaults' => [
            'config' => 'config/services/client.config.php',
            'controller' => 'client_controller',
            'action' => 'removeAction',
        ],
    ],
    'client_delete' => [
        'path' => '/condominos/eliminar/{clientId}',
        'defaults' => [
            'config' => 'config/services/client.config.php',
            'controller' => 'client_controller',
            'action' => 'deleteAction',
        ],
    ],
    'client_change-password' => [
        'path' => '/condominos/cambiar-contrasena/{clientId}',
        'defaults' => [
            'config' => 'config/services/client.config.php',
            'controller' => 'client_controller',
            'action' => 'formPasswordAction',
        ],
    ],
    'client_new_password' => [
        'path' => '/condominos/nueva-contrasena/{clientId}',
        'defaults' => [
            'config' => 'config/services/client.config.php',
            'controller' => 'client_controller',
            'action' => 'changePasswordAction',
        ],
    ],
];
