<?php
return [
    'account_balance_form' => [
        'path' => '/estado-cuenta/personalizar',
        'defaults' => [
            'config' => 'config/services/account.config.php',
            'controller' => 'account_form_controller',
            'action' => 'showBalanceForm',
        ],
    ],
    'account_show-balance' => [
        'path' => '/estado-cuenta/mostrar',
        'defaults' => [
            'config' => 'config/services/account.config.php',
            'controller' => 'account_controller',
            'action' => 'showAccountBalance',
        ],
    ],
    'account_clients-in-zone' => [
        'path' => '/condominos/fraccionamiento/{zoneId}',
        'defaults' => [
            'config' => 'config/services/account.config.php',
            'controller' => 'client_controller',
            'action' => 'showHomeownersInSubdivision',
        ],
    ],
    'account_properties-to-client' => [
        'path' => '/propiedades/condomino/{clientId}',
        'defaults' => [
            'config' => 'config/services/account.config.php',
            'controller' => 'property_controller',
            'action' => 'showCustomerProperties',
        ],
    ],
];
