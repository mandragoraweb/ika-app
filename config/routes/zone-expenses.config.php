<?php
return array(
    'zone-expenses_list' => array(
        'path' => '/gastos/fraccionamiento/listar',
        'defaults' => array(
            'config' => 'config/services/zone-expenses.config.php',
            'controller' => 'zone_expenses_list',
            'action' => 'listAction',
        ),
    ),
    'zone-expenses_create' => array(
        'path' => '/gastos/fraccionamiento/crear',
        'defaults' => array(
            'config' => 'config/services/zone-expenses.config.php',
            'controller' => 'zone_expenses_controller',
            'action' => 'createAction',
        ),
    ),
    'zone-expenses_save' => array(
        'path' => '/gastos/fraccionamiento/guardar',
        'defaults' => array(
            'config' => 'config/services/zone-expenses.config.php',
            'controller' => 'zone_expenses_controller',
            'action' => 'saveAction',
        ),
    ),
    'zone-expenses_edit' => array(
        'path' => '/gastos/fraccionamiento/editar/{zoneExpensesId}',
        'defaults' => array(
            'config' => 'config/services/zone-expenses.config.php',
            'controller' => 'zone_expenses_controller',
            'action' => 'editAction',
        ),
    ),
    'zone-expenses_update' => array(
        'path' => '/gastos/fraccionamiento/actualizar/{zoneExpensesId}',
        'defaults' => array(
            'config' => 'config/services/zone-expenses.config.php',
            'controller' => 'zone_expenses_controller',
            'action' => 'updateAction',
        ),
    ),
    'zone-expenses_remove' => array(
        'path' => '/gastos/fraccionamiento/remover/{zoneExpensesId}',
        'defaults' => array(
            'config' => 'config/services/zone-expenses.config.php',
            'controller' => 'zone_expenses_controller',
            'action' => 'removeAction',
        ),
    ),
    'zone-expenses_delete' => array(
        'path' => '/gastos/fraccionamiento/eliminar/{zoneExpensesId}',
        'defaults' => array(
            'config' => 'config/services/zone-expenses.config.php',
            'controller' => 'zone_expenses_controller',
            'action' => 'deleteAction',
        ),
    ),
    'zone-expenses_personalize' => array(
        'path' => '/gastos/fraccionamiento/personalizar',
        'defaults' => array(
            'config' => 'config/services/zone-expenses.config.php',
            'controller' => 'zone_expenses_controller',
            'action' => 'customizeAction',
        ),
    ),
    'zone-expenses_expenses-by-zone' => array(
        'path' => '/gastos/fraccionamiento/gastos/{zoneId}',
        'defaults' => array(
            'config' => 'config/services/zone-expenses.config.php',
            'controller' => 'zone_expenses_list',
            'action' => 'expensesByZoneAction',
        ),
    ),
);
