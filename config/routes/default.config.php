<?php
return array(
    'index_index' => array(
        'path' => '/',
        'defaults' => array(
            'config' => 'config/services/index.config.php',
            'controller' => 'index_controller',
            'action' => 'indexAction',
        ),
    ),
    'index_about' => array(
        'path' => '/nosotros',
        'defaults' => array(
            'config' => 'config/services/index.config.php',
            'controller' => 'index_controller',
            'action' => 'aboutAction',
        ),
    ),
    'index_services' => array(
        'path' => '/servicios',
        'defaults' => array(
            'config' => 'config/services/index.config.php',
            'controller' => 'index_controller',
            'action' => 'serviceAction',
        ),
    ),
    'index_sales' => array(
        'path' => '/ventas',
        'defaults' => array(
            'config' => 'config/services/index.config.php',
            'controller' => 'index_controller',
            'action' => 'saleAction',
        ),
    ),
    'sell_item' => array(
        'path' => '/ventas/{slug}',
        'defaults' => array(
            'config' => 'config/services/index.config.php',
            'controller' => 'index_controller',
            'action' => 'sellItemAction',
        ),
    ),
    'index_contact' => array(
        'path' => '/contacto',
        'defaults' => array(
            'config' => 'config/services/index.config.php',
            'controller' => 'index_controller',
            'action' => 'contactAction',
        ),
    ),
    'user_authenticate' => array(
        'path' => '/cuentas/verificar',
        'defaults' => array(
            'config' => 'config/services/user.config.php',
            'controller' => 'user_authenticate',
            'action' => 'authenticateAction',
        ),
    ),
    'index_send-email' => array(
        'path' => '/correo/enviar',
        'defaults' => array(
            'config' => 'config/services/index.config.php',
            'controller' => 'index_controller',
            'action' => 'sendEmailAction',
        ),
    ),
);
